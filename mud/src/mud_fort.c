/*
 *  mud_fort.c -- FORTRAN wrappers to allow basic read access
 *
 *  Revision history:
 *   v1.00  07-Feb-1994  [T. Whidden] Initial version
 */

#include <stdio.h>
#include "mud.h"

#ifdef NO_STDARG
#include <varargs.h>
#else
#include <stdarg.h>
#endif /* NO_STDARG */


/*
 *  Forward declarations
 */
#ifdef VMS
#include descrip
typedef struct dsc$descriptor_s s_dsc;

void fMUD_ctofString _ANSI_ARGS_(( s_dsc *fString , char **pcString ));
void fMUD_ftocString _ANSI_ARGS_(( char **pcString , s_dsc *fString ));
FILE* fMUD_openInput _ANSI_ARGS_(( s_dsc *pdsc_fname ));
FILE* fMUD_openOutput _ANSI_ARGS_(( s_dsc *pdsc_fname ));
#else
#endif /* VMS */

void fMUD_rewind _ANSI_ARGS_(( FILE **pFio ));
void fMUD_freeOne _ANSI_ARGS_(( MUD_SEC *pMUD ));
void fMUD_freeAll _ANSI_ARGS_(( MUD_SEC **ppMUD_head ));
int fMUD_writeEnd _ANSI_ARGS_(( FILE **pFout ));
int fMUD_write _ANSI_ARGS_(( FILE **pFout , MUD_SEC *pMUD ));
int fMUD_writeGrpStart _ANSI_ARGS_(( FILE **pFout , MUD_SEC_GRP *pMUD_parentGrp , MUD_SEC_GRP *pMUD_grp , int *pNumMems ));
int fMUD_writeGrpMem _ANSI_ARGS_(( FILE **pFout , MUD_SEC_GRP *pMUD_grp , MUD_SEC *pMUD ));
int fMUD_writeGrpEnd _ANSI_ARGS_(( FILE **pFout , MUD_SEC_GRP *pMUD_grp ));
MUD_SEC *fMUD_readFile _ANSI_ARGS_(( FILE **pFin ));
int fMUD_read _ANSI_ARGS_(( FILE **pFin , MUD_SEC *pMUD_ret ));
int fMUD_search _ANSI_ARGS_(( MUD_SEC **ppMUD_head , MUD_SEC *pMUD_ret , ...));
int fMUD_fseek _ANSI_ARGS_(( FILE **pFio , ...));



#ifdef VMS
static char*
setstrtodsc( char** pStr, s_dsc* dsc )
{
    *pStr = dsc->dsc$a_pointer;
    (*pStr)[dsc->dsc$w_length] = '\0';

    return( *pStr );
}


static s_dsc*
setdsctostr( s_dsc* dsc, char* str )
{
    dsc->dsc$w_length = _strlen( str );
    dsc->dsc$a_pointer = str;
    dsc->dsc$b_class = DSC$K_CLASS_S;	/* String dsc class */
    dsc->dsc$b_dtype = DSC$K_DTYPE_T;	/* Ascii string type */

    return( dsc );
}


static s_dsc*
setdsctobuf( s_dsc* dsc, char* buf, int len )
{
    dsc->dsc$w_length = len;
    dsc->dsc$a_pointer = buf;
    dsc->dsc$b_class = DSC$K_CLASS_S;	/* String dsc class */
    dsc->dsc$b_dtype = DSC$K_DTYPE_T;	/* Ascii string type */

    return( dsc );
}


static s_dsc*
strtodsc( s_dsc* dsc, char* str )
{
    int len;

    len = _strlen( str );
    strncpy( dsc->dsc$a_pointer, str, len );
    dsc->dsc$w_length = len;
    dsc->dsc$b_class = DSC$K_CLASS_S;	/* String dsc class */
    dsc->dsc$b_dtype = DSC$K_DTYPE_T;	/* Ascii string type */

    return( dsc );
}


static char*
dsctostr( char* str, s_dsc* dsc )
{
    strncpy( str, dsc->dsc$a_pointer, dsc->dsc$w_length );
    str[dsc->dsc$w_length] = '\0';

    return( str );
}
#endif /* VMS */


#ifdef VMS
void
fMUD_ctofString( s_dsc* fString, char** pcString )
{
    strtodsc( fString, *pcString );
}
#endif /* VMS */


#ifdef VMS
void
fMUD_ftocString( char** pcString, s_dsc* fString )
{
    setstrtodsc( pcString, fString );
}
#endif /* VMS */


#ifdef VMS
FILE*
fMUD_openInput( s_dsc* pdsc_fname )
{
    char* fname;

    setstrtodsc( &fname, pdsc_fname );
    return( MUD_openInput( fname ) );
}
#endif /* VMS */


#ifdef VMS
FILE*
fMUD_openOutput( s_dsc* pdsc_fname )
{
    char* fname;

    setstrtodsc( &fname, pdsc_fname );
    return( MUD_openOutput( fname ) );
}
#endif /* VMS */


void
fMUD_close( pFio )
    FILE** pFio;
{
    fclose( *pFio );
}


void
fMUD_rewind( pFio )
    FILE** pFio;
{
    rewind( *pFio );
}


void
fMUD_freeOne( pMUD )
    MUD_SEC* pMUD;
{
    if( pMUD == NULL ) return;

    (*pMUD->core.proc)( MUD_FREE, NULL, pMUD );
}


void
fMUD_freeAll( ppMUD_head )
    MUD_SEC** ppMUD_head;
{
    MUD_free( *ppMUD_head );
}


int
fMUD_writeEnd( pFout )
    FILE** pFout;
{
    return( MUD_writeEnd( *pFout ) );
}


int
fMUD_write( pFout, pMUD )
    FILE** pFout;
    MUD_SEC* pMUD;
{
    return( MUD_write( *pFout, pMUD, MUD_ONE ) );
}


int
fMUD_writeGrpStart( pFout, pMUD_parentGrp,
                   pMUD_grp, pNumMems )
    FILE** pFout;
    MUD_SEC_GRP* pMUD_parentGrp;
    MUD_SEC_GRP* pMUD_grp;
    int* pNumMems;
{
    return( MUD_writeGrpStart( *pFout, pMUD_parentGrp, pMUD_grp, *pNumMems ) );
}


int
fMUD_writeGrpMem( pFout, pMUD_grp, pMUD )
    FILE** pFout;
    MUD_SEC_GRP* pMUD_grp;
    MUD_SEC* pMUD;
{
    return( MUD_writeGrpMem( *pFout, pMUD_grp, pMUD ) );
}


int
fMUD_writeGrpEnd( pFout, pMUD_grp )
    FILE** pFout;
    MUD_SEC_GRP* pMUD_grp;
{
    return( MUD_writeGrpEnd( *pFout, pMUD_grp ) );
}


MUD_SEC*
fMUD_readFile( pFin )
    FILE** pFin;
{
    return( MUD_readFile( *pFin ) );
}


int
fMUD_read( pFin, pMUD_ret )
    FILE** pFin;
    MUD_SEC* pMUD_ret;
{
    MUD_SEC* pMUD_new;

    pMUD_new = MUD_read( *pFin, MUD_ONE );
    if( pMUD_new == NULL ) return( 0 );

    bcopy( pMUD_new, pMUD_ret, MUD_sizeOf( pMUD_new ) );
    _free( pMUD_new );
    return( 1 );
}


#ifdef NO_STDARG
int
fMUD_search( va_alist )
    va_dcl
#else
int
fMUD_search( MUD_SEC** ppMUD_head, MUD_SEC* pMUD_ret, ... )
#endif /* NO_STDARG */
{
    va_list args;
#ifdef NO_STDARG
    MUD_SEC** ppMUD_head;
    MUD_SEC* pMUD_ret;
#endif /* NO_STDARG */ 
    UINT32 argsArray[9];
    MUD_SEC* pMUD;
    int i;

    bzero( argsArray, 9*sizeof( UINT32 ) );

#ifdef NO_STDARG
    va_start( args );
    ppMUD_head = va_arg( args, MUD_SEC** );
    pMUD_ret = va_arg( args, MUD_SEC* );
#else
    va_start( args, pMUD_ret );
#endif /* NO_STDARG */

    for( i = 0; i < 9; i++ )
	if( ( argsArray[i] = *va_arg( args, UINT32* ) ) == 0 ) break;

    va_end( args );

    pMUD = MUD_search( *ppMUD_head, argsArray[0], argsArray[1], argsArray[2],
		       argsArray[3], argsArray[4], argsArray[5], argsArray[6],
		       argsArray[7], argsArray[8] );
    if( pMUD == NULL ) return( 0 );

    bcopy( pMUD, pMUD_ret, MUD_sizeOf( pMUD ) );
    return( 1 );
}


#ifdef NO_STDARG
int
fMUD_fseek( va_alist )
    va_dcl
#else
int
fMUD_fseek( FILE** pFio, ... )
#endif /* NO_STDARG */
{
    va_list args;
#ifdef NO_STDARG
    FILE** pFio;
#endif /* NO_STDARG */ 
    UINT32 argsArray[9];
    int i;
    int status;

    bzero( argsArray, 9*sizeof( UINT32 ) );

#ifdef NO_STDARG
    va_start( args );
    pFio = va_arg( args, FILE** );
#else
    va_start( args, pFio );
#endif /* NO_STDARG */

    for( i = 0; i < 9; i++ )
	if( ( argsArray[i] = *va_arg( args, UINT32* ) ) == 0 ) break;

    va_end( args );

#ifdef DEBUG
	printf( "fio = %X\n", *pFio );

	for( i = 0; i < 9; i++ )
	    printf( "arg[%d] = %X\n", i, argsArray[i] );
#endif /* DEBUG */

    status = MUD_fseek( *pFio, argsArray[0], argsArray[1], argsArray[2],
		       argsArray[3], argsArray[4], argsArray[5], argsArray[6],
		       argsArray[7], argsArray[8] );
    return( status );
}


