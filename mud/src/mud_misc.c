/*
 *  mud_misc.c -- some handy things
 *
 *  Revision history:
 *   v1.0  03-Feb-1994  [T. Whidden] Initial version
 *   v1.1  12-Mar-1994  [T. Whidden] Added mailbox utils
 *   v2.0  07-Jul-1994  [TW] Separated from vaxc_utils.c
 *   v3.0  20-Feb-1996  TW  Added gmf_time.c, renamed to mud_misc.c
 *         04-Mar-1996  TW  Removed memory allocation routines
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>

#include "mud.h"


/* 
 *  GMF_TIME - FORTRAN/C time interface by Michael Montour. Last
 *  modified 6/29/93. See GMF_TIME.README for more info. 
 */

/* 
 *  GMF_MKTIME calls C's mktime() function. Currently, no Daylight Savings 
 *  information is supported - standard time is assumed. This will be fixed.
 *  The input is an array of longwords in the order Y-M-D-H-M-S. The Month 
 *  field is from 1-12, instead of C's 0-11. Year is years from 1900, and
 *  everything else is normal.  
 */
void 
GMF_MKTIME( out, input )
    time_t* out;
    INT32 *input;
{

  struct tm Tm;

  Tm.tm_year = input[0];
  Tm.tm_mon  = input[1]-1;
  Tm.tm_mday = input[2];
  Tm.tm_hour = input[3];
  Tm.tm_min  = input[4];
  Tm.tm_sec  = input[5];
  Tm.tm_isdst = 0;
  
  *out = mktime (&Tm);
}


/* 
 *  Returns a longword containing the current system time. 
 */
void 
GMF_TIME( out )
    time_t* out;
{
    *out = time( NULL );
}


/* 
 *  Calls C's localtime(), writes the info into a 6-longword array (as in the
 *  input to GMF_MKTIME). Currently the extra stuff is ignored, but this is
 *  easy to change. 
 */
void 
GMF_LOCALTIME( in, out )
    time_t* in;
    INT32 *out;
{
  struct tm *Tm;

  Tm = localtime( (time_t*)in );
  out[0] = Tm->tm_year;
  out[1] = Tm->tm_mon+1;
  out[2] = Tm->tm_mday;
  out[3] = Tm->tm_hour;
  out[4] = Tm->tm_min;
  out[5] = Tm->tm_sec;
}


