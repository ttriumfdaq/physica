      SUBROUTINE PUT_VARIABLE( NAME, DIMA, IDX1, IDX2, IDX3
     &                        ,DIMX, VALX, ADRX, NROWX, NCOLX, NPLNX
     &                        ,LINE, MESS, * )

C   NAME = name of new variable
C   DIMA = number of dimensions on new variable
C          e.g., X[1,3]=... DIMA=2, X[Y]=... DIMA=1, X=... DIMA=0
C   IDXn(1) = number of dimensions in index n
C   IDXn(2) = value of index n if it is a single number
C   IDXn(3) = address of index variable n 
C   IDXn(4) = number of values of index n if it is multiple valued
C   DIMX = number of dimensions of input data
C   VALX = value of input data if it is a single value
C   ADRX  = address of input data if it is multiple valued
C   NROWX = first dimension size of input data if multiple valued
C   NCOLX = second dimension size of input data if multiple valued
C   NPLNX = third dimension size of input data if multiple valued
C   LINE  = history line to be added
C   MESS  = used for error messages (usually the command calling this routine)

      IMPLICIT NONE

      CHARACTER*(*) NAME, MESS, LINE
      REAL*8        VALX
      INTEGER*4     IDX1(4), IDX2(4), IDX3(4), DIMA, DIMX, ADRX
     &             ,NROWX, NCOLX, NPLNX

      INCLUDE 'PHYSICA.INC'
      INCLUDE 'NUMERIC_VARIABLES.INC'
      INCLUDE 'TEXT_VARIABLES.INC'

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      INTEGER*4  ISAVE, IAD(500), NALLOC(500), IBITE(500)
      COMMON /PHYSICA_TEMP_SPACE/ IAD, NALLOC, IBITE, ISAVE

C  local variables

      INTEGER*4 I, J, K, L, N, INDY, IMINY, IMAXY, JMINY, JMAXY, IV
     &         ,N1MIN, N1MAX, N2MIN, N2MAX, LNM, INEW
     &         ,BASE, IOFF1, IOFF2, NEWPTS1, NEWPTS2, IH1, IH2, IH3, IH4
      LOGICAL*4 NEWVAR
cc      integer*4 nv, lprev, idum
CCC
cc      write(6,1001)name, dima
cc 1001 format(' name,dima= ',a,1x,i3)
cc      write(6,1004)dimx,  valx,    adrx,  nrowx, ncolx
cc 1004 format(' dimx,valx,adrx,nrowx,ncolx= ',i3,d13.3,i7,2i5)
cc      write(6,1005)line, mess
cc 1005 format(' line,mess= ',a,1x,a)

      LNM = LEN(NAME)
      INDY = 0                          ! table index of an existing variable 
      IH1 = 0
      DO 1 I = 1, TVARNUM               ! check if new variable is text var.
        BASE = (TVARADR+(I-1)*TVARLEN)/4
        IF( LNM .NE. I4D(BASE+1) )GO TO 1
        DO J = 1, LNM
          IF( NAME(J:J).NE.CHAR(L1D((BASE+2)*4+J)) )GO TO 1
        END DO
        CALL DELETE_TEXT_VARIABLE( I )  ! found it, destroy it
        GO TO 3
    1 CONTINUE
      DO 2 I = 1, VARMAX                ! search for existing variable
        BASE = (VARADR+(I-1)*VARLEN)/4
        IF( LNM.NE.I4D(BASE+1) )GO TO 2 ! lengths not equal, go to next one
        DO J = 1, LNM                   ! check each character of name
          IF( NAME(J:J).NE.CHAR(L1D((BASE+2)*4+J)) )GO TO 2
        END DO
        INDY  = I                       ! found a variable with the same name
        IMINY = I4D(BASE+16)            ! initial index 1st dimension
        JMINY = I4D(BASE+17)            ! initial index 2nd dimension
        IMAXY = I4D(BASE+19)            ! final index 1st dimension
        JMAXY = I4D(BASE+20)            ! final index 2nd dimension
        N1MIN = IMINY                   ! 
        N1MAX = IMAXY                   ! 
        N2MIN = JMINY                   ! 
        N2MAX = JMAXY                   ! 
        GO TO 3                         ! stop the search
    2 CONTINUE                          !
    3 IF( DIMA .NE. 0 )THEN             ! there are indices on new variable 
        IF( IDX1(1) .EQ. 0 )THEN        ! first index is a single value
          N1MAX = IDX1(2)               ! store the index value
          N1MIN = N1MAX                 !  
        ELSE                            ! first index must be a vector
          N1MAX = I4D(IDX1(3)+1)        ! find the index extremes
          N1MIN = N1MAX                 ! 
          DO I = 1, IDX1(4)             ! 
            N = I4D(IDX1(3)+I)          ! 
            IF( N1MAX .LT. N )N1MAX=N   ! 
            IF( N1MIN .GT. N )N1MIN=N   ! 
          END DO                        ! 
        END IF                          ! 
        IF( DIMA .GT. 1 )THEN           ! there is a second index
          IF( IDX2(1) .EQ. 0 )THEN      ! 2nd index is a single value
            N2MAX = IDX2(2)             ! 
            N2MIN = N2MAX               ! 
          ELSE                          ! 2nd index must be a vector
            N2MAX = I4D(IDX2(3)+1)      ! find the index extremes
            N2MIN = N2MAX               ! 
            DO I = 1, IDX2(4)           ! 
              N = I4D(IDX2(3)+I)        ! 
              IF( N2MAX .LT. N )N2MAX=N !
              IF( N2MIN .GT. N )N2MIN=N !
            END DO                      !
          END IF                        ! 
        END IF                          ! 
      END IF                            ! 
      IF( DIMX .EQ. 0 )THEN             ! input data indicates scalar
        IF( DIMA .EQ. 0 )THEN           ! new variable had no indices
C                                         so it will be a scalar
C                                         e.g., X = 3
          IF( INDY .GT. 0 )THEN         ! new variable exists
            IF( I4D(BASE+11) .NE. 0 )THEN ! not a scalar
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF

cc          write(*,*)'indy = ',indy
          indy = indy+0

          IF( INDY .EQ. 0 )THEN         ! new variable doesn't already exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 100
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  100       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            DO I = 1, TSCAL                ! check the existence table for
              IF( L1D(ESCAL+I).EQ.0 )THEN  !  an empty slot, 1 means occupied
                I4D(BASE+12) = I           ! index to scalar table
                L1D(ESCAL+I) = 1
                R8D(ASCAL+I) = VALX
                NSCAL = NSCAL+1
                GO TO 500
              END IF
            END DO
C                                         all the slots are full, get more space
            CALL CHSIZE( ASCAL, TSCAL, TSCAL+64, 8, *9001 )
            CALL CHSIZE( ESCAL, TSCAL, TSCAL+64, 1, *9002 )
            DO I = TSCAL+1, TSCAL+64
              L1D(ESCAL+I) = 0
              R8D(ASCAL+I) = 0.0D0
            END DO
            NSCAL = NSCAL+1
            I4D(BASE+12) = NSCAL
            TSCAL = TSCAL+64
            L1D(ESCAL+NSCAL) = 1
            R8D(ASCAL+NSCAL) = VALX
            GO TO 500
          ELSE                          ! new variable already is a scalar
            IF( I4D(BASE+10) .EQ. -1 )I4D(BASE+10) = 0 ! make it a normal scalar
            R8D(ASCAL+I4D(BASE+12)) = VALX
            GO TO 500
          END IF
        ELSE IF( DIMA .EQ. 1 )THEN      ! new variable indices indicates
C                                         it is to be a vector
C                                         but data indicates scalar
C                                         e.g., X[5]=3 or X[3:9]=2
          IF( INDY .GT. 0 )THEN         ! new variable exists
            IF( I4D(BASE+11) .NE. 1 )THEN ! not a vector
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN         ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 110
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  110       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 1
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+19) = N1MAX
            IOFF1 = I4D(BASE+16)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13),8,*9003)
            DO J = 1, I4D(BASE+13)
              R8D(I4D(BASE+12)+J) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN       ! e.g., X[-2]=3
              R8D(I4D(BASE+12)+1) = VALX
            ELSE IF( IDX1(1) .EQ. 1 )THEN  ! e.g., X[-2:5]=2
              DO I = 1, IDX1(4)
                R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1) = VALX
              END DO
            END IF
            GO TO 500
          ELSE                          ! new variable already is a vector
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,IMINY) + 1
            CALL CHSIZE(IV,0,NEWPTS1,8,*9003)
            DO J = 1, NEWPTS1
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,IMINY)
            I4D(BASE+19) = MAX(N1MAX,IMAXY)
            IOFF1 = I4D(BASE+16)-1
            DO I = IMINY, IMAXY
              R8D(IV+I-IOFF1) = R8D(I4D(BASE+12)+I-IMINY+1)
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13),0,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            IF( IDX1(1) .EQ. 0 )THEN       ! e.g., X[-2]=3
              R8D(I4D(BASE+12)+N1MIN-IOFF1) = VALX
            ELSE IF( IDX1(1) .EQ. 1 )THEN  ! e.g., X[3:9]=2
              DO I = 1, IDX1(4)
                R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1) = VALX
              END DO
            END IF
            GO TO 500
          END IF
        ELSE IF( DIMA .EQ. 2 )THEN     ! new variable indices indicates
C                                        it is to be a matrix
C                                        but input data indicates scalar
C                                        e.g., X[2,5]=2 or X[2,3:9]=2
C                                           or X[2:7,3]=2 or X[2:7,3:9]=2
          IF( INDY.GT.0 )THEN          ! new variable exists 
            IF( I4D(BASE+11).NE.2 )THEN  ! not a matrix
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN        ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 120
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  120       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 2
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+14) = N2MAX-N2MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+17) = N2MIN
            I4D(BASE+19) = N1MAX
            I4D(BASE+20) = N2MAX
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13)*I4D(BASE+14)
     &       ,8,*9004)
            DO J = 1, I4D(BASE+13)*I4D(BASE+14)
              R8D(I4D(BASE+12)+J) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN  ! e.g., X[-2,-5]=2 
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           VALX
              ELSE IF( IDX2(1) .EQ. 1 )THEN  ! e.g., X[2,3:9]=2
                DO J = 1, IDX2(4)
                  R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &             (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = VALX
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN   ! e.g., X[2:7,3]=2 
                DO I = 1, IDX1(4)
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &             (N2MIN-1-IOFF2)*I4D(BASE+13)) = VALX
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN  ! e.g., X[2:7,3:9]=2
                DO J = 1, IDX2(4)
                  DO I = 1, IDX1(4)
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &               (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = VALX
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          ELSE                       ! new variable is a matrix already
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,IMINY) + 1
            NEWPTS2 = MAX(N2MAX,JMAXY) - MIN(N2MIN,JMINY) + 1
            CALL CHSIZE(IV,0,NEWPTS1*NEWPTS2,8,*9004)
            DO J = 1, NEWPTS1*NEWPTS2
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,IMINY)
            I4D(BASE+17) = MIN(N2MIN,JMINY)
            I4D(BASE+19) = MAX(N1MAX,IMAXY)
            I4D(BASE+20) = MAX(N2MAX,JMAXY)
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            DO J = JMINY, JMAXY
              DO I = IMINY, IMAXY
                R8D(IV+I-IOFF1+(J-1-IOFF2)*NEWPTS1) =
     &           R8D(I4D(BASE+12)+I-IMINY+1+(J-JMINY)*I4D(BASE+13))
              END DO
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13)*I4D(BASE+14),0
     &       ,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            I4D(BASE+14) = NEWPTS2
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN  ! e.g., X[-2,-5]=2 
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           VALX
              ELSE IF( IDX2(1) .EQ. 1 )THEN  ! e.g., X[2,3:9]=2
                DO J = 1, IDX2(4)
                  R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &             (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = VALX
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN   ! e.g., X[2:7,3]=2 
                DO I = 1, IDX1(4)
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &             (N2MIN-1-IOFF2)*I4D(BASE+13)) = VALX
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN  ! e.g., X[2:7,3:9]=2
                DO J = 1, IDX2(4)
                  DO I = 1, IDX1(4)
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &               (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13))= VALX
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          END IF
        END IF
      ELSE IF( DIMX .EQ. 1 )THEN        ! input data indicates a vector
        IF( DIMA .EQ. 0 )THEN           ! new variable had no indices
C                                         so it will be a vector
C                                         e.g., X = [3:9]
          IF( INDY .GT. 0 )THEN         ! new variable already exists 
            IF( I4D(BASE+11).NE.1 )THEN ! not a vector
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            ELSE                        ! is a vector,
              IF( .NOT.APPEND )THEN     !  but not appending to it
                IH1 = I4D(BASE+22)      !  save the history
                IH2 = I4D(BASE+23)
                CALL CHSIZE(IH3,0,IH1,4,*9500)
                CALL CHSIZE(IH4,0,IH2,1,*9501)
                DO J = 1, IH1
                  I4D(IH3+J) = I4D(I4D(BASE+24)+J)
                END DO
                DO J = 1, IH2
                  L1D(IH4+J) = L1D(I4D(BASE+25)+J)
                END DO
                CALL DELETE_VARIABLE( INDY )
                INDY = 0
              END IF
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN         ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 200
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  200       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 1
            I4D(BASE+13) = NROWX
            I4D(BASE+16) = 1
            I4D(BASE+19) = NROWX
            DO I = 1, ISAVE                ! loop over all temp. memory alloc's
              IF( ADRX .EQ. IAD(I) )THEN     ! data is a temp. memory alloc.
                IF( I4D(BASE+13) .EQ. NALLOC(I) )THEN  ! size is the same
                  I4D(BASE+12) = ADRX                  ! transfer info.
                  DO J = I, ISAVE-1                    ! shift up temp. alloc's
                    IAD(J) = IAD(J+1)                  ! so it won't be deleted
                    NALLOC(J) = NALLOC(J+1)
                    IBITE(J) = IBITE(J+1)
                  END DO
                  ISAVE = ISAVE-1
                  GO TO 500
                END IF
              END IF
            END DO
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13),8,*9003)
            DO I = 1, I4D(BASE+13)
              R8D(I4D(BASE+12)+I) = R8D(ADRX+I)
            END DO
            GO TO 500
          ELSE                       ! new variable already is a vector
C                                      must be appending data onto a vector
C                                      this could only come from READ\APPEND
            NEWPTS1 = NROWX+I4D(BASE+13)
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13),NEWPTS1,8,*9005)
            DO I = 1, NROWX
              R8D(I4D(BASE+12)+I4D(BASE+13)+I) = R8D(ADRX+I)
            END DO
            I4D(BASE+19) = I4D(BASE+19)+NROWX
            I4D(BASE+13) = NEWPTS1
            GO TO 500
          END IF
        ELSE IF( DIMA .EQ. 1 )THEN    ! new variable indices indicates
C                                       it is to be a vector
C                                       e.g., X[-2:4] = [3:9]
          IF( INDY .GT. 0 )THEN       ! new variable exists
            IF( I4D(BASE+11).NE.1 )THEN ! it is not a vector
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN      ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 210
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  210       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 1
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+19) = N1MAX
            IOFF1 = I4D(BASE+16)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13),8,*9003)
            DO I = 1, I4D(BASE+13)
              R8D(I4D(BASE+12)+I) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN
              R8D(I4D(BASE+12)+N1MIN-IOFF1) = R8D(ADRX+1)
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              DO I = 1, MIN(NROWX,IDX1(4))
                R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1) = R8D(ADRX+I)
              END DO
            END IF
            GO TO 500
          ELSE                       ! new variable already is a vector
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,I4D(BASE+16)) + 1
            CALL CHSIZE(IV,0,NEWPTS1,8,*9005)
            DO J = 1, NEWPTS1
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,I4D(BASE+16))
            I4D(BASE+19) = MAX(N1MAX,IMAXY) 
            IOFF1 = I4D(BASE+16)-1
            DO I = IMINY, IMAXY
              R8D(IV+I-IOFF1) = R8D(I4D(BASE+12)+I-IMINY+1)
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13),0,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            IF( IDX1(1) .EQ. 0 )THEN
              R8D(I4D(BASE+12)+N1MIN-IOFF1) = R8D(ADRX+1)
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              DO I = 1, MIN(NROWX,IDX1(4))
                R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1) = R8D(ADRX+I)
              END DO
            END IF
            GO TO 500
          END IF
        ELSE IF( DIMA .EQ. 2 )THEN      ! new variable indices indicates
C                                         it is to be a matrix
C                                         e.g., X[-3,-2:4] = [3:9] or
C                                               X[-2:4,-3] = [3:9]
          IF( INDY .GT. 0 )THEN         ! new variable exists already
            IF( I4D(BASE+11).NE.2 )THEN ! not a matrix
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF 
          IF( INDY .EQ. 0 )THEN         ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 220
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  220       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 2
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+14) = N2MAX-N2MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+17) = N2MIN
            I4D(BASE+19) = N1MAX
            I4D(BASE+20) = N2MAX
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13)*I4D(BASE+14)
     &       ,8,*9004)
            DO I = 1, I4D(BASE+13)*I4D(BASE+14)
              R8D(I4D(BASE+12)+I) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           R8D(ADRX+1)
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                DO J = 1, MIN(NROWX,IDX2(4))
                  R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &             (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+J)
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN
                DO I = 1, MIN(NROWX,IDX1(4))
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &             (N2MIN-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+I)
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1
                DO J = 1, IDX2(4)
                  DO I = 1, IDX1(4)
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &               (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) =
     &                R8D(ADRX+K)
                    IF( K .EQ. NROWX )GO TO 500
                    K = K+1
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          ELSE                       ! new variable already is a matrix
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,IMINY) + 1
            NEWPTS2 = MAX(N2MAX,JMAXY) - MIN(N2MIN,JMINY) + 1
            CALL CHSIZE(IV,0,NEWPTS1*NEWPTS2,8,*9004)
            DO J = 1, NEWPTS1*NEWPTS2
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,IMINY)
            I4D(BASE+17) = MIN(N2MIN,JMINY)
            I4D(BASE+19) = MAX(N1MAX,IMAXY)
            I4D(BASE+20) = MAX(N2MAX,JMAXY)
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            DO J = JMINY, JMAXY
              DO I = IMINY, IMAXY
                R8D(IV+I-IOFF1+(J-1-IOFF2)*NEWPTS1) =
     &           R8D(I4D(BASE+12)+I-IMINY+1+(J-JMINY)*I4D(BASE+13))
              END DO
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13)*I4D(BASE+14),0
     &       ,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            I4D(BASE+14) = NEWPTS2
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           R8D(ADRX+1)
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                DO J = 1, MIN(NROWX,IDX2(4))
                  R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &             (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+J)
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN
                DO I = 1, MIN(NROWX,IDX1(4))
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &             (N2MIN-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+I)
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1
                DO J = 1, IDX2(4)
                  DO I = 1, IDX1(4)
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+I)-IOFF1+
     &              (I4D(IDX2(3)+J)-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+K)
                    IF( K .EQ. NROWX )GO TO 500
                    K = K+1
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          END IF
        END IF
      ELSE IF( DIMX .EQ. 2 )THEN      ! input data indicates a matrix
        IF( DIMA .EQ. 0 )THEN         ! new variable had no indices
C                                       so it will be a matrix
C                                       e.g., X = M[2:5,3:9] 
          IF( INDY .GT. 0 )THEN       ! output variable exists 
            IF( I4D(BASE+11).EQ.2 )THEN ! is a matrix
              IH1 = I4D(BASE+22)        ! number of history lines
              IH2 = I4D(BASE+23)        ! total number of characters in history
              CALL CHSIZE(IH3,0,IH1,4,*9500)
              CALL CHSIZE(IH4,0,IH2,1,*9501)
              DO J = 1, IH1
                I4D(IH3+J) = I4D(I4D(BASE+24)+J)
              END DO
              DO J = 1, IH2
                L1D(IH4+J) = L1D(I4D(BASE+25)+J)
              END DO
            END IF
            CALL DELETE_VARIABLE( INDY )
            INDY = 0
          END IF
          DO I = 1, VARMAX
            BASE = (VARADR+(I-1)*VARLEN)/4              
            IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
              INEW = I
              GO TO 240
            END IF
          END DO
          IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
            CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
            DO I = 1, 512
              L1D(VARADR+VARALC+I) = 0
            END DO
            VARALC = VARALC+512
          END IF
          VARMAX = VARMAX+1
          INEW = VARNUM+1
  240     VARNUM = VARNUM+1
          BASE = (VARADR+(INEW-1)*VARLEN)/4
          I4D(BASE+1) = LNM
          DO J = 1, LNM
            L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
          END DO
          I4D(BASE+11) = 2
          I4D(BASE+13) = NROWX
          I4D(BASE+14) = NCOLX
          I4D(BASE+16) = 1
          I4D(BASE+17) = 1
          I4D(BASE+19) = NROWX
          I4D(BASE+20) = NCOLX
          DO I = 1, ISAVE
            IF( ADRX .EQ. IAD(I) )THEN
              IF( I4D(BASE+13)*I4D(BASE+14) .EQ. NALLOC(I) )THEN
                I4D(BASE+12) = ADRX
                DO J = I, ISAVE-1
                  IAD(J) = IAD(J+1)
                  NALLOC(J) = NALLOC(J+1)
                  IBITE(J) = IBITE(J+1)
                END DO
                ISAVE = ISAVE-1
                GO TO 500
              END IF
            END IF
          END DO
          CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13)*I4D(BASE+14),8,*9004)
          DO J = 1, I4D(BASE+14)
            DO I = 1, I4D(BASE+13)
              R8D(I4D(BASE+12)+I+(J-1)*NROWX) = R8D(ADRX+I+(J-1)*NROWX)
            END DO
          END DO
          GO TO 500
        ELSE IF( DIMA .EQ. 1 )THEN      ! new variable indices indicates
C                                         it is to be a vector
C                                         e.g., X[-14:13] = M[2:5,3:9] 
          IF( INDY .GT. 0 )THEN         ! new variable already exists
            IF( I4D(BASE+11) .NE. 1 )THEN ! not a vector
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN         ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 260
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  260       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 1
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+19) = N1MAX
            IOFF1 = I4D(BASE+16)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13),8,*9004)
            DO J = 1, I4D(BASE+13)
              R8D(I4D(BASE+12)+J) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN
              R8D(I4D(BASE+12)+N1MIN-IOFF1) = R8D(ADRX+1)
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              K = 1
              DO J = 1, NCOLX
                DO I = 1, NROWX
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1) =
     &             R8D(ADRX+I+(J-1)*NROWX)
                  IF( K .EQ. I4D(BASE+13) )GO TO 500
                  K = K+1
                END DO
              END DO
            END IF
            GO TO 500
          ELSE                       ! new variable already is a vector
C                                      e.g., X[-14:13] = M[2:5,3:9] 
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,IMINY) + 1
            CALL CHSIZE(IV,0,NEWPTS1,8,*9006)
            DO J = 1, NEWPTS1
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,IMINY)
            I4D(BASE+19) = MAX(N1MAX,IMAXY)
            IOFF1 = I4D(BASE+16)-1
            DO I = IMINY, IMAXY
              R8D(IV+I-IOFF1) = R8D(I4D(BASE+12)+I-IMINY+1)
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13),0,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            IF( IDX1(1) .EQ. 0 )THEN             ! e.g. x[-3]=m[2:5,3:9]
              R8D(I4D(BASE+12)+N1MIN-IOFF1) = R8D(ADRX+1)
            ELSE IF( IDX1(1) .EQ. 1 )THEN        ! e.g. x[-14:13]=m[2:5,3:9]
              K = 1
              DO J = 1, NCOLX
                DO I = 1, NROWX
                  R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1) =
     &             R8D(ADRX+I+(J-1)*NROWX)
                  IF( K .EQ. I4D(BASE+13) )GO TO 500
                  K = K+1
                END DO
              END DO
            END IF
            GO TO 500
          END IF
        ELSE IF( DIMA .EQ. 2 )THEN      ! new variable indices indicates
C                                         it is to be a matrix
C                                         e.g., X[-2:3,-1:2] = M[2:5,3:8] 
          IF( INDY .GT. 0 )THEN         ! new variable exists
            IF( I4D(BASE+11).NE.2 )THEN ! not a matrix
              CALL DELETE_VARIABLE( INDY )
              INDY = 0
            END IF
          END IF
          IF( INDY .EQ. 0 )THEN         ! new variable does not exist
            DO I = 1, VARMAX
              BASE = (VARADR+(I-1)*VARLEN)/4              
              IF( I4D(BASE+1) .EQ. 0 )THEN    ! found an empty section
                INEW = I
                GO TO 280
              END IF
            END DO
            IF( (VARMAX+1)*VARLEN .GT. VARALC )THEN
              CALL CHSIZE( VARADR, VARALC, VARALC+512, 1, *9000 )
              DO I = 1, 512
                L1D(VARADR+VARALC+I) = 0
              END DO
              VARALC = VARALC+512
            END IF
            VARMAX = VARMAX+1
            INEW = VARNUM+1
  280       VARNUM = VARNUM+1
            BASE = (VARADR+(INEW-1)*VARLEN)/4
            I4D(BASE+1) = LNM
            DO J = 1, LNM
              L1D((BASE+2)*4+J) = ICHAR(NAME(J:J))
            END DO
            I4D(BASE+11) = 2
            I4D(BASE+13) = N1MAX-N1MIN+1
            I4D(BASE+14) = N2MAX-N2MIN+1
            I4D(BASE+16) = N1MIN
            I4D(BASE+17) = N2MIN
            I4D(BASE+19) = N1MAX
            I4D(BASE+20) = N2MAX
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            CALL CHSIZE(I4D(BASE+12),0,I4D(BASE+13)*I4D(BASE+14)
     &       ,8,*9004)
            DO J = 1, I4D(BASE+13)*I4D(BASE+14)
              R8D(I4D(BASE+12)+J) = 0.0D0
            END DO
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN  ! e.g., X[-2,-1] = M[2:5,3:8] 
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           R8D(ADRX+1)
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1                 ! e.g., X[-2,-1:2] = M[2:5,3:8] 
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &               (I4D(IDX2(3)+K)-1-IOFF2)*I4D(BASE+13)) =
     &               R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX2(4) )GO TO 500
                    K = K+1
                  END DO
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN ! e.g., X[-2:3,-1] = M[2:5,3:8] 
                K = 1                
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1+
     &           (N2MIN-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX1(4) )GO TO 500
                    K = K+1
                  END DO
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1                 ! e.g., X[-2:3,-1:2] = M[2:5,3:8] 
                L = 1
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1+
     &               (I4D(IDX2(3)+L)-1-IOFF2)*I4D(BASE+13)) =
     &               R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX1(4) )THEN
                      IF( L .EQ. IDX2(4) )GO TO 500
                      K = 0
                      L = L+1
                    END IF
                    K = K+1
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          ELSE                           ! new variable already is a matrix
            NEWPTS1 = MAX(N1MAX,IMAXY) - MIN(N1MIN,IMINY) + 1
            NEWPTS2 = MAX(N2MAX,JMAXY) - MIN(N2MIN,JMINY) + 1
            CALL CHSIZE(IV,0,NEWPTS1*NEWPTS2,8,*9006)
            DO J = 1, NEWPTS1*NEWPTS2
              R8D(IV+J) = 0.0D0
            END DO
            I4D(BASE+16) = MIN(N1MIN,IMINY)
            I4D(BASE+17) = MIN(N2MIN,JMINY)
            I4D(BASE+19) = MAX(N1MAX,IMAXY)
            I4D(BASE+20) = MAX(N2MAX,JMAXY)
            IOFF1 = I4D(BASE+16)-1
            IOFF2 = I4D(BASE+17)-1
            DO J = JMINY, JMAXY
              DO I = IMINY, IMAXY
                R8D(IV+I-IOFF1+(J-1-IOFF2)*NEWPTS1) =
     &           R8D(I4D(BASE+12)+I-IMINY+1+(J-JMINY)*I4D(BASE+13))
              END DO
            END DO
            CALL CHSIZE(I4D(BASE+12),I4D(BASE+13)*I4D(BASE+14),0
     &       ,8,*9200)
            I4D(BASE+12) = IV
            I4D(BASE+13) = NEWPTS1
            I4D(BASE+14) = NEWPTS2
            IF( IDX1(1) .EQ. 0 )THEN
              IF( IDX2(1) .EQ. 0 )THEN   ! e.g., X[-2,-1] = M[2:5,3:8] 
            R8D(I4D(BASE+12)+N1MIN-IOFF1+(N2MIN-1-IOFF2)*I4D(BASE+13)) =
     &           R8D(ADRX+1)
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1                   ! e.g., X[-2,-1:2] = M[2:5,3:8] 
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+N1MIN-IOFF1+
     &               (I4D(IDX2(3)+K)-1-IOFF2)*I4D(BASE+13)) =
     &               R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX2(4) )GO TO 500
                    K = K+1
                  END DO
                END DO
              END IF
            ELSE IF( IDX1(1) .EQ. 1 )THEN
              IF( IDX2(1) .EQ. 0 )THEN ! e.g., X[-2:3,-1] = M[2:5,3:8] 
                K = 1
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1+
     &           (N2MIN-1-IOFF2)*I4D(BASE+13)) = R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX1(4) )GO TO 500
                    K = K+1
                  END DO
                END DO
              ELSE IF( IDX2(1) .EQ. 1 )THEN
                K = 1                ! e.g., X[-2:3,-1:2] = M[2:5,3:8] 
                L = 1
                DO J = 1, NCOLX
                  DO I = 1, NROWX
                    R8D(I4D(BASE+12)+I4D(IDX1(3)+K)-IOFF1+
     &               (I4D(IDX2(3)+L)-1-IOFF2)*I4D(BASE+13)) =
     &               R8D(ADRX+I+(J-1)*NROWX)
                    IF( K .EQ. IDX1(4) )THEN
                      IF( L .EQ. IDX2(4) )GO TO 500
                      K = 0
                      L = L+1
                    END IF
                    K = K+1
                  END DO
                END DO
              END IF
            END IF
            GO TO 500
          END IF
        END IF
      END IF
  500 NEWVAR = (INDY.EQ.0)
      CALL UPDATE_HISTORY( BASE, NEWVAR, IH1, IH2, IH3, IH4, LINE
     & ,MESS, *92 )

cc      write(*,*)' *** FROM PUT_VARIABLE ***'
cc      do nv = 1, varnum
cc        base = (varadr+(nv-1)*varlen)/4
cc        write(*,1000)nv,I4D(base+1)
cc 1000   format(' var# ',i2,': name length = ',i8)
cc        write(*,1001)(L1D((base+2)*4+i),i=1,I4D(base+1))
cc 1001   format(' name: ',32a1)
cc        if( I4D(base+11) .eq. 0 )then
cc          write(*,1002)I4D(base+10),I4D(base+12),R8D(ascal+I4D(base+12))
cc 1002     format(' scalar: type, index, value =',2(1x,i3),1x,d12.5)
cc        else if( I4D(base+11) .eq. 1 )then
cc          write(*,1003)I4D(base+13),I4D(base+16),I4D(base+19)
cc 1003     format(' vector: npts,istrt,ifinl= ',3(1x,i6))
cc        else if( I4D(base+11) .eq. 2 )then
cc          write(*,1004)I4D(base+13),I4D(base+14)
cc 1004     format(' vector: nrows,ncols= ',2(1x,i6))
cc        end if
cc        write(*,1005)I4D(base+22),I4D(base+23)
cc 1005   format(' # history lines, total length=',2(1x,i6))
cc        lprev = 0
cc        do j = 1, I4D(base+22)
cc          write(*,1006)I4D(I4D(base+24)+j)
cc 1006     format(' line length = ',i6)
cc          write(*,1007)(L1D(I4D(base+25)+lprev+k),k=1,I4D(I4D(base+24)+j))
cc 1007     format(' history: *',<I4D(I4D(base+24)+j)>a1,'*')
cc          lprev = lprev+I4D(I4D(base+24)+j)
cc        end do
cc        write(6,1008)
cc 1008   format(' Enter 0 to continue >> ',$)
cc        read(5,*)idum
cc      end do
cc      write(*,*)' *** END FROM PUT_VARIABLE ***'

      RETURN
   92 RETURN 1
 9000 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the numeric variable table')
      RETURN 1
 9001 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the scalar data table')
      VARNUM = VARNUM-1
      RETURN 1
 9002 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the auxiliary'//
     &  ' scalar data table')
      VARNUM = VARNUM-1
      CALL CHSIZE( ASCAL, TSCAL+64, TSCAL, 8, *9400 )
      RETURN 1
 9003 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the data for a vector')
      VARNUM = VARNUM-1
      RETURN 1
 9004 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the data for a matrix')
      VARNUM = VARNUM-1
      RETURN 1
 9005 CALL ERROR_MESSAGE(MESS
     & ,'altering dynamic array space for the data for a vector')
      VARNUM = VARNUM-1
      RETURN 1
 9006 CALL ERROR_MESSAGE(MESS
     & ,'altering dynamic array space for the data for a matrix')
      RETURN 1
 9200 CALL ERROR_MESSAGE(MESS
     & ,'deallocating dynamic array space from a variable data array')
      RETURN 1
 9400 CALL ERROR_MESSAGE(MESS
     & ,'deallocating dynamic array space for the scalar data table')
      RETURN 1
 9500 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the temporary'//
     &  ' history length array')
      RETURN 1
 9501 CALL ERROR_MESSAGE(MESS
     & ,'creating dynamic array space for the temporary'//
     &  ' history character array')
      RETURN 1
      END
