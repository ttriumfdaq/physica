      SUBROUTINE RESTORE_MUD( fileHandle, fname, * )

C   Subroutine that reads MuSR MUD arrays from the fileHandle
C   This unit should be opened by the calling routine

      character*(*) fname
      INTEGER*4     fileHandle

      INCLUDE 'PHYSICA.INC'

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      byte      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      INTEGER*4 IDX1(4), IDX2(4), IDX3(4)
      DATA IDX1 /0,0,0,0/, IDX2 /0,0,0,0/, IDX3 /0,0,0,0/

      character*255 slong
      character*80  string, name
      character*1   bs
      integer     pRunNumber, pType, pNum, nComments, pCounts(2) 
      integer     i, ii, istat, nolds, lent, lensig, istart, iend
      integer     nbins, ln, inith4, saddr, lens, lenf
CCC
      bs = char(92)   ! backslash
      lenf = len(fname)
      slong = 'RESTORE'//bs//'MUD '//fname(1:lenf)
      lens = lensig( slong )

cc      write(*,*)'about to call getrunnumber'

      call fMUD_getRunNumber( fileHandle, pRunNumber )
      name = 'RUNNUMBER'
      ln = 9
ccc      write(*,*)'lens = ',lens
ccc      write(*,*)'slong = |',slong(1:lens),'|'
ccx      write(*,*)'filehandle = ',filehandle,', prunnumber = ',prunnumber
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(pRunNumber)
     & ,0,0,0,0,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'run number = ',pRunNumber

      call fMUD_getRunDesc( fileHandle, i )
      name = 'RUNDESC'
      ln = 7
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(i),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'run desc = ',i

      call fMUD_getExptNumber( fileHandle, i )
      name = 'EXPTNUMBER'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(i),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'expt number = ',i

      call fMUD_getElapsedSec( fileHandle, i )

cc      write(*,*)'elapsed sec = ',i

      name = 'ELAPSEDSEC'
      ln = 10

cc      write(*,*)'elapsed sec = ',i

      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(i),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'elapsed sec = ',i

      call fMUD_getTimeBegin( fileHandle, i )
      name = 'TIMEBEGIN'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(i),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'time begin = ',i

      call fMUD_getTimeEnd( fileHandle, i )
      name = 'TIMEEND'
      ln = 7
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(i),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

cc      write(*,*)'time end = ',i

      string = ' '
      call fMUD_getTitle( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'title = ',string(1:lent)

      call get_temp_space(saddr,lent,1,*99)
      nolds = lent

      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'MUDTITLE'
      ln = 8
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getLab( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'lab = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'LAB'
      ln = 3
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getArea( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'area = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'AREA'
      ln = 4
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getMethod( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'method = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'METHOD'
      ln = 6
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getApparatus( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'apparatus = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'APPARATUS'
      ln = 9
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getInsert( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'insert = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'INSERT'
      ln = 6
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getSample( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'sample = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'SAMPLE'
      ln = 6
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getOrient( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'orient = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'ORIENT'
      ln = 6
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getDas( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'das = ',string(1:lent)

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do 
      name = 'DAS'
      ln = 3
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      string = ' '
      call fMUD_getExperimenter( fileHandle, string )
      lent = lensig( string )
      if( lent .le. 0 )then
        string = ' '
        lent = 1
      end if

cc      write(*,*)'lent = ',lent
cc      write(*,*)'experimenter = ',string(1:lent)
cc      write(*,*)'nolds = ',nolds

      if( lent .gt. nolds )then
        call get_more_space(saddr,nolds,lent,1,*99)
        nolds = lent
      end if
      do i = 1, lent
        idum = ichar(string(i:i))
        if( idum .gt. 127 )idum = 32
        L1D(saddr+i) = idum
      end do
      name = 'EXPERIMENTER'
      ln = 12
      call put_text_variable( name(1:ln), 0, 0, idx1
     & ,saddr, lent, slong(1:lens), *92 )

      call fMUD_getComments( fileHandle, pType, nComments )

ccc      write(*,*)'pType = ',pType,', nComments = ',nComments

      name = 'COMMENTTYPE'
      ln = 11
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(pType)
     & ,0,0,0,0,slong(1:lens),'RESTORE',*92)

      name = 'COMMENTNUM'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(nComments)
     & ,0,0,0,0,slong(1:lens),'RESTORE',*92)

      if( nComments .gt. 0 )then
        call get_temp_space(iaddr,nComments,8,*99)
        do ii = 1, nComments
          call fMUD_getCommentPrev( fileHandle, ii, i )
          R8D(iaddr+ii) = dble(i)
        end do
        name = 'COMMENTPREV'
        ln = 11
        call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0D0,iaddr
     &   ,nComments,0,0,slong(1:lens),'RESTORE',*92)

        call get_temp_space(iaddr,nComments,8,*99)
        do ii = 1, nComments
          call fMUD_getCommentNext( fileHandle, ii, i )
          R8D(iaddr+ii) = dble(i)
        end do
        name = 'COMMENTNEXT'
        ln = 11
        call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0D0,iaddr
     &   ,nComments,0,0,slong(1:lens),'RESTORE',*92)

        call get_temp_space(iaddr,nComments,8,*99)
        do ii = 1, nComments
          call fMUD_getCommentTime( fileHandle, ii, i )
          R8D(iaddr+ii) = dble(i)
        end do
        name = 'COMMENTTIME'
        ln = 11
        call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0D0,iaddr
     &   ,nComments,0,0,slong(1:lens),'RESTORE',*92)

        do ii = 1, nComments
          string = ' '
          call fMUD_getCommentAuthor( fileHandle, ii, string )
          lent = lensig( string )
          if( lent .le. 0 )then
            string(1:1) = ' '
            lent = 1
          end if
          if( lent .gt. nolds )then
            call get_more_space(saddr,nolds,lent,1,*99)
            nolds = lent
          end if
          do i = 1, lent
            idum = ichar(string(i:i))
            if( idum .gt. 127 )idum = 32
            L1D(saddr+i) = idum
          end do
          name = 'COMMENTAUTHOR'
          ln = 13
          call put_text_variable( name(1:ln), ii, 0, idx1
     &     ,saddr, lent, slong(1:lens), *92 )

          string = ' '
          call fMUD_getCommentTitle( fileHandle, ii, string )
          lent = lensig( string )
          if( lent .le. 0 )then
            string(1:1) = ' '
            lent = 1
          end if
          if( lent .gt. nolds )then
            call get_more_space(saddr,nolds,lent,1,*99)
            nolds = lent
          end if
          do i = 1, lent
            idum = ichar(string(i:i))
            if( idum .gt. 127 )idum = 32
            L1D(saddr+i) = idum
          end do
          name = 'COMMENTTITLE'
          ln = 12
          call put_text_variable( name(1:ln), ii, 0, idx1
     &     ,saddr, lent, slong(1:lens), *92 )

          string = ' '
          call fMUD_getCommentBody( fileHandle, ii, string )
          lent = lensig( string )
          if( lent .le. 0 )then
            string(1:1) = ' '
            lent = 1
          end if
          if( lent .gt. nolds )then
            call get_more_space(saddr,nolds,lent,1,*99)
            nolds = lent
          end if
          do i = 1, lent
            idum = ichar(string(i:i))
            if( idum .gt. 127 )idum = 32
            L1D(saddr+i) = idum
          end do
          name = 'COMMENTBODY'
          ln = 11
          call put_text_variable( name(1:ln), ii, 0, idx1
     &     ,saddr, lent, slong(1:lens), *92 )
        end do
      end if

      call fMUD_getHists( fileHandle, pType, pNum )

ccc      write(*,*)'pType = ',pType,', pNum = ',pNum

      name = 'HISTNUM'
      ln = 7
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(pNum),0,0,0,0
     & ,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      call get_temp_space( istart, pNum, 8, *99 )
      call get_temp_space( iend, pNum, 8, *99 )

      nbintot = 0
      istrt = 1
      do ii = 1, pNum
        string = ' '
        call fMUD_getHistTitle( fileHandle, ii, string )
        lent = lensig( string )
        if( lent .le. 0 )then
          string(1:1) = ' '
          lent = 1
        end if

ccc      write(*,*)'lent = ',lent
ccc      write(*,*)'hist title = ',string(1:lent)

        if( lent .gt. nolds )then
          call get_more_space(saddr,nolds,lent,1,*99)
          nolds = lent
        end if
        do i = 1, lent
          idum = ichar(string(i:i))
          if( idum .gt. 127 )idum = 32
          L1D(saddr+i) = idum
        end do
        name = 'HISTTITLE'
        ln = 9
        call put_text_variable( name(1:ln), ii, 0, idx1
     &   ,saddr, lent, slong(1:lens), *92 )

        call fMUD_getHistNumBins( fileHandle, ii, nbins )
        R8D(iaddr+ii) = dble(nbins)
        nbintot = nbintot+nbins

        R8D(istart+ii) = istrt
        istrt = istrt + nbins
        R8D(iend+ii) = istrt-1
      end do

      name = 'NUMBINS'
      ln = 7
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      name = 'HISTSTART'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,istart
     & ,pNum,0,0,slong(1:lens),'RESTORE',*92)

      name = 'HISTEND'
      ln = 7
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iend,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( inith4, nbintot, 4, *99 )
      call get_temp_space( idata, nbintot, 8, *99 )
      istrt = 1
      do ii = 1, pNum
        call fMUD_getHistData( fileHandle, ii, I4D(inith4+istrt) )
        istrt = istrt + R8D(iaddr+ii)
      end do
      do i = 1, nbintot
        R8D(idata+i) = dble(I4D(inith4+i))
      end do
      name = 'HISTDATA'
      ln = 8
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,idata
     & ,nbintot,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistType( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTTYPE'
      ln = 8
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistNumBytes( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTNUMBYTES'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistBytesPerBin( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTBYTESPERBIN'
      ln = 15
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistFsPerBin( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTFSPERBIN'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistT0_Ps( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTT0_PS'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistT0_Bin( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTT0_BIN'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistGoodBin1( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTGOODBIN1'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistGoodBin2( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTGOODBIN2'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistBkgd1( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTBKGD1'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistBkgd2( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'HISTBKGD2'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getHistNumEvents( fileHandle, ii, i )
        R8D(iaddr+ii) = dble(i)
      end do
      name = 'NUMEVENTS'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call fMUD_getScalers( fileHandle, pType, pNum )
      name = 'SCALERTYPE'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(pType)
     & ,0,0,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, 2*pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getScalerCounts( fileHandle, ii, pCounts )
        R8D(iaddr+ii) = dble(pCounts(1))
        R8D(iaddr+ii+pNum) = dble(pCounts(2))
      end do
      name = 'SCALERCOUNTS'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,2,0.0d0,iaddr,pNum
     & ,2,0,slong(1:lens),'RESTORE',*92)

      do ii = 1, pNum
        string = ' '
        call fMUD_getScalerLabel( fileHandle, ii, string )
        lent = lensig( string )
        if( lent .le. 0 )then
          string(1:1) = ' '
          lent = 1
        end if
        if( lent .gt. nolds )then
          call get_more_space(saddr,nolds,lent,1,*99)
          nolds = lent
        end if
        do i = 1, lent
          idum = ichar(string(i:i))
          if( idum .gt. 127 )idum = 32
          L1D(saddr+i) = idum
        end do
        name = 'SCALERLABEL'
        ln = 11
        call put_text_variable( name(1:ln), ii, 0, idx1
     &   ,saddr, lent, slong(1:lens), *92 )
      end do

      call fMUD_getIndVars( fileHandle, pType, pNum )
      name = 'INDVARTYPE'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,0,dble(pType)
     & ,0,0,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getIndVarLow( fileHandle, ii, R8D(iaddr+ii) )
      end do
      name = 'INDVARLOW'
      ln = 9
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getIndVarHigh( fileHandle, ii, R8D(iaddr+ii) )
      end do
      name = 'INDVARHIGH'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getIndVarMean( fileHandle, ii, R8D(iaddr+ii) )
      end do
      name = 'INDVARMEAN'
      ln = 10
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getIndVarStddev( fileHandle, ii, R8D(iaddr+ii) )
      end do
      name = 'INDVARSTDDEV'
      ln = 12
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      call get_temp_space( iaddr, pNum, 8, *99 )
      do ii = 1, pNum
        call fMUD_getIndVarSkewness( fileHandle, ii, R8D(iaddr+ii) )
      end do
      name = 'INDVARSKEWNESS'
      ln = 14
      call put_variable(name(1:ln),0,idx1,idx2,idx3,1,0.0d0,iaddr,pNum
     & ,0,0,slong(1:lens),'RESTORE',*92)

      do ii = 1, pNum
        string = ' '
        call fMUD_getIndVarName( fileHandle, ii, string )
        lent = lensig( string )
        if( lent .le. 0 )then
          string(1:1) = ' '
          lent = 1
        end if
        if( lent .gt. nolds )then
          call get_more_space(saddr,nolds,lent,1,*99)
          nolds = lent
        end if
        do i = 1, lent
          idum = ichar(string(i:i))
          if( idum .gt. 127 )idum = 32
          L1D(saddr+i) = idum
        end do
        name = 'INDVARNAME'
        ln = 10
        call put_text_variable( name(1:ln), ii, 0, idx1
     &   ,saddr, lent, slong(1:lens), *92 )
      end do

      do ii = 1, pNum
        string = ' '
        call fMUD_getIndVarDescription( fileHandle, ii, string )
        lent = lensig( string )
        if( lent .le. 0 )then
          string(1:1) = ' '
          lent = 1
        end if
        if( lent .gt. nolds )then
          call get_more_space(saddr,nolds,lent,1,*99)
          nolds = lent
        end if
        do i = 1, lent
          idum = ichar(string(i:i))
          if( idum .gt. 127 )idum = 32
          L1D(saddr+i) = idum
        end do
        name = 'INDVARDESCRIPTION'
        ln = 17
        call put_text_variable( name(1:ln), ii, 0, idx1
     &   ,saddr, lent, slong(1:lens), *92 )
      end do

      do ii = 1, pNum
        string = ' '
        call fMUD_getIndVarUnits( fileHandle, ii, string )
        lent = lensig( string )
        if( lent .le. 0 )then
          string(1:1) = ' '
          lent = 1
        end if
        if( lent .gt. nolds )then
          call get_more_space(saddr,nolds,lent,1,*99)
          nolds = lent
        end if
        do i = 1, lent
          idum = ichar(string(i:i))
          if( idum .gt. 127 )idum = 32
          L1D(saddr+i) = idum
        end do
        name = 'INDVARUNITS'
        ln = 11
        call put_text_variable( name(1:ln), ii, 0, idx1
     &   ,saddr, lent, slong(1:lens), *92 )
      end do

      call delete_temp_space
      return

C   errors

   92 call delete_temp_space
      return 1
   99 call error_message('RESTORE'//BS//'MUD'
     & ,'allocating dynamic array space')
      call delete_temp_space
      return 1
      end
