      INTEGER*4 FUNCTION EXTYPE( ISTR )

C   returns +1 if character, -1 if numeric

      INTEGER*4 LENVSM, MXEXPR, MXFUN, MXCODE, MXOPER, MXSTR
      PARAMETER (LENVSM =  32, MXEXPR = 3000, MXOPER = 35, MXSTR = 250)
      PARAMETER (MXFUN  = 222, MXCODE = 2200)

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      INTEGER*2 NCODE(MXSTR),ICODE(4,MXCODE,MXSTR),FIRSTC(MXCODE,MXSTR)
      LOGICAL*1 STRCOD(MXSTR), VALID(MXCODE,MXSTR)
      COMMON /EXCODS/ NCODE, ICODE, FIRSTC, STRCOD, VALID

C  FUNCTN  array of NFUN function names
C  NARGUM  specifies the allowed number of arguments for each function,
C          where NARGUM(1,K) = minimum number of arguments allowed
C          where NARGUM(2,K) = maximum number of arguments allowed,
C          normally, NARGUM(1,K) = NARGUM(2,K), with 0 allowed
C  IFNTYP  specifies function type
C          0 is numeric to numeric scalar function
C          1 is numeric to numeric vector function
C          2 is mixed mode to numeric function
C          3 is mixed mode to character function
C          4 is character to character function
C          5 is EXPAND function

      CHARACTER*(LENVSM) FUNCTN(MXFUN)
      INTEGER*4          NFUN, NARGUM(2,MXFUN), IFNTYP(MXFUN)
     &                  ,ARGTYP(20,MXFUN)
      COMMON /EXPFUN/ NFUN,NARGUM,IFNTYP,ARGTYP,FUNCTN

C   OPERAT  table of NOPER mathematical and logical operators
C   IPRIOR  table of NOPER*2 operator priorities which give the
C           precedence of the operator, the type of the operator
C           (unary or binary), and the associativity of the operator
C           (left or right)
C   IOPTYP  table of operator types,
C           0 is scalar operator, 1 is vector operator, 2 is character

      CHARACTER*(LENVSM) OPERAT(MXOPER)
      INTEGER*4   NOPER, IPRIOR(2,MXOPER), IOPTYP(MXOPER)
      COMMON /EXPOPR/ NOPER, IPRIOR, IOPTYP, OPERAT

C  error message variables

      CHARACTER*(MXEXPR) EXPBUF, ERRBUF
      CHARACTER*80       ERRMES
      INTEGER*4          LEXPBUF, ERRPTR 
      COMMON /EXPERR/ ERRPTR, LEXPBUF, ERRMES, EXPBUF, ERRBUF

C  Token codes: set by EXCODE routine and stored in ICODE(2,,) 
C
C  1 = function
C  2 = PHYSICA numeric variable
C  3 = numeric constant
C  4 = operator
C  5 = special character
C  6 = $NOP  (no-operation)
C  7 = intrinsic function
C  8 = work space variable
C  9 = imediate value
C 10 = range vector
C 11 = list vector
C 12 = PHYSICA text variable
C 13 = quote string
C 14 = substring (as yet unknown type)
C 15 = numeric subvariable workspaces (after exvalx)
C 16 = character subvariable workspaces (after exvalx)
C
C        1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17
C unkn.  2   3   2   3   2   4  -1   2   1   1  -1   2   2   2   2   2   3
C num.   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
C unkn. -1  -1  -1  -1  -1   4   3   2   5  -1  -1   2   2   2   2   2  -1
C char.  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
C first( 7   7   7   7   7   7   7   7   6   7  -1   7   7   7   7   7   7
C      ( 7   7   7   7   7   7   7   7   6   7   8   7   7   7   7   7   7
C inside 7   7   7   7   7   7   7   7   6   7   8   7   7   7   7   7   7
C      ) 7   7   7   7   7   7   7   7   6   7   8   7   7   7   7   7   7

      INTEGER*4 ISTATE, TABLE(17,8), CLASS
      DATA TABLE
     & / 2,4,2,  3,  2,  4,  1,  2,  1,  1,  1,  2,  2,  2,  2,  2,  4,
     &   0,0,0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     &   3,3,3,  3,  3,  4,  3,  2,  5,  3,  3,  2,  2,  2,  2,  2,  3,
     &   0,0,0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     &   7,7,7,  7,  7,  7,  7,  7,  6,  7,  8,  7,  7,  7,  7,  7,  7,
     &   7,7,7,  7,  7,  7,  7,  7,  6,  7,  8,  7,  7,  7,  7,  7,  7,
     &   7,7,7,  7,  7,  7,  7,  7,  6,  7,  8,  7,  7,  7,  7,  7,  7,
     &   7,7,7,  7,  7,  7,  7,  7,  6,  7,  8,  7,  7,  7,  7,  7,  7 /
CCC
      ISTATE = 1
      DO I = 1, NCODE(ISTR)
        IF( ICODE(2,I,ISTR) .EQ. 1 )THEN        ! function
          IF( IFNTYP(ICODE(3,I,ISTR)) .LE. 2 )THEN   ! numeric
            CLASS = 1
          ELSE                                       ! character
            CLASS = 2
          END IF
        ELSE IF( ICODE(2,I,ISTR) .EQ. 2 )THEN   ! numeric variable
          CLASS = 3
        ELSE IF( ICODE(2,I,ISTR) .EQ. 12 )THEN  ! character variable
          CLASS = 4
        ELSE IF( ICODE(2,I,ISTR) .EQ. 3 )THEN   ! numeric constant
          CLASS = 5
        ELSE IF( ICODE(2,I,ISTR) .EQ. 13 )THEN  ! quote string
          CLASS = 6
        ELSE IF( ICODE(2,I,ISTR) .EQ. 4 )THEN   ! operator
          IF( IOPTYP(ICODE(3,I,ISTR)) .EQ. 2 )THEN   ! //
            CLASS = 7
          ELSE                                       ! other
            CLASS = 8
          END IF
        ELSE IF( ICODE(2,I,ISTR) .EQ. 5 )THEN   ! special character
          IF( ICODE(3,I,ISTR) .EQ. 5 )THEN           ! ({[
            CLASS = 9
          ELSE IF( ICODE(3,I,ISTR) .EQ. 4 )THEN      ! )}]
            CLASS = 11
          ELSE                                       ! other
            CLASS = 10
          END IF
        ELSE IF( ICODE(2,I,ISTR) .EQ. 8 )THEN   ! workspace variable
          CLASS = 12
        ELSE IF( ICODE(2,I,ISTR) .EQ. 9 )THEN   ! immediate value
          CLASS = 13
        ELSE IF( ICODE(2,I,ISTR) .EQ. 10 )THEN  ! range vector
          CLASS = 14
        ELSE IF( ICODE(2,I,ISTR) .EQ. 11 )THEN  ! list vector
          CLASS = 15
        ELSE IF( ICODE(2,I,ISTR) .EQ. 15 )THEN  ! numeric subvariable
          CLASS = 16
        ELSE IF( ICODE(2,I,ISTR) .EQ. 16 )THEN  ! character subvariable
          CLASS = 17
        END IF
        ISTATE = TABLE(CLASS,ISTATE)
        IF( ISTATE .EQ. 2 )THEN       ! must be numeric, stop
          EXTYPE = -1
          RETURN
        ELSE IF( ISTATE .EQ. 4 )THEN  ! must be character, stop
          EXTYPE = +1
          RETURN
        ELSE IF( ISTATE .EQ. 5 )THEN  ! opening bracket
          IOPEN = 1
        ELSE IF( ISTATE .EQ. 6 )THEN  ! opening bracket
          IOPEN = IOPEN+1
        ELSE IF( ISTATE .EQ. 8 )THEN  ! closing bracket
          IOPEN = IOPEN-1
          IF( IOPEN .EQ. 0 )ISTATE = 3
        END IF
      END DO
      EXTYPE = +2    ! ambiguous, probably character
      RETURN
      END
