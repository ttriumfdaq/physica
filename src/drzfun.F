      SUBROUTINE DRZFUN( NR, C, INDX, OUTPUT, * )

      IMPLICIT REAL*8 (A-H,O-Z) 
      PARAMETER (IT=60, E1=1.0D-7, E2=1.0D-20, E3=1.0D-20, ETA=1.0D-4)

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,MXEXPR, NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (MXEXPR=3000)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      INTEGER*4 VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &         ,TSCAL, NSCAL, ASCAL, ESCAL

C   VARALC = number of bytes allocated for numeric variable
C            characteristics (does not include the data)
C            VARALC should be a multiple of 512 
C   VARADR = starting address of the numeric variable table
C   VARLEN = # of bytes needed for an entry in the table
C   VARNUM = # of numeric variables currently defined
C   VARMAX = # of variables (including scalars) for which space is allocated 
C
C   TSCAL  = # of scalars for which space is allocated 
C   NSCAL  = # of current scalars
C   ASCAL  = starting address for scalar data
C   ESCAL  = starting address of the scalars existence list
C
C   Allocate the space as bytes. Let BASE = (VARADR+(I-1)*VARLEN)/4, for
C   1 <= I <= VARNUM.  The last byte used for I'th variable at 4*BASE+VARLEN
C   and the start of the next variable is at 4*BASE+VARLEN+1
C
C   I4$(BASE+1)   = length of the I'th variable name
C                   always allocate 32 (4*8) bytes for every name
C                   name is in L1$((BASE+2)*4+j) for j = 1, length
C   I4$(BASE+10)  = -1 if scalar is a dummy index
C                    0 if normal scalar
C                   +1 if scalar is allowed to vary in fit
C   I4$(BASE+11)  = number of dimensions: 0=scalar, 1=vector, 2=matrix, 3=tensor
C   I4$(BASE+12)  = starting address of data
C   I4$(BASE+13)  = number of elements if a vector
C                   or number of rows if a matrix (1st dimension)
C   I4$(BASE+14)  = number of columns (2nd dimension)
C   I4$(BASE+15)  = number of planes (3rd dimension)
C   I4$(BASE+16)  = initial index of 1st dimension
C   I4$(BASE+17)  = initial index of 2nd dimension
C   I4$(BASE+18)  = initial index of 3rd dimension
C   I4$(BASE+19)  = final index of 1st dimension
C   I4$(BASE+20)  = final index of 2nd dimension
C   I4$(BASE+21)  = final index of 3rd dimension
C   I4$(BASE+22)  = number of lines of history for variable I
C   I4$(BASE+23)  = total allocation for history for variable I
C   I4$(BASE+24)  = address for history line lengths
C   I4$(BASE+25)  = address for history line characters
C    for history line 1:
C       length = I4$(I4$(BASE+24)+1) 
C       line = L1$(I4$(BASE+25)+j) for j = 1 --> I4$(I4$(BASE+24)+1) 
C    for history line k > 1:
C       length = I4$(I4$(BASE+24)+k) 
C         prev_len = sum(I4$(I4$(BASE+24)+j),j,1:k-1)
C       line = L1$(I4$(BASE+25)+prev_len+j) for j = 1 --> I4$(I4$(BASE+24)+k)

      COMMON /NUMERIC_VARIABLES/ VARALC, VARADR, VARLEN, VARNUM, VARMAX
     &                          ,TSCAL, NSCAL, ASCAL, ESCAL

C  This routine finds the real zeros of an arbitrary function (f(x))
C  using Muller's method. Each iteration determines a zero of a
C  quadratic passing through last 3 function values.
C
C  NR - number of roots required
C  IT - maximum number of iterations
C   C - array, size nr, containing roots. on entry, if C(i)=0
C       starting values are -1,1,0 respectively, if C(i)=b
C       starting values are .9b,1.1b,b
C  E1,E2 - convergence criteria for (a) relative change in estimates 
C          for root, (b) absolute change in function values
C  E3,ETA - ETA is spread for multiple roots. If |root-C(i)|<E3, C(i) 
C           is a previously found root, ROOT is replaced by ROOT+ETA 

      REAL*8        C(1)
      INTEGER*4     NR, INDX
      LOGICAL*4     OUTPUT

      LOGICAL CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

C  EXVALX error message variables

      CHARACTER*(MXEXPR) EXPBUF, ERRBUF
      CHARACTER*80       ERRMES
      INTEGER*4          LEXPBUF, ERRPTR
      COMMON /EXPERR/ ERRPTR, LEXPBUF, ERRMES, EXPBUF, ERRBUF

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      CHARACTER*64 STRING
      INTEGER*4    BASE, ordered
CCC
      IF( TRAP )CALL LOK_E_TRAP( *116, ISTAT )

      BASE = (VARADR+(INDX-1)*VARLEN)/4

      L = 0
 1000 L = L+1
        IF( L .GT. NR )GO TO 141
        IF( CTRLC_CALLED )GO TO 141
        JK = 0 

C  Get starting values and corresponding function values 

        IF( C(L) .EQ. 0.D0 )THEN
          P  = -1.D0 
          P1 =  1.D0 
          P2 =  0.D0 
        ELSE
          P2 = C(L) 
          P  = 0.9D0*P2 
          P1 = 1.1D0*P2 
        END IF
        RT = P 
        GO TO 9 
    3   RT = P1 
        X0 = FPRT 
        GO TO 9 
    4   RT = P2 
        X1 = FPRT 
        GO TO 9 
    5   X2 = FPRT 
        H = -1.D0 
        IF( P2 .NE. 0.D0 )H = -0.1D0*P2 
        D = -0.5D0 

C  calculate new estimate for root 

    6   DD = 1.D0+D 
        BI = X0*D*D-X1*DD*DD+X2*(D+DD) 
        DEN = BI*BI-4.D0*X2*D*DD*(X0*D-X1*DD+X2) 
        IF( DEN .LE. 0.D0 )THEN
          DN = BI 
        ELSE
          DEN = SQRT(DEN) 
          DN = BI+DEN 
          DM = BI-DEN 
          IF( ABS(DM) .GT. ABS(DN) )DN = DM 
        END IF
        IF( DN .EQ. 0.D0 )DN = 1.D0 
        D = -2.D0*X2*DD/DN 
        H = D*H 
        RT = RT+H 

C  check relative convergence 

        IF( ABS(H/RT) .LT. E1 )GO TO 14 
        IF( (ABS(RT).LT.E1) .AND. (ABS(H).LT.E1) )GO TO 14
    9   JK = JK+1 
        IF( JK .GE. IT )GO TO 16 
        R8D(ASCAL+I4D(BASE+12)) = RT
        CALL EXCALX( FRT, IDUM, NDOUT, IDIM1, IDIM2, IDIM3, ordered
     &   ,*991 )
        FPRT = FRT 

C  check spread for multiple roots and deflate function 

        IF( L .EQ. 1 )GO TO 11 
        DO I = 2, L 
          TEM = RT-C(I-1) 
          IF( ABS(TEM) .LT. E3 )GO TO 13 
          FPRT = FPRT/TEM 
        END DO

C  check absolute convergence on function values 

   11   AFT = ABS(FPRT) 
        IF( (ABS(FRT).LT.E2) .AND. (AFT.LT.E2) )GO TO 14 
        GO TO (3,4,5), JK 

C  halve D if too great a difference in ratio of f(x(jk-1)) 
C  and f(x(jk)) where x(jk) is current estimate of root. 

        IF( AFT .GT. ABS(X2*10.D0) )GO TO 12 
        X0 = X1 
        X1 = X2 
        X2 = FPRT 
        GO TO 6 
   12   D = D*0.5D0 
        H = H*0.5D0 
        RT = RT-H 
        GO TO 9 
   13   RT = RT+ETA 
        JK = JK-1 
        GO TO 9 
   14   C(L) = RT 
        IF( OUTPUT )THEN
          WRITE(STRING,140)L,C(L),JK
  140     FORMAT('Root#',I4,' =',1PD12.4,' after ',I4,' iterations')
          CALL RITE( STRING(1:45) )
        END IF
      GO TO 1000
  141 IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 
   16 NR = L-1
      IF( L .EQ. 1 )THEN
        WRITE(STRING,150)IT
  150   FORMAT('1st root did not converge in',I5,' iterations')
        CALL WARNING_MESSAGE('FZERO',STRING(1:44))
      ELSE IF( L .EQ. 2 )THEN
        WRITE(STRING,151)IT
  151   FORMAT('2nd root did not converge in',I5,' iterations')
        CALL WARNING_MESSAGE('FZERO',STRING(1:44))
      ELSE IF( L .EQ. 3 )THEN
        WRITE(STRING,152)IT
  152   FORMAT('3rd root did not converge in',I5,' iterations')
        CALL WARNING_MESSAGE('FZERO',STRING(1:44))
      ELSE
        WRITE(STRING,153)L,IT
  153   FORMAT(I5,'th root did not converge in',I5,' iterations')
        CALL WARNING_MESSAGE('FZERO',STRING(1:48))
      END IF
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN
  116 NR = L-1
      C(L) = RT
      IF( L .EQ. 1 )THEN
        CALL ERROR_MESSAGE('FZERO','1st root yielded arithmetic error')
      ELSE IF( L .EQ. 2 )THEN
        CALL ERROR_MESSAGE('FZERO','2nd root yielded arithmetic error')
      ELSE IF( L .EQ. 3 )THEN
        CALL ERROR_MESSAGE('FZERO','3rd root yielded arithmetic error')
      ELSE 
        WRITE(STRING,163)L
  163   FORMAT(I5,'th root yielded arithmetic error')
        CALL ERROR_MESSAGE('FZERO',STRING(1:37))
      END IF
      CALL PUT_SYSMSG( ISTAT )
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN
  991 CALL ERROR_MESSAGE('FZERO'
     & ,ERRMES(1:LENSIG(ERRMES))//': routine DRZFUN')
      IF( TRAP )CALL LOK_E_UNTRAP
      RETURN 1
      END 
