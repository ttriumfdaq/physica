      SUBROUTINE PLOT3D( IVXYZ, XDATA, YDATA, ZDATA, XSCALE,
     &   YSCALE, ZSCALE, NLINE, NPNTS, PHI, THETA, XREF,
     &   YREF, XLENTH, MASK, VERTEX )
C                                                05/AUG/1980
C                                                C.J. KOST SIN
C Masked 3-dimensional plot program with rotations this routine will
C accept 3-dimensional data in various forms as input, rotate it in
C 3-space to any angle, and plot the projection of the resulting
C figure onto the xy plane.  Linear interpolation is used between data
C points.  Those lines of a figure which should be hidden by a previous
C line are masked. The masking technique used by this routine is based
C on two premises -
C  lines in the foreground (positive z direction) are plotted before
C    lines in the background.
C  a line or portion of a line is masked (hidden) if it lies within
C    the region bounded by previously plotted lines.
C Each call to plot3d causes one line of a figure to be plotted.
C Two parameters of the plotter are set on the initial call for each
C figure -
C  PIPI is the number of plotter increments per inch.
C  NYPI is the number of increments available across the width of the
C   page (y-direction).
C When a new figure is initiated, the plotter origin is set at the
C bottom of the paper by plot3d and should not be moved until the
C figure is completed.
C Input parameters -
C  IVXYZ is a four digit decimal integer which is used to select
C   various input/output options.  These digits, in decreasing order of
C   magnitude, will be referred to as v, x, y, and z.
C   If v .ne. 0, the vertices of the current figure and their
C   projection onto the y=0 plane, will be stored in a 16 entry real
C   array, VERTEX, and will be updated as each line is plotted.  These
C   coordinates are in inches and relative to the current plotter
C   origin.  The x y pairs are ordered so that the first pair
C   corresponds to the first point of the figure, the second pair
C   corresponds to the last point of the first line, and the following
C   pairs are ordered in a circular fashion.  The pairs on the y=0
C   plane of the figure, then follow in the same order. If v=0, the
C   vertex parameter is ignored, but should not be deleted.
C   If x=0, the x-components of this line are assumed to be equally
C   spaced, and are computed by x(i)=xdata+(i-1)*xscale where XDATA is
C   the initial value in inches and XSCALE is the spacing between
C   points in inches.  if x .ne. 0, the x-components of this line are
C   read from an array and modified by x(i)=xdata(i)*xscale where
C   XSCALE is a scale factor. The same relations hold for the
C   y-components, that is, if y=0  y(i)=ydata+(i-1)*yscale and if
C   y .ne. 0  y(i)=ydata(i)*yscale  If z=0, the z-components of this
C   line are all assumed to be equal, and are computed by
C   z(i)=zdata+(nline-1)*zscale  where NLINE is some integer associated
C   with this line.  If z .ne. 0, again we have z(i)=zdata(i)*zscale
C   when (nline) is equal to one, it indicates the beginning of a new
C   figure.  A call to plot3d with NLINE equal to zero before
C   initiating a new figure simulates a line drawn at the bottom of
C   the page.  Therefore only those portions of a line lying above all
C   previuos lines will be plotted. All other parameters are ignored
C   on such a call.
C  NPNTS is the number of points on this line, and may be altered from
C   line to line.
C PHI and THETA are the two angles (in degrees) used to specify the
C  desired 3-dimensional rotation.  The following two definitions of
C  these rotations are equivalent - in terms of rotations of axes, the
C  initial system of axes, xyz, is rotated by an angle PHI
C  counterclockwise about the y-axis, and the resultant system is
C  labelled the tuv axes.  The tuv axes are then rotated by an angle
C  THETA counterclockwise about the t-axis, and this final system
C  is labelled the pqr axes.  The plotted figure is the projection of
C  the original figure onto the pq-plane. In terms of rotation of
C  coordinates, the figure is first rotated by an angle THETA
C  clockwise about the x-axis. The resultant figure is then rotated by
C  an angle PHI clockwise about its y-axis.  The plotted figure is the
C  projection of this final figure onto the xy-plane.
C  WARNING. some rotations will alter the foreground/background
C  relationships between the lines, and thus the order in which they
C  should be plotted.
C XREF and YREF are the coordinates, in inches, relative to the
C  plotter origin, to be used as the origin of the figure.
C XLENTH is the length, in inches, to which the plot is restricted.
C  Any point which exceeds this limit, or the limits of the paper in
C  the y-direction NYPI, will be set to that limit.
C MASK is an integer array of 2*XLENTH*PIPI entries which is used to
C  store the mask.  The contents of this array should not be altered
C  during the plotting of any given figure.
C All parameters except MASK and VERTEX are returned unchanged.
C Between any two calls for the same figure, any parameter can be
C meaningfully changed except XLENTH, MASK, and VERTEX.
CCC
      IMPLICIT REAL*8 (A-H,O-Z)

      REAL*8    XDATA(1), YDATA(1), ZDATA(1), VERTEX(1)
      REAL*8    XSCALE, YSCALE, ZSCALE, PHI, THETA, XREF, YREF, XLENTH
      INTEGER*4 MASK(1), IVXYZ, NLINE, NPNTS

      REAL*8    A11, A13, A21, A23, A22, PIPI
      INTEGER*4 INDX, INDY, INDZ, NYPI, LIMITX
      COMMON /PL3D/ A11, A13, A21, A23, A22, PIPI
     &             ,INDX, INDY, INDZ, NYPI, LIMITX

      REAL*8 SPHI, STHETA
      COMMON /PL3DB/ SPHI, STHETA

      REAL*8    ARMIN, ARMAX, YDATANEW
      LOGICAL*4 FLAT, FIRST
      COMMON /PL3DC/ ARMIN, ARMAX, YDATANEW, FIRST, FLAT

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      INTEGER*4 JXOLD, JYOLD, IPNOLD
      COMMON /JXPLT/ JXOLD, JYOLD, IPNOLD

      INTEGER*4 HIGH, OLDHI, OLDLOW

      DATA INIT /-1/, JVXYZ /-1/, SPHI /-1.0D38/, STHETA /-1.0D38/
CCC
C   Initialization procedure for a new figure
C   Test for special mask modifying call
      IF (NLINE.EQ.0) GO TO 550
C   Determine if initialization is required
      IF (NLINE.NE.1) GO TO 20
C   Set plotter parameters
      PIPI = 400.0D0
      NYPI = 4360
C   Compute length of plot page in increments
      LIMITX = XLENTH*PIPI + 0.5D0
      I = LIMITX + LIMITX
C   Initialize masking array over the length of the plot page
      DO K = 1, I
        MASK(K) = INIT
      END DO
      INIT = -1
C Set the necessary indicators for the first line of a new figure
      INCI = -1
      I = 0
C Input type and vertex initialization
C Determine if initialization is required
   20      IF (JVXYZ.EQ.IVXYZ) GO TO 70
C Set indicators for types of input data and saving vertices
      JVXYZ = IVXYZ
      INDZ = 1
      INDY = 1
      INDX = 1
      INDV = 1
      IF (JVXYZ.LT.1000) GO TO 30
      INDV = 2
      JVXYZ = JVXYZ - 1000
   30      IF (JVXYZ.LT.100) GO TO 40
      INDX = 2
      JVXYZ = JVXYZ - 100
   40      IF (JVXYZ.LT.10) GO TO 50
      INDY = 2
      JVXYZ = JVXYZ - 10
   50      IF (JVXYZ.LT.1) GO TO 60
      INDZ = 2
   60      JVXYZ = IVXYZ
C Rotation initialization
C Determine if initialization is required
   70      IF (PHI.EQ.SPHI .AND. THETA.EQ.STHETA) GO TO 80
C Compute rotation factors
      SPHI = SIN(0.0174532925D0*PHI)
      CPHI = COS(0.0174532925D0*PHI)
      STHETA = SIN(0.0174532925D0*THETA)
      CTHETA = COS(0.0174532925D0*THETA)
      A11 = CPHI
      A13 = -SPHI
      A21 = STHETA*SPHI
      A22 = CTHETA
      A23 = STHETA*CPHI
      SPHI = PHI
      STHETA = THETA
C Processing procedures
C Set flag to move through the data arrays in the opposite direction
   80      INCI = -INCI
C Set indicator to the first point to be processed
      IF (I.NE.0) I = NPNTS + 1
C Loop to process each point in the data arrays
      DO 530 K=1,NPNTS
        IF( CTRLC_CALLED )RETURN
C Data calculation
      I = I + INCI
      GO TO (90,100), INDX
   90      X = XDATA(1) + (I-1)*XSCALE
      GO TO 110
  100      X = XDATA(I)*XSCALE
  110      GO TO (120,130), INDY
  120      Y = YDATA(1) + (I-1)*YSCALE
      GO TO 140
  130      Y = YDATA(I)*YSCALE
140        YDATANEW = YDATA(I)
           GO TO (150,160), INDZ
  150      Z = ZDATA(1) + (NLINE-1)*ZSCALE
      GO TO 170
  160      Z = ZDATA(I)*ZSCALE
C Data rotation
  170      XXX = A11*X + A13*Z + XREF
      XX = XXX
      IX = XX*PIPI + 0.5D0
      YYY = A21*X + A23*Z + YREF
      YY = YYY + A22*Y
      IY = YY*PIPI + 0.5D0
      IF (K.NE.1) GO TO 250
C (LOC) is the position of the previous point with respect to the mask
C     +1  ABOVE THE MASK
C      0  WITHIN THE LIMITS OF THE MASK
C     -1  BELOW THE MASK
C Procedure for initial point of each line
C Locate initial point with respect to the mask then update the mask
      LOW = IX + IX
      HIGH = LOW - 1
      MLOW = MASK(LOW)
      MHIGH = MASK(HIGH)
      IF (MHIGH-IY) 200, 210, 180
  180      IF (MLOW-IY) 190, 230, 220
  190      LOCOLD = 0
      GO TO 240
  200      MASK(HIGH) = IY
      IF (MLOW.EQ.-1) MASK(LOW) = IY
  210      LOCOLD = +1
      GO TO 240
  220      MASK(LOW) = IY
  230      LOCOLD = -1
C Move the raised pen to this initial point
  240      CALL IPLOT(IX, IY, 3)
      JX = IX
      JY = IY
      IYREF = IY
      JXOLD=JX
      JYOLD=JY
      IPNOLD=2
      IF(LOCOLD.EQ.0) IPNOLD=3
C Store vertices if requested
      IF (INDV.EQ.1) GO TO 530
      INDEX = INCI + 6
      VERTEX(INDEX) = XX
      VERTEX(INDEX+1) = YY
      VERTEX(INDEX+8) = XXX
      VERTEX(INDEX+9) = YYY
      IF (NLINE.NE.1) GO TO 530
      VERTEX(1) = XX
      VERTEX(2) = YY
      VERTEX(9) = XXX
      VERTEX(10) = YYY
      GO TO 530
C Special case where change in x coordinate is zero
C A special provision is made at this point so that a line
C will not mask itself as long as the x coordinate remains constant
  250      IF (IX.NE.JX) GO TO 260
      JY = IY
      GO TO 280
C Compute constants for linear interpolation
  260      YINC = FLOAT(IY-JY)/ABS(FLOAT(IX-JX))
      INCX = (IX-JX)/IABS(IX-JX)
      YJ = JY
C Perform linear interpolation at each incremental step on the x axis
  270      JX = JX + INCX
      YJ = YJ + YINC
      JY = YJ + 0.5D0
C Locate the current point with respect to the mask at that
C point then plot the increment as a function of the
C location of the previous point with respect to its mask
      LOW = JX + JX
      HIGH = LOW -1
      MLOW = MASK(LOW)
      MHIGH = MASK(HIGH)
  280      IF (MHIGH-JY) 300, 300, 290
  290      IF (MLOW-JY) 310, 320, 320
C The current point is above the mask
  300      LOC = +1
      IF (LOCOLD) 360, 370, 430
C The current point is within the mask
  310      LOC = 0
      IF (LOCOLD) 340, 350, 330
C The current point is below the mask
  320      LOC = -1
      IF (LOCOLD) 510, 450, 440
C Plot from above the mask to within the mask
  330      IF (MHIGH.LE.IYREF) CALL MY_JPLOT(JX, MHIGH, 2)
      GO TO 350
C Plot from below the mask to within the mask
  340      IF (MLOW.GE.IYREF) CALL MY_JPLOT(JX, MLOW, 2)
C Plot from within the mask to within the mask
  350      CALL MY_JPLOT(JX, JY, 3)
      GO TO 520
C Plot from below the mask to above the mask
  360      IF (MLOW-IYREF) 370, 380, 380
C Plot from within the mask to above the mask
  370      IF (MHIGH-IYREF) 400, 390, 390
  380      CALL MY_JPLOT(JX, MLOW, 2)
  390      CALL MY_JPLOT(JX, MHIGH, 3)
      GO TO 430
  400      IF (MHIGH.EQ.-1) GO TO 430
      OLDHI = HIGH - 2*INCX
      IF (MASK(OLDHI)-JY) 420, 420, 410
  410      CALL MY_JPLOT(JX, JY, 3)
      GO TO 430
  420      CALL MY_JPLOT(JX-INCX, MASK(OLDHI), 3)
C Plot from above the mask to above the mask
  430      MASK(HIGH) = JY
      IF (MLOW.EQ.-1) MASK(LOW) = JY
      CALL MY_JPLOT(JX, JY, 2)
      GO TO 520
C Plot from above the mask to below the mask
  440      IF (MHIGH-IYREF) 460, 460, 450
C Plot from within the mask to below the mask
  450      IF (MLOW-IYREF) 470, 470, 480
  460      CALL MY_JPLOT(JX, MHIGH, 2)
  470      CALL MY_JPLOT(JX, MLOW, 3)
      GO TO 510
  480      OLDLOW = LOW - 2*INCX
      IF (MASK(OLDLOW)-JY) 490, 500, 500
  490      CALL MY_JPLOT(JX, JY, 3)
      GO TO 510
  500      CALL MY_JPLOT(JX-INCX, MASK(OLDLOW), 3)
C Plot from below the mask to below the mask
  510      MASK(LOW) = JY
      CALL MY_JPLOT(JX, JY, 2)
  520      IYREF = JY
      LOCOLD = LOC
      IF (JX.NE.IX) GO TO 270
      ISV=IPNOLD
      CALL MY_JPLOT(JX, JY, -1)
      IPNOLD=ISV
  530      CONTINUE
C Raise pen
      CALL IPLOT(JX, JY, 3)
C Store vertices if requested
      IF (INDV.EQ.1) GO TO 540
      INDEX = -INCI + 6
      VERTEX(INDEX) = XX
      VERTEX(INDEX+1) = YY
      VERTEX(INDEX+8) = XXX
      VERTEX(INDEX+9) = YYY
      IF (NLINE.NE.1) GO TO 540
      VERTEX(3) = XX
      VERTEX(4) = YY
      VERTEX(11) = XXX
      VERTEX(12)= YYY
  540      I = I - 1
      RETURN
C Option to modify the masking technique to be used on the
C following figure so as to plot only above all previous lines.
  550      INIT = 0
      RETURN
      END
