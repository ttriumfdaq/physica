      SUBROUTINE PHYSICA_GPLOT( INITX, INITXE1, INITXE2
     &                          ,INITY, INITYE1, INITYE2
     &                          ,NPT, IAXIS, CHAR, SIZPT, COLPT, ANGPT )

      REAL*4    MASK, SIZPT(1), ANGPT(1)
      INTEGER*4 INITX, INITXE1, INITXE2, INITY, INITYE1, INITYE2
     &         ,NPT, IAXIS
      LOGICAL*1 CHAR(1), COLPT(1)

      LOGICAL   FIRST

      COMMON /PLOT_COLOURS/     ICOLR1, ICOLR2
      COMMON /GPLOT_R_FIRST/    FIRST
      COMMON /PLOT_INPUT_UNIT/  IINS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      COMMON /LONG_INCS/ NXS, NYS
CCC
      NXS = GETNAM('NLXINC')   ! # of major x increments
C
C  NXS < 0      -->  drop first and last numbers on the x axis
C  NXS = -1000  -->  determine NLXINC & drop first & last numbers 
C
      NLXINC = NXS
      IF( NLXINC .EQ. -1000 )NLXINC = 0
      IF( NLXINC .LT.     0 )NLXINC = ABS(NLXINC)
      CALL SETNAM('NLXINC',FLOAT(NLXINC))
C
      NYS = GETNAM('NLYINC')   ! # of major y increments
C
C  NYS < 0      -->  drop first and last numbers on the y axis
C  NYS = -1000  -->  determine NLYINC & drop first & last numbers 
C
      NLYINC = NYS
      IF( NLYINC .EQ. -1000 )NLYINC = 0
      IF( NLYINC .LT.     0 )NLYINC = ABS(NLYINC)
      CALL SETNAM('NLYINC',FLOAT(NLYINC))
C
      IF( NPT .LE. 0 )RETURN
      IPEN_SAVE = ICOLR2
      ICOL_SAVE = ICOLR1
      CALL PHYSICA_GAUTO( INITX, INITY, NPT )
      FIRST = .TRUE.
C
      MASK = GETNAM('MASK')
      PTYPE = GETNAM('PTYPE')
C
C    Go to the appropriate section depending on |IAXIS|
C
C   |IAXIS| = 1 ==> plot axes, then points
C           = 2 ==> plot points only
C           = 3 ==> plot points, then axes
C           = 4 ==> plot axes only
C
      IGO = ABS(IAXIS)
      IF( (IGO .EQ. 0) .OR. (IGO .GT. 4) )THEN
        CALL RITE('*** GPLOT ERROR: IAXIS is invalid')
        RETURN
      ELSE IF( IGO .LE. 2 )THEN
C
C    Plot the axes first ...
C
        IF( IGO .EQ. 1 )CALL GAXIS(*99)
C
C    Plot the data points ...
C
        CALL PHYSICA_GPLT( INITX, INITXE1, INITXE2
     &                     ,INITY, INITYE1, INITYE2
     &                     ,NPT, CHAR, SIZPT, COLPT, ANGPT, *99 )
        IF( MASK .LE. -3. )THEN
          IF( PTYPE .NE. 1. )THEN
            IF( (ICOLR2 .NE. IPEN_SAVE) .OR. (ICOLR1 .NE. ICOL_SAVE) )
     &      CALL PLOT_COLOR(ICOL_SAVE,IPEN_SAVE)
          END IF
        END IF
      ELSE
C
C    Plot the data points first ...
C
        IF( IGO .EQ. 3 )THEN
          CALL PHYSICA_GPLT( INITX, INITXE1, INITXE2
     &                       ,INITY, INITYE1, INITYE2
     &                       ,NPT, CHAR, SIZPT, COLPT, ANGPT, *99 )
          IF( MASK .LE. -3. )THEN
            IF( PTYPE .NE. 1. )THEN
              IF( (ICOLR2 .NE. IPEN_SAVE) .OR. (ICOLR1 .NE. ICOL_SAVE) )
     &        CALL PLOT_COLOR(ICOL_SAVE,IPEN_SAVE)
            END IF
          END IF
        END IF
C
C    Now plot the axes ...
C
        CALL GAXIS(*99)
      END IF
      CALL FLUSH_PLOT
99    IF( NXS .EQ. -1000 )CALL SETNAM('NLXINC',-1000.)
      IF( NYS .EQ. -1000 )CALL SETNAM('NLYINC',-1000.)
      RETURN
      END
