#include <stdio.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
 
void readline_wrapper_( prompt, line, length, lg1, lg2 )
   const char *prompt;
   char *line;
   int *length;
   long int lg1, lg2;
{
  static int first = 1;
  if( first )
  {
    using_history();
    first = 0;
  }
  char *line2 = readline( prompt );
  if( line2 == NULL )
  {
    *length = 0;
    printf("\n");
    return;
  }
  *length = (int)strlen( (const char *)line2 );
  if( *length==0 )
  {
    *length = 0;
    return;
  }
  strcpy( line, (const char *)line2 );
  free( (void*)line2 );
  add_history( line );
  /*printf("c: line=|%s|, length=%d",line,*length);*/
}
 
  
      
