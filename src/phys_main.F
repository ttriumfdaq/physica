      PROGRAM PHYSICA_MAIN

      INTEGER*4 NTOTP, NTOTQ, MXCHAR
      PARAMETER (NTOTP=50, NTOTQ=10, MXCHAR=1024)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      INTEGER*4 IMONITOR, IOUTM
      COMMON /PLOTMONITOR/ IMONITOR, IOUTM

      LOGICAL*4 ACTIVE
      COMMON /TT_ACTIVE/ ACTIVE

      LOGICAL*4 USE_MODE
      INTEGER*4 USE_UNIT
      COMMON /USE_COMMON/ USE_MODE, USE_UNIT

      INTEGER*4 IINS
      COMMON /PLOT_INPUT_UNIT/ IINS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      LOGICAL*4 MXHELP
      CHARACTER*20 MXTOPIC
      COMMON /VHELP_MX/ MXHELP, MXTOPIC

      CHARACTER*1024 JFILE
      INTEGER*4     LNJ
      LOGICAL*1     JOURNAL, JOURNAL_MACRO
      COMMON /JOURNAL/ IJRNL, LNJ, JOURNAL, JOURNAL_MACRO, JFILE

C   local variables

      CHARACTER*1024 LINE
      CHARACTER*80  CLINE, FNAME, NEWSFILE
      CHARACTER*20  TIME, NEWSDATE
      CHARACTER*12  USERNAME, GET_USERNAME
      CHARACTER*11  TERMTYPES(0:10)
      CHARACTER*9   MONTH
      INTEGER*4     NDEVICT(0:10)

#ifdef VMS
      CHARACTER*7   TERM
      CHARACTER*23  CHAR_DATE_TIME
      CHARACTER*256 SYSINP, SYSCOM
      INTEGER*4     ISTAT, LIB$SET_LOGICAL, LIB$SYS_TRNLOG
      INCLUDE '($SSDEF)'
#endif

      DATA REPLACE_TEXT_NAME/.FALSE./, REPLACE_NAME/.FALSE./
      DATA PAUSE_FLAG/.FALSE./, TRAP/.TRUE./, IWIDTH /80/
      DATA NO_PROMPT/.FALSE./, HISTORY_FLAG/.TRUE./
      DATA ECHO/.FALSE./, CONFIRM/.TRUE./
      DATA EXECOMM/.TRUE./, STACK/.FALSE./, STACK1/.FALSE./
      DATA TRANSFLAG/.TRUE./, APPEND/.FALSE./, NUNIT/1/
      DATA NCURVES/0/, BATCH_MODE/.FALSE./, DISPLAY_FILE/.FALSE./
      DATA SAVE_LINE/' '/, SAVE_NLINE/0/, MOTIF_VERSION/.FALSE./
      DATA BITMAP_DEVICENAME
     &  / 'PRINTRONIX','HPLASERJET','HPTHINKJET'
     &   ,'LA100     ','HPPAINTJET' /
      DATA LBIT/10,10,10,5,10/
      DATA DEVICE_SIZE
     &  / 'A:  21.59 x  27.64 cm','B:  27.94 x  43.18 cm'
     &   ,'C:  43.18 x  55.88 cm','D:  55.88 x  86.36 cm'
     &   ,'E:  86.36 x 111.76 cm','A3: 20.32 x  26.67 cm'
     &   ,'A4: 19.73 x  28.42 cm' /
      DATA DEVICE_NAMES
     &  / 'none      ','VT640     ','TK4010    ','unused    '
     &   ,'unused    ','HP        ','CIT467    ','TK4107    '
     &   ,'VT241     ','PT100G    ','unused    ','HOUSTON   '
     &   ,'SEIKO     ','IMAGEN    ','POSTSCRIPT','unused    '
     &   ,'LN03      ','generic   ','X         ','GKS       '
     &   ,'RDGL      ' /
      DATA LDEV / 4,5,6,6, 6,2,6,6, 5,6,6,7, 5,6,10,6, 4,7,1,3, 4 /
      DATA JOURNAL /.FALSE./, JOURNAL_MACRO /.FALSE./
      DATA TERMTYPES
     & / 'NONGRAPHICS'
     &  ,'VT640      ','VT241      ','CIT467     ','TK4010     '
     &  ,'TK4107     ','PT100G     ','SEIKO      ','XWINDOWS   '
     &  ,'GENERIC    ','DECWINDOWS ' /
      DATA NDEVICT / 0,  1, 8, 6, 2,  7, 9,12,18, 17, 18 /
      DATA USE_MODE /.FALSE./, USE_UNIT /0/
CCC
      VERSION      = ' 2.92'
      VERSION_DATE = 'October 23, 2007    ' ! this is the versiondate
      NEWSDATE     = '*** unknown ***     '
      MONTH        = 'October  '
      IINS         = 5
      IOUTS        = 6
#ifdef VMS
      ISTAT = LIB$SET_LOGICAL('FOR$CONVERT001','NATIVE')
      IF( .NOT.ISTAT )CALL PUT_SYSMSG( ISTAT )

C   The above logical definition fixes a problem with the LOAD/CALL commands...
C   The problem is that Fortran RTL module FOR$OPEN should have been included
C   in the objects provided by DEC Fortran V6.1, etc., but was not.  Though
C   the source for this module did not change, it DID need to be recompiled to
C   pick up an updated definition.  If one links against the "old" FOR$OPEN,
C   the state of the CONVERT= keyword is unpredictable. It was this that was
C   causing the differences; the RTL thought that a big-endian CONVERT option
C   had been specified. This only affects images linked /NOSYSSHR on a system
C   earlier than OpenVMS VAX V6.1 where DEC Fortran V6.1 or V6.2 had been
C   installed, but not V6.0.  The fix is to extract module FOR$OPEN from either
C   a VMS V6.1 system or a system from which DEC Fortran V6.0 was installed,
C   then insert that module in STARLET.OLB.  This problem goes away once
C   OpenVMS VAX V6.1 is installed.

      ISTAT = LIB$SYS_TRNLOG('SYS$INPUT',,SYSINP)
      IF( .NOT.ISTAT )CALL PUT_SYSMSG( ISTAT )
      ISTAT = LIB$SYS_TRNLOG('SYS$COMMAND',,SYSCOM)
      IF( .NOT.ISTAT )CALL PUT_SYSMSG( ISTAT )
      SYSFLAG = ( SYSINP(1:LENSIG(SYSINP)) .EQ. 'SYS$COMMAND' )
      BATCH_MODE = IBATCH() .OR. (SYSINP .NE. SYSCOM)

C    log the user name, the terminal, the group, the group number, the date,
C    and the time of any user in the specified logfile

      USERNAME = GET_USERNAME( LL )
      CALL UPRCASE( USERNAME, USERNAME )

cc      IF( INDEX('CHUMA',USERNAME(1:LENSIG(USERNAME))) .EQ. 1 )GO TO 40

      CALL TERMINAL( TERM )
cc      CALL GROUP_MEM( LGROUP, LMEM )
      CALL SYS$GETTIM( BIN_DATE_TIME )
      CALL SYS$ASCTIM( LENTIME, CHAR_DATE_TIME, BIN_DATE_TIME, 0 )

      CALL FIND_UNIT( LUNZ )
      OPEN(UNIT=LUNZ,STATUS='OLD',FILE='PHYSICA$DIR:PHYSICA.LOG'
     & ,ACCESS='APPEND',ERR=20,IOSTAT=IERR)
      WRITE(LUNZ,10,ERR=30,IOSTAT=IERR)USERNAME,TERM
     & ,CHAR_DATE_TIME,VERSION
   10 FORMAT(1X,A,3X,9x,3X,A,3X,A,2X,'V',A5)
cc      WRITE(LUNZ,10,ERR=30,IOSTAT=IERR)USERNAME,LGROUP,LMEM,TERM
cc     & ,CHAR_DATE_TIME,VERSION
cc   10 FORMAT(1X,A,3X,'[',O3.3,',',O3.3,']',3X,A,3X,A,2X,'V',A5)
      CLOSE(UNIT=LUNZ)
      GO TO 40
   20 CALL PUT_FORMSG
      WRITE(*,*)' *** WARNING: could not open PHYSICA log file'
      GO TO 40
   30 CALL PUT_FORMSG
      WRITE(*,*)' *** WARNING: could not write to PHYSICA log file'
      CLOSE(UNIT=LUNZ)

C  Unit IINS is assigned to the terminal for interactive input

   40 IF( .NOT.BATCH_MODE )THEN
        OPEN(UNIT=IOUTS,FILE='TT:',STATUS='UNKNOWN',RECL=1024)
        CALL FIND_UNIT(IINS)
        OPEN(UNIT=IINS,FILE='TT:',STATUS='UNKNOWN')
      END IF
      CALL FIND_UNIT(IJRNL)       ! unit # for default journal file
      JFILE = 'PHYSICA.JOURNAL'   ! default journal file name
      LNJ = 15                    ! default journal filename length
      OPEN(UNIT=IJRNL,FILE=JFILE(1:LNJ),STATUS='NEW'
     & ,CARRIAGECONTROL='LIST',RECL=1024,ERR=50)
      JOURNAL = .TRUE.
      GO TO 60
   50 WRITE(*,*)' *** Unable to open journal file: '//JFILE(1:LNJ)
      JOURNAL = .FALSE.
   60 ISTAT = LIB$SYS_TRNLOG('PHYSICA$DIR',,FNAME)
      IF( .NOT.ISTAT .OR. (ISTAT.EQ.SS$_NOTRAN) )FNAME = ' '
      IF( FNAME .EQ. ' ' )GO TO 70
      LENF = LENSIG(FNAME)
      NEWSFILE = FNAME(1:LENF)//'PHYSICA.NEW'
      LENF = LENF+12
      CALL FIND_UNIT(LUNZ)
      OPEN(UNIT=LUNZ,STATUS='OLD',READONLY,SHARED
     &  ,FILE='PHYSICA$DIR:PHYSICA.NEW',ERR=80)
      GO TO 90
   70 WRITE(*,*)' *** Unknown logical name: PHYSICA$DIR'
      CLINE(3:22) = '********************'
      GO TO 120
   80 WRITE(*,*)' *** Unable to open '//NEWSFILE(1:LENF)
      CLINE(3:22) = '********************'
      GO TO 120
#else
      JFILE = 'physica.journal'   ! default journal file name
      LNJ = 15                    ! default journal filename length
      CALL FIND_UNIT(IJRNL)       ! unit # for default journal file
      OPEN(UNIT=IJRNL,FILE=JFILE(1:LNJ),STATUS='UNKNOWN',ERR=50)
      JOURNAL = .TRUE.
      GO TO 60
   50 WRITE(*,*)'*** Unable to open journal file: '//JFILE(1:LNJ)
      JOURNAL = .FALSE.
   60 CALL GETENV('PHYSICA_DIR',FNAME)
      IF( FNAME .EQ. ' ' )GO TO 70
      LENF = LENSIG(FNAME)
      NEWSFILE = FNAME(1:LENF)//'/physica.new'
      LENF = LENF+12
      CALL FIND_UNIT(LUNZ)
      OPEN(UNIT=LUNZ,STATUS='OLD',FILE=NEWSFILE(1:LENF),ERR=80)
      GO TO 90
   70 WRITE(*,*)'*** Unknown environment variable: PHYSICA_DIR'
      CLINE(3:22) = '********************'
      GO TO 120
   80 WRITE(*,*)'*** Unable to open '//NEWSFILE(1:LENF)
      CLINE(3:22) = '********************'
      GO TO 120
#endif
   90 READ(LUNZ,100,END=110,ERR=110)CLINE
      READ(LUNZ,100,END=110,ERR=110)CLINE
      READ(LUNZ,100,END=110,ERR=110)CLINE
      READ(LUNZ,100,END=110,ERR=110)CLINE
      READ(LUNZ,100,END=110,ERR=110)CLINE
  100 FORMAT(A)
  110 CLOSE(UNIT=LUNZ)
  120 NEWSDATE = CLINE(3:22)
#ifdef VMS
      WRITE(IOUTS,121)
  121 FORMAT(' ',80('*'))
      IF( JOURNAL )WRITE(IJRNL,122)
  122 FORMAT(80('*'))
      WRITE(IOUTS,129)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,123)VERSION,MONTH
      IF( JOURNAL )WRITE(IJRNL,124)VERSION,MONTH
  123 FORMAT(' *** PHYSICA',22X,'VERSION #',A5,15X,A9,' 2007 ***')
  124 FORMAT('*** PHYSICA',22X,'VERSION #',A5,15X,A9,' 2007 ***')
      WRITE(IOUTS,125)
      IF( JOURNAL )WRITE(IJRNL,126)
  125 FORMAT(' *** Copyright (C) 1992,...,2007',30X
     & ,'Joseph L. Chuma ***')
  126 FORMAT('*** Copyright (C) 1992,...,2007',30X
     & ,'Joseph L. Chuma ***')
      WRITE(IOUTS,127)
      IF( JOURNAL )WRITE(IJRNL,128)
  127 FORMAT(' ***',27X,'All rights reserved',28X,'***')
  128 FORMAT('***',27X,'All rights reserved',28X,'***')
      WRITE(IOUTS,129)
      IF( JOURNAL )WRITE(IJRNL,130)
  129 FORMAT(' ***',74X,'***')
  130 FORMAT('***',74X,'***')
      WRITE(IOUTS,131)
      IF( JOURNAL )WRITE(IJRNL,132)
  131 FORMAT(' ***  Enter NEWS to get the latest',
     & ' information on new commands and features   ***')
  132 FORMAT('***  Enter NEWS to get the latest',
     & ' information on new commands and features   ***')
      WRITE(IOUTS,133)NEWSDATE
      IF( JOURNAL )WRITE(IJRNL,134)NEWSDATE
  133 FORMAT(' ***  Latest news added on ',A20,31X,'***')
  134 FORMAT('***  Latest news added on ',A20,31X,'***')
      WRITE(IOUTS,135)JFILE(1:LNJ)
      IF( JOURNAL )WRITE(IJRNL,136)JFILE(1:LNJ)
  135 FORMAT(' ***  Journal file: ',A,<58-LNJ>X,'***')
  136 FORMAT('***  Journal file: ',A,<58-LNJ>X,'***')
      WRITE(IOUTS,129)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,143)
      IF( JOURNAL )WRITE(IJRNL,144)
  143 FORMAT(' ***',12X
     & ,'UNAUTHORIZED COMMERCIAL USE IS STRICTLY PROHIBITED'
     & ,12X,'***') 
  144 FORMAT('***',12X
     & ,'UNAUTHORIZED COMMERCIAL USE IS STRICTLY PROHIBITED'
     & ,12X,'***') 
      WRITE(IOUTS,129)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,121)
      IF( JOURNAL )WRITE(IJRNL,122)
      CALL DATETIME( TIME )
      USERNAME = GET_USERNAME(LUSER)
      IF( JOURNAL )WRITE(IJRNL,146)USERNAME(1:LUSER),TIME
  146 FORMAT('User: ',A,'   Date: ',A)
#else
      WRITE(IOUTS,122)
      IF( JOURNAL )WRITE(IJRNL,122)
  122 FORMAT(80('*'))
      WRITE(IOUTS,130)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,124)VERSION,MONTH
      IF( JOURNAL )WRITE(IJRNL,124)VERSION,MONTH
  124 FORMAT('*** PHYSICA',22X,'VERSION #',A5,15X,A9,' 2007 ***')
      WRITE(IOUTS,126)
      IF( JOURNAL )WRITE(IJRNL,126)
  126 FORMAT('*** Copyright (C) 1992,...,2007',30X
     & ,'Joseph L. Chuma ***')
      WRITE(IOUTS,128)
      IF( JOURNAL )WRITE(IJRNL,128)
  128 FORMAT('***',27X,'All rights reserved',28X,'***')
      WRITE(IOUTS,130)
      IF( JOURNAL )WRITE(IJRNL,130)
  130 FORMAT('***',74X,'***')
      WRITE(IOUTS,132)
      IF( JOURNAL )WRITE(IJRNL,132)
  132 FORMAT('***  Enter NEWS to get the latest',
     & ' information on new commands and features   ***')
      WRITE(IOUTS,134)NEWSDATE
      IF( JOURNAL )WRITE(IJRNL,134)NEWSDATE
  134 FORMAT('***  Latest news added on ',A20,31X,'***')
      CLINE = ' '
      CLINE = '***  Journal file: '//JFILE(1:LNJ)
      CLINE(78:80) = '***'
      WRITE(IOUTS,136)CLINE 
      IF( JOURNAL )WRITE(IJRNL,136)CLINE
  136 FORMAT(A)
      WRITE(IOUTS,130)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,144)
      IF( JOURNAL )WRITE(IJRNL,144)
  144 FORMAT('***',12X
     & ,'UNAUTHORIZED COMMERCIAL USE IS STRICTLY PROHIBITED'
     & ,12X,'***') 

      WRITE(IOUTS,130)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,1441)
      IF( JOURNAL )WRITE(IJRNL,1441)
 1441 FORMAT('*** Physica 3.0 for the Windows environment has been ',
     & 'released under the name ***')
      write(iouts,1442)
      if( journal )write(ijrnl,1442)
 1442 format('*** Extrema. See www.extremasoftware.com',37x,'***')

      WRITE(IOUTS,130)
      IF( JOURNAL )WRITE(IJRNL,130)
      WRITE(IOUTS,122)
      IF( JOURNAL )WRITE(IJRNL,122)
      CALL DATETIME( TIME )
      USERNAME = GET_USERNAME(LUSER)
      IF( JOURNAL )WRITE(IJRNL,146)USERNAME(1:LUSER),TIME
  146 FORMAT('User: ',A,'   Date: ',A)
#endif
      CALL GET_TERMTYPE( STRINGS(1) )
      LENST(1) = LENSIG( STRINGS(1) )
      ITYPE(1) = 0
      IF( LENST(1) .GT. 0 )THEN
        ITYPE(1) = 2
        GO TO 232
      END IF
  210 WRITE(*,*)'                                        '
      WRITE(*,*)' nongraphics  0  |                      '
      WRITE(*,*)' VT640        1  |  PT100G   6          '
      WRITE(*,*)' VT241        2  |  SEIKO    7          '
      WRITE(*,*)' CIT467       3  |  X        8 (default)'
      WRITE(*,*)' TK4010       4  |  generic  9          '
      WRITE(*,*)' TK4107       5  |                      '
      WRITE(*,*)'                                        '
  220 WRITE(*,*)
     & ' Enter: monitor name or number, type <RETURN> key for default'
      READ(*,230,END=220,ERR=220)STRINGS(2)
  230 FORMAT(A)
      LENST(2) = LENSIG(STRINGS(2))
      NFLD = 1
      CALL UPRCASE( STRINGS(2)(1:LENST(2)), STRINGS(2)(1:LENST(2)) )
      CALL PARSE_INPUT_LINE( STRINGS(2)(1:LENST(2)), *220 )
  232 IF( ITYPE(1) .EQ. 1 )THEN
        I = INT(REALS(1))
        IF( (I .GT. 9) .OR. (I .LT. 0) )THEN
          WRITE(*,*)'invalid monitor type number'
          GO TO 210
        END IF
        IM = NDEVICT(I)
        GO TO 240
      ELSE IF( ITYPE(1) .EQ. 2 )THEN
        DO I = 0, 10
          IF( INDEX(TERMTYPES(I),STRINGS(1)(1:LENST(1))) .EQ. 1 )THEN
            IM = NDEVICT(I)
            GO TO 240
          END IF
        END DO
        WRITE(*,*)'invalid monitor type'
        GO TO 210
      ELSE
        IM = 18
        GO TO 240
      END IF
  240 CALL SET_PLOT_DEVICES( IM,6,14,7,0,'CM','L',1, *210,*210,*210 )
      CALL CLEAR_PLOT

      CALL INITIALIZE_PHYSICA( 0 )
      CALL EXECUTE( ' ', -1 )      ! initialization file: physica$init
#ifdef VMS
      NC = 1
#else
#ifdef __hpux
      NC = 1
#else
#ifdef _AIX
      NC = 1
#else
cc      NC = IARGC()  // disabled the motif interface Dec 10, 1996
      NC = 1
#endif
#endif
#endif
      IF( (IMONITOR.EQ.18) .AND. (NC.EQ.0) )THEN
        WRITE(*,*)' enter "physica -c" to run in command mode'
        WRITE(*,*)' this is a preliminary motif version of physica'
        WRITE(*,*)
     &   ' more point&click features will be added as time permits'
        WRITE(*,*)' user feedback will be appreciated'
        MOTIF_VERSION = .TRUE.
        MXHELP        = .TRUE.
        NO_PROMPT     = .TRUE.
        CALL PHYSICA_X
      ELSE
  530   CALL GET_CMND( LINE, NLINE, IERR )
        IF( IERR .EQ. 0 )THEN
          CALL PROCESS_CMND( LINE, NLINE )
        ELSE
          IF( BATCH_MODE .AND. .NOT.USE_MODE )GO TO 999
        END IF
        GO TO 530
      END IF

  999 STOP 'unloading PHYSICA ...'
      END
