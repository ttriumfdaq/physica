      SUBROUTINE DSSOL( A, B, L, M, N, KEY )
C
C This routine, obtained sans documentation from the University of
C British Columbia computing centre, solves a linear symmetric system
C of equations with multiple right hand side vectors. The lower half of
C the symmetric matrix representing the system is packed linearly into
C the 1-dim. array A. The array of right hand sides is passed in B,
C with L indicating the number of right hand sides for which the system
C is to be solved. M is the number of points passed in A, and N is the
C number of parameters in the least squares fit.
C Note: M=N*(N+1)/2
C
C Parameters:
C    A: array containing lower triangular half of symmetric 
C       matrix (left hand side of linear system to be solved), 
C       dimensioned as a linear (1-dim.) array
C    B: array of right hand side vectors for which the system  
C       is to be solved, dimensioned as a 2-dim. array
C    L: number of right hand sides stored in B
C    M: total linear length of array A
C    N: number of parameters in original least squares fit. this
C       generates a symmetric matrix A with M = N*(N+1)/2
C  KEY: =0 upon successful completion
C       =1 upon failure
C
      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER (MXFITP=50)
      DIMENSION A(M), B(MXFITP,L)
CCC
      IF( A(1) .LE. 0.0D0 )THEN
        CALL ERROR_MESSAGE('FIT','#1 in DSSOL routine')
        KEY = 1
        RETURN
      END IF
      IF( M .EQ. 1 )THEN
        B(1,1) = B(1,1)/A(1)
        KEY = 0
        RETURN
      END IF
      A(1) = 1.0D0/DSQRT(A(1))
      DO I = 2, N
        A(I) = A(I)*A(1)
      END DO

      INC = N
      I1  = 1
      IN  = N
      NM1 = N - 1
  20  INC = INC - 1
      I1  = IN + 1
      IN  = IN + INC
      NS  = N - INC

      X = 0.0D0
      ISUB = I1
      DO I = INC, NM1
        ISUB = ISUB - I
        X = X + A(ISUB)*A(ISUB)
      END DO
      IF( A(I1)-X .LT. 0.0D0 )THEN
        CALL ERROR_MESSAGE('FIT','#2 in DSSOL routine')
        KEY = 1
        RETURN
      END IF
      A(I1) = DSQRT(A(I1) - X)

      IF( A(I1) .EQ. 0.0D0 )THEN
        CALL ERROR_MESSAGE('FIT','#3 in DSSOL routine')
        KEY = 1
        RETURN
      END IF
      A(I1) = 1.0D0/A(I1)
      IF( INC .NE. 1 )THEN
        I11 = I1 + 1
        L11 = I1 - INC
        DO I = I11, IN
          X  = 0.0D0
          L1 = L11
          L2 = I - INC
          DO J = 1, NS
            X  = X + A(L1)*A(L2)
            L1 = L1 - INC - J
            L2 = L2 - INC - J
          END DO
          A(I) = (A(I)-X)*A(I1)
        END DO
        GO TO 20
      END IF
      DO K = 1, L
        B(1,K) = B(1,K)*A(1)
        DO I = 2, N
          JM   = I-1
          ISUB = I
          INC  = N
          X    = 0.0D0
          DO J = 1, JM
            X    = A(ISUB)*B(J,K) + X
            INC  = INC - 1
            ISUB = ISUB + INC
          END DO
          B(I,K) = (B(I,K)-X)*A(ISUB)
        END DO

        B(N,K) = B(N,K)*A(M)
        INC = -1
        J1  = M+1
        DO I = 2, N
          INC  = INC + 1
          JM   = J1 - 2
          J1   = JM - INC
          JSUB = N-INC-1
          II   = JSUB
          X    = 0.0D0
          DO J = J1, JM
            JSUB = JSUB + 1
            X = X + A(J)*B(JSUB,K)
          END DO
          B(II,K) = (B(II,K)-X)*A(J1-1)
        END DO
      END DO
      KEY = 0
      RETURN
      END
