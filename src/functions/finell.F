       REAL*8 FUNCTION FINELL(X,PHI)
C**********************************************************************
C    FINELL CALCILATES THE INCOMPLETE ELLIPTIC INTEGRALS OF THE FIRST
C      KIND, F(X,PHI) 
C        where X = modulus,    PHI = amplitude in radians
C   DEFINITION: 
C     FINELL(X,PHI)=INTEGRAL from 0 to PHI of (1-(X*sin(t))**2)**-0.5 dt
C    NB. If PHI=PI/2 then FINELL is the COMPLETE ELLIPTIC FUNCTION 
C         ELLICK.
C
C   REFERENCE: CACM ALGORITHM # 73
C***********************************************************************
C   RESTRICTIONS: ABS(X) <= 1   and   ABS(PHI) <= pi/2
C                 otherwise integral will be infinite
C***********************************************************************
C   ACCURACY: TO AT LEAST 1.0E-8 where 0 <= X <=sin(88 degrees), 
C               and  0 <= PHI <= 90 (degree)
C***********************************************************************
       IMPLICIT REAL*8 (A-H,O-Z)
       DIMENSION SIGMA(4)
       DATA PID2/1.57079 63267 94896 7/      ! pi/2
       DATA TANH1/0.76159 41559 56/          ! tanh 1
C
1      H1=1.D0
       DO 2 K=1,4
2        SIGMA(K)=0.D0
       BN=0
       SINPHI=SIN(PHI)
       IF(ABS(X*SINPHI).LE. TANH1) GO TO 5
       IF(ABS(X).LE.1.D0 .AND. ABS(PHI).LE.PID2) GO TO 30
         WRITE(6,*) '  FINELLIC -- INTEGRAL IS INFINITY'
         FINELL=1.D10
         RETURN
5      A1=PHI
       COSPHI=COS(PHI)
10     BN=BN+1
       BN2=2*BN
       EINE=(BN2-1)/BN2
       H2=EINE*X*X*H1
       A2=EINE * A1  -  SINPHI**(BN2-1) * COSPHI / BN2
       DEL1=H2*A2
       SIGMA(1)=SIGMA(1)+DEL1
       H1=H2
       A1=A2
C
       IF (ABS(DEL1).GT.0.D0 .AND. ABS(PHI*SINPHI**BN2).GE.ABS(A2))
     *   GO TO 10
       FINELL=PHI+SIGMA(1)
       RETURN
C
30     PX=SQRT(1-X*X)
       A1=1.D0
       AL1=0.D0
       AM1=0.D0
       AN1=0.D0
40     BN=BN+1
       BN2=2*BN
       EINE=(BN2-1)/BN2
       FINELL=(ABS(X)*SQRT(1-SINPHI**2)*(1-X*X*SINPHI**2)**(BN-0.5))/BN2
       H2=EINE*H1
       A2=EINE**2 * PX*PX * A1
       AL2=AL1 + 1/(BN*(BN2-1))
       AM2=(AM1-FINELL*H2) * ((BN2+1)/(BN2+2))**2 * PX*PX
       AN2=(AN1-FINELL*H1) * EINE * (BN2+1) * PX*PX/(BN2+2)
       DEL1=AM2 - A2*AL2
       DEL3=A2
       SIGMA(1)=SIGMA(1)+DEL1
       SIGMA(3)=SIGMA(3)+DEL3
       H1=H2
       A1=A2
       AL1=AL2
       AM1=AM2
       AN1=AN2
C
       IF (ABS(DEL1).GT.0.D0) GO TO 40
       XSIN= SQRT(1-X*X*SINPHI*SINPHI)
       T1=LOG(4 / ( XSIN + ABS(X)*COSPHI ))
       T2=ABS(X) * COSPHI / XSIN
       FINELL=T1*(1+SIGMA(3)) + T2*LOG(0.5+0.5*ABS(X*SINPHI)) + SIGMA(1)
C
       IF (PHI .GE. 0.D0)  RETURN
       FINELL= -FINELL
       RETURN
       END

