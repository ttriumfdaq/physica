      REAL*8  FUNCTION ELLICE(X)
C     **************************************************************
C     ELLICE IS THE COMPLETE ELLIPTIC INTEGRAL OF THE SECOND KIND
C     ELLICE(X)= INTEGRAL(sqrt(1-(X*sin(t))**2))dt, limits 0 to pi/2
C
C     ELLICE is a special case of the INCOMPLETE ELLIPTIC INTEGRAL
C       ELLICE(X) = EINELL(X,pi/2)
C     REFERENCE: CERN library routine C308
C     **************************************************************
C     RESTRICTION ON ARGUMENT X:  ABS(X) <= 1
C       if ABS(X) > 1 , ELLICE is set to zero with error message
C     **************************************************************
C     ACCURACY: IN GENERAL TO AT LEAST 1.0E-6
C       REGION TESTED: 0.0 <= X <= 1.00
C     **************************************************************
C
      IMPLICIT REAL*8 (A-H, O-Z)
      DIMENSION A(4),B(4)
      DATA N /4/
      DATA A
     1/1.73631 49E-2, 4.75740 44E-2, 6.26076 19E-2, 4.43251 51E-1/
      DATA B
     1/5.26378 93E-3, 4.06946 84E-2, 9.20010 94E-2, 2.49983 66E-1/
      DATA IF/0/, NF/10/
C
C  START
      IF (ABS(X)-1.0) 1,2,3
    1 ETA= 1.0 - X**2
      PA=A(1)
      PB=B(1)
      DO 4 I = 2,N
        PA= PA*ETA + A(I)
    4   PB= PB*ETA + B(I)
      ELLICE= 1.0 + (PA-LOG(ETA)*PB)*ETA
      RETURN
C
    2 ELLICE=1.
      RETURN
C
    3 ELLICE=0.0
      IF=IF+1
      IF (IF .LE. NF) PRINT 100, X
      RETURN
 
  100 FORMAT(1X,27HELLICE ... ILLEGAL ARGUMENT,E20.10)
      END
 
