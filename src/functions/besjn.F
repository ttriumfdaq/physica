      SUBROUTINE DBESJN(X,A,NNN,D,J)
C
C     ******************************************************************
C     DBESJN CALCULATES THE BESSEL FUNCTIONS J(X) OF ORDER A, A+1,
C       A+2, ..., A+NNN IN DOUBLE PRECISION
C
C     REFERENCE : CERN library routine C330
C     ******************************************************************
C     ARGUMENT TYPE :
C       X ----- ARGUMENT OF J(X), REAL*8, X > 0
C       A ----- REAL*8, 0 <= A < 1; if NNN < 0, A # 0
C       NNN --- INTEGER, -100 <= NNN <= +100
C       D ----- INTEGER, NO. OF SOGNIFICANT FIGURES DESIRED
C       J ----- REAL*8 ARRAY, DIMENSION 101, J(X) order A+I is stored
C               in J(ABS(I)+1)
C       If any parameter is out of range, error message results
C     ******************************************************************
C     EXTERNAL ROUTINE REFERENCED: GAMMA (C322)
C     ******************************************************************
C     ACCURACY : WITH A=0,NNN=20,D=10; X=1,5,10,50,100
C       DBESJN is at least as accurate as specified (i.e. 10 sig. fig. )
C     ******************************************************************
C
      INTEGER D
      DOUBLE PRECISION X,A,L,M1,N1,R1,S1,RR(101),J(101),J1(101),EPS,SUM,
     1                 DGAMMA,LAMDA
      LOGICAL LL
C
C  START
      IF ((A .LT. 0.D 0)   .OR.   (A .GE. 1.D 0)   .OR.
     1    (X .LE. 0.D 0)   .OR.   (IABS(NNN) .GT. 100))    GOTO 6666
      NMAX= NNN
      LL  = (NMAX .GE. 0)
      IF (LL)                                              GOTO 10
      IF (A   .EQ.   0.0D 0)                               GOTO 6666
      NM  = -NMAX
      NMAX= 1
   10 EPS = 0.5D 0 * (10.D 0 ** (-D))
      D1  = 2.3026 * FLOAT(D)   +   1.3863
      SUM =  ((X/2.D 0)**A)/DGAMMA(1.D 0 + A)
      KK  = 1
      T2  = 0.0
      X1  =  X
      NMA = NMAX + 1
      DO   11                          K=1,NMA
   11   J1(K)  =  0.0D 0
      IF  (NMAX   .EQ.   0)                                GOTO 50
      Y   = (D1 * 0.5)/FLOAT(NMAX)
   20 IF  (Y   .GT.   10.)                                 GOTO 30
      P   =  Y * 5.7941E-5   -   1.76148E-3
      P   =  Y * P   +   2.08645E-2
      P   =  Y * P   -   1.29013E-1
      P   =  Y * P   +   8.57770E-1
      T2  =  Y * P   +   1.01250E+0
      GOTO 40
   30 Q   =  ALOG(Y)   -   0.775
      P   =  (0.775  -  ALOG(Q))/(1.+ Q)
      P   =  1./(1.+ P)
      T2  =  Y * P/Q
   40 IF  (KK - 2)                               50,60,60
   50 T1  =  T2 * FLOAT(NMAX)
      Y   =  (D1 * 0.73576)/X1
      KK  =  KK + 1
      GOTO 20
   60 T2  =  1.3591 * X1 * T2
      IF  (T1 - T2)                              61,61,62
   61 NU  =  (1.+ T2)
      GOTO 70
   62 NU  =  (1.+ T1)
   70 L  = 1.D 0
      M  = 0
      LIM= (NU/2)
   80 M  = M + 1
      M1 = DBLE(FLOAT(M))
      L  = L * ((M1 + A)/(M1 + 1.D 0))
      IF (M - LIM)                               80,81,81
   81 N  = M + M
      R1 = 0.D 0
      S1 = 0.D 0
   82 N1 = DBLE(FLOAT(N))
      R1 = 1.D 0/(2.D 0 * (A+N1)/X  -  R1)
      LAMDA = 0.0D 0
      IF ((N/2) * 2    .NE.    N)                          GOTO 83
      L  = L * (N1 + 2.D 0)/(N1 + 2.D 0*A)
      LAMDA = L * (N1 + A)
   83 S1 = R1 * (LAMDA + S1)
      IF (N   .LE.   NMAX)                                 RR(N) = R1
      N  = N - 1
      IF (N   .GE.   1)                                    GOTO 82
      J(1) = SUM/(1.D 0 + S1)
      IF(NMAX.EQ.0) GO TO 99
      DO   90                          K=1,NMAX
   90   J(K+1) = RR(K) * J(K)
   99 CONTINUE
      DO   91                          K=1,NMA
        IF  (DABS((J(K) - J1(K)) / J(K)) .GT. EPS)  GO TO 100
   91 CONTINUE
      GOTO 102
  100 NU = NU + 5
      DO   101                         K=1,NMA
  101   J1(K) = J(K)
      GOTO 70
  102 IF (LL)                                              RETURN
      J(2) = 2.D 0 * A * J(1)/X  -  J(2)
      DO   110                         K=2,NM
  110  J(K+1) = 2.D 0 * (A - DBLE(FLOAT(K)) + 1.D 0) * J(K)/X  -  J(K-1)
      RETURN
C
 6666 PRINT 1000
 1000 FORMAT (/, 2X, 42HDBESJN .... UNREASONABLE ARGUMENT OR ORDER)
      RETURN
      END
 
