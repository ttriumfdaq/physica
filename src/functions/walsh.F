       REAL*8  FUNCTION WALSH(K,X)
C*******************************************************************
C   BINARY ORDERED WALSH FUNCTION
C
C  DEFINITION:
C     Let K be a binary number, i.e.,
C         K = a(0)*2**0 + a(1)*2**1 + a(2)*2**2 + a(3)*2**3 + ...
C             with a(i) = 0 or 1, i=0,1,2,...
C     Then WALSH(K,X)= (RADMAC(0,X)**a(0)) * (RADMAC(1,X)**a(1)) * ...
C     Again, the WALSH functions form a complete set of orthogonal,
C       normalized rectangular periodic functions with period of one.
C     where K=non-negative integer, X is real
C     NB. WALSH can assume values of 1 or -1 only.
C
C   REFERENCE: CACM ALGORITHMS # 389
C***********************************************************************
       IMPLICIT REAL*8 (A-H,O-Z)
C
       L=K
       M=1
       MW=1
       WALSH=MW
       I=-1
  10   IF (M .GT. L) RETURN
       I=I+1
       IF (K/(2.D0*M) .EQ. INT(K/(2*M))) GO TO 20
         MW=MW * RADMAC(I,X)
         K= K - M
  20   M=M+M
       WALSH=MW
       GO TO 10
       END

