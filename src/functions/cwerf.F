      COMPLEX FUNCTION CWERF(Z)
C
C     ****************************************************************
C     CWERF IS THE COMPLEX ERROR FUNCTION OF COMPLEX ARGUMENT Z
C
C     REFERENCE: CERN library routine C335
C     ****************************************************************
C     ACCURACY : TO AT LEAST 1.0E-6 IN TESTED REGION
C       TESTED REGION: For Z = X + iY,
c                      0.0 <=X <= 3.9, 0.0 <= Y <= 3.0
C     ****************************************************************
C
      COMPLEX Z
      REAL LAMBDA
      LOGICAL B
C
C   START
      XX= REAL(Z)
      YY= AIMAG(Z)
      X= ABS(XX)
      Y= ABS(YY)
      IF (Y .LT. 4.29 .AND. X .LT. 5.33) GO TO 1
      H=0.0
      NC=0
      NU=8
      LAMBDA=0.0
      B=.TRUE.
      GO TO 2
C
    1 S= (1.0-Y/4.29 ) * SQRT(1.0-X**2/28.41)
      H= 1.6 * S
      H2= 2.0 * H
      NC= 6 + INT(23.0*S)
      NU= 9 + INT(21.0*S)
      LAMBDA= H2**NC
      B= LAMBDA .EQ. 0.0
    2 R1=0.0
      R2=0.0
      S1=0.0
      S2=0.0
      N= NU + 1
    3 N= N - 1
      FN= N + 1
      T1= Y + H + FN*R1
      T2= X - FN*R2
      C= 0.5 / (T1**2+T2**2)
      R1= C * T1
      R2= C * T2
      IF (H .LE. 0.0 .OR. N .GT. NC) GO TO 4
      T1= LAMBDA + S1
      S1= R1*T1 - R2*S2
      S2= R2*T1 + R1*S2
      LAMBDA= LAMBDA / H2
    4 IF (N .GT. 0) GO TO 3
      IF (B) GO TO 6
      RS1=S1
      RS2=S2
      GO TO 7
C
    6 RS1=R1
      RS2=R2
    7 RS1= 1.12837 91670 9551  *RS1
      IF (Y .EQ. 0.0) RS1=EXP(-X**2)
      CWERF = CMPLX(RS1,1.12837 91670 9551 *RS2)
      IF (YY .LT. 0.0) GO TO 8
      IF (XX .LT. 0.0) CWERF=CONJG(CWERF)
      RETURN
C
    8 CWERF= 2.0 * CEXP(-CMPLX(X,Y)**2) - CWERF
      IF (XX .GT. 0.0) CWERF=CONJG(CWERF)
      RETURN
C
      END
 
