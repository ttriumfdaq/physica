      REAL*8 FUNCTION EI (X)
C*********************************************************************
C    REFERENCE: SLATEC LIBRARY; CATEGORY B5E
C
C*** PURPOSE
C    COMPUTES THE EXPONENTIAL INTEGRAL Ei(x)
C*** EXTERNAL ROUTINE CALLED: EXPINT
C*********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      EI = - EXPINT(-X)
      RETURN
      END
