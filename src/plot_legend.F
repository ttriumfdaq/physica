      SUBROUTINE PLOT_LEGEND( AREPLOT, IREPLOT
     &                       ,LPT, RSZ, LCOL, RANG, NUM, LABEL, LENL, *)

      INTEGER*4 NTOTP, NTOTQ, MXREPL, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXREPL=700,   MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

C  WINDOW dependent legend variables

      CHARACTER*255 LEGEND_TITLE(0:MXWNDO)
      REAL*4        XLEGLO(0:MXWNDO), XLEGUP(0:MXWNDO)
     &             ,YLEGLO(0:MXWNDO), YLEGUP(0:MXWNDO)
     &             ,LEGEND_TITLE_HEIGHT(0:MXWNDO), HITMIN(0:MXWNDO)
      INTEGER*4     LEGEND_NUMSYM(0:MXWNDO), LEGEND_ENTRIES(0:MXWNDO)
     &             ,LEGEND_TITLE_LEN(0:MXWNDO), LEGEND_UNITS(0:MXWNDO)
      LOGICAL*4     LEGEND_ON(0:MXWNDO), LEGEND_BLANKING(0:MXWNDO)
     &             ,FRAME_GIVEN(0:MXWNDO), LEGEND_FRAME_ON(0:MXWNDO)
     &             ,LEGEND_LABEL_AUTO(0:MXWNDO)
      COMMON /GRAPH_LEGEND/ XLEGLO, XLEGUP, YLEGLO, YLEGUP
     & ,LEGEND_TITLE_HEIGHT, HITMIN, LEGEND_NUMSYM, LEGEND_ENTRIES
     & ,LEGEND_TITLE_LEN, LEGEND_UNITS, LEGEND_ON, LEGEND_BLANKING
     & ,FRAME_GIVEN, LEGEND_FRAME_ON, LEGEND_LABEL_AUTO, LEGEND_TITLE

C  GRAPH dependent legend variables

      CHARACTER*255 LBL_REPLOT(0:MXREPL)
      INTEGER*4     ILEG_REPLOT, LEN_LBL_REPLOT(0:MXREPL)
     &             ,IPT(0:MXREPL), LEG_NSYM_REPLOT(0:MXREPL)
     &             ,LEGWIND(0:MXREPL)
      COMMON /LEGEND_REPLOT/ ILEG_REPLOT, LEN_LBL_REPLOT
     & ,IPT, LEG_NSYM_REPLOT, LEGWIND, LBL_REPLOT

      REAL*4    XL(0:MXWNDO), YL(0:MXWNDO), XU(0:MXWNDO), YU(0:MXWNDO)
     &         ,XLW(0:MXWNDO),YLW(0:MXWNDO),XUW(0:MXWNDO),YUW(0:MXWNDO)
      INTEGER*4 NUMWINDOW, NUMMAX, NUMS(0:MXWNDO)
      LOGICAL*4 BORDER
      COMMON /PHYSICA_WINDOW/ NUMWINDOW, BORDER, NUMMAX, NUMS
     &                       ,XL, YL, XU, YU, XLW, YLW, XUW, YUW

      PARAMETER (JMAX=40, TOLLERANCE=0.1)

      CHARACTER*1024 LABEL
      REAL*4        RSZ(NUM), RANG(NUM)
      INTEGER*4     IREPLOT, NUM, LENL
      LOGICAL*4     AREPLOT
      BYTE          LPT(NUM), LCOL(NUM)

      REAL*8    R8D(1)
      INTEGER*4 I4D(1)
      COMMON /XD/ I4D
      EQUIVALENCE ( I4D, R8D )

C   Convert from world units to graph units

      SCX(X) = (X - XLAXIS) / (XUAXIS - XLAXIS) * (XMAX - XMIN) + XMIN
      SCY(Y) = (Y - YLAXIS) / (YUAXIS - YLAXIS) * (YMAX - YMIN) + YMIN

C   Convert from graph units to world units

      SCX_INV(X) = (X - XMIN)*(XUAXIS - XLAXIS)/(XMAX - XMIN) + XLAXIS
      SCY_INV(Y) = (Y - YMIN)*(YUAXIS - YLAXIS)/(YMAX - YMIN) + YLAXIS

C   Convert from world coordinates to percentages

      SCXP(X)  = 100. * (X - XLWIND) / (XUWIND - XLWIND)
      SCYP(Y)  = 100. * (Y - YLWIND) / (YUWIND - YLWIND)

C   Convert from percentages to world units

      SCXP_INV(X)  = X / 100. * (XUWIND - XLWIND) + XLWIND
      SCYP_INV(Y)  = Y / 100. * (YUWIND - YLWIND) + YLWIND
CCC
      NW = NUMWINDOW
      XLAXIS = GETNAM('XLAXIS')
      XUAXIS = GETNAM('XUAXIS')
      YLAXIS = GETNAM('YLAXIS')
      YUAXIS = GETNAM('YUAXIS')
      XUWIND = XUW(NW)
      XLWIND = XLW(NW)
      YUWIND = YUW(NW)
      YLWIND = YLW(NW)

      XMIN = GETNAM('XMIN')
      XMAX = GETNAM('XMAX')
      AXLOG = ABS(GETNAM('XLOG'))
      IF( AXLOG .GT. 1. )ALOGX = ALOG(AXLOG)
      YMIN = GETNAM('YMIN')
      YMAX = GETNAM('YMAX')
      AYLOG = ABS(GETNAM('YLOG'))
      IF( AYLOG .GT. 1. )ALOGY = ALOG(AYLOG)

      IF( .NOT.FRAME_GIVEN(NW) )THEN
        XLAXISP = SCXP(XLAXIS)
        XUAXISP = SCXP(XUAXIS)
        YLAXISP = SCYP(YLAXIS)
        YUAXISP = SCYP(YUAXIS)
        XLEGLO(NW) = XLAXISP + 0.5*(XUAXISP-XLAXISP)
        YLEGLO(NW) = YLAXISP + 0.1*(YUAXISP-YLAXISP)
        XLEGUP(NW) = XLAXISP + 0.9*(XUAXISP-XLAXISP)
        YLEGUP(NW) = YLAXISP + 0.4*(YUAXISP-YLAXISP)
        LEGEND_UNITS(NW) = 1  
      END IF
      IF( LEGEND_UNITS(NW) .EQ. 0 )THEN         ! graph units
        XLOG = XLEGLO(NW)
        XUPG = XLEGUP(NW)
        YLOG = YLEGLO(NW)
        YUPG = YLEGUP(NW)
        IF( AXLOG .GT. 1. )THEN
          XLOW = SCX_INV(ALOG(XLEGLO(NW))/ALOGX)
          XUPW = SCX_INV(ALOG(XLEGUP(NW))/ALOGX)
        ELSE
          XLOW = SCX_INV(XLEGLO(NW))
          XUPW = SCX_INV(XLEGUP(NW))
        END IF
        IF( AYLOG .GT. 1. )THEN
          YLOW = SCY_INV(ALOG(YLEGLO(NW))/ALOGY)
          YUPW = SCY_INV(ALOG(YLEGUP(NW))/ALOGY)
        ELSE
          YLOW = SCY_INV(YLEGLO(NW))
          YUPW = SCY_INV(YLEGUP(NW))
        END IF
        XLOP = SCXP(XLOW)
        XUPP = SCXP(XUPW)
        YLOP = SCYP(YLOW)
        YUPP = SCYP(YUPW)
      ELSE IF( LEGEND_UNITS(NW) .EQ. 1 )THEN ! percentages of the window
        XLOW = SCXP_INV(XLEGLO(NW))
        XUPW = SCXP_INV(XLEGUP(NW))
        YLOW = SCYP_INV(YLEGLO(NW))
        YUPW = SCYP_INV(YLEGUP(NW))
        XLOG = SCX(XLOW)
        XUPG = SCX(XUPW)
        YLOG = SCY(YLOW)
        YUPG = SCY(YUPW)
        IF( AXLOG .GT. 1. )THEN
          XLOG = AXLOG**XLOG
          XUPG = AXLOG**XUPG
        END IF
        IF( AYLOG .GT. 1. )THEN
          YLOG = AYLOG**YLOG
          YUPG = AYLOG**YUPG
        END IF
        XLOP = XLEGLO(NW)
        XUPP = XLEGUP(NW)
        YLOP = YLEGLO(NW)
        YUPP = YLEGUP(NW)
      ELSE IF( LEGEND_UNITS(NW) .EQ. 2 )THEN    ! world coordinate units
        XLOG = SCX(XLEGLO(NW))
        XUPG = SCX(XLEGUP(NW))
        YLOG = SCY(YLEGLO(NW))
        YUPG = SCY(YLEGUP(NW))
        IF( AXLOG .GT. 1. )THEN
          XLOG = AXLOG**XLOG
          XUPG = AXLOG**XUPG
        END IF
        IF( AYLOG .GT. 1. )THEN
          YLOG = AYLOG**YLOG
          YUPG = AYLOG**YUPG
        END IF
        XLOP = SCXP(XLEGLO(NW))
        XUPP = SCXP(XLEGUP(NW))
        YLOP = SCYP(YLEGLO(NW))
        YUPP = SCYP(YLEGUP(NW))
        XLOW = XLEGLO(NW)
        XUPW = XLEGUP(NW)
        YLOW = YLEGLO(NW)
        YUPW = YLEGUP(NW)
      END IF
CC      write(6,1000)legend_units(nw)
CC1000  format(' legend_units= ',i4)
CC      write(6,1001)xlog,xupg,ylog,yupg
CC1001  format(' xlog,xupg,ylog,yupg= ',4(f10.3,1x))
CC      write(6,1002)xlow,xupw,ylow,yupw
CC1002  format(' xlow,xupw,ylow,yupw= ',4(f10.3,1x))
CC      write(6,1003)xlop,xupp,ylop,yupp
CC1003  format(' xlop,xupp,ylop,yupp= ',4(f10.3,1x))

C  XLOG, XUPG, YLOG, YUPG  are in graph units
C  XLOP, XUPP, YLOP, YUPP  are in percentages of the window
C  XLOW, XUPW, YLOW, YUPW  are in world coordinates

      CLIP = GETNAM('CLIP')
      CALL SETNAM('CLIP',0.)
      HISTYP = GETNAM('HISTYP')
      CALL SETNAM('HISTYP',0.)

C First plot the frame

      IF( LEGEND_FRAME_ON(NW) .AND. (LEGEND_ENTRIES(NW).EQ.0) )THEN
        CALL PLOT_R(XLOW,YLOW,3)
        CALL PLOT_R(XUPW,YLOW,2)
        CALL PLOT_R(XUPW,YUPW,2)
        CALL PLOT_R(XLOW,YUPW,2)
        CALL PLOT_R(XLOW,YLOW,2)
      END IF

C  Determine the length of the text string part of the legend

      CURSOR = GETNAM('CURSOR')
      XLOC   = GETNAM('XLOC')
      YLOC   = GETNAM('YLOC')
      TXTHIT = GETNAM('TXTHIT')
      IF( LEGEND_ENTRIES(NW) .EQ. 0 )THEN
        HEIGHT = TXTHIT
        HITMIN(NW) = 100.*HEIGHT/(YUWIND-YLWIND)
        YLEG = YUPW
        IF( (LEGEND_TITLE_LEN(NW).GT.0) .AND.
     &      (LEGEND_TITLE_HEIGHT(NW).GT.0.0) )THEN
          CALL SETNAM('CURSOR',-2.)
          CALL SETNAM('XLOC',0.5*(XUPW+XLOW))
          CALL SETNAM('YLOC'
     &     ,YUPW+0.5*LEGEND_TITLE_HEIGHT(NW)/100.*(YUWIND-YLWIND))
          CALL SETNAM('%TXTHIT',LEGEND_TITLE_HEIGHT(NW))
          CALL SETLAB('TEXT',LEGEND_TITLE(NW)(1:LEGEND_TITLE_LEN(NW)))
        END IF
      END IF

      X1T = XLOW+(XUPW-XLOW)/3.
      X2T = XUPW-(XUPW-XLOW)/40.
      XLEN = X2T-X1T
      LEGEND_ENTRIES(NW) = LEGEND_ENTRIES(NW)+1
      HMAX = (YUPW-YLOW-3.*(YUPW-YLOW)/40.)/LEGEND_ENTRIES(NW)
      IF( .NOT.LEGEND_LABEL_AUTO(NW) )THEN
        HEIGHT = TXTHIT
        GO TO 22
      END IF
      H1 = 0.0
      H2 = TXTHIT
   8  CALL LEGEND_LABEL_LENGTH( PLEN, H2, HITMAX, LABEL, LENL, *92)
      IF( PLEN .LT. XLEN )THEN
        H2 = 1.25*H2
        GO TO 8
      END IF
      RTBIS = H2
      DX = H1 - H2
      DO JCNT = 1, JMAX
        DX = DX*0.5
        XMID = RTBIS+DX
        CALL LEGEND_LABEL_LENGTH( PLEN, XMID, HITMAX, LABEL, LENL, *92)
        FMID = XLEN - PLEN
        IF( FMID .LE. 0.0 )RTBIS = XMID
        IF( (ABS(DX).LT.TOLLERANCE) .OR. (FMID.EQ.0.0) )GO TO 20
      END DO
      GO TO 21
  20  HEIGHT = RTBIS
  21  CALL LEGEND_LABEL_LENGTH( PLEN, HEIGHT, HITMAX, LABEL, LENL, *92)
      IF( PLEN .GT. XLEN )THEN
        HEIGHT = 0.98*HEIGHT
        GO TO 21
      END IF
  22  PHEIGHT = 100.*HEIGHT/(YUWIND-YLWIND)
      HITMIN(NW) = MIN( HITMIN(NW), PHEIGHT )

C  Plot the text string part of the legend

      CALL SETNAM('CURSOR',-1.)
      CALL SETNAM('XLOC',X1T)
      YLEG = YLEG-2.0*HEIGHT
      CALL SETNAM('YLOC',YLEG)
      CALL SETNAM('%TXTHIT',PHEIGHT)
      CALL SETLAB('TEXT',LABEL(1:LENL))
      IF( ILEG_REPLOT .EQ. MXREPL )AREPLOT = .FALSE.
      IF( AREPLOT )THEN
        ILEG_REPLOT = ILEG_REPLOT+1
        LBL_REPLOT(ILEG_REPLOT) = LABEL(1:LENL)
        LEN_LBL_REPLOT(ILEG_REPLOT) = LENL
        IPT(ILEG_REPLOT) = IREPLOT
        LEGWIND(ILEG_REPLOT) = NW
      END IF
      CALL SETNAM('CURSOR',CURSOR)
      CALL SETNAM('%XLOC',SCXP(XLOC))
      CALL SETNAM('%YLOC',SCYP(YLOC))
      CALL SETNAM('%TXTHIT',100.*TXTHIT/ABS(YUWIND-YLWIND))

C  Now plot the legend entry
C  X1E, X2E, YC are in world units

      X1E   = XLOW + (XUPW-XLOW)/30.
      X2E   = X1T  - (XUPW-XLOW)/15.
      X1EG  = SCX(X1E)             ! convert to graph units
      X2EG  = SCX(X2E)             ! convert to graph units
      YC    = YLEG+HEIGHT/2.
      SCYYC = SCY(YC)
      IF( AXLOG .GT. 1. )THEN
        X1EG = AXLOG**X1EG
        X2EG = AXLOG**X2EG
      END IF
      IF( AYLOG .GT. 1. )THEN
        SCYYC = AYLOG**SCYYC
      END IF

C  Plot the line segment part of the legend

      N = MIN( LEGEND_NUMSYM(NW), NUM )
      MASK = GETNAM('MASK')
      PMODE = GETNAM('PMODE')
      ICHARI = 1
      IF( MASK .LT. 0 )ICHARI = LPT(1)*NINT(PMODE)
cc      IF( (ICHARI.GE.0) .AND. (N.GT.1) )THEN
      IF( ICHARI.GE.0 )THEN
        XLTYPE = GETNAM('LINTYP')
        CALL PLOT_R(X1E,YC,3)
        CALL DLINE(X2E,YC,IFIX(XLTYPE),1)
        CALL FLUSH_PLOT
      END IF

C  Plot the symbols on the line segment

      IF( AREPLOT )LEG_NSYM_REPLOT(ILEG_REPLOT) = N
      CALL GET_TEMP_SPACE(IXDUM,N,8,*991)
      CALL GET_TEMP_SPACE(IYDUM,N,8,*991)
      DO I = 1, N
        R8D(IYDUM+I) = SCYYC
      END DO
      IF( N .GT. 1 )THEN
        XINC = (X2EG-X1EG)/(N-1.)
        DO I = 1, N-1
          R8D(IXDUM+I) = X1EG+(I-1)*XINC
        END DO
        R8D(IXDUM+N) = X2EG

C  Plot the line segment part of the legend

      ELSE IF( N .EQ. 1 )THEN
        R8D(IXDUM+1) = (X1EG+X2EG)/2.
      END IF
      IF( ICHARI .GE. 0 )THEN
        CALL SETNAM('PMODE',-1.0)
      ELSE
        CALL SETNAM('PMODE',1.0)
      END IF
      CALL PHYSICA_GPLOT(IXDUM,0,0,IYDUM,0,0,N,2,LPT,RSZ,LCOL,RANG)
      CALL SETNAM('PMODE',PMODE)
      CALL SETNAM('CLIP',CLIP)
      CALL SETNAM('HISTYP',HISTYP)
      RETURN
  92  RETURN 1
 991  CALL ERROR_MESSAGE('PLOT_LEGEND routine'
     & ,'allocating dynamic array space')
      CALL SETNAM('CLIP',CLIP)
      RETURN 1
      END
