 
 /* Include the most common X11 header files. */

#include <stdio.h>
#include <memory.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <X11/Xatom.h>

 /* default big string size */
 
#ifndef BUFSIZE
#define BUFSIZE 256
#endif

 /*
   common event mask for top-level windows.
 */
 
#define EVENT_MASK (long) ( ButtonPressMask | KeyPressMask | KeyReleaseMask )
 
#define LEFT_BUTTON  1
#define RIGHT_BUTTON 3
 
 Window t_window;
 Display *dpy;
 
 Window FindSubWindow( Window, Window, int, int );
 void digitize_any_( int *, int *, float *, float *, int * );
 
 void digitize_any_( int *result, int *num, float xa[], float ya[], int ba[] )
  {
    /*
      This routine allows the user to pick a window interactively with the mouse
      and then digitize points from a graph in that window.
    
      num initially is the max number of points one can pick
      after return, it is the number of points which were picked
    */
    XEvent event;
    Window root_window, window;
    Cursor cursor;
    int status, nEvent, flag;
    int screen;
    
    int cntr, x, y, button;
    float x1[3], y1[3], x2[3], y2[3];
    float a, b, c, d, e, f;
    int initN = *num;
    
    /* find a window with the pointer */
    
    printf( "\nMove the mouse pointer over the window\n" );
    printf( "you want, then press a mouse button\n\n" );
    
    /*
     *   pick a window on the display interactively with the mouse.
     *   The mouse pointer is grabbed until a button is pressed, and
     *   the window selected is the window the mouse is in.
     */
    
    root_window = RootWindow( dpy, screen );

    PrintWindowInfo( root_window );

    window = (Window) None;
    cursor = XCreateFontCursor( dpy, XC_crosshair );
    XDefineCursor( dpy, root_window, cursor );
    
    status = XGrabPointer( dpy, root_window, False, ButtonPressMask,
                           GrabModeSync, GrabModeSync, None, cursor, CurrentTime );
    if( status != GrabSuccess )
    {
      printf( "\nError: Unable to grab pointer\n" );
      *result = 1;
      return;
    }
    
    XAllowEvents( dpy, SyncBoth, CurrentTime );    
    XWindowEvent( dpy, root_window, EVENT_MASK, &event );
    
    if( event.type == ButtonPress ) /* find lowest child */
    {
      window = FindSubWindow( root_window,
                              event.xbutton.subwindow,
                              event.xbutton.x, event.xbutton.y );
      if( window == (Window) None )window = root_window;
    }
    
    PrintWindowInfo( window );
    
    XUngrabPointer( dpy, CurrentTime );
   
    XSelectInput( dpy, window, EVENT_MASK );
    XSelectInput( dpy, t_window, KeyPressMask );
    XFlush( dpy );
    
    printf( "\nPosition the crosshair on a point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );
    printf( "Type a Q to quit\n" );
    XFlush( dpy );
    
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    x1[0] = (float)x;
    y1[0] = (float)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%f %f", &x2[0], &y2[0] ) < 2 );
    
    printf( "\nPosition the crosshair on another point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );
    
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    x1[1] = (float)x;
    y1[1] = (float)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%f %f", &x2[1], &y2[1] ) < 2 );
    printf( "\nPosition the crosshair on a non-colinear point on the graph\n" );
    printf( "that you know the coordinates of and press the left mouse button\n" );
    
    do
    {
      nEvent = EventLoop( &button, &x, &y );
      if( nEvent == 0 )
      {
        CloseAndQuit( cursor );
        *result = 0;
        return;
      }
    }
    while( nEvent == 1 );
    x1[2] = (float)x;
    y1[2] = (float)y;
    printf( "\nEnter the x and y graph coordinates of this point\n" );
    while( scanf( "%f %f", &x2[2], &y2[2] ) < 2 );
    printf( "\nNow pick points on the graph\n" );
    printf( "Click left mouse button for pen down, right button for pen up\n" );
    printf( "Type a Q to quit\n\n" );
    /*
      set the keyboard focus to the png window
    */
    XSetInputFocus( dpy, window, RevertToPointerRoot, CurrentTime );
    XNextEvent( dpy, &event );

    if( !CoordinateTransform( x1, y1, x2, y2, &a, &b, &c, &d, &e, &f ) )
    {
      printf( "\nError: division by zero in coordinate transform\n" );
      CloseAndQuit( cursor );
      *result = 1;
      return;
    }
    cntr = 0;
    flag = 1; /* True */
    while( nEvent = EventLoop( &button, &x, &y ) )
    {
      if( nEvent == 1 ) /* keypress other than q */
      {
        flag = 0; /* False */
      }
      else /* mouse button click */
      {
        if( flag )
        {
          xa[cntr] = a + b*x + c*y;
          ya[cntr] = d + e*x + f*y;
          ba[cntr] = button;
          if( button == 1 )
          {
            printf( "(x,y) = (%f,%f), pen down\n", xa[cntr], ya[cntr] );
          }
          else
          {
            printf( "(x,y) = (%f,%f), pen up\n", xa[cntr], ya[cntr] );
          }
          if( ++cntr == initN )break;
        }
        flag = 1;
      }
    }
    *num = cntr;
    CloseAndQuit( cursor );
    *result = 0;
    return;
  }
 
 Window FindSubWindow( Window topWindow, Window window_to_check,
                       int x, int y )
  {
    /*
     * This routine finds the lowest child window (called a subwindow),
     * for a given set of coordinates in a parent window.
     *
     * If no child window can be found at the given coordinates, then
     * the lowest window is the parent.
     */
    
    int newx, newy;
    Window window;

    /* check for bad input */

    if( topWindow == (Window) None )return( (Window) None );
    if( window_to_check == (Window) None )return( topWindow );

    /* check for children */

    window = window_to_check;
    while( ( XTranslateCoordinates( dpy, topWindow, window_to_check, x, y,
                                    &newx, &newy, &window ) != 0 ) &&
           ( window != (Window) None ) )
    {
      if( window != (Window) None )
      {
        topWindow = window_to_check;
        window_to_check = window;
        x = newx;
        y = newy;
      }
    }
    if( window == (Window) None )
    {
      window = window_to_check;
    }
    return( window );
  }
 
 /* end of file */
 
