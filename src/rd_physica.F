      SUBROUTINE READ_PHYSICA( * )

C  ascii files:
C
C READ\MATRIX FILE{\range} matrix\(nc{,nr})
C READ\MATRIX\FORMAT FILE{\range} (frmt) matrix/(nc{,nr})
C READ\SCALAR FILE{\line} s1/{ c1 } ... sN/{ cN }
C READ\SCALAR\FORMAT FILE{\line} (frmt) s1/{ c1 } ... sN/{ cN }
C READ\VECTOR FILE{\range} x1/{ c1 } ... xN/{ cN }
C READ\VECTOR\FORMAT FILE{\range} (frmt) x1/{ c1 } ... xN/{ cN }
C READ\TEXT FILE{\range} tvar
C
C  unformatted binary files:
C
C READ\MATRIX FILE{\range} (frmt) matrix/(nc{,nr})
C READ\SCALAR FILE{\line} (frmt) s1/{ c1 } ... sN/{ cN }
C READ\VECTOR FILE{\range} (frmt) x1/{ c1 } ... xN/{ cN }
C
C  where frmt is really a "prescription" for reading the unformatted file.
C  If frmt is of the form  nB  for n=1,2,4,8  then a stream read is done.
C  The current length of the input variables is used, since the records are
C  variable length.  If frmt is of the form  R4,I2,R8, etc. then the file is
C  read by record with the record length implied by the specified number of
C  bytes.

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      CHARACTER*1024 FNAME, SRNG, LINE2
      CHARACTER*1   BS
      REAL*8        DUM8
      INTEGER*4     LENF, NLN2, LRNG, NRNG, IDUM, NDIM, IADR, N2, N3, N4
     &             ,I, II, LENQ, IROW1
      LOGICAL*1     TEXT, SCALARS, MATRIX, VECTORS, CONTINUE, FCLOSE
     &             ,MESSAGES, BINARY, OVERLAY, EXTEND, EFILL
     &             ,ESKIP, ESTOP, OUTPUT, FRMTIN, FLIPPED, fast
CCC
C   A file can be ASCII or UNFORMATTED (binary)
C   ASCII:       no format --> PHYSICA free format (parsed records)
C                (*)       --> read each record with FORTRAN free format
C                (format)  --> read each record with this format
C   UNFORMATTED: (format)  --> if BINDEC format, e.g. (R4,I2), read by record
C                (format)  --> if nB format, e.g. (4B), stream read

      BS = CHAR(92)       ! backslash

      LINE2    = 'READ'
      NLN2     = 4

      VECTORS  = .TRUE.
      SCALARS  = .FALSE.
      MATRIX   = .FALSE.
      TEXT     = .FALSE.
      FRMTIN   = .FALSE.
      CONTINUE = .FALSE.
      FCLOSE   = .FALSE.
      BINARY   = .FALSE.
      EXTEND   = .TRUE.
      APPEND   = .FALSE.
      OVERLAY  = .FALSE.
      ESTOP    = .TRUE.
      ESKIP    = .FALSE.
      EFILL    = .FALSE.
      MESSAGES = .TRUE.
      FLIPPED  = .TRUE.
      fast     = .false.

      DO I = 1, NQUAL(1)
        IF( QUALIFIERS(1,I)(1:1) .EQ. '-' )THEN
          II = 2
        ELSE
          II = 1
        END IF
        LENQ = LENQL(1,I)
        IF( INDEX('BINARY',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            BINARY = .TRUE.
          ELSE
            BINARY = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF(INDEX('UNFORMATTED',QUALIFIERS(1,I)(II:LENQ)).EQ.1)THEN
          IF( II .EQ. 1 )THEN
            BINARY = .TRUE.
          ELSE
            BINARY = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('ASCII',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            BINARY = .FALSE.
          ELSE
            BINARY = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('CONTINUE',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            CONTINUE = .TRUE.
          ELSE
            CONTINUE = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOCONTINUE',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            CONTINUE = .FALSE.
          ELSE
            CONTINUE = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('FORMAT',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            FRMTIN = .TRUE.
          ELSE
            FRMTIN = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOFORMAT',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            FRMTIN = .FALSE.
          ELSE
            FRMTIN = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('FLIPPED',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            FLIPPED = .TRUE.
          ELSE
            FLIPPED = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOFLIPPED',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            FLIPPED = .FALSE.
          ELSE
            FLIPPED = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('ERRSTOP',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            ESTOP = .TRUE.
            ESKIP = .FALSE.
            EFILL = .FALSE.
          ELSE
            ESTOP = .FALSE.
            ESKIP = .TRUE.
            EFILL = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOERRSTOP',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            ESTOP = .FALSE.
            ESKIP = .TRUE.
            EFILL = .FALSE.
          ELSE 
            ESTOP = .TRUE.
            ESKIP = .FALSE.
            EFILL = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('ERRSKIP',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            ESKIP = .TRUE.
            ESTOP = .FALSE.
            EFILL = .FALSE.
          ELSE
            ESTOP = .TRUE.
            ESKIP = .FALSE.
            EFILL = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOERRSKIP',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            ESTOP = .TRUE.
            ESKIP = .FALSE.
            EFILL = .FALSE.
          ELSE
            ESKIP = .TRUE.
            EFILL = .FALSE.
            ESTOP = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('ERRFILL',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            EFILL = .TRUE.
            ESKIP = .FALSE.
            ESTOP = .FALSE.
          ELSE
            ESTOP = .TRUE.
            EFILL = .FALSE.
            ESKIP = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOERRFILL',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            ESTOP = .TRUE.
            EFILL = .FALSE.
            ESKIP = .FALSE.
          ELSE
            EFILL = .TRUE.
            ESKIP = .FALSE.
            ESTOP = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('CLOSE',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            FCLOSE = .TRUE.
          ELSE
            FCLOSE = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOCLOSE',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            FCLOSE = .FALSE.
          ELSE
            FCLOSE = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('SCALARS',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            SCALARS = .TRUE.
            MATRIX  = .FALSE.
            VECTORS = .FALSE.
            TEXT    = .FALSE.
          ELSE
            SCALARS = .FALSE.
            VECTORS = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('MATRIX',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            MATRIX  = .TRUE.
            SCALARS = .FALSE.
            TEXT    = .FALSE.
            VECTORS = .FALSE.
          ELSE
            MATRIX  = .FALSE.
            VECTORS = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('VECTORS',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            VECTORS = .TRUE.
            SCALARS = .FALSE.
            MATRIX  = .FALSE.
            TEXT    = .FALSE.
          ELSE
            MATRIX  = .TRUE.
            VECTORS = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('TEXT',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            TEXT    = .TRUE.
            SCALARS = .FALSE.
            MATRIX  = .FALSE.
            VECTORS = .FALSE.
          ELSE
            TEXT    = .FALSE.
            VECTORS = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('MESSAGES',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .TRUE.
          ELSE
            MESSAGES = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOMESSAGES',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            MESSAGES = .FALSE.
          ELSE
            MESSAGES = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('APPEND',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            APPEND = .TRUE.
          ELSE
            APPEND = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOAPPEND',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            APPEND = .FALSE.
          ELSE
            APPEND = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('OVERLAY',QUALIFIERS(1,I)(II:LENQ)) .EQ. 1 )THEN
          IF( II .EQ. 1 )THEN
            OVERLAY = .TRUE.
          ELSE
            OVERLAY = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOOVERLAY',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            OVERLAY = .FALSE.
          ELSE
            OVERLAY = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('LENGTH',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            EXTEND = .FALSE.
          ELSE
            EXTEND = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('EXTEND',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            EXTEND = .TRUE.
          ELSE
            EXTEND = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOEXTEND',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            EXTEND = .FALSE.
          ELSE
            EXTEND = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('FAST',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            FAST = .TRUE.
          ELSE
            FAST = .FALSE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE IF( INDEX('NOFAST',QUALIFIERS(1,I)(II:LENQ)).EQ.1 )THEN
          IF( II .EQ. 1 )THEN
            FAST = .FALSE.
          ELSE
            FAST = .TRUE.
          END IF
          LINE2 = LINE2(1:NLN2)//BS//QUALIFIERS(1,I)(1:LENQ)
          NLN2 = NLN2+1+LENQ
        ELSE
          CALL WARNING_MESSAGE('READ'
     &     ,'unknown qualifier: '//QUALIFIERS(1,I)(1:LENQ))
        END IF
      END DO

      OUTPUT = ( MESSAGES .OR. (NUNIT.EQ.1) .OR. ECHO )
      IF( .NOT.MESSAGES )OUTPUT = .FALSE.

      IF( ITYPE(2) .NE. 2 )THEN
        CALL ERROR_MESSAGE('READ','filename not entered')
        GO TO 92
      END IF
      fname = ' '
      CALL GET_TEXT_VARIABLE( STRINGS(2), -LENST(2), FNAME, LENF, *92 )
      IROW1 = 1
      NRNG = -1                   ! length of line range vector
C                                   -1 indicates no line range was entered
      IF( NQUAL(2) .GT. 0 )THEN   ! a line range was entered
        NRNG = 0                  ! 0 indicates a line range was entered
        SRNG = QUALIFIERS(2,1)
        LRNG = LENQL(2,1)
        CALL CH_REAL8( SRNG(1:LRNG), DUM8, *10 )
        IROW1 = INT( DUM8 )
        IF( IROW1 .LT. 1 )THEN
          CALL ERROR_MESSAGE('READ','record number < 1')
          GO TO 92
        END IF
        GO TO 20
   10   CALL GET_VARIABLE(SRNG(1:LRNG),NDIM,DUM8,IADR,N2,N3,N4,*92)
        IF( NDIM .EQ. 0 )THEN
          IROW1 = INT( DUM8 )
          IF( IROW1 .LT. 1 )THEN
            CALL ERROR_MESSAGE('READ','record number < 1')
            GO TO 92
          END IF
          GO TO 20
        ELSE IF( NDIM .EQ. 1 )THEN
          IROW1 = INT( R8D(IADR+1) )
          IF( IROW1 .LT. 1 )THEN
            CALL ERROR_MESSAGE('READ','initial record number < 1')
            GO TO 92
          END IF
          IDUM = IROW1
          NRNG = N2               ! >0 is vector range length
          DO I = 2, NRNG
            IF( INT( R8D(IADR+I) ) .LE. IDUM )THEN
              CALL ERROR_MESSAGE('READ'
     &         ,SRNG(1:LRNG)//' is not monotonically increasing')
              GO TO 92
            END IF
            IDUM = INT( R8D(IADR+I) )
          END DO
        ELSE IF( NDIM .EQ. 2 )THEN
          CALL ERROR_MESSAGE('READ',SRNG(1:LRNG)//' is a matrix')
          GO TO 92
        ELSE IF( NDIM .EQ. 3 )THEN
          CALL ERROR_MESSAGE('READ',SRNG(1:LRNG)//' is a tensor')
          GO TO 92
        END IF
      END IF
   20 IF( SCALARS )THEN
        IF( ESKIP )THEN
          CALL WARNING_MESSAGE('READ'//BS//'SCALARS'
     &     ,BS//'ERRSKIP is meaningless when reading one record')
          ESTOP = .TRUE.
        END IF
        IF( EFILL .AND. FRMTIN )THEN
          CALL WARNING_MESSAGE('READ'//BS//'SCALARS'//BS//'FORMAT'
     &     ,BS//'ERRFILL cannot be used with a format')
          ESTOP = .TRUE.
        END IF
        IF( OVERLAY )CALL WARNING_MESSAGE('READ'//BS//'SCALARS'
     &   ,BS//'OVERLAY is meaningless when reading scalars')
        IF( APPEND )CALL WARNING_MESSAGE('READ'//BS//'SCALARS'
     &   ,BS//'APPEND is meaningless when reading scalars')
        IF( .NOT.EXTEND )CALL WARNING_MESSAGE('READ'//BS//'SCALARS'
     &   ,BS//'LENGTH is meaningless when reading scalars')
        IF( .NOT.FLIPPED )CALL WARNING_MESSAGE('READ'//BS//'SCALARS'
     &   ,BS//'-FLIPPED is meaningless when reading scalars')
        CALL READ_SCALARS( FNAME(1:LENF), SRNG(1:LRNG), NRNG
     &                    ,IROW1, FCLOSE, BINARY, LINE2, NLN2, OUTPUT
     &                    ,FRMTIN, CONTINUE, MESSAGES, ESTOP, *92 )
      ELSE IF( TEXT )THEN
        IF( EFILL )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'ERRFILL is meaningless when reading text')
        IF( ESKIP )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'ERRSKIP is meaningless when reading text')
        IF( OVERLAY )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'OVERLAY is meaningless when reading text')
        IF( APPEND )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'APPEND is meaningless when reading text')
        IF( .NOT.EXTEND )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'LENGTH is meaningless when reading text')
        IF( .NOT.FLIPPED )CALL WARNING_MESSAGE('READ'//BS//'TEXT'
     &   ,BS//'-FLIPPED is meaningless when reading text')
        CALL READ_TEXT( FNAME(1:LENF), SRNG(1:LRNG), NRNG, IADR
     &                 ,IROW1, FCLOSE, BINARY, LINE2, NLN2
     &                 ,FRMTIN, CONTINUE, MESSAGES, *92 )
      ELSE IF( MATRIX )THEN
        IF( EFILL )CALL WARNING_MESSAGE('READ'//BS//'MATRIX'
     &   ,BS//'ERRFILL is meaningless when reading a matrix')
        IF( ESKIP )CALL WARNING_MESSAGE('READ'//BS//'MATRIX'
     &   ,BS//'ERRSKIP is meaningless when reading a matrix')
        IF( OVERLAY )CALL WARNING_MESSAGE('READ'//BS//'MATRIX'
     &   ,BS//'OVERLAY is meaningless when reading a matrix')
        IF( APPEND )CALL WARNING_MESSAGE('READ'//BS//'MATRIX'
     &   ,BS//'APPEND is meaningless when reading a matrix')
        IF( .NOT.EXTEND )CALL WARNING_MESSAGE('READ'//BS//'MATRIX'
     &   ,BS//'LENGTH is meaningless when reading a matrix')
        CALL READ_MATRIX( FNAME(1:LENF), SRNG(1:LRNG), NRNG
     &                   ,IROW1, FCLOSE, BINARY, LINE2, NLN2, OUTPUT
     &                   ,FRMTIN, CONTINUE, FLIPPED, *92 )
      ELSE                         ! must be \VECTORS
        IF( BINARY .AND. EFILL )CALL WARNING_MESSAGE(
     &    'READ'//BS//'VECTORS'
     &   ,BS//'ERRFILL cannot be used with '//BS//'UNFORMATTED')
        IF( BINARY .AND. ESKIP )CALL WARNING_MESSAGE(
     &   'READ'//BS//'VECTORS'
     &    ,BS//'ERRSKIP cannot be used with '//BS//'UNFORMATTED')
        IF( .NOT.EXTEND .AND. OVERLAY )
     &   CALL WARNING_MESSAGE('READ'//BS//'VECTORS'
     &   ,BS//'-EXTEND cannot be used with '//BS//'OVERLAY')
        IF( .NOT.EXTEND .AND. APPEND )CALL WARNING_MESSAGE(
     &   'READ'//BS//'VECTORS'
     &    ,BS//'-EXTEND cannot be used with '//BS//'APPEND')
        IF( .NOT.FLIPPED )CALL WARNING_MESSAGE('READ'//BS//'VECTORS'
     &   ,BS//'-FLIPPED is meaningless when reading vectors')
        CALL READ_VECTORS( FNAME(1:LENF), SRNG(1:LRNG), NRNG, IADR
     &                    ,IROW1, FCLOSE, BINARY, LINE2, NLN2, OUTPUT
     &                    ,FRMTIN, CONTINUE, MESSAGES, OVERLAY, EXTEND
     &                    ,ESKIP, ESTOP, fast, *92 )
      END IF
      IF( STACK )WRITE(IOUT_UNIT,80)LINE2(1:NLN2)
   80 FORMAT(' ',A)
      RETURN
   92 RETURN 1
      END
