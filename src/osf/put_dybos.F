      SUBROUTINE PUT_DYBOS( COMMAND, YBS, * )

C  This routine is intended only for ALPHA-OSF1
C  puts YBOS scattered points (dots) into PHYSICA variables

      INTEGER*4 NTOTP, NTOTQ, MXREPL, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXFITP
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXREPL=103,   MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=1000,   MXLAB=100,   MXDO=50,      MXIF=100
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20)
      PARAMETER (MXFITP=25)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE
C Here is the new include file......everything else stays the same. Can you let
C me know when you have completed the compilations ?
C Oh, I am not sure how you map to the shared memory segment or
C wether you call malloc, but the new structure size is
C      parameter (ybs_structure_size =  3230617)
C  Gertjan
c
c Addition Jan'94(GJH)  added another equivalence to YBS.JW, YBS.RW to
c        access individual bytes. This should not affect anything.
c      
c Addition: 26-Oct-93 (KJR) the /off_tests/ structure used by dotio and
c           programs that call that subroutine (dot, ...) was included
c           at the end of the YBS structure and called /dot_extra_st/.
c           This change necessitated an increase in the ybs_structure_size.

c NOTE: (KJR). The size of the YBS structure is specified as the parameter
c    "ybs_structure_size". Any additions to the structure must be reflected
c    in a corresponding change to this parameter.
c
      integer ybs_structure_size
c     parameter (ybs_structure_size =  2290149)
c     parameter (ybs_structure_size =  2530945)
c     parameter (ybs_structure_size =  2531745)
c     parameter (ybs_structure_size =  2788613)
      parameter (ybs_structure_size =  3230617)
c
c NOTE: (GJH).   Note that any new additions to the
c    structure must be declared between the variables
c    begin_of_ybs_strucuture   and  end_of_ybs_strucuture
c    You can run the program ybs_size to find out the size of the
c    structure. Basically, in fortran it is given by
c    start= %LOC(ybs.begin_of_ybs_structure)
c    end  = %LOC(ybs.end_of_ybs_structure)
c    size = end-start+1          in BYTES
c
c    Revised  25-Oct-94 to allow more scalers and more dots.
      
      integer*4 ybos_size
      parameter (ybos_size=24000)        ! size of basic ybos array
c      
      integer test_size
      parameter (test_size = 1000)        ! number of different tests
c
      integer test_room
      parameter (test_room = 7500)  ! size of array containing tests defs.
c      
      integer test_counters
      parameter (test_counters = 1000) ! size of the array containing test 
c                                      ! counters
      integer number_blocks
      parameter (number_blocks = 20)   ! Number of test blocks
c      
      integer data_size
      parameter (data_size = 17000)    ! Number of different data symbols
c
      integer num_banks
      parameter (num_banks = 200)      ! Number of different banks
c
      integer n_scal,n_bins,n_histos
      parameter (n_scal = 300)         ! Number of scalers
      parameter (n_bins = 160000)      ! Number of bins available for histos
      parameter (n_histos = 2000)      ! Number of histograms
c
      integer user_size
      parameter (user_size = 2000)     ! size of user arrays
c
      integer calib_size                
      parameter (calib_size = 10000)   ! size of calib and cons arrays
c
      integer n_dots
      parameter (n_dots = 100)         ! number of dot plots
c
      integer n_dot_bins
      parameter (n_dot_bins = 150000)  ! total number of dots plotted
c
      integer n_dplot_cols
      parameter (n_dplot_cols = 4)     ! number of dotplot colours
c     
c  Here are the components of the /test_stuff/ data structure.
c     
      structure /test_stuff/
      character*16 tname               ! variable containing the test name
      integer*4 res_tests              ! Number of tests reserved in this test
      integer*4 num_tests              ! Actual number of true tests
      integer*4 test_pos               ! position in true_test array of results
      integer*4 count_pos              ! position in counters array of counter
      integer*4 test_info              ! position in test_defs array of def.
      integer*4 low_test               ! smallest identifier in a vector test
      end structure
c
c  Here are the components of the /data_symbol/ data structure
c      
      structure /data_symbol/
      character*16 dname             ! Variable containing data symbol name
      character*4 bankname           ! Bank name of this data symbol
      character*1 data_typ           ! Data type of this symbol
      integer*4 raw_ident            ! Identifier of this symbol
      integer*4 loc_in_bank          ! location in the bank of this symbol
      integer*4 loc_in_data          ! location in ybos array of this symbol
      integer*4 bank_loc         ! location in bank structure of symbols' bank
      end structure
c
c  Bank structure
c
      structure /bank_stuff/
      integer*4 bank_names          ! Bank name
      integer*4 bank_size           ! Size of this bank
      integer*4 bank_mask           ! Mask on this bank
      integer*4 bank_shift          ! shift on this bank
      integer*4 bank_pointers   !location in symbol structure of first element 
      integer*4 ybos_pointer        ! location in ybos array of this bank
      integer*4 dyn_size            ! actual size of the ybos bank
      end structure
c
c Calibration data structure
c
      structure /cal_stuff/
      character*16 cal_name       ! name of block of calibration data
      integer*4 cons_pointer      ! pointer to the start of data in cons array
      integer*4 cal_size          ! size of block of calibration data
      end structure
c
c Histogram structure      
c
      structure /histo_stuff/
      real*4 xlow               ! low cut of this histogram
      real*4 xhi                ! high cut of this histogram
      real*4 xunder             ! underflow counter
      real*4 xover              ! overflow counter
      integer*4 test_pointer    ! location in test structure of test on histo
      integer*4 data_pointer    ! location in symbol structure of data in histo
      integer*4 n_bins          ! number of bins in the histo
      integer*4 start_address   ! first bin in histo array
      integer*4 weight_pointer !location in symbol structure of weight of histo
      character*16 title_name   ! title of histo
      character*16 title_desc   ! 
      character*16 test_name    ! test name on histo
      character*16 data_name    ! data name of histo
      character*16 histo_type   ! histogram type
      end structure
c
c  dot plot colour substructure
c
      structure /col_stuff/
      integer*4 col_test         ! index of test for this colour
      integer*4 col_def          ! number of bins reserevd for this colour
      integer*4 col_pointer      ! pointer to first bin of this colour
      integer*4 col_off          ! offset from pointer of next free bin
      end structure
c
c dot plot main structure
c      
      structure /dot_stuff/
      character*16 dot_name      ! name of the dotplot
      character*4 dot_type       ! defines the type of variables of the dotplot
      integer*4 x_pointer        ! pointer to x-variable
      integer*4 y_pointer        ! pointer to y-variable
      real*4 x_low               ! minimum value of x-variable
      real*4 x_high              ! maximum value of x-variable
      real*4 y_low               ! minimum value of y-variable
      real*4 y_high              ! maximum value of y-variable
      integer*4 over_all         ! pointer to the overall_test of the dotplot
      integer*4 n_colours        ! number of colours defined in this dotplot
      record /col_stuff/ col_st(n_dplot_cols)        ! colour subsructure
      end structure
c
c names of tests and x and y variables of each dot plot    
c
      structure /dot_extra_st/
        character*16 x_symbol
        character*16 y_symbol
        character*16 stest_all
        character*16 sdot_name
        character*16 sctest(n_dplot_cols)
      end structure
c     
c  This is the code for the full SUSIYBOS data structure
c
      structure /ybos_all/
      
c dummy variables to indicate size

      integer*4 begin_of_ybs_structure
      
      union     ! YBOS structures
         map
            integer*4 jw(ybos_size)       ! YBOS array
         end map
         map
            integer*2 jw2(2*ybos_size)    ! integer*2 YBOS ayyray
         end map
         map
            real*4  rw(ybos_size)         ! array for access to real*4
         end map
         map
            character*1 cw(4*ybos_size)   ! array for access to bytes.
         end map
      end union
c
      record /test_stuff/ test_st(test_size)   ! test structure
      integer*4 true_tests(test_room)          ! array containing true tests
      integer*4 number_true                    ! number of true tests
      integer*4 counters(test_counters)        ! array containing counters
      integer*4 number_counters                ! number of counters
      integer*4 test_defs(test_room)           ! array containing test defs.
      integer*4 number_defs                    !next free position in test_defs
      integer*4 order_tests(test_size)         !Array giving alphabetic order
      integer*4 blocks(number_blocks) !loc. in test struc. of 1st test of block
      integer*4 num_blocks                     ! number of blocks defined
c
      record /data_symbol/ data_sym(data_size) ! data symbol structure
      integer*4 order(data_size)               ! array giving alphabetic order
      integer*4 hasha(128)                     ! hash array
      integer*4 num_order                      ! number of entries in order
      integer*4 tot_symbols                    ! number of symbols defined
      integer*4 sym_pointers(data_size)        !
      integer*4 num_pointers                   !
c
      record /bank_stuff/ bank_st(num_banks)   ! Bank structure
      integer*4 number_banks                   ! number of defined banks
c     
      record /cal_stuff/ cal_st(num_banks)     ! Calibration data structure
      integer*4 number_blocks                  ! number of calibration blocks
c
      record /histo_stuff/ histo_st(n_histos)  ! histo structure
      real*4  	scl(n_scal)                    ! accumlated scaler array
      real*4  	cur_scl(n_scal)                ! current scaler array
      real*4    num_scaler                     ! number of defined scalers
      logical*1	new_scaler_f                   ! flag if scalers are present
      real*4	histo(n_bins)                  ! histograms
      integer*4 number_bins                    ! number of defined bins
      integer*4 number_histos                  ! number of defined histos
c
      record /dot_stuff/ dot_st(n_dots)    ! dotplot structure
      real*4 xdot(n_dot_bins)              ! x-values of dotplots
      real*4 ydot(n_dot_bins)              ! y-values of dotplots
      integer*4 number_dplots              ! number of defined dotplots
      integer*4 number_dbins               ! number of defined dotplot bins
c     
c  Here are the process control data
c
      
      integer*4 anal_pid          ! process id number
      character*16 expt_name      ! experiment name
      character*16 batch          ! variable for foreground/background
      character*16 geant_mode     ! geant_map
      character*16 isuspend       ! variable to suspend data analysis
      character*16 norm_test      ! normalization test
      character*80 data_input     ! present data source file
      character*80 data_input_list(20) !  list of data source files to analyze
      character*80 data_output1   ! output file 1
      character*80 data_output2   ! output file 2
      character*80 data_output3   ! output file 3
      character*80 data_output4   ! output file 4
      character*80 targ_mat       ! target material
      real*4 targ_pol             ! target polarization
      real*4 beam_mom             ! beam momentum
      real*4 beam_mom_width       ! beam momentum spread
      character*80 beam_part      ! beam particle
      real*4 chaos_field          ! central magnetic field of CHAOS
      real*4 chaos_ang            ! rotation angle of CHAOS
      real*4 chaos_dist           ! CHAOS displacement from zero
      real*4 cpu_time             ! CPU time used by analyzer
      real*4 start_time           !
      character*80 trigger1       ! 1st level trigger description
      character*80 trigger2       ! 2nd level trigger description
      character*80 trigger3       ! 3rd level trigger description
      character*80 comment        ! comment
      integer*4 evnt_no           ! event number of current event
      integer*4 evnt_typ          ! event type of current event
      integer*4 experiment        ! experiment number
      integer*4 on_off_status     ! variable for online/offline switch
      integer*4 i_runno           ! present run number.
      integer*4 i_runno_list(20)  ! list of run numbers to analyze
      integer*4 i_analyzed        ! number of events seen by the analyzer
      integer*4 no_files_to_ana   ! no of runs to analyze in one go
      integer*4 inp_file_cnt      ! counter pointing to run no to analzye
      integer*4 n_evt_max         ! max. number of events to process
      integer*4 n_scl_rec         ! number of scaler events
      integer*4 n_scl_max         ! max. number of scaler events to process
      integer*4 bad_evt_type      ! number of bad events
      integer*4 llunit            ! ybos input unit number
      integer*4 out_unit1         ! ybos unit number for output channel 1
      integer*4 out_unit2         ! ybos unit number for output channel 2
      integer*4 out_unit3         ! ybos unit number for output channel 3
      integer*4 out_unit4         ! ybos unit number for output channel 4
      integer*4 bfield_map        ! number corrosponding to choice of maps
      logical*1 clhis             ! flag to clear histos at beginning of run
      logical*1 cldot             ! flag to clear dotplots at beginning of run
      logical*1 clscl             ! flag to clear scalers at beginning of run
      logical*1 cltst             ! flag to clear tests at beginning of run
      logical*1 clhit             ! flag to clear history at beginning of run
      logical*1 svhis             ! flag to save histos at end of run
      logical*1 svdot             ! flag to save dotplots at end of run
      logical*1 svscl             ! flag to save scalers at end of run
      logical*1 svtst             ! flag to save tests at end of run
      logical*1 svhit             ! flag to save history during run
      
c
c  Now user areas to interchange data through shared memory
c
      integer*4 user_i4(user_size)  
      real*4 user_r4(user_size)
      logical*1 user_l1(user_size)
c
c  Next some user arrays for variables not in YBOS structures
c
      real*4 cons(calib_size)
      real*4 calib(calib_size)
c
c  The extra dot stuff (KJR 26-Oct-93)
c
      record /dot_extra_st/ dot_extra(n_dots)
c  End of mamoth stucture
      logical*1 end_of_ybs_structure
        
      end structure
        record /ybos_all/ ybs

      CHARACTER*(*) COMMAND

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      LOGICAL*1 L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

      CHARACTER*80 STRING
      CHARACTER*15 SOUT
      CHARACTER*12 NAME
      CHARACTER*1  BS
      INTEGER*4    IDOT, DPLOTS, DBINS, IXDOT, IYDOT, LS
     &            ,IXLOW, IXHIH, IYLOW, IYHIH, STST1, STST2
     &            ,CDEF, CPTR, COFF, NOLDS, LENT, TLENT, LN, LENS
     &            ,J, K, SADDR, IDX1(4), IDX2(4), IDX3(4)
      DATA BS /92/, IDX1 /0,0,0,0/, IDX2 /0,0,0,0/, IDX3 /0,0,0,0/
CCC 
      LN = LEN(COMMAND)
      STRING = COMMAND(1:LN)//BS//'YBOS'
      LS = LN+5

      DPLOTS = YBS.NUMBER_DPLOTS               ! & of dot plots
      DBINS  = YBS.NUMBER_DBINS                ! total & of dots
      IF( DPLOTS .LE. 0 )THEN
        CALL WARNING_MESSAGE(STRING(1:LS),'number of dot plots <= 0')
        RETURN
      END IF
      IF( DBINS .LE. 0 )THEN
        CALL WARNING_MESSAGE(STRING(1:LS),'total number of dot <= 0')
        RETURN
      END IF
      CALL GET_TEMP_SPACE(IXDOT,DBINS,8,*99)
      CALL GET_TEMP_SPACE(IYDOT,DBINS,8,*99)
      CALL GET_TEMP_SPACE(IXLOW,DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(IXHIH,DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(IYLOW,DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(IYHIH,DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(STST1,4*DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(STST2,4*DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(CDEF,4*DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(CPTR,4*DPLOTS,8,*99)
      CALL GET_TEMP_SPACE(COFF,4*DPLOTS,8,*99)
      NOLDS = 0
      DO IDOT = 1, DPLOTS
        LENT = LENSIG(YBS.DOT_EXTRA(IDOT).X_SYMBOL)
        NAME = 'X_SYMBOL'
        LN = 8
        IF( LENT .GT. NOLDS )THEN
          CALL GET_MORE_SPACE(SADDR,NOLDS,LENT,1,*99)
          NOLDS = LENT
        END IF
        DO J = 1, LENT
          L1D(SADDR+J) = ICHAR(YBS.DOT_EXTRA(IDOT).X_SYMBOL(J:J))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), IDOT, 0, IDX2
     &   ,SADDR, LENT, STRING(1:LS), *92 )
        LENT = LENSIG(YBS.DOT_EXTRA(IDOT).Y_SYMBOL)
        NAME = 'Y_SYMBOL'
        LN = 8
        IF( LENT .GT. NOLDS )THEN
          CALL GET_MORE_SPACE(SADDR,NOLDS,LENT,1,*99)
          NOLDS = LENT
        END IF
        DO J = 1, LENT
          L1D(SADDR+J) = ICHAR(YBS.DOT_EXTRA(IDOT).Y_SYMBOL(J:J))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), IDOT, 0, IDX2
     &   ,SADDR, LENT, STRING(1:LS), *92 )
        LENT = LENSIG(YBS.DOT_EXTRA(IDOT).STEST_ALL)
        NAME = 'STEST_ALL'
        LN = 9
        IF( LENT .GT. NOLDS )THEN
          CALL GET_MORE_SPACE(SADDR,NOLDS,LENT,1,*99)
          NOLDS = LENT
        END IF
        DO J = 1, LENT
          L1D(SADDR+J) = ICHAR(YBS.DOT_EXTRA(IDOT).STEST_ALL(J:J))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), IDOT, 0, IDX2
     &   ,SADDR, LENT, STRING(1:LS), *92 )
        LENT = LENSIG(YBS.DOT_ST(IDOT).DOT_NAME)
        NAME = 'DOT_NAME'
        LN = 8
        IF( LENT .GT. NOLDS )THEN
          CALL GET_MORE_SPACE(SADDR,NOLDS,LENT,1,*99)
          NOLDS = LENT
        END IF
        DO J = 1, LENT
          L1D(SADDR+J) = ICHAR(YBS.DOT_ST(IDOT).DOT_NAME(J:J))
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), IDOT, 0, IDX2
     &   ,SADDR, LENT, STRING(1:LS), *92 )

        NAME = 'SCTEST'
        LN = 6
        TLENT = 0
        DO K = 1, 4
          LENT = MAX(1,LENSIG(YBS.DOT_EXTRA(IDOT).SCTEST(K)))
          TLENT = TLENT+LENT
        END DO
        IF( TLENT .GT. NOLDS )THEN
          CALL GET_MORE_SPACE(SADDR,NOLDS,TLENT,1,*99)
          NOLDS = TLENT
        END IF
        TLENT = 0
        DO K = 1, 4
          R8D(STST1+IDOT+(K-1)*DPLOTS) = DBLE(FLOAT(TLENT+1))
          LENT = MAX(1,LENSIG(YBS.DOT_EXTRA(IDOT).SCTEST(K)))
          R8D(STST2+IDOT+(K-1)*DPLOTS) = DBLE(FLOAT(TLENT+LENT))
          DO J = 1, LENT
            L1D(SADDR+J+TLENT) =
     &       ICHAR(YBS.DOT_EXTRA(IDOT).SCTEST(K)(J:J))
          END DO
          TLENT = TLENT+LENT
        END DO
        CALL PUT_TEXT_VARIABLE( NAME(1:LN), IDOT, 0, IDX2
     &   ,SADDR, TLENT, STRING(1:LS), *92 )

        R8D(IXLOW+IDOT) = DBLE(YBS.DOT_ST(IDOT).X_LOW)
        R8D(IXHIH+IDOT) = DBLE(YBS.DOT_ST(IDOT).X_HIGH)
        R8D(IYLOW+IDOT) = DBLE(YBS.DOT_ST(IDOT).Y_LOW)
        R8D(IYHIH+IDOT) = DBLE(YBS.DOT_ST(IDOT).Y_HIGH)
        DO K = 1, 4
          R8D(CDEF+IDOT+(K-1)*DPLOTS) =
     &     YBS.DOT_ST(IDOT).COL_ST(K).COL_DEF
          R8D(CPTR+IDOT+(K-1)*DPLOTS) =
     &     YBS.DOT_ST(IDOT).COL_ST(K).COL_POINTER
          R8D(COFF+IDOT+(K-1)*DPLOTS) =
     &     YBS.DOT_ST(IDOT).COL_ST(K).COL_OFF
        END DO
      END DO
      DO J = 1, DBINS
        R8D(IXDOT+J) = YBS.XDOT(J)
        R8D(IYDOT+J) = YBS.YDOT(J)
      END DO

      NAME = 'DRUNNO'
      LN = 6
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,0, DBLE(FLOAT(YBS.I_RUNNO)), 0,0,0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'XDOT'
      LN = 4
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IXDOT, DBINS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'YDOT'
      LN = 4
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IYDOT, DBINS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'XLOW'
      LN = 4
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IXLOW, DPLOTS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'XHIGH'
      LN = 5
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IXHIH, DPLOTS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'YLOW'
      LN = 4
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IYLOW, DPLOTS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'YHIGH'
      LN = 5
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,1, 0.0D0, IYHIH, DPLOTS, 0,0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'SCTEST_START'
      LN = 12
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,2, 0.0D0, STST1, DPLOTS, 4, 0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'SCTEST_END'
      LN = 10
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,2, 0.0D0, STST2, DPLOTS, 4, 0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'COL_DEF'
      LN = 7
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,2, 0.0D0, CDEF, DPLOTS, 4, 0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'COL_POINTER'
      LN = 11
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,2, 0.0D0, CPTR, DPLOTS, 4, 0
     & ,STRING(1:LS), STRING(1:LS), *92 )
      NAME = 'COL_OFF'
      LN = 7
      CALL PUT_VARIABLE( NAME(1:LN), 0,IDX1,IDX2,IDX3
     & ,2, 0.0D0, COFF, DPLOTS, 4, 0
     & ,STRING(1:LS), STRING(1:LS), *92 )

   92 CALL DELETE_TEMP_SPACE
      RETURN
   99 CALL ERROR_MESSAGE(STRING(1:LS),'modifying dynamic array space')
      RETURN
      END   
