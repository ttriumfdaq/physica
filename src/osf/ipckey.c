/* ipckey -- returns a key for use in create_shm, mapt_to_shm or remove_shm */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>

key_t ipckey( n )
     int n;
{
  key_t key;
  char *ybos_user;

  /* make key */

  if(n<0 || n>15)
    printf("%%ipckey-E, bad segment number.\n");
  else if( (ybos_user = getenv("ybos_user")) == (char *) 0)
    printf("%%ipckey-E, ybos_user undefined.\n");
  else if( (key = ftok( ybos_user, (char) (n<<4)+1)) == (key_t) -1)
    printf("%%ipckey-E, unable to make key.\n");
  else
    return key;
  exit(1);
}
