      SUBROUTINE EXECUTE( CLINE, IFLAG, * )
C
C  PHYSICA commands will be read from a file instead of from the terminal
C
C  The order in which things are done:
C    1) parameters input with the EXECUTE command are stored in PARAM common
C    2) read a line from the file 
C    3) continuation lines are fixed up, i.e., made into single line
C    4) the line is stored, along with start and end pointers
C    5) any ?'s that were not supplied with the EXEC command are now requested
C
      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*1 COMENT   ! comment delimiter
      LOGICAL*4   IGNORE_COMENT
      COMMON /COMENTC/ IGNORE_COMENT, COMENT

      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      CHARACTER*1024 PARMCHAR(0:MXPARM,MXFILE)
      CHARACTER*1   PPAR
      INTEGER*4     PARMCOUNT(MXFILE), PARMMAX(MXFILE)
     &             ,PARMTYPE(0:MXPARM,MXFILE), PARMCLEN(0:MXPARM,MXFILE)
      REAL*8        PARMREAL(0:MXPARM,MXFILE)
      COMMON /PARAMS/ PARMREAL, PARMCLEN, PARMTYPE, PARMCOUNT, PARMMAX
     &               ,PPAR, PARMCHAR
      CHARACTER*1024 EXELAB(MXFILE,MXLAB)
      INTEGER*4     LINE_NUM(MXFILE), EXEC_INIT(MXFILE)
     &             ,EXEC_ALLOC(MXFILE), LINE_COUNT(MXFILE)
     &             ,LCH(MXFILE,MXLINE,2), NLAB(MXFILE)
     &             ,LINLAB(MXFILE,MXLAB), DOCNT(MXFILE,MXDO)
     &             ,NDOS(MXFILE), DOLINE(MXFILE,MXDO)
     &             ,LDOEND(MXFILE,MXDO), NIFS(MXFILE)
     &             ,IFLINE(MXFILE,MXIF), LIFEND(MXFILE,MXIF)
      COMMON /UNIT_LINE2/ LINE_NUM, EXEC_INIT, EXEC_ALLOC, LINE_COUNT
     &                   ,LCH, NLAB, LINLAB, NDOS, DOCNT, DOLINE
     &                   ,LDOEND, NIFS, IFLINE, LIFEND, EXELAB

      CHARACTER*(*) CLINE
      INTEGER*4     IFLAG

      LOGICAL*4 ECHOS(MXFILE)
      COMMON /ECHO2/ ECHOS

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      REAL*8    R8D(1)
      REAL*4    R4D(1)
      INTEGER*4 I4D(1)
      INTEGER*2 I2D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, R4D, I2D, L1D)

C   local variables

      CHARACTER*1024 FNAME, LINE, LNE, LIN2, LUPPER
#ifdef unix 
      CHARACTER*1024 TEMP
#endif
      CHARACTER*15  SOUT
      CHARACTER*1   TAB, CQT, OQT
      REAL*8        X8
      INTEGER*4     IQT, IFCNT, IDOCNT, IFSTACK(MXIF), DOSTACK(MXDO)
      LOGICAL*4     FOUNDIT, ASK
#ifdef VMS
      LOGICAL*4     DOT_IN
      INCLUDE '($SSDEF)'
#endif
CCC
      OQT = CHAR(96)
      CQT = CHAR(39)
      TAB = CHAR(9)

      NLINE = LEN(CLINE)
      CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      IF( NUNIT .EQ. 1 )THEN

C   STACK is set .FALSE. when executing the first file so STACK1 is
C   used to reset STACK after finishing with the execute file

        STACK1 = STACK
        STACK  = .FALSE.
      END IF
      NUNIT = NUNIT+1
      IF( NUNIT .GT. MXFILE )THEN
        CALL INTEGER_TO_CHAR(MXFILE,.FALSE.,SOUT,LENS)
        CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &   'max. # of nested files = '//SOUT(1:LENS))
        GO TO 9998                             
      END IF
      DO I = 1, PARMMAX(NUNIT)
        PARMCHAR(I,NUNIT) = ' '
        PARMCLEN(I,NUNIT) = 0
        PARMTYPE(I,NUNIT) = 0
      END DO
      PARMMAX(NUNIT) = 0                                            
      ECHOS(NUNIT-1) = ECHO
      CALL FIND_UNIT( IIUNIT )  ! get a free logical unit number
#ifdef VMS
      IF( IFLAG .EQ. -1 )THEN   ! special case, executing initialization file
        ISTAT = LIB$SYS_TRNLOG('PHYSICA$INIT',,FNAME)
        IF( .NOT.ISTAT .OR. (ISTAT .EQ. SS$_NOTRAN) )FNAME = ' '
        LENF = LENSIG( FNAME )
        IF( LENF .EQ. 0 )GO TO 98
        CALL SCAN_FOR_DOT( FNAME, LENF, DOT_IN, *100 )
        IF( .NOT.DOT_IN )THEN
          FNAME = FNAME(1:LENF)//'.'//DEFEXT(1:LDEFEXT)
          LENF = LENF+LDEFEXT+1
        END IF
        OPEN(FILE=FNAME(1:LENF),UNIT=IIUNIT,READONLY
     &   ,IOSTAT=IERROR,STATUS='OLD',ERR=98)
        CALL RITE('executing initialization file: '//FNAME(1:LENF))
        GO TO 2030
   98   NUNIT = NUNIT - 1
        IF( NUNIT .EQ. 1 )STACK = STACK1
  100   RETURN
      END IF
#else
      IF( IFLAG .EQ. -1 )THEN   ! special case, executing initialization file
        CALL GETENV('PHYSICA_INIT',FNAME)
        LENF = LENSIG( FNAME )
        IF( LENF .NE. 0 )GO TO 96
        FNAME = '.physicarc'
        LENF = 10
        INQUIRE( FILE=FNAME(1:LENF), EXIST=FOUNDIT, ERR=94 )
        IF( FOUNDIT )GO TO 96
   94   CALL GETENV('HOME',FNAME)
        LENF = LENSIG( FNAME )
        IF( LENF .EQ. 0 )GO TO 98
        FNAME(1:LENF+11) = FNAME(1:LENF)//'/.physicarc'
        LENF = LENF+11
        INQUIRE( FILE=FNAME(1:LENF), EXIST=FOUNDIT, ERR=98 )
        IF( FOUNDIT )GO TO 96
        GO TO 98
   96   OPEN(FILE=FNAME(1:LENF),UNIT=IIUNIT,IOSTAT=IERROR,STATUS='OLD'
     &   ,ERR=98)
        CALL RITE('executing initialization file: '//FNAME(1:LENF))
        GO TO 2030
   98   NUNIT = NUNIT - 1
        IF( NUNIT .EQ. 1 )STACK = STACK1
        RETURN
      END IF
#endif

C    All other executable files go through here
C    Determine the file name

      NFLD = NTOTP
      CALL PARSE_INPUT_LINE( CLINE(1:NLINE), *9998 )
      IF( ITYPE(2) .NE. 2 )THEN
        CALL ERROR_MESSAGE('EXECUTE','expecting macro filename')
        GO TO 9998
      END IF
      fname = ' '
      CALL GET_TEXT_VARIABLE( STRINGS(2), -LENST(2), FNAME, LENF
     & ,*9998 )
#ifdef VMS
      ISTAT = LIB$SYS_TRNLOG(FNAME(1:LENF),,FNAME) ! filename a logical name?
      CALL LEFT_JUSTIFY(FNAME)   ! chop leading blanks, if any
      LENF = LENSIG(FNAME)
      IF( LENF .EQ. 0 )THEN
        CALL ERROR_MESSAGE('EXECUTE','macro filename is blank')
        GO TO 9998
      END IF

C   Append default file extension if needed

      CALL SCAN_FOR_DOT( FNAME, LENF, DOT_IN, *9998 )
      IF( .NOT.DOT_IN )THEN
        FNAME(1:LENF+LDEFEXT+1) = FNAME(1:LENF)//'.'//DEFEXT(1:LDEFEXT)
        LENF = LENF+LDEFEXT+1
      END IF
      INQUIRE( FILE=FNAME(1:LENF), EXIST=FOUNDIT, ERR=1970 )
      IF( FOUNDIT )GO TO 2000
 1970 IF( INDEX(FNAME(1:LENF),'[').NE.0 )GO TO 2011
      IF( INDEX(FNAME(1:LENF),':').NE.0 )GO TO 2011

C   Filename doesn't contain disk or directory specification

      ISTAT = LIB$SYS_TRNLOG( 'PHYSICA$LIB',, LIN2 )
      IF( .NOT.ISTAT .OR. (ISTAT.EQ.SS$_NOTRAN) )GO TO 2011

C   PHYSICA$LIB can be translated, may be a search list

CC      LEN2 = LENSIG(LIN2)
CC      INQUIRE( FILE=LIN2(1:LEN2)//FNAME(1:LENF), EXIST=FOUNDIT, ERR=2011 )
CC      IF( .NOT.FOUNDIT )GO TO 2011
CC      FNAME(1:LENF+LEN2) = LIN2(1:LEN2)//FNAME(1:LENF)
CC      LENF = LENF+LEN2
      FNAME(1:12+LENF) = 'PHYSICA$LIB:'//FNAME(1:LENF)
      LENF = 12+LENF
 2000 OPEN(UNIT=IIUNIT,FILE=FNAME(1:LENF),STATUS='OLD',READONLY
     & ,IOSTAT=IERROR,ERR=2011)
      IF( STACK )THEN
        LINE = '@'//STRINGS(2)(1:LENST(2))
        NLINE = 1+LENST(2)
      END IF
#else
      TEMP = FNAME(1:LENF)
      CALL GETENV(FNAME(1:LENF),FNAME)  ! filename an environment variable?
      IF( FNAME .EQ. ' ' )FNAME = TEMP(1:LENF)
      CALL LEFT_JUSTIFY(FNAME)   ! chop leading blanks, if any
      LENF = LENSIG(FNAME)
      IF( LENF .EQ. 0 )THEN
        CALL ERROR_MESSAGE('EXECUTE','macro filename is blank')
        GO TO 9998
      END IF
      INQUIRE( FILE=FNAME(1:LENF), EXIST=FOUNDIT, ERR=1970 )
      IF( FOUNDIT )GO TO 2000
 1970 TEMP = FNAME(1:LENF)//'.'//DEFEXT(1:LDEFEXT)
      LTMP = LENF+1+LDEFEXT
      INQUIRE( FILE=TEMP(1:LTMP), EXIST=FOUNDIT, ERR=1980 )
      IF( FOUNDIT )THEN
        FNAME(1:LTMP) = TEMP(1:LTMP)
        LENF = LTMP
        GO TO 2000
      END IF
 1980 IF( INDEX(FNAME(1:LENF),'/').NE.0 )GO TO 2011
      CALL GETENV( 'PHYSICA_LIB', LIN2 )
      IF( LIN2 .EQ. ' ' )GO TO 2011  
      LEN2 = LENSIG(LIN2)

C   PHYSICA_LIB can be translated

      TEMP = LIN2(1:LEN2)//'/'//FNAME(1:LENF)
      LTMP = LEN2+1+LENF
      INQUIRE( FILE=TEMP(1:LTMP), EXIST=FOUNDIT, ERR=1992 )
      IF( FOUNDIT )THEN
        FNAME(1:LTMP) = TEMP(1:LTMP)
        LENF = LTMP
        GO TO 2000
      END IF
 1992 TEMP = LIN2(1:LEN2)//'/'//FNAME(1:LENF)//'.'//DEFEXT(1:LDEFEXT)
      LTMP = LEN2+1+LENF+1+LDEFEXT
      INQUIRE( FILE=TEMP(1:LTMP), EXIST=FOUNDIT, ERR=1980 )
      IF( .NOT.FOUNDIT )GO TO 2011
      FNAME(1:LTMP) = TEMP(1:LTMP)
      LENF = LTMP
 2000 OPEN(UNIT=IIUNIT,FILE=FNAME(1:LENF),STATUS='OLD',IOSTAT=IERROR
     & ,ERR=2011)
      IF( STACK )THEN
        LINE = '@'//STRINGS(2)(1:LENST(2))
        NLINE = 1+LENST(2)
      END IF
#endif
      
 2030 parmchar(0,nunit) = fname(1:lenf)
      parmclen(0,nunit) = lenf
      parmtype(0,nunit) = 2

C   NPMMX is the number of parameters that were entered with the execute
C   command and the file name, e.g.  PHYSICA: EXECUTE FILE.COM X Y Z
C   has 3 parameters, X, Y, and Z

      NPMMX = NFLD - 2
      DO I = 1, NPMMX
        IF( ITYPE(I+2) .EQ. 0 )THEN    ! No value for parameter
          PARMTYPE(I,NUNIT) = 0
          IF( STACK1 )THEN
            LINE = LINE(1:NLINE)//',,'
            NLINE = NLINE+2
          END IF
        ELSE IF( ITYPE(I+2) .EQ. 1 )THEN              ! Real parameter
          PARMREAL(I,NUNIT) = REALS(I+2)
          PARMTYPE(I,NUNIT) = 1
          IF( STACK1 )THEN
            CALL REAL8_TO_CHAR(REALS(I+2),.FALSE.,SOUT,LENS)
            LINE = LINE(1:NLINE)//' '//SOUT(1:LENS)
            NLINE = NLINE+1+LENS
          END IF
        ELSE IF( ITYPE(I+2) .EQ. 2 )THEN              ! String parameter
          PARMCHAR(I,NUNIT) = STRINGS(I+2)(1:LENST(I+2))
          PARMCLEN(I,NUNIT) = LENST(I+2)
          PARMTYPE(I,NUNIT) = 2
          IF( STACK1 )THEN
            LINE = LINE(1:NLINE)//' '//STRINGS(I+2)(1:LENST(I+2))
            NLINE = NLINE+1+LENST(I+2)
          END IF
        ELSE IF( ITYPE(I+2) .EQ. 3 )THEN              ! = sign
          PARMCHAR(I,NUNIT) = '='
          PARMCLEN(I,NUNIT) = 1
          PARMTYPE(I,NUNIT) = 3
          IF( STACK1 )THEN
            LINE = LINE(1:NLINE)//' ='
            NLINE = NLINE+2
          END IF
        END IF
      END DO

C   Initial allocation

      NALLOC = 512
      CALL CHSIZE(EXEC_INIT(NUNIT),0,NALLOC,1,*99)
      ITOTAL = 0

C    Find out how many parameters there are in the file by reading each
C    line of the file and, if it is not a comment line, then search the
C    line for ?'s

      PARAM_COUNT = 0
      LINE_COUNT(NUNIT) = 0
      NDOS(NUNIT) = 0           ! # of DO blocks
      NIFS(NUNIT) = 0           ! # of IF blocks
      IDOCNT = 0                ! DO loop stack counter
      IFCNT = 0                 ! IF block stack counter
    8 NLNE = 0
   10 READ(IIUNIT,50,END=70,ERR=991)LIN2
   50 FORMAT(A)
      NLNE2 = LENSIG(LIN2)
      IF( NLNE2 .EQ. 0 )THEN
        NLNE2 = 1
        LIN2(1:1) = ' '
      END IF
      DO J = 1, NLNE2
        IF( LIN2(J:J) .EQ. TAB )LIN2(J:J) = ' '   ! de-tab the input line
      END DO
      ISTART = 1
      DO WHILE ( (ISTART.LT.NLNE2) .AND. (LIN2(ISTART:ISTART).EQ.' ') )
        ISTART = ISTART+1
      END DO
      IF( ISTART .GT. 1 )THEN
        DO J = ISTART, NLNE2
          LIN2(J-ISTART+1:J-ISTART+1) = LIN2(J:J)
        END DO
        NLNE2 = NLNE2-ISTART+1
      END IF
      I = 0
      IQT = 0
      IEND = NLNE2
      DO WHILE ( I .LT. NLNE2 )
        I = I + 1
        IF( LIN2(I:I) .EQ. OQT )THEN                  !  Character is `
          IQT = IQT+1
        ELSE IF( LIN2(I:I) .EQ. CQT )THEN             !  Character is '
          IQT = IQT-1
        ELSE IF( (LIN2(I:I).EQ.COMENT) .AND. (IQT.EQ.0) )THEN ! start comment 
          IEND = I-1
          DO WHILE ( (IEND.GE.1) .AND. (LIN2(IEND:IEND).EQ.' ') )
            IEND = IEND-1
          END DO
          GO TO 51
        END IF
      END DO
   51 NLNE2 = IEND
      IF( IEND .GT. 0 )THEN
        IF( LIN2(IEND:IEND) .EQ. '-' )THEN
          NLNE2 = IEND-1
          IF( NLNE2 .EQ. 0 )THEN
            NLNE2 = 1
            LIN2(1:1) = ' '
          END IF
          IF( NLNE+NLNE2 .GT. 1024 )THEN
            CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &       ', exceeded maximum line length (1024)')
            GO TO 9999
          END IF
          IF( NLNE .GT. 0 )THEN
            LNE(1:NLNE+NLNE2) = LNE(1:NLNE)//LIN2(1:NLNE2)
            NLNE = NLNE+NLNE2
          ELSE
            LNE(1:NLNE2) = LIN2(1:NLNE2)
            NLNE = NLNE2
          END IF
          GO TO 10
        END IF
      END IF
      IF( NLNE2 .EQ. 0 )THEN
        NLNE2 = 1
        LIN2(1:1) = ' '
      END IF
      IF( NLNE+NLNE2 .GT. 1024 )THEN
        CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &   ', exceeded maximum line length (1024)')
        GO TO 9999
      END IF
      IF( NLNE .GT. 0 )THEN
        LNE(1:NLNE+NLNE2) = LNE(1:NLNE)//LIN2(1:NLNE2)
        NLNE = NLNE+NLNE2
      ELSE
        LNE(1:NLNE2) = LIN2(1:NLNE2)
        NLNE = NLNE2
      END IF
      IF( LINE_COUNT(NUNIT)+1 .GT. MXLINE )THEN
        CALL INTEGER_TO_CHAR(MXLINE,.FALSE.,SOUT,LENS)
        CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &   ', maximum number of lines in a macro file = '//SOUT)
        GO TO 9999
      END IF
      LINE_COUNT(NUNIT) = LINE_COUNT(NUNIT) + 1
      LC = LINE_COUNT(NUNIT)        

CC      write(6,1001)nunit,line_count(nunit),itotal,nlne,nalloc
CC1001  format(' nunit,line_count,itotal,nlne,nalloc= ',5i5)

C   Store the line for interpretation later

      DO WHILE( ITOTAL+NLNE .GT. NALLOC )   ! Allocate more space
        CALL CHSIZE(EXEC_INIT(NUNIT),NALLOC,NALLOC+512,1,*99)
        NALLOC = NALLOC + 512
      END DO

C    LCH(N,LC,1) = character count of first character in line LC
C    LCH(N,LC,2) = character count of last character in line LC

      LCH(NUNIT,LC,1) = ITOTAL + 1
      LCH(NUNIT,LC,2) = ITOTAL + NLNE
      DO I = 1, NLNE
        L1D(EXEC_INIT(NUNIT)+ITOTAL+1) = ICHAR(LNE(I:I))
        ITOTAL = ITOTAL + 1
      END DO
      IF( LNE(1:NLNE) .EQ. ' ' )GO TO 8
      CALL UPRCASE( LNE(1:NLNE), LUPPER(1:NLNE) )

CC      write(6,1002)nlne,lne(1:nlne)
CC1002  format(' nlne= ',i5,' lne= ',a)
CC      write(6,1004)
CC     &(char(L1D(exec_init(nunit)+j)),j=lch(nunit,lc,1),lch(nunit,lc,2))
CC1004  format(' L1D= ',1024a1)

      IF( LNE(NLNE:NLNE) .EQ. ':' )THEN          ! A label line
        IF( (NLAB(NUNIT)+1) .GT. MXLAB )THEN
          CALL INTEGER_TO_CHAR(MXLAB,.FALSE.,SOUT,LENS)
          CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &     ', maximum number of labels in a macro file = '//SOUT)
          GO TO 9999
        END IF
        DO J = 1, NLNE-1
          IF( LUPPER(J:J) .EQ. ' ' )THEN
            CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &       ', blanks are not allowed in a label: '//LNE(1:NLNE-1))
            GO TO 9999
          END IF
        END DO
C # of labels
        NLAB(NUNIT) = NLAB(NUNIT) + 1
C the label itself
        EXELAB(NUNIT,NLAB(NUNIT)) = ' '
        EXELAB(NUNIT,NLAB(NUNIT))(1:NLNE-1) = LUPPER(1:NLNE-1)
C the label line #
        LINLAB(NUNIT,NLAB(NUNIT)) = LC
        GO TO 8
      ELSE IF( LUPPER(1:3) .EQ. 'IF ' )THEN
C start IF block
        ITHEN = INDEX(LUPPER(1:NLNE),'THEN')
        IF( ITHEN .EQ. 0 )THEN
          CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &     ', missing THEN following an IF')
          GO TO 9999
        END IF
        IF( NLNE .LE. (ITHEN+4) )THEN          ! search for ENDIF
          IF( NIFS(NUNIT) .EQ. MXIF )THEN
            CALL INTEGER_TO_CHAR(MXIF,.FALSE.,SOUT,LENS)
            CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &       ', maximum number of IF THEN blocks = '//SOUT)
            GO TO 9999
          END IF
          NIFS(NUNIT) = NIFS(NUNIT) + 1         ! # of IF blocks
          IFLINE(NUNIT,NIFS(NUNIT)) = LC       ! the IF THEN line #
          IFCNT = IFCNT + 1
          IFSTACK(IFCNT) = NIFS(NUNIT)
        END IF
      ELSE IF( LUPPER(1:NLNE) .EQ. 'ENDIF' )THEN ! end of IF block
        IF( IFCNT .EQ. 0 )THEN
          CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &     ', ENDIF without a preceeding IF')
          GO TO 9999
        END IF
        LIFEND(NUNIT,IFSTACK(IFCNT)) = LC      ! The ENDIF line #
        IFCNT = IFCNT-1
        GO TO 8
      ELSE IF( LUPPER(1:3) .EQ. 'DO ' )THEN    ! start DO block
        IF( NDOS(NUNIT) .EQ. MXDO )THEN
          CALL INTEGER_TO_CHAR(MXDO,.FALSE.,SOUT,LENS)
          CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &     ', maximum number of DO blocks in a macro file = '//SOUT)
          GO TO 9999
        END IF
        NDOS(NUNIT) = NDOS(NUNIT) + 1         ! # of DO blocks
        DOLINE(NUNIT,NDOS(NUNIT)) = LC        ! the DO line #
        IDOCNT = IDOCNT + 1
        DOCNT(NUNIT,NDOS(NUNIT)) = IDOCNT
        DOSTACK(IDOCNT) = NDOS(NUNIT)

CC      write(6,1000)nunit,ndos(nunit),doline(nunit,ndos(nunit))
CC     & ,docnt(nunit,ndos(nunit)),dostack(idocnt)
CC1000  format(' nunit,ndos,doline,docnt,dostack= ',5i5)

      ELSE IF( LUPPER(1:NLNE) .EQ. 'ENDDO' )THEN  ! end of DO block
        IF( IDOCNT .EQ. 0 )THEN
          CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &     ', ENDDO without a preceeding DO')
          GO TO 9999
        END IF
        LDOEND(NUNIT,DOSTACK(IDOCNT)) = LC       ! The ENDDO line #
        IDOCNT = IDOCNT - 1

CC      write(6,1003)nunit,dostack(idocnt+1),idocnt+1
CC1003  format(' nunit,dostack,idocnt= ',3i5)

        GO TO 8
      END IF

      I = 0
      IQT = 0
      DO WHILE ( I .LT. NLNE )
        I = I + 1
        IF( LNE(I:I) .EQ. OQT )THEN                  !  Character is `
          IQT = IQT+1
        ELSE IF( LNE(I:I) .EQ. CQT )THEN             !  Character is '
          IQT = IQT-1
        ELSE IF( (LNE(I:I).EQ.PPAR).AND.(IQT.EQ.0) )THEN
C   Character is ?
C   and there is a parameter to substitute
          IF( I.EQ.NLNE )THEN
            ICL = 0
          ELSE
            ICL = ICHAR(LNE(I+1:I+1))
          END IF
          IF( (ICL .LT. 48) .OR. (ICL .GT. 57) )THEN ! Sequential parameter
            PARAM_COUNT = PARAM_COUNT + 1
            ASK         = .FALSE.
            IPINDEX     = PARAM_COUNT
            IF( PARAM_COUNT .GT. NPMMX )THEN
              ASK   = .TRUE.
              NPMMX = NPMMX + 1
            END IF
            NPL = I
            NPN = I + 1
          ELSE            ! Numbered parameter substitution
            NPL = I
            K = 1
            I = I + 1
            SOUT(K:K) = LNE(I:I)
   20       IF( (I+1) .LE. NLNE )THEN
              ICL = ICHAR(LNE(I+1:I+1))
              IF( (ICL .GE. 48) .AND. (ICL .LE. 57) )THEN
                I = I + 1
                K = K + 1
                SOUT(K:K) = LNE(I:I)
                GO TO 20
              END IF
            END IF
            NPN = I + 1
            CALL CH_REAL8(SOUT(1:K),X8)
            NTEMP = INT(X8)
c            IF( NTEMP .EQ. 0 )THEN
c              CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
c     &         ', parameter number = 0')
c              GO TO 9999
c            END IF
            ASK     = .FALSE.
            IPINDEX = NTEMP
            IF( NTEMP .GT. NPMMX )THEN
              ASK   = .TRUE.
              NPMMX = NTEMP
            END IF

c      write(*,*)' file=|',fname(1:lenf),'|'
c      write(*,*)' ipindex=',ipindex

          END IF                                 
          IF( ASK )THEN  ! get a value to substitute into this parameter
            IF( BATCH_MODE .OR. NO_PROMPT )THEN
              CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &         ', parameter missing')
              GO TO 9999
            END IF
            LIN2          = ' '
            LIN2(NPL:NPL) = '$'
            CALL RITE(LNE(1:NLNE))
            CALL RITE(LIN2(1:NPL))
            CALL RITE(' ')
   30       LLEN = 0
            DO WHILE ( LLEN .EQ. 0 )
              CALL INTEGER_TO_CHAR(NPMMX,.FALSE.,SOUT,LS)
cc              CALL TT_GET_INPUT(' parameter number '//SOUT(1:LS)//' >> '
cc     &         ,LIN2,LLEN,*9997,*9999)
              CALL readline_wrapper(
     &         ' parameter number '//SOUT(1:LS)//' >> '//char(0),
     &         LIN2,LLEN)
              if( llen .eq. 0 )goto 9999
            END DO
            NFLD = 1
            CALL PARSE_INPUT_LINE( LIN2(1:LLEN), *30 )

CC            write(6,1013)nfld,npmmx
CC1013        format(' nfld= ',i5,', npmmx= ',i5)

            IF( NFLD .EQ. 0 )GO TO 30
            IF( ITYPE(1) .EQ. 0 )THEN                ! Null parameter
              PARMTYPE(NPMMX,NUNIT) = 0
              STRINGS(1) = ',,'
              LENST(1)   = 2

CC            write(6,1010)
CC1010        format(' null parameter found')

            ELSE IF( ITYPE(1) .EQ. 1 )THEN           ! Real parameter
              PARMREAL(NPMMX,NUNIT) = REALS(1)
              PARMTYPE(NPMMX,NUNIT) = 1
              CALL REAL8_TO_CHAR(REALS(1),.FALSE.,STRINGS(1)(1:15),LENS)
              LENST(1) = LENS

CC            write(6,1011)parmreal(npmmx,nunit)
CC1011        format(' real parameter found: ',e10.3)

            ELSE IF( ITYPE(1) .EQ. 2 )THEN           ! String parameter
              PARMCHAR(NPMMX,NUNIT) = STRINGS(1)(1:LENST(1))
              PARMCLEN(NPMMX,NUNIT) = LENST(1)
              PARMTYPE(NPMMX,NUNIT) = 2

CC            write(6,1014)lenst(1),strings(1)(1:lenst(1))
CC1014        format(' lenst= ',i5,', string= ',a)
CC            write(6,1012)parmchar(npmmx,nunit)(1:parmclen(npmmx,nunit))
CC1012        format(' string parameter found: ',a)

            END IF
          ELSE
            if( ipindex .eq. 0 )then
              strings(1) = fname(1:lenf)
              lenst(1) = lenf

c      write(*,*)' s1=|',strings(1)(1:lenst(1)),'|'

            else
              IF( PARMTYPE(IPINDEX,NUNIT) .EQ. 0 )THEN
                STRINGS(1) = ',,'
                LENST(1)   = 2
              ELSE IF( PARMTYPE(IPINDEX,NUNIT) .EQ. 1 )THEN
                CALL REAL8_TO_CHAR(PARMREAL(IPINDEX,NUNIT)
     &           ,.FALSE.,STRINGS(1)(1:15),LENS)
                LENST(1) = LENS
              ELSE IF( PARMTYPE(IPINDEX,NUNIT) .EQ. 2 )THEN
                STRINGS(1) = PARMCHAR(IPINDEX,NUNIT)
                LENST(1)   = PARMCLEN(IPINDEX,NUNIT)
              ELSE IF( PARMTYPE(IPINDEX,NUNIT) .EQ. 3 )THEN
                STRINGS(1) = '='
                LENST(1)   = 1
              ENDIF
            endif
          ENDIF

c      write(*,*)' npn=',npn,', nlne=',nlne

          IF( NPN .LE. NLNE )THEN
            LNE = LNE(1:NPL-1)//' '//STRINGS(1)(1:LENST(1))//
     &            ' '//LNE(NPN:NLNE)
            NLNE = NLNE + LENST(1) + NPL - NPN + 2
          ELSE
            LNE = LNE(1:NPL-1)//' '//STRINGS(1)(1:LENST(1))
            NLNE = NPL + LENST(1) 
          END IF

c      write(*,*)' lne=|',lne(1:nlne),'|'

C   substitution done

          I = NPL + LENST(1)
          IF( STACK1.AND.ASK )THEN
            LINE = LINE(1:NLINE)//' '//STRINGS(1)(1:LENST(1))
            NLINE = NLINE+1+LENST(1)
          END IF
        END IF
      END DO
      GO TO 8   ! Go back and read another line from the file
   70 IF( IFCNT .GT. 0 )THEN ! Finished reading the file, so close it
        CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &   ', missing ENDIF')
        GO TO 9999
      END IF
      IF( IDOCNT .GT. 0 )THEN
        CALL ERROR_MESSAGE('EXECUTE','file: '//FNAME(1:LENF)//
     &   ', missing ENDDO')
        GO TO 9999
      END IF
      CLOSE(IIUNIT)

C    Increment the file number and initialize the total number of parameters,
C    the current line number, the current parameter number for this file

      EXEC_ALLOC(NUNIT) = NALLOC
      PARMMAX(NUNIT)    = NPMMX
      LINE_NUM(NUNIT)   = 0
      PARMCOUNT(NUNIT)  = 0
      IF( STACK1 )WRITE(IOUT_UNIT,2020)LINE(1:NLINE)
 2020 FORMAT(' ',A)
      RETURN
   99 CALL ERROR_MESSAGE('EXECUTE','creating dynamic array space')
      RETURN 1
  991 CALL ERROR_MESSAGE('EXECUTE','reading macro file')
      GO TO 9999
  999 CALL ERROR_MESSAGE('EXECUTE','deleting dynamic array space')
      RETURN 1
 2011 NUNIT = NUNIT - 1
      IF( NUNIT.EQ.1 )CALL RITE('*** ERROR in EXECUTE')
      CALL ERROR_MESSAGE('EXECUTE','opening file '//FNAME(1:LENF))
      GO TO 9989
 9997 CALL CHSIZE(EXEC_INIT(NUNIT),NALLOC,0,1,*999)
 9988 NUNIT = NUNIT - 1
      CLOSE(IIUNIT)
      RETURN

C   Abort all current macro command files

 9999 CALL CHSIZE(EXEC_INIT(NUNIT),NALLOC,0,1,*999)
 9998 NUNIT = NUNIT - 1
 9989 CLOSE(IIUNIT)
      DO I = NUNIT, 2, -1
        CALL CHSIZE(EXEC_INIT(I),EXEC_ALLOC(I),0,1,*999)
        LINE_NUM(I)   = 0
        EXEC_ALLOC(I) = 0
        LINE_COUNT(I) = 0
        NLAB(I)       = 0
      END DO
      NUNIT = 1
      STACK = STACK1
      ECHO  = ECHOS(1)
      RETURN 1
      END
#ifdef VMS
      SUBROUTINE SCAN_FOR_DOT( FNAME, LENGTH, DOT_IN, * )

      CHARACTER*1024 FNAME
      INTEGER*4     LENGTH
      LOGICAL*4     DOT_IN

      INTEGER*2 CLASSES(0:127), STATE_TABLE(4,1)
      DATA CLASSES
     &/  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
     &   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
     &   4,  4,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  1,
     &   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
     &   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
     &   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  3,  1,  1,
     &   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,
     &   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  4 /
      DATA STATE_TABLE
C         valid  .    ]  invalid
     & /   1,  100, 200, -100 /
CCC
C   strip off the quotes, if any

      IF( FNAME(1:1) .EQ.  CHAR(96) )THEN
        FNAME(1:LENGTH-2) = FNAME(2:LENGTH-1)
        LENGTH = LENGTH-2
      END IF
      NEW_STATE = 1
      I = LENGTH+1
   10 I = I - 1
      IF( I .LE. 0 )THEN
        DOT_IN = .FALSE.
        RETURN
      END IF
      ISTATE = NEW_STATE
      NEW_STATE = STATE_TABLE(CLASSES(ICHAR(FNAME(I:I))),ISTATE)
      IF( NEW_STATE .EQ. 100 )THEN
        DOT_IN = .TRUE.
        RETURN
      ELSE IF( NEW_STATE .EQ. 200 )THEN
        DOT_IN = .FALSE.
        RETURN
      ELSE IF( NEW_STATE .EQ. -100 )THEN
        CALL ERROR_MESSAGE('EXECUTE','invalid file name')
        RETURN 1
      END IF
      GO TO 10
      END
#endif
