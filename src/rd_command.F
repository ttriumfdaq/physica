      SUBROUTINE READ_COMMAND_LINE( LINE, * )

C    This routine write the message in LINE and uses READLINE
C    to parse the information read in and returns the fields in
C    STRINGS, and REALS; and the types in ITYPE; and the number of
C    fields in NFLD

      INTEGER*4 NTOTP, NTOTQ, MXCHAR
      PARAMETER (NTOTP=50, NTOTQ=10, MXCHAR=1024)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      CHARACTER*1024 JFILE
      INTEGER*4     LNJ
      LOGICAL*1     JOURNAL, JOURNAL_MACRO
      COMMON /JOURNAL/ IJRNL, LNJ, JOURNAL, JOURNAL_MACRO, JFILE

C   local variables

      CHARACTER*1024 LINE, CLINE
      CHARACTER*1   BS
CCC
      BS = CHAR(92)    ! backslash

      IF( NUNIT .NE. 1 )THEN
        NFLD = 1
        GO TO 90
      END IF
      IF( MOTIF_VERSION )THEN
        CALL RITE(STRINGS(1)(1:LENST(1))//
     &   ' expects the following parameters')
        CALL RITE(LINE(1:LENSIG(LINE)))
        GO TO 90
      ELSE
        CALL RITE(LINE(1:LENSIG(LINE)))
      END IF
      LINE = STRINGS(1)
      LENL = LENST(1)
      DO I = 1, NQUAL(1)
        LINE = LINE(1:LENL)//BS//QUALIFIERS(1,I)(1:LENQL(1,I))
        LENL = LENL+1+LENQL(1,I)
      END DO
      LLEN = 0
cc      CALL TT_GET_INPUT(' PHYSICA: '//LINE(1:LENL)//' ',CLINE,LLEN
cc     & ,*90, *92 )
      CALL readline_wrapper(' PHYSICA: '//LINE(1:LENL)//' '//char(0),
     &  CLINE,LLEN)
      if( llen .eq. 0 )goto 92
      CALL PRE_PARSE_LINE( CLINE, LLEN, *92 )
      NFLD_START = 1
      CALL PARSE_INPUT_LINE( CLINE(1:LLEN), *92 )
      IF( JOURNAL )WRITE(IJRNL,10)'PHYSICA: '//CLINE(1:LLEN)
   10 FORMAT(' ',A)
   90 RETURN
   92 RETURN 1
      END
