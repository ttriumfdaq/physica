      SUBROUTINE KEYSEARCH( LINE, NLNE )

      CHARACTER*80 LINE
      INTEGER*4    NLNE

      REAL*8    R8D(1)
      INTEGER*4 I4D(1)
      BYTE      L1D(1)
      COMMON /XD/ I4D
      EQUIVALENCE (I4D, R8D, L1D)

      CHARACTER*32000 KEYSTRING
      CHARACTER*19    WORD
      CHARACTER*1     ESC, READ_KEY, ALTKEY, C1, CTRLM, TAB, DELETE
     &               ,CTRLA, CTRLU, CTRLE, CTRLX, CTRLH, BELL, CTRLJ
      LOGICAL*1       FIRST, CR, INSERT, WCARD1, WCARD2
      DATA INSERT /.TRUE./, FIRST /.TRUE./
CCC
      BELL = CHAR(7) 
      ESC = CHAR(27)
      CTRLM = CHAR(13)
      TAB = CHAR(9)
      DELETE = CHAR(127)      
      CTRLA = CHAR(1)
      CTRLU = CHAR(21)
      CTRLE = CHAR(5)
      CTRLX = CHAR(24)
      CTRLH = CHAR(8)
      CTRLJ = CHAR(10)

      IF( .NOT.FIRST )GO TO 200

      FIRST = .FALSE.
      NALLOC = 163840     ! = 320*512
      KALLOC = 320*3
c      write(*,*)' nalloc=',nalloc
c      write(*,*)' kalloc=',kalloc
      CALL CHSIZE(IADR,0,NALLOC,1,*9991)
      CALL CHSIZE(IKADR,0,KALLOC,4,*9991)
      ITOTAL = 0
      NKEY = 0
      KEYSTRING = ' '

      CALL FIND_UNIT( LUKEY )
#ifdef VMS
      OPEN(UNIT=LUKEY,FILE=LINE(1:NLNE),STATUS='OLD'
     & ,READONLY,SHARED,ERR=990)
#else
      OPEN(UNIT=LUKEY,FILE=LINE(1:NLNE),STATUS='OLD',ERR=990)
#endif
  100 READ(LUKEY,110,ERR=993,END=199)LINE
  110 FORMAT(A)
      LENL = LENSIG(LINE)
      J = 0
      DO WHILE (LINE(J+1:J+1) .NE. ' ' )
        J = J+1
      END DO
      NKEY = NKEY+1
      IF( NKEY*3 .GT. KALLOC )THEN
        NOLD = KALLOC
        KALLOC = KALLOC+64*3
c      write(*,*)' kalloc=',kalloc
        CALL CHSIZE(IKADR,NOLD,KALLOC,4,*9991)
      END IF
      IF( ITOTAL+LENL .GT. NALLOC )THEN
        NOLD = NALLOC
        NALLOC = NALLOC+256
c      write(*,*)' nalloc=',nalloc
        CALL CHSIZE(IADR,NOLD,NALLOC,1,*9991)
      END IF

      I4D(IKADR+(NKEY-1)*3+1) = J         ! length of keyword
      KEYSTRING((NKEY-1)*20+1:(NKEY-1)*20+J) = LINE(1:J)

      LTMP = LENL
      IF( LINE(LENL:LENL) .EQ. '&' )LTMP = LENL-2
      I4D(IKADR+(NKEY-1)*3+2) = ITOTAL+1  ! char. count of 1st char. of entries
      DO I = J+1, LTMP
        ITOTAL = ITOTAL+1
        L1D(IADR+ITOTAL) = ICHAR(LINE(I:I))
      END DO
      DO WHILE ( LINE(LENL:LENL) .EQ. '&' )
        READ(LUKEY,110)LINE
        LENL = LENSIG(LINE)
        IF( ITOTAL+LENL .GT. NALLOC )THEN
          NOLD = NALLOC
          NALLOC = NALLOC+128
c      write(*,*)' nalloc=',nalloc
          CALL CHSIZE(IADR,NOLD,NALLOC,1,*9991)
        END IF
        LTMP = LENL
        IF( LINE(LENL:LENL) .EQ. '&' )LTMP = LENL-2
        DO I = 1, LTMP
          L1D(IADR+ITOTAL+1) = ICHAR(LINE(I:I))
          ITOTAL = ITOTAL+1
        END DO
      END DO
      I4D(IKADR+(NKEY-1)*3+3) = ITOTAL  ! char. count of final char. of entries
      GO TO 100
  199 CLOSE( LUKEY )
  200 WRITE(*,210)'keyword > '
  210 FORMAT(' ',A,$)
      LOCR = 1
      WORD = ' '
      LENW = 0
      WCARD1 = .FALSE.
      WCARD2 = .FALSE.
#ifdef VMS
  220 C1 = READ_KEY('PURGE',' ',ALTKEY,0)
#else
  220 C1 = READ_KEY('PURGE FAST ',' ',ALTKEY,0)
#endif
      CR = .FALSE.
      IF( ICHAR(ALTKEY) .NE. 0 )GO TO 222
      ICH = ICHAR( C1 )
      IF( (ICH.GT.32) .AND. (ICH.LT.126) )THEN
        IF( INSERT )THEN
          IF( ICH .EQ. 42 )THEN   ! inserting a wildcard
            IF( (LOCR.EQ.1) .AND. (.NOT.WCARD1) )THEN
              WCARD1 = .TRUE.
            ELSE IF( (LOCR.EQ.LENW+1) .AND. (.NOT.WCARD2) )THEN
              WCARD2 = .TRUE.
            ELSE
              WRITE(*,2201)BELL
              GO TO 220
            END IF
          ELSE                    ! not a wildcard
            IF( (LOCR.EQ.1) .AND. WCARD1 )THEN
              WRITE(*,2201)BELL
              GO TO 220
            ELSE IF( (LOCR.EQ.LENW+1) .AND. WCARD2 )THEN
              WRITE(*,2201)BELL
              GO TO 220
            END IF
          END IF
          WRITE(*,2201)C1
          IF( LENW .GE. LOCR )THEN
            WRITE(*,2208)ESC
            WRITE(*,2206)WORD(LOCR:LENW)
          END IF
          IF( LOCR .EQ. 1 )THEN
            IF( LENW .EQ. 0 )THEN
              WORD = C1
            ELSE
              WORD = C1//WORD(1:LENW)
            END IF
          ELSE
            IF( LOCR .GT. LENW )THEN
              WORD = WORD(1:LOCR-1)//C1
            ELSE
              WORD = WORD(1:LOCR-1)//C1//WORD(LOCR:LENW)
            END IF
          END IF
          IF( LENW .GE. LOCR )WRITE(*,2202)ESC
          LENW = LENW+1
          LOCR = LOCR+1
        ELSE                      ! overstrike mode
          IF( ICH .EQ. 42 )THEN
            IF( LOCR .EQ. 1 )THEN
              WCARD1 = .TRUE.
            ELSE IF( (LOCR.EQ.LENW+1).AND.(.NOT.WCARD2) )THEN
              WCARD2 = .TRUE.
            ELSE IF( LOCR .EQ. LENW )THEN
              WCARD2 = .TRUE.
            ELSE
              WRITE(*,2201)BELL
              GO TO 220
            END IF
          END IF
          WRITE(*,2201)C1
          WORD(LOCR:LOCR) = C1
          IF( LOCR .GT. LENW )LENW = LENW+1
          LOCR = LOCR+1
        END IF
        GO TO 220
      END IF
#ifdef VMS
      IF( C1 .EQ. CTRLM )THEN
        CR = .TRUE.
        GO TO 230
      ELSE IF( C1 .EQ. TAB )THEN
        GO TO 230
#else
      IF( C1 .EQ. CTRLJ )THEN
        CR = .TRUE.
cc        WRITE(*,2201)CTRLJ
        GO TO 230
      ELSE IF( C1 .EQ. TAB )THEN
cc        WRITE(*,2201)CTRLJ
        GO TO 230
#endif
      ELSE IF( C1 .EQ. CTRLA )THEN   ! toggle insert/overstrike
        INSERT = .NOT.INSERT
      ELSE IF( C1 .EQ. CTRLE )THEN   ! position cursor at end of line
        IF( LOCR .EQ. LENW+1 )GO TO 220 
        WRITE(*,2203)ESC,LENW-LOCR+1
        LOCR = LENW+1
      ELSE IF( C1 .EQ. CTRLH )THEN   ! position cursor at start of line
        IF( LOCR .GT. 1 )WRITE(*,2204)ESC,LOCR-1
        LOCR = 1
      ELSE IF( (C1.EQ.CTRLU) .OR. (C1.EQ.CTRLX) )THEN
        WORD = WORD(LOCR:)
        LENW = LENW-LOCR+1
        WCARD1 = .FALSE.
        WCARD2 = .FALSE.
        IF( LENW .GT. 0 )THEN
          IF( WORD(1:1) .EQ. '*' )WCARD1 = .TRUE.
          IF( (LENW.GT.1).AND.(WORD(LENW:LENW).EQ.'*') )WCARD2 = .TRUE.
        END IF
        IF( LOCR .LE. 1 )GO TO 220
        WRITE(*,2205)ESC
        WRITE(*,2201)CTRLM
        WRITE(*,2206)'keyword > '
        WRITE(*,2208)ESC
        IF( LENW .GT. 0 )WRITE(*,2206)WORD(1:LENW)
        WRITE(*,2202)ESC
        LOCR = 1
      ELSE IF( C1 .EQ. DELETE )THEN
        IF( LOCR .EQ. 1 )GO TO 220
        WRITE(*,2209)ESC
        WRITE(*,2208)ESC
        IF( LENW .GE. LOCR )THEN
          WRITE(*,2206)WORD(LOCR:LENW)//' '
        ELSE
          WRITE(*,2206)' '
        END IF
        WRITE(*,2202)ESC
        IF( LOCR .GT. 2 )THEN
          WORD = WORD(1:LOCR-2)//WORD(LOCR:)
        ELSE
          WORD = WORD(2:)
        END IF
        LOCR = LOCR-1
        LENW = LENW-1
        WCARD1 = .FALSE.
        WCARD2 = .FALSE.
        IF( LENW .GT. 0 )THEN
          IF( WORD(1:1) .EQ. '*' )WCARD1 = .TRUE.
          IF( (LENW.GT.1).AND.(WORD(LENW:LENW).EQ.'*') )WCARD2 = .TRUE.
        END IF
      ELSE
        WRITE(*,2201)BELL
      END IF
      GO TO 220
  222 IF( ALTKEY .EQ. '<' )THEN
        IF( LOCR .GT. 1 )THEN
          LOCR = LOCR-1
          WRITE(*,2209)ESC
        END IF
      ELSE IF( ALTKEY .EQ. '>' )THEN
        IF( LOCR .EQ. LENW+1 )GO TO 220
        LOCR = LOCR+1
        WRITE(*,2210)ESC
      END IF
      GO TO 220
#ifdef VMS
 2201 FORMAT('+',A1,$)
 2202 FORMAT('+',A1,'8',$)
 2203 FORMAT('+',A1,'[',I3.3,'C',$)
 2204 FORMAT('+',A1,'[',I3.3,'D',$)
 2205 FORMAT('+',A1,'[2K',$)   ! erase line
 2206 FORMAT('+',A,$)
 2207 FORMAT('+',A1,'[D',$)
 2208 FORMAT('+',A1,'7',$)
 2209 FORMAT('+',A1,'[1D',$)
 2210 FORMAT('+',A1,'[1C',$)
#else
 2201 FORMAT(A1,$)
 2202 FORMAT(A1,'8',$)
 2203 FORMAT(A1,'[',I3.3,'C',$)
 2204 FORMAT(A1,'[',I3.3,'D',$)
 2205 FORMAT(A1,'[2K',$)   ! erase line
 2206 FORMAT(A,$)
 2207 FORMAT(A1,'[D',$)
 2208 FORMAT(A1,'7',$)
 2209 FORMAT(A1,'[1D',$)
 2210 FORMAT(A1,'[1C',$)
#endif
  230 IF( LENW .EQ. 0 )THEN
#ifdef unix
        WRITE(*,2201)CTRLJ
        C1 = READ_KEY('RESET',' ',ALTKEY,0)
#endif
        RETURN
      END IF
      CALL LWRCASE( WORD(1:LENW), WORD(1:LENW) )
      IF( WCARD1 .OR. WCARD2 )GO TO 700
      NMAX = 0
      K1 = 0
      K2 = 0
      DO 400 I = 1, NKEY
        LK = I4D(IKADR+(I-1)*3+1)
        IF( LENW .GT. LK )GO TO 300
        N = 0
        DO J = 1, LENW
          IF( KEYSTRING((I-1)*20+J:(I-1)*20+J) .EQ. WORD(J:J) )THEN
            NMAX = MAX(NMAX,J)
            N = J
          ELSE
            GO TO 300
          END IF
        END DO
        IF( K1 .EQ. 0 )THEN
          K1 = I
          IF( CR .AND. (LENW.EQ.LK) )GO TO 550
        END IF
        GO TO 400
  300   IF( K1 .GT. 0 )THEN
          K2 = I-1
          GO TO 500
        END IF
        IF( NMAX .GT. N )THEN
          IF( CR )THEN
            WRITE(*,1000)' keyword not found'
            GO TO 200
          ELSE                 ! tab typed
            WRITE(*,2201)BELL
            GO TO 220
          END IF
        END IF
  400 CONTINUE
      IF( K1 .GT. 0 )THEN
        K2 = NKEY
        GO TO 500
      END IF
      IF( CR )THEN                          ! carriage return typed
        WRITE(*,1000)' keyword not found'
        GO TO 200
      ELSE                                  ! tab typed
        WRITE(*,2201)BELL
        GO TO 220
      END IF
  500 IF( K1 .NE. K2 )THEN                  ! multiple finds
        IF( CR )THEN                        ! carriage return typed
          WRITE(*,2201)BELL
        ELSE                                ! tab typed
#ifdef unix
          WRITE(*,2201)CTRLJ
#endif
          LK = I4D(IKADR+(K1-1)*3+1)
          LINE(1:LK) = KEYSTRING((K1-1)*20+1:(K1-1)*20+LK)
          NL = LK
          DO I = K1+1, K2
            LK = I4D(IKADR+(I-1)*3+1)
            IF( NL+LK .GT. 79 )THEN
              WRITE(*,1000)LINE(1:NL)
              LINE(1:LK) = KEYSTRING((I-1)*20+1:(I-1)*20+LK)
              NL = LK
            ELSE
              LINE(NL+1:NL+1+LK)= ' '//KEYSTRING((I-1)*20+1:(I-1)*20+LK)
              NL = NL+1+LK
            END IF
          END DO
          WRITE(*,1000)LINE(1:NL)
          WRITE(*,210)'keyword > '//WORD(1:LENW)
          LOCR = LENW+1
        END IF
        GO TO 220
      END IF
                                            ! unique keyword found
  550 IF( CR )THEN                          ! carriage return typed
#ifdef unix
        WRITE(*,2201)CTRLJ
#endif
        NL = 0
        DO J = I4D(IKADR+(K1-1)*3+2), I4D(IKADR+(K1-1)*3+3)
          IF( CHAR(L1D(IADR+J)) .EQ. ' ' )THEN
            JL = 0
            DO JJ = J+1, I4D(IKADR+(K1-1)*3+3)
              IF( CHAR(L1D(IADR+JJ)) .EQ. ' ' )GO TO 600
              JL = JL+1
            END DO
  600       IF( NL+JL+1 .GT. 78 )THEN
              WRITE(*,1000)LINE(1:NL)
              NL = 1
              LINE(NL:NL) = ' '
            ELSE
              NL = NL+1
              LINE(NL:NL) = CHAR(L1D(IADR+J))
            END IF
          ELSE
            NL = NL+1
            LINE(NL:NL) = CHAR(L1D(IADR+J))
          END IF
        END DO
        WRITE(*,1000)LINE(1:NL)
        GO TO 200
      ELSE                                  ! tab typed
        IF( LOCR .NE. LENW+1 )WRITE(*,2203)ESC,LENW-LOCR+1
        LK = I4D(IKADR+(K1-1)*3+1)
        IF( LK .GT. LENW )THEN
          WORD(LENW+1:LK) = KEYSTRING((K1-1)*20+1+LENW:(K1-1)*20+LK)
          WRITE(*,2206)WORD(LENW+1:LK)
          LENW = LK
        END IF
        LOCR = LENW+1
        GO TO 220
      END IF 
  700 IF( WCARD1 )THEN
        WORD(1:LENW-1) = WORD(2:LENW)
        LENW = LENW-1
      END IF
      IF( WCARD2 )LENW = LENW-1
      N = 1
      NL = 0
  710 IDX = INDEX( KEYSTRING((N-1)*20+1:NKEY*20), WORD(1:LENW) )
      IF( IDX .LE. 0 )GO TO 800
      N = IDX/20+N                       ! keyword number
      LK = I4D(IKADR+(N-1)*3+1)                 ! length of keyword
      IST = (N-1)*20
      IF( WCARD1 )THEN
        IF( WCARD2 )THEN  ! found a match
          IF( NL+LK .GT. 79 )THEN
            WRITE(*,1000)LINE(1:NL)
            LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
            NL = LK
          ELSE IF( NL .EQ. 0 )THEN
            LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
            NL = LK
          ELSE
            NL = NL+1
            LINE(NL:NL) = ' '
            LINE(NL+1:NL+LK) = KEYSTRING(IST+1:IST+LK)
            NL = NL+LK
          END IF
        ELSE              ! does tail match ?
          IF( KEYSTRING(IST+1+LK-LENW:IST+LK) .EQ. WORD(1:LENW) )THEN
            IF( NL+LK .GT. 79 )THEN
              WRITE(*,1000)LINE(1:NL)
              LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
              NL = LK
            ELSE IF( NL .EQ. 0 )THEN
              LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
              NL = LK
            ELSE
              NL = NL+1
              LINE(NL:NL) = ' '
              LINE(NL+1:NL+LK) = KEYSTRING(IST+1:IST+LK)
              NL = NL+LK
            END IF
          END IF
        END IF
      ELSE                ! no initial wildcard
        IF( KEYSTRING(IST+1:IST+LENW) .EQ. WORD(1:LENW) )THEN
          IF( NL+LK .GT. 79 )THEN
            WRITE(*,1000)LINE(1:NL)
            LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
            NL = LK
          ELSE IF( NL .EQ. 0 )THEN
            LINE(1:LK) = KEYSTRING(IST+1:IST+LK)
            NL = LK
          ELSE
            NL = NL+1
            LINE(NL:NL) = ' '
            LINE(NL+1:NL+LK) = KEYSTRING(IST+1:IST+LK)
            NL = NL+LK
          END IF
        END IF
      END IF
      N = N+1
      IF( N .GT. NKEY )GO TO 800
      GO TO 710
  800 IF( NL .EQ. 0 )THEN
        WRITE(*,2201)BELL
      ELSE
        WRITE(*,1000)LINE(1:NL)
      END IF
      WRITE(*,210)'keyword > '//WORD(1:LENW)
      LOCR = LENW+1
      WCARD1 = .FALSE.
      WCARD2 = .FALSE.
      GO TO 220
  990 CALL WARNING_MESSAGE('KEY','unable to open keyword file')
      CALL CHSIZE(IADR,NALLOC,0,1,*9992)
      CALL CHSIZE(IKADR,KALLOC,0,4,*9992)
#ifdef unix
      C1 = READ_KEY('RESET',' ',ALTKEY,0)
#endif
      RETURN 
  993 CALL WARNING_MESSAGE('KEY'
     & ,'error while reading from keyword file')
      CALL CHSIZE(IADR,NALLOC,0,1,*9992)
      CALL CHSIZE(IKADR,KALLOC,0,4,*9992)
#ifdef unix
      C1 = READ_KEY('RESET',' ',ALTKEY,0)
#endif
      RETURN 
 9991 CALL WARNING_MESSAGE('KEY','error allocating dynamic array space')
#ifdef unix
      C1 = READ_KEY('RESET',' ',ALTKEY,0)
#endif
      RETURN
 9992 CALL WARNING_MESSAGE('KEY'
     & ,'error de-allocating dynamic array space')
#ifdef unix
      C1 = READ_KEY('RESET',' ',ALTKEY,0)
#endif
      RETURN
 1000 FORMAT(' ',A)
      END
