      SUBROUTINE PLOTTEXT( * )

C    PLOTTEXT filename

      PARAMETER (NACC=13,NFONT=43)

      INTEGER*4 NTOTP, NTOTQ, MXPARM, MXFILE, MXVAR, MXHIS
     &         ,LENVSM, MXLINE, MXLAB, MXDO, MXIF, MXTEXT, MXFNM
     &         ,MXVART, MXVART2, MXDEFEXT, MXCHAR
     &         ,NRMAX, NPMAX, MXWNDO
      PARAMETER (NTOTP=50,      NTOTQ=10,    MXPARM=100
     &          ,MXFILE=20,     MXVAR=1000,  MXHIS=20,     LENVSM=32
     &          ,MXLINE=10000,  MXLAB=1000,  MXDO=500,     MXIF=1000
     &          ,MXTEXT=20,     MXFNM=20,    MXVART=100,   MXVART2=500
     &          ,MXDEFEXT=20,   MXCHAR=1024)
      PARAMETER (NRMAX=512) !Max number of fields to read per record
      PARAMETER (NPMAX=512)
      PARAMETER (MXWNDO=100)

C  Autoscale variable
C  IAUTO =  0  means OFF
C        = -1  means COMMENSURATE
C        =  1  means ON,  = 4  means ON /VIRTUAL
C        =  2  means X,   = 5  means X /VIRTUAL
C        =  3  means Y,   = 6  means Y /VIRTUAL

C  CRSRDOUT =  0 --> graph units
C           =  1 --> percentages of the window
C           =  2 --> world units ( cm | in )

C  NCURVES = number of curves on the screen

      CHARACTER*1024 SAVE_LINE
      CHARACTER*132 LGFRMT
      CHARACTER*21  DEVICE_SIZE(0:6)
      CHARACTER*20  DEFEXT, VERSION_DATE
      CHARACTER*10  BITMAP_DEVICENAME(5), DEVICE_NAMES(0:20)
      CHARACTER*9   MODE
      CHARACTER*5   VERSION
      CHARACTER*2   UNITS
      REAL*8        SPTENS
      REAL*4        COLOUR, ERRFILL, CNTSEP, LABSIZ
     &             ,LEGSIZ, ARROWID, ARROLEN, ARROTYP
      INTEGER*4     LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT, HISTORY_SHOW
     &             ,HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,NLGFRMT, IPFILL, PLSEED, IPSPD, POSTRES, IWIDTH
     &             ,IAUTO, CRSRDOUT, NCURVES, SAVE_NLINE, IPAGES
     &             ,IPLOTTER, ITERMINAL, LDEV(0:20), LBIT(5)
      LOGICAL*4     ECHO, STACK, STACK1, BATCH_MODE, NO_PROMPT, APPEND
     &             ,MIROR, TRAP, REPLACE_NAME, REPLACE_TEXT_NAME
     &             ,HISTORY_FLAG, REPLOT, CONFIRM, EXECOMM
     &             ,GRAPHICS_OFF, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
      COMMON /MISCELLANEOUS/ LDEFEXT, NUNIT, NUNIT_SAVE, IOUT_UNIT
     &             ,HISTORY_SHOW, HISTORY_MAX, HISTORY_WRAP, IVIRTUAL
     &             ,COLOUR, ERRFILL, CNTSEP, LABSIZ, LEGSIZ, NLGFRMT
     &             ,SPTENS, IPSPD, ARROWID, ARROLEN, ARROTYP, IPFILL
     &             ,PLSEED, POSTRES, IWIDTH, IAUTO, CRSRDOUT, NCURVES
     &             ,SAVE_NLINE, IPAGES, IPLOTTER, ITERMINAL
     &             ,LDEV, LBIT, ECHO, STACK, STACK1, GRAPHICS_OFF
     &             ,MIROR, BATCH_MODE, NO_PROMPT, CONFIRM, TRAP
     &             ,REPLACE_NAME, REPLACE_TEXT_NAME, HISTORY_FLAG
     &             ,REPLOT, EXECOMM, PAUSE_FLAG, BITMAP, DISPLAY_FILE
     &             ,TRANSFLAG, SYSFLAG, XVST_REPLAY, MOTIF_VERSION
     &             ,APPEND, DEFEXT, VERSION_DATE, LGFRMT, MODE
     &             ,VERSION, UNITS, BITMAP_DEVICENAME, DEVICE_NAMES
     &             ,DEVICE_SIZE, SAVE_LINE

      CHARACTER*1 COMENT   ! comment delimiter
      LOGICAL*4   IGNORE_COMENT
      COMMON /COMENTC/ IGNORE_COMENT, COMENT

      CHARACTER*(MXCHAR) LINE_IN, STRINGS(NTOTP)
      CHARACTER*60       QUALIFIERS(NTOTP,NTOTQ)
      REAL*8             REALS(NTOTP)
      INTEGER*4          LENST(NTOTP), ITYPE(NTOTP), NQUAL(NTOTP)
     &                  ,LENQL(NTOTP,NTOTQ), NFLD, NFLD_START
      COMMON /PARSEINPUTLINE/ REALS, LENST, ITYPE, NQUAL, LENQL, NFLD
     &                       ,NFLD_START, QUALIFIERS, STRINGS, LINE_IN

      INTEGER*4  NHATCH(2)
      COMMON /PSYM_HATCHING/ NHATCH 

      INTEGER*4 IFONT
      COMMON /FONT_NUMBER/ IFONT

      LOGICAL*4 CTRLC_CALLED
      COMMON /CTRLC/ CTRLC_CALLED

      LOGICAL*4 ASIS
      COMMON /MODE/ ASIS

      INTEGER*4 LWIDTH
      COMMON /PLOT_HARDCOPY_LINEWIDTH/  LWIDTH

C   local variables

      CHARACTER*25000 LINE2
      CHARACTER*132   LINE
      CHARACTER*20    FONT, FONT_OLD, COMM, GETLAB, FONTSAVE
      CHARACTER*15    SOUT 
      CHARACTER*14    FONT2
      CHARACTER*2     COMM12, CTRL, CTRL_OLD, HEXCODE
      CHARACTER*1     COMM1, BS

      REAL*8     V8
      REAL*4     DH, XMARGIN, ANGL, ANGL_OLD, ADUM

      LOGICAL*4  BOLD, CENTRE, ITALICS, RIGHT_JUST
     &          ,ASIS_OLD, ITALICS_OLD, NEXT_LINE, AASIS
      BYTE       HEX(100)

      DATA DH /0.6/

C  Variables for accents

      INTEGER*2 IACC, CLASS(0:127)

C  c  b  d  ^  '  `  "  ~  =  .   u   v   H
C  1  2  3  4  5  6  7  8  9  10  11  12  13

      LOGICAL*4    ABOLD
      LOGICAL*1    AHEX
      CHARACTER*60 FACC
      LOGICAL*1    ACCENTH(NACC)
      INTEGER*2    ACCENTS(NACC,NFONT), ACCENTX(NACC,NFONT)
     &            ,ACCENTY(NACC,NFONT), ACCENTLF(NACC)
      CHARACTER*60 ACCENTF(NACC)
      DATA CLASS
     &/  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     &   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     &   0,  0,  7,  0,  0,  0,  0,  5,  0,  0,  0,  0,  0,  0, 10,  0,
     &   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  9,  0,  0,
     &   0,  0,  0,  0,  0,  0,  0,  0, 13,  0,  0,  0,  0,  0,  0,  0,
     &   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  4,  0,
     &   6,  0,  2,  1,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     &   0,  0,  0,  0,  0, 11, 12,  0,  0,  0,  0,  0,  0,  0,  8,  0 /
CC
CC  1  STANDARD             2  HELVETICA.1          3  GOTHIC.ENGLISH
CC  4  ROMAN.3              5  ITALIC.3             6  GREEK.2A
CC  7  ITALIC.2A            8  ROMAN.2A             9  SANSERIF.2
CC 10  GREEK.1             11  SANSERIF.1          12  SCRIPT.1
CC 13  GOTHIC.FRAKTUR      14  GOTHIC.ITALIAN      15  SCRIPT.2
CC 16  ROMAN.2             17  ITALIC.2            18  GREEK.2
CC 19  CYRILLIC.2          20  SANSERIF.CART       21  GREEK.CART
CC 22  HIRAGANA            23  KATAKANA            24  KANJI1
CC 25  KANJI2              26  KANJI3              27  KANJI4
CC 28  KANJI5              29  OLDALPH             30  TRIUMF.1
CC 31  TRIUMF.2            32  TSAN                33  ROMAN.FUTURA
CC 34  ROMAN.SERIF         35  ROMAN.FASHON        36  ROMAN.LOGO1
CC 37  ROMAN.SWISSL        38  ROMAN.SWISSM        39  ROMAN.SWISSB
CC 40  SPECIAL             41  MATH                42  HEBREW
CC 43  TRIUMF.OUTLINE
CC
      DATA ACCENTF /' ','STANDARD','STANDARD','TSAN','TSAN','TSAN'
     & ,'STANDARD','TSAN','STANDARD','STANDARD','SPECIAL','STANDARD'
     & ,'SPECIAL'/
      DATA ACCENTLF / 0,8,8,4,4,4,8,4,8,8,7,8,7 /
      DATA ACCENTH / '00'X, 'BF'X, '4B'X, 'B1'X, 'B2'X, 'B3'X, '7F'X
     & ,'B5'X, '60'X, 'A1'X, '9A'X, 'A5'X, '4E'X /
      DATA ACCENTS
     & / 0, 85, 70, 40, 45, 45, 40, 35, 50, 40, 35, 25, 30 ! STANDARD
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! HELVETICA.1
     &  ,13*0
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.3
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ITALIC.3
     &  ,13*0
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ITALIC.2A
     &  ,0,100, 70, 42, 50, 50, 40, 40, 75, 40, 40, 30, 30 ! ROMAN.2A
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! SANSERIF.2
     &  ,13*0
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! SANSERIF.1
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 35, 30, 25, 30 ! SCRIPT.1
     &  ,13*0, 13*0
     &  ,0, 90, 65, 35, 40, 40, 40, 40, 60, 35, 30, 25, 30 ! SCRIPT.2
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.2
     &  ,0, 95, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ITALIC.2
     &  ,13*0, 13*0, 13*0
     &  ,13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! TRIUMF.1
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! TRIUMF.2
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! TSAN
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.FUTUR
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.SERIF
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.FASHO
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.LOGO1
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.SWISL
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.SWISM
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30 ! ROMAN.SWISB
     &  ,13*0, 13*0, 13*0
     &  ,0, 90, 70, 42, 50, 50, 40, 40, 60, 40, 40, 30, 30/! TRIUMF.OUTL
      DATA ACCENTX
     & / 0, -87, -72, -80, -70, -74, -74, -80, -80, -70, -77, -71, -71
     &  ,0, -82, -68, -79, -65, -72, -72, -79, -80, -70, -76, -72, -72
     &  ,13*0
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,13*0
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,0,-105, -82, -93, -80, -90, -90, -95,-100, -82, -90, -88, -82
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,13*0
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,0, -80, -60, -70, -60, -67, -65, -72, -72, -62, -62, -60, -60
     &  ,13*0, 13*0
     &  ,0, -70, -50, -55, -45, -50, -50, -58, -58, -45, -45, -45, -45
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,0, -85, -65, -67, -55, -64, -60, -67, -65, -55, -60, -55, -52
     &  ,13*0, 13*0, 13*0
     &  ,13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0
     &  ,0, -85, -67, -82, -65, -70, -72, -78, -80, -67, -72, -70, -68
     &  ,0, -85, -67, -82, -65, -70, -72, -78, -80, -67, -72, -70, -68
     &  ,0, -80, -60, -72, -60, -67, -65, -72, -72, -60, -67, -62, -60
     &  ,0, -80, -65, -75, -65, -65, -70, -75, -78, -65, -70, -65, -65
     &  ,0, -80, -65, -75, -65, -65, -70, -75, -78, -65, -70, -65, -65
     &  ,0, -80, -65, -75, -65, -65, -70, -75, -78, -65, -70, -65, -65
     &  ,0, -80, -65, -75, -65, -65, -70, -75, -78, -65, -70, -65, -65
     &  ,0, -80, -65, -72, -60, -65, -65, -70, -72, -60, -67, -62, -62
     &  ,0, -80, -65, -74, -60, -65, -68, -72, -75, -63, -70, -64, -64
     &  ,0, -83, -67, -76, -60, -70, -70, -75, -78, -65, -73, -67, -67
     &  ,13*0, 13*0, 13*0
     &  ,0, -80, -65, -75, -65, -65, -70, -75, -78, -65, -70, -65, -65 /
      DATA ACCENTY
     & / 0, -40, -15, 35, 28, 28, 45, 36, 42, 45, 45, 72, 53
     &  ,0, -41, -15, 38, 30, 30, 48, 37, 40, 47, 47, 75, 59
     &  ,13*0
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,13*0
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -48, -22, 29, 25, 25, 42, 30, 20, 40, 40, 70, 52
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,13*0
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 17, 17, 17, 25, 12, 17, 25, 30, 50, 35
     &  ,13*0, 13*0
     &  ,0, -40, -15, 17, 17, 17, 25, 12, 17, 25, 30, 50, 35
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,13*0, 13*0, 13*0
     &  ,13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0, 13*0
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 37, 30, 30, 47, 35, 40, 45, 45, 74, 58
     &  ,0, -40, -15, 30, 30, 30, 47, 35, 35, 45, 45, 72, 58
     &  ,0, -40, -15, 30, 30, 30, 47, 35, 35, 45, 45, 72, 58
     &  ,0, -40, -15, 30, 30, 30, 47, 35, 35, 45, 45, 72, 58
     &  ,0, -40, -15, 30, 30, 30, 47, 35, 35, 45, 45, 72, 58
     &  ,0, -40, -15, 37, 32, 32, 50, 38, 42, 50, 50, 78, 60
     &  ,0, -45, -17, 39, 35, 35, 54, 42, 47, 55, 55, 82, 65
     &  ,0, -45, -17, 39, 35, 35, 54, 42, 47, 55, 55, 82, 65
     &  ,13*0, 13*0, 13*0
     &  ,0, -40, -15, 30, 30, 30, 47, 35, 35, 45, 45, 72, 58 /

      CHARACTER*1 DUM(16)
      DATA DUM /'0','1','2','3','4','5','6','7'
     &         ,'8','9','A','B','C','D','E','F'/
CCC
      BS = CHAR(92)

      STRINGS(1) = 'PLOTTEXT'
      LENST(1) = 8

      IF( ITYPE(2) .NE. 2 )THEN
        CALL ERROR_MESSAGE('PLOTTEXT','expecting filename')
        RETURN 1
      END IF

      STRINGS(1) = STRINGS(1)(1:LENST(1))//' '//STRINGS(2)(1:LENST(2))
      LENST(1) = LENST(1)+1+LENST(2)
      IF( STACK )WRITE( IOUT_UNIT, 5 )STRINGS(1)(1:LENST(1))
   5  FORMAT(' ',A)

      LENST(2) = -LENST(2)
      CALL GET_TEXT_VARIABLE( STRINGS(2),LENST(2), STRINGS(2),LEN2, *92)
      LENST(2) = LEN2
      CALL FIND_UNIT( IN_UNIT )
#ifdef VMS
      OPEN( FILE=STRINGS(2)(1:LENST(2)), UNIT=IN_UNIT, STATUS='OLD'
     & ,READONLY,ERR=991 )
#else
      OPEN( FILE=STRINGS(2)(1:LENST(2)), UNIT=IN_UNIT, STATUS='OLD'
     & ,ERR=991 )
#endif
      YUWIND  = GETNAM('YUWIND')
      YLWIND  = GETNAM('YLWIND')
      XUWIND  = GETNAM('XUWIND')
      XLWIND  = GETNAM('XLWIND')
      HEIGHT  = GETNAM('TXTHIT')
      ICOL    = IFIX(GETNAM('COLOUR'))
      CTRL    = '<>'
      BOLD    = .FALSE.
      NHATCH(1) = 0
      NHATCH(2) = 0
      ITALICS = .FALSE.
      ANGL    = 0.0
      FONT    = GETLAB('FONT')
      LENFONT = LENSIG(FONT)
      FONTSAVE= FONT
      LENFSAVE= LENFONT
      SPACING = 1.5*HEIGHT
      XMARGIN = 0.01 * (XUWIND - XLWIND)
      Y       = YUWIND
      XLINE   = 0.
      NEXT_LINE = .TRUE.
      LINE2   = ' '
      LENS2   = 0
  10  READ(IN_UNIT,20,ERR=30,END=800)LINE
  20  FORMAT(A)
      LENS = LENSIG(LINE)
      IF( CTRLC_CALLED )GO TO 800
      XLINE = XLINE + 1.
      IF( LENS .EQ. 0 )GO TO 10
cc      write(*,*)' input line length= ',lens
cc      write(*,*)line(1:lens)

C  A comment line

      IF( LINE(1:2) .EQ. CTRL(1:1)//COMENT )GO TO 10

C  A continuation line 

      IF( LENS .GT. 3 )THEN
        IF( LINE(LENS-2:LENS) .EQ. CTRL(1:1)//'-'//CTRL(2:2) )THEN
          LINE2 = LINE2(1:LENS2)//LINE(1:LENS-3)
          LENS2 = LENS2+LENS-3
          GO TO 10
        END IF
      END IF
      LINE2 = LINE2(1:LENS2)//LINE(1:LENS)
      LENS2 = LENS2+LENS

C   Preprocess the line to see if any special names are to substituted

      I = 0
      DO WHILE ( I .LT. LENS2 )
        I = I + 1
        IF( LINE2(I:I) .EQ. CTRL(1:1) )THEN
          J = I + 1
          DO WHILE ((J.LT.LENS2) .AND. (LINE2(J:J).NE.CTRL(2:2)))
            J = J + 1
          END DO
          CALL NAME_IN_HEXCODE(LINE2(I+1:J-1),HEXCODE,FONT2,LF)
          IF( HEXCODE .NE. '  ' )THEN
            IF( I .GT. 1 )THEN
              LINE2(1:) = LINE2(1:I-1)//
     &         CTRL(1:1)//'F'//FONT2(1:LF)//CTRL(2:2)//
     &         CTRL(1:1)//'X'//CTRL(2:2)//HEXCODE//CTRL(1:1)//'X'//
     &         CTRL(2:2)//CTRL(1:1)//'F'//FONT(1:LENFONT)//CTRL(2:2)//
     &         LINE2(J+1:LENS2)
            ELSE
              LINE2(1:) =
     &         CTRL(1:1)//'F'//FONT2(1:LF)//CTRL(2:2)//
     &         CTRL(1:1)//'X'//CTRL(2:2)//HEXCODE//CTRL(1:1)//'X'//
     &         CTRL(2:2)//CTRL(1:1)//'F'//FONT(1:LENFONT)//CTRL(2:2)//
     &         LINE2(J+1:LENS2)
            END IF
            I = I + LENFONT + LF + 13
            LENS2 = LENS2 - J + I
          ELSE
            I = J
          END IF
        END IF
      END DO

CC      write(*,*)' Remade line length= ',lens2
CC      write(*,*)line2(1:lens2)

      CENTRE     = .FALSE.
      RIGHT_JUST = .FALSE.
      PLEN       = 0.
      ITWO       = 0
      IONE       = 1
      IF( NEXT_LINE )THEN
        Y = Y - SPACING
        NEXT_LINE = .FALSE.
      END IF
      X = XLWIND + XMARGIN
      IDCNT = 0
      IUCNT = 0
      GO TO 60
  30  CENTRE = .FALSE.
      X = XCENTRE - 0.5 * PLEN
      GO TO 50
  40  RIGHT_JUST = .FALSE.
      X = XRIGHT - PLEN
  50  PLEN = 0.
      Y = Y_OLD
      ITWO = ITWO_OLD
      IONE = IONE_OLD
      ASIS = ASIS_OLD
      FONT = FONT_OLD
      LENFONT = LENFONT_OLD
      CALL PFONT(FONT(1:LENFONT),0)
      HEIGHT = HEIGHT_OLD
      SPACING = SPACING_OLD
      XMARGIN = XMARGIN_OLD
      CTRL = CTRL_OLD
      ITALICS = ITALICS_OLD
      ICOL = ICOL_OLD
      IF( ICOL .EQ. 0 )THEN
        CALL SETNAM('PTYPE',1.0)
      ELSE
        CALL SETNAM('PTYPE',0.0)
        CALL SETNAM('COLOUR',FLOAT(ICOL))
      END IF
      ANGL = ANGL_OLD
      CALL PSANGL(ANGL,ADUM,*993)
      DO WHILE ( IDCNT .GT. 0 )
        IDCNT = IDCNT - 1
        Y = Y + HEIGHT
        HEIGHT = HEIGHT / DH
      END DO
      DO WHILE ( IUCNT .GT. 0 )
        IUCNT = IUCNT - 1
        Y = Y - HEIGHT
        HEIGHT = HEIGHT / DH
      END DO
  60  ITWO = ITWO + 1
      IF( (LINE2(ITWO:ITWO).EQ.CTRL(1:1)) .OR. (ITWO.EQ.LENS2) )THEN
        IF( ITWO .LT. LENS2 )ITWO = ITWO - 1
        IF( ITWO .LT. IONE )GO TO 70
        ILEN = ITWO - IONE + 1
        NEXT_LINE = .TRUE.
        IF( ASIS )THEN
          CALL UPRCASE(LINE2(IONE:ITWO),LINE2(IONE:ITWO))
          II = IONE
          DO I = 2, ILEN, 2
            DO IJ = 1, 16
              IF( LINE2(II:II) .EQ. DUM(IJ) )THEN
                I1 = IJ-1
                GO TO 610
              END IF
            END DO
            GO TO 996
  610       DO IJ = 1, 16
              IF( LINE2(II+1:II+1) .EQ. DUM(IJ) )THEN
                I2 = IJ-1
                GO TO 620
              END IF
            END DO
            GO TO 996
  620       ISS = 16*I1+I2
            IF( ISS .GT. 127 )ISS = ISS-256
            HEX(I/2) = ISS
            II = II + 2
          END DO
          ILEN = ILEN / 2
          IF( CENTRE .OR. RIGHT_JUST )THEN
            PLEN = PLEN + PSMLEN(HEX,ILEN,HEIGHT)
          ELSE
            CALL PSYMBOLD(X,Y,HEIGHT,HEX,0.,ILEN)
            CALL PSYMLC(X,YTEMP)
          END IF
        ELSE
#ifdef VMS
          IF( CENTRE .OR. RIGHT_JUST )THEN
            PLEN = PLEN + PSMLEN(%REF(LINE2(IONE:ITWO)),ILEN,HEIGHT)
          ELSE
            CALL PSYMBOLD(X,Y,HEIGHT,%REF(LINE2(IONE:ITWO)),0.,ILEN)
            CALL PSYMLC(X,YTEMP)
          END IF
#else
          IF( CENTRE .OR. RIGHT_JUST )THEN
            PLEN = PLEN + PSMLEN(LINE2(IONE:ITWO),ILEN,HEIGHT)
          ELSE
            CALL PSYMBOLD(X,Y,HEIGHT,LINE2(IONE:ITWO),0.,ILEN)
            CALL PSYMLC(X,YTEMP)
          END IF
#endif
        END IF
  70    IF( ITWO .EQ. LENS2 )THEN
          IF( RIGHT_JUST )GO TO 40
          IF( CENTRE )GO TO 30
          GO TO 100
        END IF
        J1 = ITWO + 2
        J2 = J1 - 1
  80    J2 = J2 + 1
        IF( J2 .GT. LENS2 )GO TO 995
        IF( LINE2(J2:J2) .EQ. CTRL(2:2) )THEN
          IONE = J2 + 1
          ITWO = J2
          IF( J1 .EQ. J2 )GO TO 60
          J2 = J2 - 1
          LEN = J2 - J1 + 1
          LEN1 = LEN - 1
          COMM = ' '
          COMM(1:LEN) = LINE2(J1:J2)
          CALL UPRCASE(COMM(1:1),COMM1)
          CALL UPRCASE(COMM(1:2),COMM12)  
          IF( COMM1 .EQ. 'X' )THEN

C   Hexadecimal input flag

            ASIS = .NOT.ASIS
          ELSE IF( COMM12 .EQ. 'EM' )THEN

C   Emphasis or Italics flag

            IF( ITALICS )THEN
              ITALICS = .FALSE.
              ANGL = 0.0
            ELSE
              ITALICS = .TRUE.
              ANGL = 20.0
              IF( LEN .GT. 2 )THEN
                CALL CH_REAL8( COMM(3:LEN), V8, *992 )
                ANGLE = SNGL(V8)
              END IF
            END IF
            CALL PSANGL(ANGL,ADUM,*993)
          ELSE IF( COMM1 .EQ. 'F' )THEN

C   Set the font

            CALL UPRCASE( COMM(2:LEN), FONT(1:LEN1) )
            LENFONT = LEN1
            CALL PFONT(FONT(1:LENFONT),0)
            CALL PSANGL(ANGL,ADUM,*993)
          ELSE IF( COMM1 .EQ. 'H' )THEN

C   Set the height

            IF( COMM(LEN:LEN) .EQ. '%' )THEN
              CALL CH_REAL8( COMM(2:LEN1), V8, *992 )
              HEIGHT = V8 * (YUWIND - YLWIND) / 100.
            ELSE
              CALL CH_REAL8( COMM(2:LEN), V8, *992 )
              HEIGHT = SNGL(V8)
            END IF
          ELSE IF( COMM1 .EQ. 'B' )THEN

C   Bolding flag

            IF( .NOT.CENTRE .AND. .NOT.RIGHT_JUST )THEN
              BOLD = .FALSE.
              NHATCH(1) = 0
              NHATCH(2) = 0
              IF( LEN .GE. 2 )THEN
                BOLD = .TRUE.
                LENB1 = LEN
                LENB2 = LEN1 - 1
                INDX  = INDEX(COMM(1:LEN),':')
                IF( INDX .GT. 0 )LENB1 = INDX-1
                CALL CH_REAL8( COMM(2:LENB1), V8, *992 )
                XBOLD = SNGL(V8)
                NHATCH(1) = IFIX(XBOLD)
                NHATCH(2) = 0
                IF( INDX .GT. 0 )THEN
                  CALL CH_REAL8( COMM(INDX+1:LEN), V8, *992 )
                  XBOLD = SNGL(V8)
                  NHATCH(2) = IFIX(XBOLD) 
                END IF
              END IF
            END IF
          ELSE IF( COMM1 .EQ. 'L' )THEN

C   Set the line thickness (for bitmap only)

            CALL CH_REAL8( COMM(2:LEN), V8, *992 )
            THICK = SNGL(V8)
            LWIDTH = THICK
          ELSE IF( COMM1 .EQ. 'Z' )THEN

C   Include a horizontal space

            IF( COMM(LEN:LEN) .EQ. '%' )THEN
              CALL CH_REAL8( COMM(2:LEN1), V8, *992 )
              HORIZ_SPACE = V8 * (XUWIND - XLWIND) / 100.
            ELSE
              CALL CH_REAL8( COMM(2:LEN), V8, *992 )
              HORIZ_SPACE = SNGL(V8)
            END IF
            X = X + HORIZ_SPACE
            IF( RIGHT_JUST .OR. CENTRE )PLEN = PLEN + HORIZ_SPACE
          ELSE IF( COMM1 .EQ. BS )THEN

C   Insert an accent

            IF( CENTRE .OR. RIGHT_JUST )GO TO 90
            IACC = CLASS(ICHAR(COMM(2:2)))
            IF( IACC .EQ. 0 )GO TO 994
            SACC  = ACCENTS(IACC,IFONT)/100.
            IF( SACC .EQ. 0.0 )THEN
              CALL TRANSPARENT_MODE(0)
              CALL WARNING_MESSAGE('PLOTTEXT'
     &         ,'accents not supported for font: '//FONT(1:LENFONT))
              GO TO 90
            END IF
            XACC  = ACCENTX(IACC,IFONT)/100.
            YACC  = ACCENTY(IACC,IFONT)/100.
CC        write(*,2000) 
CC2000    format(' enter x,y,s > ',$)
CC        read(*,*)xacc,yacc,sacc
CC        xacc=xacc/100.
CC        yacc=yacc/100.
CC        sacc=sacc/100.
            FACC  = ACCENTF(IACC)
            LFACC = ACCENTLF(IACC)
            AHEX  = ACCENTH(IACC)

            XACC = X + XACC*HEIGHT
            YACC = Y + YACC*HEIGHT
            SACC = SACC*HEIGHT
#ifdef VMS
            CALL PALPHA(%REF(FACC(1:LFACC)),0)
#else
            CALL PALPHA(FACC(1:LFACC),0)
#endif
            CALL PSANGL(ANGL,ADUM,*993)
            ABOLD = BOLD
            BOLD = .FALSE.
            AASIS = ASIS
            ASIS = .TRUE.
            CALL PSYMBOLD(XACC,YACC,SACC,AHEX,0.,1)
            ASIS = AASIS
            BOLD = ABOLD
            CALL PFONT(FONT(1:LENFONT),0)
            CALL PSANGL(ANGL,ADUM,*993)
          ELSE IF( COMM1 .EQ. 'S' )THEN

C  Set the line spacing

            OLD_SPACING = SPACING
            IF( COMM(LEN:LEN) .EQ. '%' )THEN
              CALL CH_REAL8( COMM(2:LEN1), V8, *992 )
              SPACING = V8 * (YUWIND - YLWIND) / 100.
            ELSE
              CALL CH_REAL8( COMM(2:LEN), V8, *992 )
              SPACING = SNGL(V8)
            END IF
            Y = Y + OLD_SPACING - SPACING
          ELSE IF( COMM1 .EQ. 'M' )THEN

C   Set the left margin

            IF( COMM(LEN:LEN) .EQ. '%' )THEN
              CALL CH_REAL8( COMM(2:LEN1), V8, *992 )
              XMARGIN = V8 * (XUWIND - XLWIND) / 100.
            ELSE
              CALL CH_REAL8( COMM(2:LEN), V8, *992 )
              XMARGIN = SNGL(V8)
            END IF
          ELSE IF( COMM1 .EQ. '^' )THEN

C   Super-script flag

            IF( IDCNT .GT. 0 )THEN
              IDCNT = IDCNT - 1
              Y = Y + HEIGHT
              HEIGHT = HEIGHT / DH
            ELSE
              IUCNT = IUCNT + 1
              HEIGHT = HEIGHT * DH
              Y = Y + HEIGHT
            END IF
          ELSE IF( COMM1 .EQ. '_' )THEN

C   Sub-script flag

            IF( IUCNT .GT. 0 )THEN
              IUCNT = IUCNT - 1
              Y = Y - HEIGHT
              HEIGHT = HEIGHT / DH
            ELSE
              IDCNT = IDCNT + 1
              HEIGHT = HEIGHT * DH
              Y = Y - HEIGHT
            END IF
          ELSE IF( COMM1 .EQ. 'Q' )THEN

C   Set the control characters

            IF( LEN .EQ. 2 )THEN
              CTRL = COMM(2:2)//COMM(2:2)
            ELSE IF( LEN .EQ. 3 )THEN
              CTRL = COMM(2:3)
            ELSE
              GO TO 40
            END IF
          ELSE IF( COMM1 .EQ. ' ' )THEN

C   Include a blank line

            Y = Y - SPACING
            X = XLWIND + XMARGIN
          ELSE IF( COMM1 .EQ. 'C' )THEN

C   Set the colour 

            CALL CH_REAL8( COMM(2:LEN), V8, *992 )
            ICOL = INT( V8 )
            IF( CENTRE .OR. RIGHT_JUST )GO TO 90
            IF( ICOL .EQ. 0 )THEN
              CALL SETNAM('PTYPE',1.0)
            ELSE
              CALL SETNAM('PTYPE',0.0)
              CALL SETNAM('COLOUR',FLOAT(ICOL))
            END IF
          ELSE IF( COMM12 .EQ. 'JL')THEN

C  Justify left

            IF( RIGHT_JUST )GO TO 40
            IF(  CENTRE    )GO TO 30
            IF( LEN1 .GT. 1 )THEN
              IF( COMM(LEN:LEN) .EQ. '%' )THEN
                CALL CH_REAL8( COMM(3:LEN1), V8, *992 )
                XLEFT = XLWIND + V8 * (XUWIND - XLWIND) / 100.
              ELSE
                CALL CH_REAL8( COMM(3:LEN), V8, *992 )
                XLEFT = SNGL( V8 )
              END IF
            ELSE
              XLEFT = XMARGIN+XLWIND
            END IF
            X = XLEFT
          ELSE IF( COMM12 .EQ. 'JR')THEN

C   Justify right

            IF( RIGHT_JUST )GO TO 40
            IF(   CENTRE   )GO TO 30
            IF( LEN1 .GT. 1 )THEN
              IF( COMM(LEN:LEN) .EQ. '%' )THEN
                CALL CH_REAL8( COMM(3:LEN1), V8, *992 )
                XRIGHT = XLWIND + V8 * (XUWIND - XLWIND) / 100.
              ELSE
                CALL CH_REAL8( COMM(3:LEN), V8, *992 )
                XRIGHT = SNGL(V8)
              END IF
            ELSE
              XRIGHT = XUWIND
            END IF
            RIGHT_JUST = .TRUE.
            GO TO 88
          ELSE IF( COMM12 .EQ. 'JC')THEN

C   Justify centre

            IF( RIGHT_JUST )GO TO 40
            IF(   CENTRE   )GO TO 30
            IF( LEN1 .GT. 1 )THEN
              IF( COMM(LEN:LEN) .EQ. '%' )THEN
                CALL CH_REAL8( COMM(3:LEN1), V8, *992 )
                XCENTRE = XLWIND + V8 * (XUWIND - XLWIND) / 100.
              ELSE
                CALL CH_REAL8( COMM(3:LEN), V8, *992 )
                XCENTRE = SNGL(V8)
              END IF
            ELSE
              XCENTRE = XMARGIN+XLWIND+0.5*(XUWIND-XLWIND-XMARGIN)
            END IF
            CENTRE = .TRUE.
            GO TO 88
          ELSE
            GO TO 994
          END IF
          IF( CENTRE .OR. RIGHT_JUST )GO TO 90
  88      IONE_OLD = IONE
          ITWO_OLD = ITWO
          Y_OLD = Y
          ASIS_OLD = ASIS
          FONT_OLD = FONT
          LENFONT_OLD = LENFONT
          HEIGHT_OLD = HEIGHT
          SPACING_OLD = SPACING
          XMARGIN_OLD = XMARGIN
          CTRL_OLD = CTRL
          ICOL_OLD = ICOL
          ITALICS_OLD = ITALICS
          IF( ITALICS )THEN
            ANGL_OLD = 20.
          ELSE
            ANGL_OLD = 0.
          END IF
  90      IF( ITWO .EQ. LENS2 )THEN
            IF( RIGHT_JUST )GO TO 40
            IF( CENTRE )GO TO 30
            GO TO 100
          END IF
        ELSE
          GO TO 80
        END IF
      END IF
      GO TO 60
 100  LINE2 = ' '
      LENS2 = 0
      GO TO 10
 800  CLOSE(IN_UNIT)
      CALL FLUSH_PLOT
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN
 991  CALL ERROR_MESSAGE('PLOTTEXT','opening '//STRINGS(2)(1:LENST(2)))
      CALL PUT_FORMSG
  92  RETURN 1
 992  CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL ERROR_MESSAGE('PLOTTEXT'
     & ,'converting '//CTRL(1:1)//COMM(1:LEN)//CTRL(2:2)//
     &  ' to real number(s)')
      CALL FLUSH_PLOT
      CLOSE(IN_UNIT)
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN 1
 993  CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL REAL4_TO_CHAR(ANGL,.FALSE.,SOUT,LENS)
      CALL ERROR_MESSAGE('PLOTTEXT'
     & ,'calling PSANGL with angle= '//SOUT(1:LENS))
      CALL FLUSH_PLOT
      CLOSE(IN_UNIT)
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN 1
 994  CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL REAL4_TO_CHAR(XLINE,.FALSE.,SOUT,LENS)
      CALL ERROR_MESSAGE('PLOTTEXT','incorrect text-format command '//
     & CTRL(1:1)//COMM(1:LEN)//CTRL(2:2)//' on line '//SOUT(1:LENS))
      CALL FLUSH_PLOT
      CLOSE(IN_UNIT)
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN 1
 995  CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL REAL4_TO_CHAR(XLINE,.FALSE.,SOUT,LENS)
      CALL ERROR_MESSAGE('PLOTTEXT'
     & ,'unpaired control characters on line '//SOUT(1:LENS))
      CALL FLUSH_PLOT
      CLOSE(IN_UNIT)
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN 1
 996  CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      CALL REAL4_TO_CHAR(XLINE,.FALSE.,SOUT,LENS)
      CALL ERROR_MESSAGE('PLOTTEXT','converting '//LINE2(II:II+1)//
     & ' to hexadecimal, on text line '//SOUT(1:LENS))
      CALL FLUSH_PLOT
      CLOSE(IN_UNIT)
      ASIS = .FALSE.
      ANGL = 0.0
      CALL PSANGL(ANGL,ADUM,*993)
      CALL PFONT(FONTSAVE(1:LENFSAVE),0)
      CALL SETNAM('PTYPE',0.0)
      RETURN 1
      END
