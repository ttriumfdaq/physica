      SUBROUTINE INTEGER_TO_CHAR( INT, PLUS, SOUT, LENS )
C
C   This subroutine converts a integer*4 number, INT, to a character
C   string, SOUT, with length LENS.
C   If PLUS is true, then positive numbers will have a plus sign in
C   front, otherwise not.
C   
C   Input:    INT      ---   INTEGER*4
C             PLUS     ---   LOGICAL*4
C
C   Output:   SOUT     ---   CHARACTER*15
C             LENS     ---   INTEGER*4
C
      CHARACTER*15 SOUT, SOUT2
      INTEGER*4    INT
      INTEGER*4    LENS
      LOGICAL      PLUS
CCC
      LENS = 1
      SOUT = ' '
      IF( PLUS )THEN
        IF( INT .LE. 0 )THEN
          WRITE(SOUT2,10)INT
   10     FORMAT(I15)
        ELSE
          WRITE(SOUT2,20)INT
   20     FORMAT(SP,I15)
        END IF
      ELSE
        WRITE(SOUT2,10)INT
      END IF
      DO I = 1, 15
        IF( SOUT2(I:I) .NE. ' ' )THEN
          LENS = 15-I+1
          SOUT(1:LENS) = SOUT2(I:15)
          RETURN
        END IF
      END DO
      RETURN
      END
