#include <stdlib.h>
/*#include <stdio.h>*/

 void *physica_malloc_( nbytes )
     int *nbytes;
  {
    /*
    void *dum;
    dum = valloc( *nbytes );
    printf("c:  new = %d\n",dum);
    return dum;
    */
    return (void*)malloc( *nbytes );
  }

/*
#include <malloc.h>

void physica_malloc_( nbytes, newaddr )
  int *nbytes;
  int *newaddr;
{
  int *p;
  p = (int *)malloc( (int)(*nbytes) );
  *newaddr = (int)p;
}
*/
