      SUBROUTINE MY_JPLOT( IX, IY, IPEN )
C                                                05/AUG/1980
C                                                C.J. KOST SIN
C     June 19th,1991: Renamed to MY_JPLOT to distinguish 
C            from the JPLOT routine of the GPLOT library
C 
      IMPLICIT REAL*8 (A-H,O-Z)

      REAL*8    A11, A13, A21, A23, A22, PIPI
      INTEGER*4 INDX, INDY, INDZ, NYPI, LIMITX
      COMMON /PL3D/ A11, A13, A21, A23, A22, PIPI
     &             ,INDX, INDY, INDZ, NYPI, LIMITX

      INTEGER*4 JXOLD, JYOLD, IPNOLD
      COMMON /JXPLT/ JXOLD, JYOLD, IPNOLD

      REAL*8    ARMIN, ARMAX, YDATANEW
      LOGICAL*4 FLAT, FIRST
      COMMON /PL3DC/ ARMIN, ARMAX, YDATANEW, FIRST, FLAT

      REAL*8    PDSTL
      LOGICAL*4 HCOLOUR
      COMMON /HST2D_COMMON/ PDSTL, HCOLOUR

      INTEGER*4 ICOL1, ICOL2
      COMMON /PLOT_COLOURS/ ICOL1, ICOL2

      INTEGER*4 IBIT_COMMON
      COMMON /BITMAP_DEVICE/ IBIT_COMMON

      REAL*8    ALEV(6), XX, YY
      INTEGER*4 ICIT(6), ipng(6)
      DATA ICIT / 2, 6, 4, 1, 3, 7 /
      data ipng / 4, 6, 3, 2, 7, 1 /
CCC
      GO TO 5
      ENTRY IPLOT( IX, IY, IPEN )
      IF( FIRST )THEN
        FIRST = .FALSE.
        IF( (ARMIN .EQ. ARMAX) .OR. .NOT.HCOLOUR )THEN
          FLAT = .TRUE.
        ELSE
          FLAT  = .FALSE.
          ATEMP = 0.0001D0*(ARMAX - ARMIN)
          ARMIN = ARMIN - ATEMP
          ARMAX = ARMAX + ATEMP
          ARINC = (ARMAX - ARMIN) / 6.D0
          DO I = 1, 6
            ALEV(I) = ARMIN + (I-1.D0)*ARINC
          END DO
        END IF
      END IF
      YDATA1 = YDATANEW
      YDATA2 = YDATANEW
      IF( HCOLOUR )THEN
        IF( FLAT )THEN
          IC2 = 6
        ELSE
          IDUM = INT((YDATA2-ARMIN)/ARINC)+1
          IDUM = MIN(6,IDUM)
          IDUM = MAX(1,IDUM)
          IC2 = IDUM
        END IF
        IF( IC2 .NE. ICOL2 )then
          if( ibit_common.eq.6 )then
            CALL PLOT_COLOR(icit(IC2),ipng(IC2))
          else
            CALL PLOT_COLOR(ICIT(IC2),IC2)
          endif
        endif
      END IF
      JXSAVE = IX
      JYSAVE = IY
      XX = IX/PIPI
      YY = IY/PIPI
      CALL HPLOT( XX, YY, 3 )
      JXOLD = IX
      JYOLD = IY
      RETURN
5     IF( .NOT.HCOLOUR .OR. FLAT )THEN
        IF( IPEN .NE. IPNOLD )THEN
          XX = JXOLD/PIPI
          YY = JYOLD/PIPI
          CALL HPLOT( XX, YY, IPNOLD )
        END IF
        IPNOLD = IPEN
        JXOLD  = IX
        JYOLD  = IY
        RETURN
      END IF
      IF( IPEN .EQ. IPNOLD )THEN
        YDATA2 = YDATANEW
        GO TO 100
      END IF

      LEND = INT( (YDATA2 - ARMIN) / ARINC ) + 1
      LEND = MAX(1,MIN(6,LEND))
      LSTR = INT( (YDATA1 - ARMIN) / ARINC ) + 1
      LSTR = MAX(1,MIN(6,LSTR))
20    IF( LSTR .NE. ICOL2 )then
        if( ibit_common.eq.6 )then
          CALL PLOT_COLOR(ICIT(lstr),ipng(lstr))
        else
          CALL PLOT_COLOR(ICIT(LSTR),LSTR)
        endif
      endif
      IF( LEND .EQ. LSTR )THEN
        XX = JXOLD/PIPI
        YY = JYOLD/PIPI
        CALL HPLOT( XX, YY, IPNOLD )
        JXSAVE = JXOLD
        JYSAVE = JYOLD
        IF( IPNOLD .NE. 3 )YDATA1 = YDATA2
        YDATA2 = YDATANEW
      ELSE IF( LEND .GT. LSTR )THEN
        LSTR  = LSTR + 1
        ATEMP = (ALEV(LSTR)-YDATA1)/(YDATA2-YDATA1)
        XX = (ATEMP * (JXOLD-JXSAVE) + JXSAVE)/PIPI
        YY = (ATEMP * (JYOLD-JYSAVE) + JYSAVE)/PIPI
        CALL HPLOT( XX, YY, IPNOLD )
        GO TO 20
      ELSE IF( LEND .LT. LSTR )THEN
        ATEMP = (ALEV(LSTR)-YDATA1)/(YDATA2-YDATA1)
        LSTR  = LSTR - 1
        XX = (ATEMP * (JXOLD-JXSAVE) + JXSAVE)/PIPI
        YY = (ATEMP * (JYOLD-JYSAVE) + JYSAVE)/PIPI
        CALL HPLOT( XX, YY, IPNOLD )
        GO TO 20
      END IF
100   IPNOLD = IPEN
      JXOLD  = IX
      JYOLD  = IY
      RETURN
      END
