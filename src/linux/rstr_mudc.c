#include "mud.h"

 int fmud_openread_( filename )
     char* filename;
  {
    int filehandle;
    UINT32 dum;
    filehandle = MUD_openRead( filename, &dum );
    return filehandle;
  }

 void fmud_closeread_( fh )
     int* fh;
  {
    int istat;
    istat = MUD_closeRead( *fh );
    return;
  }
 
 void fmud_getrunnumber_( fh, runNumber )
     int* fh;
     int* runNumber;
  {
    int istat;
    /*
    UINT32 rn;
    rn = (UINT32)(*runNumber);
    istat = MUD_getRunNumber( *fh, &rn );
    */
    istat = MUD_getRunNumber( *fh, (UINT32*)runNumber );
    return;
  }
 
 void fmud_getrundesc_( fh, i )
     int* fh;
     int* i;
  {
    int istat;
    /*
    UINT32 pType;
    pType = (UINT32)(*i);
    istat = MUD_getRunDesc( *fh, &pType );
    */
    istat = MUD_getRunDesc( *fh, (UINT32*)i );    
    return;
  }

 void fmud_getexptnumber_( fh, i )
     int* fh;
     int* i;
  {
    int istat;
    /*
    UINT32 pExptNumber;
    pExptNumber = (UINT32)(*i);
    istat = MUD_getExptNumber( *fh, &pExptNumber );
    */
    istat = MUD_getExptNumber( *fh, (UINT32*)i );
    return;
  }

 void fmud_getelapsedsec_( fh, i )
     int* fh;
     int* i;
  {
    int istat;
    /*
    UINT32 pElapsedSec;
    pElapsedSec = (UINT32)(*i);
    istat = MUD_getElapsedSec( *fh, &pElapsedSec );
    */
    istat = MUD_getElapsedSec( *fh, (UINT32*)i );
    return;
  }

 void fmud_gettimebegin_( fh, i )
     int* fh;
     int* i;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*i);
    istat = MUD_getTimeBegin( *fh, &dum );
    */
    istat = MUD_getTimeBegin( *fh, (UINT32*)i );
    return;
  }
 
 void fmud_gettimeend_( fh, i )
     int* fh;
     int* i;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*i);
    istat = MUD_getTimeEnd( *fh, &dum );
    */
    istat = MUD_getTimeEnd( *fh, (UINT32*)i );
    return;
  }

 void fmud_gettitle_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getTitle( *fh, s );
    return;
  }

 void fmud_getlab_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getLab( *fh, s );
    return;
  }

 void fmud_getarea_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getArea( *fh, s );
    return;
  }

 void fmud_getmethod_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getMethod( *fh, s );
    return;
  }

 void fmud_getapparatus_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getApparatus( *fh, s );
    return;
  }

 void fmud_getinsert_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getInsert( *fh, s );
    return;
  }

 void fmud_getsample_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getSample( *fh, s );
    return;
  }

 void fmud_getorient_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getOrient( *fh, s );
    return;
  }

 void fmud_gettemperature_( fh, s, istat )
     int* fh;
     char* s;
     int* istat;
  {
    *istat = MUD_getTemperature( *fh, s );
    return;
  }

 void fmud_getfield_( fh, s, istat )
     int* fh;
     char* s;
     int* istat;
  {
    *istat = MUD_getField( *fh, s );
    return;
  }

 void fmud_getdas_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getDas( *fh, s );
    return;
  }

 void fmud_getexperimenter_( fh, s )
     int* fh;
     char* s;
  {
    int istat;
    istat = MUD_getExperimenter( *fh, s );
    return;
  }

 void fmud_getcomments_( fh, type, num )
     int* fh;
     int* type;
     int* num;
  {
    int istat;
    /*
    UINT32 dum1, dum2;
    dum1 = (UINT32)(*type);
    dum2 = (UINT32)(*num);
    istat = MUD_getComments( *fh, &dum1, &dum2 );
    */
    istat = MUD_getComments( *fh, (UINT32*)type, (UINT32*)num );
    return;
  }

 void fmud_getcommentprev_( fh, num, prev )
     int* fh;
     int* num;
     int* prev;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*prev);
    istat = MUD_getCommentPrev( *fh, *num, &dum );
    */
    istat = MUD_getCommentPrev( *fh, *num, (UINT32*)prev );
    return;
  }

 void fmud_getcommentnext_( fh, num, next )
     int* fh;
     int* num;
     int* next;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*next);
    istat = MUD_getCommentPrev( *fh, *num, &dum );
    */
    istat = MUD_getCommentPrev( *fh, *num, (UINT32*)next );
    return;
  }

 void fmud_getcommenttime_( fh, num, time )
     int* fh;
     int* num;
     int* time;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*time);
    istat = MUD_getCommentTime( *fh, *num, &dum );
    */
    istat = MUD_getCommentTime( *fh, *num, (UINT32*)time );
    return;
  }

 void fmud_getcommentauthor_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getCommentAuthor( *fh, *num, s );
    return;
  }

 void fmud_getcommenttitle_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getCommentTitle( *fh, *num, s );
    return;
  }

 void fmud_getcommentbody_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getCommentBody( *fh, *num, s );
    return;
  }

 void fmud_gethists_( fh, type, num )
     int* fh;
     int* type;
     int* num;
  {
    int istat;
    /*
    UINT32 dum1, dum2;
    dum1 = (UINT32)(*type);
    dum2 = (UINT32)(*num);
    istat = MUD_getHists( *fh, &dum1, &dum2 );
    */
    istat = MUD_getHists( *fh, (UINT32*)type, (UINT32*)num );
    return;
  }

 void fmud_gethisttitle_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getHistTitle( *fh, *num, s );
    return;
  }

 void fmud_gethistnumbins_( fh, num, nbins )
     int* fh;
     int* num;
     int* nbins;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*nbins);
    istat = MUD_getHistNumBins( *fh, *num, &dum );
    */
    istat = MUD_getHistNumBins( *fh, *num, (UINT32*)nbins );
    return;
  }

 void fmud_gethistdata_( fh, num, data )
     int* fh;
     int* num;
     int* data;
  {
    int istat;
    istat = MUD_getHistData( *fh, *num, data );
    return;
  }

 void fmud_gethisttype_( fh, num, type )
     int* fh;
     int* num;
     int* type;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*type);
    istat = MUD_getHistType( *fh, *num, &dum );
    */
    istat = MUD_getHistType( *fh, *num, (UINT32*)type );
    return;
  }

 void fmud_gethistnumbytes_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistNumBytes( *fh, *num, &dum );
    */
    istat = MUD_getHistNumBytes( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistbytesperbin_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistBytesPerBin( *fh, *num, &dum );
    */
    istat = MUD_getHistBytesPerBin( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistfsperbin_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistFsPerBin( *fh, *num, &dum );
    */
    istat = MUD_getHistFsPerBin( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistt0_ps_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistT0_Ps( *fh, *num, &dum );
    */
    istat = MUD_getHistT0_Ps( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistt0_bin_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistT0_Bin( *fh, *num, &dum );
    */
    istat = MUD_getHistT0_Bin( *fh, *num, (UINT32*)bytes );
    return;
  }
 
 void fmud_gethistgoodbin1_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistGoodBin1( *fh, *num, &dum );
    */
    istat = MUD_getHistGoodBin1( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistgoodbin2_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistGoodBin2( *fh, *num, &dum );
    */
    istat = MUD_getHistGoodBin2( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistbkgd1_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistBkgd1( *fh, *num, &dum );
    */
    istat = MUD_getHistBkgd1( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistbkgd2_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistBkgd2( *fh, *num, &dum );
    */
    istat = MUD_getHistBkgd2( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_gethistnumevents_( fh, num, bytes )
     int* fh;
     int* num;
     int* bytes;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*bytes);
    istat = MUD_getHistNumEvents( *fh, *num, &dum );
    */
    istat = MUD_getHistNumEvents( *fh, *num, (UINT32*)bytes );
    return;
  }

 void fmud_getscalers_( fh, ptype, pnum )
     int* fh;
     int* ptype;
     int* pnum;
  {
    int istat;
    /*
    UINT32 dum1;
    UINT32 dum2;
    dum1 = (UINT32)(*ptype);
    dum2 = (UINT32)(*pnum);
    istat = MUD_getScalers( *fh, &dum1, &dum2 );
    */
    istat = MUD_getScalers( *fh, (UINT32*)ptype, (UINT32*)pnum );
    return;
  }

 void fmud_getscalercounts_( fh, num, counts )
     int* fh;
     int* num;
     int* counts;
  {
    int istat;
    /*
    UINT32 dum;
    dum = (UINT32)(*counts);
    istat = MUD_getScalerCounts( *fh, *num, &dum );
    */
    istat = MUD_getScalerCounts( *fh, *num, (UINT32*)counts );
    return;
  }

 void fmud_getscalerlabel_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getScalerLabel( *fh, *num, s );
    return;
  }

 void fmud_getindvars_( fh, ptype, pnum )
     int* fh;
     int* ptype;
     int* pnum;
  {
    int istat;
    /*
    UINT32 dum1;
    UINT32 dum2;
    dum1 = (UINT32)(*ptype);
    dum2 = (UINT32)(*pnum);
    istat = MUD_getIndVars( *fh, &dum1, &dum2 );
    */
    istat = MUD_getIndVars( *fh, (UINT32*)ptype, (UINT32*)pnum );
    return;
  }

 void fmud_getindvarlow_( fh, num, d )
     int* fh;
     int* num;
     double* d;
  {
    int istat;
    istat = MUD_getIndVarLow( *fh, *num, d );
    return;
  }

 void fmud_getindvarhigh_( fh, num, d )
     int* fh;
     int* num;
     double* d;
  {
    int istat;
    istat = MUD_getIndVarHigh( *fh, *num, d );
    return;
  }

 void fmud_getindvarmean_( fh, num, d )
     int* fh;
     int* num;
     double* d;
  {
    int istat;
    istat = MUD_getIndVarMean( *fh, *num, d );
    return;
  }

 void fmud_getindvarstddev_( fh, num, d )
     int* fh;
     int* num;
     double* d;
  {
    int istat;
    istat = MUD_getIndVarStddev( *fh, *num, d );
    return;
  }

 void fmud_getindvarskewness_( fh, num, d )
     int* fh;
     int* num;
     double* d;
  {
    int istat;
    istat = MUD_getIndVarSkewness( *fh, *num, d );
    return;
  }

 void fmud_getindvarname_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getIndVarName( *fh, *num, s );
    return;
  }

 void fmud_getindvardescription_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getIndVarDescription( *fh, *num, s );
    return;
  }

 void fmud_getindvarunits_( fh, num, s )
     int* fh;
     int* num;
     char* s;
  {
    int istat;
    istat = MUD_getIndVarUnits( *fh, *num, s );
    return;
  }
 
