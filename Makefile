# makefile for PHYSICA 

SHELL = /bin/sh

SRC = src
GSRC = gplot/src
MSRC = mud/src
CERNDIR = 

UNAMES := $(shell uname -s)

#FORTRAN   = gfortran -O -g -m32 -std=legacy -fdec # gfortran cannot compile physica! KO Nov2021.
FORTRAN   = g77 -O -g -m32
CC        = gcc -O -g -m32
#MACROS    = -Dg77 -Dunix -DBEND
MACROS    = -Dg77 -Dunix
#FFLAGS    = -malign-power -fno-second-underscore -fno-automatic
FFLAGS    = -fno-second-underscore -fno-automatic -finit-local-zero
CFLAGS    = 
#CERNLIBS  = $(CERNDIR)/lib/libkernlib.a $(CERNDIR)/lib/libpacklib.a
MYLIBS    = lib/physica.a lib/mud.a lib/gplot.a

ifeq ($(UNAMES), Darwin)

INCLUDES  = -I/opt/local/include
OTHERLIBS = -L/opt/local/lib -lX11 -lgd -lpng -lz -lreadline -lncurses

STATIC_OTHERLIBS = /opt/local/lib/libX11.dylib /opt/local/lib/libgd.dylib /opt/local/lib/libpng.dylib /opt/local/lib/libreadline.dylib /usr/local/lib/libg2c.a /usr/local/lib/libfrtbegin.a
#-lX11
# /usr/lib/libgd.a /usr/lib/libpng12.a /usr/lib/libz.a /usr/lib/libreadline.a /usr/lib/libncurses.a /usr/lib/libm.a -L/usr/lib/gcc/i386-redhat-linux/3.4.6 -lfrtbegin /usr/lib/gcc/i386-redhat-linux/3.4.6/libg2c.a

else

INCLUDES  = -I/usr/X11R6/include -I/usr/local/include

OTHERLIBS = -lX11 -lgd -lpng12 -lz -lreadline -lncurses

STATIC_OTHERLIBS = -lX11 /usr/lib/libgd.a /usr/lib/libpng12.a /usr/lib/libz.a /usr/lib/libreadline.a /usr/lib/libncurses.a /usr/lib/libm.a -L/usr/lib/gcc/i386-redhat-linux/3.4.6 -lfrtbegin /usr/lib/gcc/i386-redhat-linux/3.4.6/libg2c.a

endif

FSRCS = $(SRC)/acm624.F \
        $(SRC)/acm626.F \
        $(SRC)/auto_axes.F \
        $(SRC)/best_fit.F \
        $(SRC)/bin.F \
        $(SRC)/bin2d.F \
        $(SRC)/bindec.F \
        $(SRC)/box_dens.F \
        $(SRC)/calcfn.F \
        $(SRC)/calcop.F \
        $(SRC)/call_sharable.F \
        $(SRC)/cft.F \
        $(SRC)/ch_real8.F \
        $(SRC)/chng_banner.F \
        $(SRC)/chck_resname.F \
        $(SRC)/cntour.F \
        $(SRC)/commensurate.F \
        $(SRC)/contour.F \
        $(SRC)/cnvrt_units.F \
        $(SRC)/crd_trnsfrm.F \
        $(SRC)/copy.F \
        $(SRC)/dlt_txt_var.F \
        $(SRC)/dlt_var.F \
        $(SRC)/density_mono.F \
        $(SRC)/density_plot.F \
        $(SRC)/destroy.F \
        $(SRC)/device.F \
        $(SRC)/diffusion.F \
        $(SRC)/dinter.F \
        $(SRC)/display.F \
        $(SRC)/dspl_fill.F \
        $(SRC)/dspl_font.F \
        $(SRC)/dspl_lines.F \
        $(SRC)/dspl_pchar.F \
        $(SRC)/dspl_special.F \
        $(SRC)/dither.F \
        $(SRC)/dlqf.F \
        $(SRC)/draw_border.F \
        $(SRC)/drzfun.F \
        $(SRC)/dssol.F \
        $(SRC)/eigen_rs.F \
        $(SRC)/ellfit.F \
        $(SRC)/ellipse.F \
        $(SRC)/erase_rect.F \
        $(SRC)/error_msg.F \
        $(SRC)/eval_exprs.F \
        $(SRC)/excalt.F \
        $(SRC)/excalx.F \
        $(SRC)/excode.F \
        $(SRC)/execut_dum.F \
        $(SRC)/exfnx.F \
        $(SRC)/exopx.F \
        $(SRC)/expand.F \
        $(SRC)/exptxt.F \
        $(SRC)/exrpn.F \
        $(SRC)/exrpnt.F \
        $(SRC)/extype.F \
        $(SRC)/exvalx.F \
        $(SRC)/fcint.F \
        $(SRC)/figure.F \
        $(SRC)/filter.F \
        $(SRC)/find_median.F \
        $(SRC)/fit.F \
        $(SRC)/fmin.F \
        $(SRC)/framer.F \
        $(SRC)/fzero.F \
        $(SRC)/gen_vec.F \
        $(SRC)/get_matrix.F \
        $(SRC)/get_otxtvar.F \
        $(SRC)/get_ovar.F \
        $(SRC)/get_ovname.F \
        $(SRC)/get_scalar.F \
        $(SRC)/get_tmp_spc.F \
        $(SRC)/get_txt_var.F \
        $(SRC)/get_variable.F \
        $(SRC)/get_verror.F \
        $(SRC)/get_vindex.F \
        $(SRC)/get_vector.F \
        $(SRC)/get_wild.F \
        $(SRC)/getnam.F \
        $(SRC)/glegend.F \
        $(SRC)/menu_gxy.F \
        $(SRC)/graph_replot.F \
        $(SRC)/graph_vecs.F \
        $(SRC)/grid.F \
        $(SRC)/hid12.F \
        $(SRC)/hide.F \
        $(SRC)/hplot.F \
        $(SRC)/hst2d.F \
        $(SRC)/init_physica.F \
        $(SRC)/inp_matrix.F \
        $(SRC)/inside_box.F \
        $(SRC)/inside_poly.F \
        $(SRC)/int_to_char.F \
        $(SRC)/labels.F \
        $(SRC)/lgrng_deriv.F \
        $(SRC)/lgnd_lbl_len.F \
        $(SRC)/list_matrix.F \
        $(SRC)/list_vectors.F \
        $(SRC)/lqline.F \
        $(SRC)/lubksb.F \
        $(SRC)/ludcmp.F \
        $(SRC)/median8.F \
        $(SRC)/minvar.F \
        $(SRC)/mirror.F \
        $(SRC)/my_hist2d.F \
        $(SRC)/my_inquire.F \
        $(SRC)/my_jplot.F \
        $(SRC)/nextprime.F \
        $(SRC)/nova_dum.F \
        $(SRC)/prs_in_line.F \
        $(SRC)/parse_name.F \
        $(SRC)/pause2.F \
        $(SRC)/pd_xhair.F \
        $(SRC)/peak.F \
        $(SRC)/pearson.F \
        $(SRC)/phys_ascale.F \
        $(SRC)/phys_close.F \
        $(SRC)/phys_gauto.F \
        $(SRC)/phys_get.F \
        $(SRC)/phys_gplot.F \
        $(SRC)/phys_gplot_r.F \
        $(SRC)/phys_gplt.F \
        $(SRC)/phys_menu.F \
        $(SRC)/phys_set.F \
        $(SRC)/phys_stat.F \
        $(SRC)/phys_user.F \
        $(SRC)/physica_fcn.F \
        $(SRC)/pie_graph.F \
        $(SRC)/pixel_extent.F \
        $(SRC)/plot3d.F \
        $(SRC)/plot_legend.F \
        $(SRC)/plot_line.F \
        $(SRC)/plot_slices.F \
        $(SRC)/plottext.F \
        $(SRC)/polygon.F \
        $(SRC)/pre_parse.F \
        $(SRC)/psangl.F \
        $(SRC)/put_fiowa_v.F \
        $(SRC)/put_hbook_v.F \
        $(SRC)/put_txt_var.F \
        $(SRC)/put_var.F \
        $(SRC)/pxdens.F \
        $(SRC)/question.F \
        $(SRC)/rd_command.F \
        $(SRC)/rd_exec_file.F \
        $(SRC)/rd_matrix.F \
        $(SRC)/rd_physica.F \
        $(SRC)/rd_scalars.F \
        $(SRC)/rd_text.F \
        $(SRC)/rd_vectors.F \
        $(SRC)/r4_to_char.F \
        $(SRC)/r8_to_char.F \
        $(SRC)/rebin.F \
        $(SRC)/remake_ln.F \
        $(SRC)/rename_var.F \
        $(SRC)/rplt_clear.F \
        $(SRC)/rplt_lgnd.F \
        $(SRC)/rplt_store.F \
        $(SRC)/reset_wndos.F \
        $(SRC)/rstr_chaos.F \
        $(SRC)/rstr_fiowa.F \
        $(SRC)/rstr_xfiowa.F \
        $(SRC)/rstr_imsr_dum.F \
        $(SRC)/rstr_msr.F \
        $(SRC)/rite.F \
        $(SRC)/save.F \
        $(SRC)/savgol.F \
        $(SRC)/scale1d.F \
        $(SRC)/scale2.F \
        $(SRC)/scales.F \
        $(SRC)/set_window.F \
        $(SRC)/show.F \
        $(SRC)/simq.F \
        $(SRC)/sinter1.F \
        $(SRC)/sinter2.F \
        $(SRC)/sort.F \
        $(SRC)/ssort8.F \
        $(SRC)/text.F \
        $(SRC)/three_d_plot.F \
        $(SRC)/tile.F \
        $(SRC)/teqs.F \
        $(SRC)/tlen.F \
        $(SRC)/transform.F \
        $(SRC)/tt_buff.F \
        $(SRC)/unique.F \
        $(SRC)/update_hist.F \
        $(SRC)/variable.F \
        $(SRC)/volumecalc.F \
        $(SRC)/warning_msg.F \
        $(SRC)/window.F \
        $(SRC)/wrld_phys.F \
        $(SRC)/wrld_to_gplt.F \
        $(SRC)/zero_lines.F \
        $(SRC)/bell.F \
        $(SRC)/calctn.F \
        $(SRC)/chsize.F \
        $(SRC)/digitize.F \
        $(SRC)/execute.F \
        $(SRC)/free_space.F \
        $(SRC)/get_cmnd.F \
        $(SRC)/get_space.F \
        $(SRC)/hardcopy.F \
        $(SRC)/inp_vector.F \
        $(SRC)/keysearch.F \
        $(SRC)/lib_free_vm.F \
        $(SRC)/lib_get_vm.F \
        $(SRC)/my_pick.F \
        $(SRC)/phys_main.F \
        $(SRC)/prcs_cmnd.F \
        $(SRC)/put_formsg.F \
        $(SRC)/put_sysmsg.F \
        $(SRC)/rd_key.F \
        $(SRC)/restore.F \
        $(SRC)/cern_dum.F \
        $(SRC)/sleep2.F \
        $(SRC)/write_phys.F \
        $(SRC)/dum_motif.F \
        $(SRC)/resize.F \
        $(SRC)/use_file.F \
        $(SRC)/rstr_mud.F

FSRCSL = $(SRC)/linux/trigd.F

FSRCSF = $(SRC)/functions/adigam.F \
        $(SRC)/functions/aerf.F \
        $(SRC)/functions/airey.F \
        $(SRC)/functions/airy.F \
        $(SRC)/functions/alogam.F \
        $(SRC)/functions/aslegf.F \
        $(SRC)/functions/besi0.F \
        $(SRC)/functions/besi1.F \
        $(SRC)/functions/besj0.F \
        $(SRC)/functions/besj1.F \
        $(SRC)/functions/besjn.F \
        $(SRC)/functions/besk0.F \
        $(SRC)/functions/besk1.F \
        $(SRC)/functions/beslri.F \
        $(SRC)/functions/besy0.F \
        $(SRC)/functions/besy1.F \
        $(SRC)/functions/beta.F \
        $(SRC)/functions/betai.F \
        $(SRC)/functions/binom.F \
        $(SRC)/functions/biry.F \
        $(SRC)/functions/bivnor.F \
        $(SRC)/functions/cdigam.F \
        $(SRC)/functions/chisq.F \
        $(SRC)/functions/chisqi.F \
        $(SRC)/functions/chlogu.F \
        $(SRC)/functions/clogam.F \
        $(SRC)/functions/cosint.F \
        $(SRC)/functions/coulomb.F \
        $(SRC)/functions/cragam.F \
        $(SRC)/functions/cwerf.F \
        $(SRC)/functions/d9aimp.F \
        $(SRC)/functions/d9b0mp.F \
        $(SRC)/functions/d9b1mp.F \
        $(SRC)/functions/d9chu.F \
        $(SRC)/functions/d9gmic.F \
        $(SRC)/functions/d9gmit.F \
        $(SRC)/functions/d9lgic.F \
        $(SRC)/functions/d9lgit.F \
        $(SRC)/functions/d9lgmc.F \
        $(SRC)/functions/daie.F \
        $(SRC)/functions/daws.F \
        $(SRC)/functions/dawson.F \
        $(SRC)/functions/dbie.F \
        $(SRC)/functions/dcot.F \
        $(SRC)/functions/dcsevl.F \
        $(SRC)/functions/dexprl.F \
        $(SRC)/functions/dgamlm.F \
        $(SRC)/functions/dgamma.F \
        $(SRC)/functions/dgamr.F \
        $(SRC)/functions/dilog.F \
        $(SRC)/functions/dlbeta.F \
        $(SRC)/functions/dlgams.F \
        $(SRC)/functions/dlngam.F \
        $(SRC)/functions/dlnrel.F \
        $(SRC)/functions/ebesi0.F \
        $(SRC)/functions/ebesi1.F \
        $(SRC)/functions/ebesk0.F \
        $(SRC)/functions/ebesk1.F \
        $(SRC)/functions/ei.F \
        $(SRC)/functions/einell.F \
        $(SRC)/functions/ellice.F \
        $(SRC)/functions/ellick.F \
        $(SRC)/functions/eltime.F \
        $(SRC)/functions/erf.F \
        $(SRC)/functions/erfc.F \
        $(SRC)/functions/expint.F \
        $(SRC)/functions/expn.F \
        $(SRC)/functions/factor.F \
        $(SRC)/functions/ferdir.F \
        $(SRC)/functions/finell.F \
        $(SRC)/functions/fisher.F \
        $(SRC)/functions/frec12.F \
        $(SRC)/functions/freq.F \
        $(SRC)/functions/fres12.F \
        $(SRC)/functions/gamaic.F \
        $(SRC)/functions/gamait.F \
        $(SRC)/functions/gamma.F \
        $(SRC)/functions/gammai.F \
        $(SRC)/functions/gammln.F \
        $(SRC)/functions/gammq.F \
        $(SRC)/functions/gamra.F \
        $(SRC)/functions/gamra1.F \
        $(SRC)/functions/gaussin.F \
        $(SRC)/functions/hypgeo.F \
        $(SRC)/functions/initds.F \
        $(SRC)/functions/kel2.F \
        $(SRC)/functions/kelvin.F \
        $(SRC)/functions/normal.F \
        $(SRC)/functions/polgam.F \
        $(SRC)/functions/polyml.F \
        $(SRC)/functions/prob.F \
        $(SRC)/functions/psi.F \
        $(SRC)/functions/radmac.F \
        $(SRC)/functions/sinint.F \
        $(SRC)/functions/spence.F \
        $(SRC)/functions/strh0.F \
        $(SRC)/functions/strh1.F \
        $(SRC)/functions/stud.F \
        $(SRC)/functions/studin.F \
        $(SRC)/functions/tina.F \
        $(SRC)/functions/voigt.F \
        $(SRC)/functions/walsh.F \
        $(SRC)/functions/wigner.F \
        $(SRC)/functions/d1mach_osx.F

CSRCS = $(SRC)/physica_malloc.c \
        $(SRC)/physica_free.c \
        $(SRC)/x_resize_window.c \
        $(SRC)/digitize_png.c \
        $(SRC)/readline_wrapper.c

CSRCSL = $(SRC)/linux/rstr_mudc.c

GPLOT_FSRCS = $(GSRC)/amsinit.F \
              $(GSRC)/arc.F \
              $(GSRC)/arc_dwg.F \
              $(GSRC)/arcv.F \
              $(GSRC)/ascebc.F \
              $(GSRC)/asort.F \
              $(GSRC)/aux_port_out.F \
              $(GSRC)/bell.F \
              $(GSRC)/bitmap_free.F \
              $(GSRC)/bitmap_get.F \
              $(GSRC)/btd.F \
              $(GSRC)/calculator.F \
              $(GSRC)/cft.F \
              $(GSRC)/ch_real.F \
              $(GSRC)/check_buff.F \
              $(GSRC)/check_buff2.F \
              $(GSRC)/chsize.F \
              $(GSRC)/cils2.F \
              $(GSRC)/cltrans.F \
              $(GSRC)/conv_r_to_ch.F \
              $(GSRC)/conv_to_ch.F \
              $(GSRC)/conv_to_uc.F \
              $(GSRC)/convlc_to_uc.F \
              $(GSRC)/copy_array.F \
              $(GSRC)/del_array.F \
              $(GSRC)/datetime.F \
              $(GSRC)/deriv3.F \
              $(GSRC)/dexp10.F \
              $(GSRC)/dither.F \
              $(GSRC)/dline.F \
              $(GSRC)/dspl11.F \
              $(GSRC)/dsplxy.F \
              $(GSRC)/dspxys.F \
              $(GSRC)/dwg_batch_cl.F \
              $(GSRC)/dwg_close.F \
              $(GSRC)/dwg_format.F \
              $(GSRC)/dwg_next.F \
              $(GSRC)/dwg_output.F \
              $(GSRC)/dwg_stroke.F \
              $(GSRC)/ellcon.F \
              $(GSRC)/ellfit.F \
              $(GSRC)/ellips.F \
              $(GSRC)/end_plot.F \
              $(GSRC)/erase_recta.F \
              $(GSRC)/find_unit.F \
              $(GSRC)/flush_plot.F \
              $(GSRC)/fnice.F \
              $(GSRC)/fourt.F \
              $(GSRC)/free_space.F \
              $(GSRC)/get_array.F \
              $(GSRC)/get_plot_dev.F \
              $(GSRC)/get_space.F \
              $(GSRC)/gks_plot.F \
              $(GSRC)/gksdum.F \
              $(GSRC)/hardcopy_rng.F \
              $(GSRC)/hatch.F \
              $(GSRC)/hex_to_ascii.F \
              $(GSRC)/houston_plot.F \
              $(GSRC)/hpsub.F \
              $(GSRC)/impress_plot.F \
              $(GSRC)/iplot.F \
              $(GSRC)/it_is.F \
              $(GSRC)/jplot.F \
              $(GSRC)/left_justify.F \
              $(GSRC)/lensig.F \
              $(GSRC)/lib_out.F \
              $(GSRC)/line_thick.F \
              $(GSRC)/lower_case.F \
              $(GSRC)/lqline.F \
              $(GSRC)/lwrcase.F \
              $(GSRC)/minmax.F \
              $(GSRC)/mod2.F \
              $(GSRC)/monitor2_rng.F \
              $(GSRC)/monitor_rng.F \
              $(GSRC)/movec.F \
              $(GSRC)/mplot.F \
              $(GSRC)/mxhelp.F \
              $(GSRC)/nargs.F \
              $(GSRC)/pack_buffer.F \
              $(GSRC)/pack_buffer2.F \
              $(GSRC)/pen_down.F \
              $(GSRC)/pen_up.F \
              $(GSRC)/pixel_extent.F \
              $(GSRC)/plot_color.F \
              $(GSRC)/plot_device.F \
              $(GSRC)/plot_device2.F \
              $(GSRC)/plot_end.F \
              $(GSRC)/plot_level.F \
              $(GSRC)/plot_level2.F \
              $(GSRC)/plot_mode.F \
              $(GSRC)/plot_mode2.F \
              $(GSRC)/plot_mon.F \
              $(GSRC)/plot_mon2.F \
              $(GSRC)/plot_point.F \
              $(GSRC)/plot_r.F \
              $(GSRC)/plot_x.F \
              $(GSRC)/plots.F \
              $(GSRC)/plottplt.F \
              $(GSRC)/ps_plot.F \
              $(GSRC)/r_plot.F \
              $(GSRC)/rdgl.F \
              $(GSRC)/read_plot.F \
              $(GSRC)/readline.F \
              $(GSRC)/readln.F \
              $(GSRC)/regis_mode.F \
              $(GSRC)/regis_mode2.F \
              $(GSRC)/ricon.F \
              $(GSRC)/scale1.F \
              $(GSRC)/scale2.F \
              $(GSRC)/segment_cr.F \
              $(GSRC)/segment_crh.F \
              $(GSRC)/seiko_plot.F \
              $(GSRC)/seiko_plot2.F \
              $(GSRC)/set_plot_dev.F \
              $(GSRC)/setc.F \
              $(GSRC)/simq.F \
              $(GSRC)/sinter.F \
              $(GSRC)/status.F \
              $(GSRC)/symbol.F \
              $(GSRC)/transparent.F \
              $(GSRC)/upper_case.F \
              $(GSRC)/uprcase.F \
              $(GSRC)/uprcasel.F \
              $(GSRC)/vbuff.F \
              $(GSRC)/vdt_cursor.F \
              $(GSRC)/vhelp.F \
              $(GSRC)/video_off.F \
              $(GSRC)/window_clip.F \
              $(GSRC)/write_hppai.F \
              $(GSRC)/write_la100.F \
              $(GSRC)/write_gks.F \
              $(GSRC)/write_houst.F \
              $(GSRC)/write_hpp.F \
              $(GSRC)/write_impr.F \
              $(GSRC)/write_ln03.F \
              $(GSRC)/write_rdgl.F \
              $(GSRC)/write_ps.F \
              $(GSRC)/write_pt.F \
              $(GSRC)/write_px.F \
              $(GSRC)/zero_array.F \
              $(GSRC)/chreal.F \
              $(GSRC)/clear_plot.F \
              $(GSRC)/cpu_limit.F \
              $(GSRC)/cpu_time.F \
              $(GSRC)/crosshair_r.F \
              $(GSRC)/ctrlc_trap.F \
              $(GSRC)/dsplft.F \
              $(GSRC)/dwg.F \
              $(GSRC)/dwg_batch.F \
              $(GSRC)/dwg_list.F \
              $(GSRC)/dwg_open.F \
              $(GSRC)/findc.F \
              $(GSRC)/findst.F \
              $(GSRC)/formsg.F \
              $(GSRC)/generic_term.F \
              $(GSRC)/get_hardtype.F \
              $(GSRC)/get_pltype.F \
              $(GSRC)/get_prcnam.F \
              $(GSRC)/get_termtype.F \
              $(GSRC)/get_username.F \
              $(GSRC)/graphics_hc.F \
              $(GSRC)/igc.F \
              $(GSRC)/lib_free_vm.F \
              $(GSRC)/lib_get_vm.F \
              $(GSRC)/list_fnames.F \
              $(GSRC)/lswap.F \
              $(GSRC)/node_name.F \
              $(GSRC)/pascebc.F \
              $(GSRC)/pause2.F \
              $(GSRC)/pebcasc.F \
              $(GSRC)/pfont.F \
              $(GSRC)/plot_hardc.F \
              $(GSRC)/print_buff.F \
              $(GSRC)/print_buff2.F \
              $(GSRC)/psym.F \
              $(GSRC)/psym_dwg.F \
              $(GSRC)/psymbold.F \
              $(GSRC)/put_formsg.F \
              $(GSRC)/put_sysmsg.F \
              $(GSRC)/read_key.F \
              $(GSRC)/realch.F \
              $(GSRC)/sleep2.F \
              $(GSRC)/sysmsg.F \
              $(GSRC)/term_width.F \
              $(GSRC)/tt_input.F \
              $(GSRC)/write_hp300.F \
              $(GSRC)/write_hp300c.F \
              $(GSRC)/write_hpjet.F \
              $(GSRC)/write_hpjetc.F \
              $(GSRC)/put_dwg.F

GPLOT_FSRCSU = $(GSRC)/unix/read_key_res.F \
              $(GSRC)/unix/ctrlc_hand.F \
              $(GSRC)/unix/evaluate.F \
              $(GSRC)/unix/ibatch.F \
              $(GSRC)/unix/lok_e_trap.F

GPLOT_FSRCSG = $(GSRC)/gplot/auto_scale.F \
              $(GSRC)/gplot/axline.F \
              $(GSRC)/gplot/axlog.F \
              $(GSRC)/gplot/do_command.F \
              $(GSRC)/gplot/gauto.F \
              $(GSRC)/gplot/gaxis.F \
              $(GSRC)/gplot/get_item.F \
              $(GSRC)/gplot/getdyn.F \
              $(GSRC)/gplot/getlab.F \
              $(GSRC)/gplot/getnam.F \
              $(GSRC)/gplot/getpcnt.F \
              $(GSRC)/gplot/gplot.F \
              $(GSRC)/gplot/gplot_conv.F \
              $(GSRC)/gplot/gplot_disp_n.F \
              $(GSRC)/gplot/gplot_menu.F \
              $(GSRC)/gplot/gplot_number.F \
              $(GSRC)/gplot/gplot_r.F \
              $(GSRC)/gplot/gplot_r_init.F \
              $(GSRC)/gplot/gplot_rest.F \
              $(GSRC)/gplot/gplot_rest_f.F \
              $(GSRC)/gplot/gplot_save.F \
              $(GSRC)/gplot/gplot_save_f.F \
              $(GSRC)/gplot/gplot_setup.F \
              $(GSRC)/gplot/gplot_short.F \
              $(GSRC)/gplot/gplot_symbol.F \
              $(GSRC)/gplot/gplot_txt.F \
              $(GSRC)/gplot/gploti.F \
              $(GSRC)/gplot/gplt.F \
              $(GSRC)/gplot/is_it_dynam.F \
              $(GSRC)/gplot/labxy.F \
              $(GSRC)/gplot/name_in_hex.F \
              $(GSRC)/gplot/setlab.F \
              $(GSRC)/gplot/setnam.F \
              $(GSRC)/gplot/symbol2.F \
              $(GSRC)/gplot/gline_sub.F \
              $(GSRC)/gplot/gplot_cont.F \
              $(GSRC)/gplot/gplot_line.F

GPLOT_FSRCS7 = $(GSRC)/g77/trigd.F

GPLOT_CSRCS = $(GSRC)/keypress.c \
              $(GSRC)/resize_act.c \
              $(GSRC)/sig_off_on.c \
              $(GSRC)/png_setup.c \
              $(GSRC)/readpng.c \
              $(GSRC)/rpng-x.c \
              $(GSRC)/x_alpha.c \
              $(GSRC)/x_close_z.c \
              $(GSRC)/x_create_z.c \
              $(GSRC)/x_crosshair.c \
              $(GSRC)/x_define_col.c \
              $(GSRC)/x_draw_fsc.c \
              $(GSRC)/x_draw_zb.c \
              $(GSRC)/x_error.c \
              $(GSRC)/x_event.c \
              $(GSRC)/x_plot.c \
              $(GSRC)/x_plot_col.c \
              $(GSRC)/x_plot_rep.c \
              $(GSRC)/x_refresh.c \
              $(GSRC)/x_replay.c \
              $(GSRC)/x_setup.c \
              $(GSRC)/x_store.c \
              $(GSRC)/x_sync.c \
              $(GSRC)/x_wname.c \
              $(GSRC)/x_zoom.c

GPLOT_CSRCSU = $(GSRC)/unix/read_key_trp.c \
              $(GSRC)/unix/fgetcpulimit.c \
              $(GSRC)/unix/fitof.c \
              $(GSRC)/unix/tcgeta.c \
              $(GSRC)/unix/tcseta.c \
              $(GSRC)/unix/tcsetaf.c \
              $(GSRC)/unix/tistor_put.c

GPLOT_CSRCS7 = $(GSRC)/g77/perror.c \
              $(GSRC)/g77/malloc.c \
              $(GSRC)/g77/getpid.c \
              $(GSRC)/g77/sleep.c \
              $(GSRC)/g77/getc.c \

MUD_CSRCS = $(MSRC)/mud.c \
	    $(MSRC)/mud_misc.c \
	    $(MSRC)/mud_all.c \
	    $(MSRC)/mud_new.c \
	    $(MSRC)/mud_gen.c \
	    $(MSRC)/mud_fort.c \
	    $(MSRC)/mud_tri_ti.c \
	    $(MSRC)/mud_friendly.c \
	    $(MSRC)/mud_encode.c

FOBJS       = ${FSRCS:.F=.o}
FOBJSL      = ${FSRCSL:.F=.o}
FOBJSF      = ${FSRCSF:.F=.o}
COBJS       = ${CSRCS:.c=.o}
COBJSL      = ${CSRCSL:.c=.o}
GPLOT_FOBJS = ${GPLOT_FSRCS:.F=.o}
GPLOT_FOBJSU = ${GPLOT_FSRCSU:.F=.o}
GPLOT_FOBJSG = ${GPLOT_FSRCSG:.F=.o}
GPLOT_FOBJS7 = ${GPLOT_FSRCS7:.F=.o}
GPLOT_COBJS = ${GPLOT_CSRCS:.c=.o}
GPLOT_COBJSU = ${GPLOT_CSRCSU:.c=.o}
GPLOT_COBJS7 = ${GPLOT_CSRCS7:.c=.o}
MUD_COBJS   = ${MUD_CSRCS:.c=.o}

.SUFFIXES :
.SUFFIXES :	.c .F .o

ALL= physica physica-half-static physica-static

all:
	-$(MAKE) -k $(ALL)

libs:		lib/gplot.a lib/mud.a lib/physica.a

physica:	$(MYLIBS)
		$(FORTRAN) -o $@ $(SRC)/phys_main.o $(MYLIBS) $(CERNLIBS) $(OTHERLIBS)

physica-half-static:	$(MYLIBS)
		$(CC) -o $@ $(SRC)/phys_main.o $(MYLIBS) $(CERNLIBS) $(STATIC_OTHERLIBS)

physica-static:	$(MYLIBS)
		$(FORTRAN) -static -o $@ $(SRC)/phys_main.o $(MYLIBS) $(CERNLIBS) $(OTHERLIBS)

lib/mud.a:	$(MUD_COBJS)
		@echo "*** Making mud archive ***"
		-rm -vf $@
		ar rsv $@ $^
		@echo "*** Finished mud archive ***"

lib/gplot.a:	$(GPLOT_FOBJS) $(GPLOT_FOBJSU) $(GPLOT_FOBJSG) $(GPLOT_FOBJS7) \
                $(GPLOT_COBJS) $(GPLOT_COBJSU) $(GPLOT_COBJS7)
		@echo "*** Making gplot archive ***"
		-rm -vf $@
		ar rsv $@ $^
		@echo "*** Finished gplot archive ***"

lib/physica.a:	$(FOBJS) $(FOBJSL) $(FOBJSF) $(COBJS) $(COBJSL)
		@echo "*** Making physica archive ***"
		-rm -vf $@
		ar rsv $@ $^
		@echo "*** Finished physica archive ***"

clean:
	-rm -vf $(SRC)/*.o $(SRC)/functions/*.o $(SRC)/linux/*.o \
		$(GSRC)/*.o $(GSRC)/unix/*.o $(GSRC)/g77/*.o $(GSRC)/gplot/*.o \
		$(MSRC)/*.o lib/*.a physica 
	-rm -vf $(ALL)

.c.o :
		$(CC) $(MACROS) $(CFLAGS) $(INCLUDES) -o $*.o -c $<

.F.o :
		$(FORTRAN) $(MACROS) $(FFLAGS) -o $*.o -c $<
