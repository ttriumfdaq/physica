      SUBROUTINE GAXIS(*)
C
C                                               Arthur Haynes &
C                                               Henry Baragar
C                                               TRIUMF, UBC
C                                               December, 1982
C
      CHARACTER*255 XLABEL, YLABEL, GETLAB
      CHARACTER*60  FONT, FONTS
      CHARACTER*1   CPOW
      CHARACTER*7   VARFMT
      REAL*4  AXIS(2), XS0(2), YS0(2), SLEN(2), THETAS(2), XMIN(2)
     &       ,XMAX(2), TICL(2), TICS(2), TICANG(2), TICLAB(2), TICANL(2)
     &       ,ANGLAB(2), SIZLAB(2), BOXXS0(2), BOXYS0(2), BTICL(2)
     &       ,BTICS(2), BTICLB(2), BSIZLB(2), LSIZLB(2)
     &       ,LOG(2), AXMOD(2), XOFF(2), LEADZ(2), GRID(2)
     &       ,XTICARR(200,2,2), YTICARR(200,2,2), XDUM(200), YDUM(200)
     &       ,XVMIN(2), XVMAX(2)
      INTEGER NLINC(2), NSINC(2), ITYPTC(2), NPOS(2), NDEC(2), IPOW(2)
     &       ,ITYPLB(2), NUMTIC(2), MAXTIC(2)
C
      CHARACTER*2 CTRL, CTRLS
      COMMON /GPLOT_CONTROLS/ CTRL
C
      LOGICAL ASIS, ASISS
      COMMON /MODE/ ASIS
C
      LOGICAL BOLD, BOLDS
      COMMON /GPLOT_BOLDING/ BOLD
C
      INTEGER*4 NHATCH(2), NHATCHS(2)
      COMMON /PSYM_HATCHING/ NHATCH
C
      LOGICAL ITALICS, ITALICSS
      REAL*4 ANGL, ANGL_OLD, ANGLS
      COMMON /GPLOT_PSALPH/ ITALICS, ANGL, ANGL_OLD
C
      COMMON /PLOT_LEVEL/ ILEV
      COMMON /PLOT_COLOURS/ ICOLR1, ICOLR2
C
      COMMON /HARDCOPYRANGE/ XMINHP,XMAXHP,YMINHP,YMAXHP,IORIENTH
C
      COMMON /LONG_INCS/ NXS, NYS
C
C  NXS < 0      -->  drop first and last numbers on the x axis
C  NXS = -1000  -->  determine NLXINC & drop first & last numbers 
C
C
C  NYS < 0      -->  drop first and last numbers on the y axis
C  NYS = -1000  -->  determine NLYINC & drop first & last numbers 
C
C
      LOGICAL CTRLC_CALLED, CTRLC_FLAG
      COMMON /CTRLC/ CTRLC_FLAG

C  Modified by J.L.Chuma, 08-Apr-1997 to elimate SIND, COSD for g77
      REAL*4 DTOR /0.017453292519943/
CCC
      IF( CTRLC_FLAG )RETURN 1
      CALL CTRLC_TRAP( CTRLC_CALLED )
C
C  Check the input parameters
C
      IF( (GETNAM('XAUTO').NE.0.) .OR. (GETNAM('YAUTO').NE.0.) )RETURN
C
C  Set up all the variables:  <var>(1) for the X axis
C                         &:  <var>(2) for the Y axis
C
C  NOTE:  See the the documentation in in the subroutine AXLINE and
C         AXLOG for complete detail on what most of the parameters do.
C
      BOLDS      = BOLD
      ITALICSS   = ITALICS
      ANGLS      = ANGL
      NHATCHS(1) = NHATCH(1)
      NHATCHS(2) = NHATCH(2)
      ICOLR1S    = ICOLR1
      ICOLR2S    = ICOLR2
      CTRLS      = CTRL
      ASISS      = ASIS
      FONT       = GETLAB('FONT')
      LENFONT    = LENSIG(FONT)
      FONTS      = FONT
      LENFONTS   = LENFONT
C
      BOX      = GETNAM('BOX')
      ALIAS    = GETNAM('ALIAS')
      XCROSS   = GETNAM('XCROSS')
      XLAXIS   = GETNAM('XLAXIS')
      XUAXIS   = GETNAM('XUAXIS')
      XMIN(1)  = GETNAM('XMIN')
      XMAX(1)  = GETNAM('XMAX')
      LOG(1)   = GETNAM('XLOG')
      NLINC(1) = GETNAM('NLXINC')
cc      IF( NLINC(1) .EQ. 0 )NLINC(1) = 1
      IF( NLINC(1).EQ.0 .AND. ABS(LOG(1)).LE.1. )NLINC(1) = 1
      NSINC(1) = GETNAM('NSXINC')
      IF( NSINC(1) .EQ. 0 )NSINC(1) = 1
C
      YCROSS  = GETNAM('YCROSS')
      YLAXIS  = GETNAM('YLAXIS')
      YUAXIS  = GETNAM('YUAXIS')
      XMIN(2) = GETNAM('YMIN')
      XMAX(2) = GETNAM('YMAX')
      LOG(2)  = GETNAM('YLOG')
C
      XAXISA  = GETNAM('XAXISA')
      YAXISA  = GETNAM('YAXISA')
      AXIS(1) = GETNAM('XAXIS')
      AXIS(2) = GETNAM('YAXIS')
      COSX    = COS(XAXISA*DTOR)
      SINX    = SIN(XAXISA*DTOR)
      COSY    = COS(YAXISA*DTOR)
      SINY    = SIN(YAXISA*DTOR)
      IF( ABS(COSX) .LE. 1.E-7 )COSX = 0.0
      IF( ABS(SINX) .LE. 1.E-7 )SINX = 0.0
      IF( ABS(COSY) .LE. 1.E-7 )COSY = 0.0
      IF( ABS(SINY) .LE. 1.E-7 )SINY = 0.0
      XS0(1) = XLAXIS
      YS0(1) = YLAXIS
      IF( (ABS(LOG(2)) .LE. 1.) .AND. (YCROSS .NE. 0.) .AND.
     &    ( XMIN(2)*XMAX(2) .LE. 0.) )THEN
        IF( XMAX(2) .EQ. XMIN(2) )RETURN
        YLEN   = (YLAXIS - YUAXIS) * XMIN(2) / (XMAX(2) - XMIN(2))
        XS0(1) = XLAXIS + YLEN * COSY
        YS0(1) = YLAXIS + YLEN * SINY
      END IF
      THETAS(1) = XAXISA
      SLEN(1)   = XUAXIS - XLAXIS
      XVMIN(1) = GETNAM('XVMIN')
      XVMAX(1) = GETNAM('XVMAX')
      IF( ABS(LOG(1)) .LE. 1. )SLEN(1)  = SLEN(1)*
     &   (1.+((XMIN(1)-XVMIN(1))+(XVMAX(1)-XMAX(1)))/(XMAX(1)-XMIN(1)))
      NINCS = NLINC(1) * NSINC(1)
      IF( ALIAS .EQ. 0. )SLEN(1) = IFIX(SLEN(1)/NINCS) * NINCS
      TICL(1)   = GETNAM('XTICL')
      TICS(1)   = GETNAM('XTICS')
      TICANG(1) = GETNAM('XTICA')
      ITYPTC(1) = GETNAM('XTICTP')
      TICLAB(1) = GETNAM('XITICL')
      TICANL(1) = GETNAM('XITICA')
      ANGLAB(1) = GETNAM('XNUMA')
      SIZLAB(1) = GETNAM('XNUMSZ')
      NPOS(1)   = MAX(GETNAM('NXDIG'),0.)
      NDEC(1)   = GETNAM('NXDEC')
      XPAUTO    = GETNAM('XPAUTO')
      IPOW(1)   = GETNAM('XPOW')
      ITYPLB(1) = 2
      AXMOD(1)  = GETNAM('XMOD')
      XOFF(1)   = GETNAM('XOFF')
      LEADZ(1)  = IFIX(GETNAM('XLEADZ'))
      IF( (XPAUTO .NE. 0.) .AND. (ABS(LOG(1)) .LE. 1.) )THEN
        NPOS(1) = -NPOS(1)
        CALL FNICE(XVMIN(1),XVMAX(1),(XVMAX(1)-XVMIN(1))/NLINC(1)
     &   ,NPOS(1),NDEC(1),IPOW(1))
C
C    Update the values of NXDIG, NXDEC and XPOW
C
        CALL SETNAM('NXDEC',FLOAT(NDEC(1)))
        CALL SETNAM('NXDIG',FLOAT(NPOS(1)))
        CALL SETNAM('XPOW',FLOAT(IPOW(1)))
      END IF
      XS0(2) = XLAXIS
      YS0(2) = YLAXIS
      IF( (ABS(LOG(1)) .LE. 1.) .AND. (XCROSS .NE. 0.) .AND.
     &    (XMIN(1)*XMAX(1) .LE. 0.) )THEN
        IF( XMAX(1) .EQ. XMIN(1) )RETURN
        XLEN      = (XLAXIS - XUAXIS) * XMIN(1) / (XMAX(1) - XMIN(1))
        XS0(2)    = XLAXIS + XLEN * COSX
        YS0(2)    = YLAXIS + XLEN * SINX
      END IF
      THETAS(2) = YAXISA
      SLEN(2)   = YUAXIS - YLAXIS
      XVMIN(2)  = GETNAM('YVMIN')
      XVMAX(2)  = GETNAM('YVMAX')
      IF( ABS(LOG(2)) .LE. 1. )SLEN(2) = SLEN(2) *
     &   (1.+((XMIN(2)-XVMIN(2))+(XVMAX(2)-XMAX(2)))/(XMAX(2)-XMIN(2)))
      NLINC(2)  = GETNAM('NLYINC')
cc      IF( NLINC(2) .EQ. 0 )NLINC(2) = 1
      IF( NLINC(2).EQ.0 .AND. ABS(LOG(2)).LE.1. )NLINC(2) = 1
      NSINC(2)  = GETNAM('NSYINC')
      IF( NSINC(2) .EQ. 0 )NSINC(2) = 1
      NINCS     = NLINC(2) * NSINC(2)
      IF( ALIAS .EQ. 0. )SLEN(2) = IFIX(SLEN(2)/NINCS) * NINCS
      TICL(2)   = GETNAM('YTICL')
      TICS(2)   = GETNAM('YTICS')
      TICANG(2) = GETNAM('YTICA')
      ITYPTC(2) = GETNAM('YTICTP')
      TICLAB(2) = GETNAM('YITICL')
      TICANL(2) = GETNAM('YITICA')
      ANGLAB(2) = GETNAM('YNUMA')
      SIZLAB(2) = GETNAM('YNUMSZ')
      NPOS(2)   = MAX(GETNAM('NYDIG'),0.)
      NDEC(2)   = GETNAM('NYDEC')
      YPAUTO    = GETNAM('YPAUTO')
      IPOW(2)   = GETNAM('YPOW')
      ITYPLB(2) = 2
      AXMOD(2)  = GETNAM('YMOD')
      XOFF(2)   = GETNAM('YOFF')
      LEADZ(2)  = IFIX(GETNAM('YLEADZ'))
      IF( (YPAUTO .NE. 0.) .AND. (ABS(LOG(2)) .LE. 1.) )THEN
        NPOS(2) = -NPOS(2)
        CALL FNICE(XVMIN(2),XVMAX(2),(XVMAX(2)-XVMIN(2))/NLINC(2)
     &   ,NPOS(2),NDEC(2),IPOW(2))
C
C    Update the values of NYDIG, NYDEC and YPOW
C
        CALL SETNAM('NYDEC',FLOAT(NDEC(2)))
        CALL SETNAM('NYDIG',FLOAT(NPOS(2)))
        CALL SETNAM('YPOW',FLOAT(IPOW(2)))
      END IF
C
C    Plot the axes
C
C NOTE:  The above code simply removed all the required information
C        from the data base for use by the subroutines AXLINE and AXLOG
C
      GRID(1) = GETNAM('NXGRID')
      IF( GRID(1) .NE. 0. )THEN
        MAXTIC(1) = 200
      ELSE
        MAXTIC(1) = 0
      END IF
      NUMTIC(1) = 0
C
      GRID(2) = GETNAM('NYGRID')
      IF( GRID(2) .NE. 0. )THEN
        MAXTIC(2) = 200
      ELSE
        MAXTIC(2) = 0
      END IF
      NUMTIC(2) = 0
C
C    Plot the x axis
C
      IF( CTRLC_FLAG )RETURN 1
      XS1 = (XMIN(1)-XVMIN(1))/(XMAX(1)-XMIN(1))*(XUAXIS-XLAXIS)
      YS1 = (XMIN(2)-XVMIN(2))/(XMAX(2)-XMIN(2))*(YUAXIS-YLAXIS)
      IF( AXIS(1) .NE. 0. )THEN
        IF( ABS(LOG(1)) .LE. 1. )THEN
          CALL AXLINE(XS0(1),YS0(1),SLEN(1),
     &       THETAS(1),XMIN(1),XMAX(1),NLINC(1),NSINC(1),TICL(1),
     &       TICS(1),TICANG(1),ITYPTC(1),TICLAB(1),TICANL(1),ANGLAB(1),
     &       SIZLAB(1),NPOS(1),NDEC(1),IPOW(1),ITYPLB(1),MAX_NLABCHX,
     &       AXMOD(1),XOFF(1),LEADZ(1),GRID(1),XDUM,YDUM,NTIC,MAXTIC(1),
     &       NXS,XVMIN(1),XVMAX(1),XLAXIS,XUAXIS,XS1,0.)
        ELSE
          CALL AXLOG(LOG(1),XS0(1),YS0(1),SLEN(1),
     &       THETAS(1),XMIN(1),XMAX(1),NLINC(1),TICL(1),
     &       NSINC(1),TICS(1),TICANG(1),ITYPTC(1),TICLAB(1),TICANL(1),
     &       ANGLAB(1),SIZLAB(1),ITYPLB(1),MAX_NLABCHX,
     &       GRID(1),XDUM,YDUM,NTIC,MAXTIC(1))
        END IF
        DO J = 1, NTIC
          XTICARR(J,1,1) = XDUM(J)
          YTICARR(J,1,1) = YDUM(J)
        END DO
        NUMTIC(1) = NTIC
      END IF
C
C    Plot the y axis
C
      IF( CTRLC_FLAG )RETURN 1
      IF( AXIS(2) .NE. 0. )THEN
        IF( ABS(LOG(2)) .LE. 1. )THEN
          CALL AXLINE(XS0(2),YS0(2),SLEN(2),
     &       THETAS(2),XMIN(2),XMAX(2),NLINC(2),NSINC(2),TICL(2),
     &       TICS(2),TICANG(2),ITYPTC(2),TICLAB(2),TICANL(2),ANGLAB(2),
     &       SIZLAB(2),NPOS(2),NDEC(2),IPOW(2),ITYPLB(2),MAX_NLABCHY,
     &       AXMOD(2),XOFF(2),LEADZ(2),GRID(2),XDUM,YDUM,NTIC,MAXTIC(2),
     &       NYS,XVMIN(2),XVMAX(2),YLAXIS,YUAXIS,0.,YS1)
        ELSE
          CALL AXLOG(LOG(2),XS0(2),YS0(2),SLEN(2),
     &       THETAS(2),XMIN(2),XMAX(2),NLINC(2),TICL(2),
     &       NSINC(2),TICS(2),TICANG(2),ITYPTC(2),TICLAB(2),TICANL(2),
     &       ANGLAB(2),SIZLAB(2),ITYPLB(2),MAX_NLABCHY,
     &       GRID(2),XDUM,YDUM,NTIC,MAXTIC(2))
        END IF
        DO J = 1, NTIC
          XTICARR(J,1,2) = XDUM(J)
          YTICARR(J,1,2) = YDUM(J)
        END DO
        NUMTIC(2) = NTIC
      END IF
C
C    Plot the BOX if it was asked for
C
      IF( BOX .EQ. 0. )GO TO 600
C
C    Plot the bottom and the right sides of the BOX, after getting the
C    required parameters
C
C  NOTE:  1) To produce the box, we simply replot the axes where the
C            edge of the box is to be
C  	  2) BOTTIC etc. are just scale factors used for scaling all
C            the various sizes used in plotting the original axes, for
C   	     the appropriate edge
C
      BOXXS0(1) = XLAXIS
      BOXYS0(1) = YLAXIS
      BOXTIC    = GETNAM('BOTTIC')
      BOXNUM    = GETNAM('BOTNUM')
      BTICL(1)  = TICL(1) * BOXTIC
      BTICS(1)  = TICS(1) * BOXTIC
      BTICLB(1) = TICLAB(1) * BOXNUM
      BSIZLB(1) = SIZLAB(1) * ABS(BOXNUM)
      BOXXS0(2) = XLAXIS + (XUAXIS-XLAXIS) * COSX
      BOXYS0(2) = YLAXIS + (XUAXIS-XLAXIS) * SINX
      BOXTIC    = GETNAM('RITTIC')
      BOXNUM    = GETNAM('RITNUM')
      BTICL(2)  = TICL(2) * BOXTIC
      BTICS(2)  = TICS(2) * BOXTIC
      BTICLB(2) = TICLAB(2) * BOXNUM
      BSIZLB(2) = SIZLAB(2) * ABS(BOXNUM)
C
C  Plot the bottom side of the BOX
C
      IF( CTRLC_FLAG )RETURN 1
      IF( (XS0(1) .NE. BOXXS0(1)) .OR. (YS0(1) .NE. BOXYS0(1)) .OR.
     &    (AXIS(1) .EQ. 0.) )THEN
        IF( ABS(LOG(1)) .LE. 1. )THEN
          CALL AXLINE(BOXXS0(1),BOXYS0(1),SLEN(1),THETAS(1),
     &       XMIN(1),XMAX(1),NLINC(1),NSINC(1),BTICL(1),BTICS(1),
     &       TICANG(1),ITYPTC(1),BTICLB(1),TICANL(1),ANGLAB(1),
     &       BSIZLB(1),NPOS(1),NDEC(1),IPOW(1),ITYPLB(1),MAX_NLABCH,
     &       AXMOD(1),XOFF(1),LEADZ(1),GRID(1),XDUM,YDUM,NTIC,MAXTIC(1),
     &       NXS,XVMIN(1),XVMAX(1),XLAXIS,XUAXIS,XS1,0.)
        ELSE
          CALL AXLOG(LOG(1),BOXXS0(1),BOXYS0(1),
     &       SLEN(1),THETAS(1),XMIN(1),XMAX(1),NLINC(1),
     &       BTICL(1),NSINC(1),BTICS(1),TICANG(1),ITYPTC(1),BTICLB(1),
     &       TICANL(1),ANGLAB(1),BSIZLB(1),ITYPLB(1),MAX_NLABCH,
     &       GRID(1),XDUM,YDUM,NTIC,MAXTIC(1))
        END IF
        MAX_NLABCHX = MAX(MAX_NLABCHX,MAX_NLABCH)
        DO J = 1, NTIC
          XTICARR(J,1,1) = XDUM(J)
          YTICARR(J,1,1) = YDUM(J)
        END DO
        NUMTIC(1) = NTIC
      END IF
C
C  Plot the right side of the BOX
C
      IF( CTRLC_FLAG )RETURN 1
      IF( (XS0(2) .NE. BOXXS0(2)) .OR. (YS0(2) .NE. BOXYS0(2)) .OR.
     &    (AXIS(2) .EQ. 0.) )THEN
        IF( ABS(LOG(2)) .LE. 1. )THEN
          CALL AXLINE(BOXXS0(2),BOXYS0(2),SLEN(2),THETAS(2),
     &       XMIN(2),XMAX(2),NLINC(2),NSINC(2),BTICL(2),BTICS(2),
     &       TICANG(2),ITYPTC(2),BTICLB(2),TICANL(2),ANGLAB(2),
     &       BSIZLB(2),NPOS(2),NDEC(2),IPOW(2),ITYPLB(2),MAX_NLABCH,
     &       AXMOD(2),XOFF(2),LEADZ(2),GRID(2),XDUM,YDUM,NTIC,MAXTIC(2),
     &       NYS,XVMIN(2),XVMAX(2),YLAXIS,YUAXIS,0.,YS1)
        ELSE
          CALL AXLOG(LOG(2),BOXXS0(2),BOXYS0(2),
     &       SLEN(2),THETAS(2),XMIN(2),XMAX(2),NLINC(2),
     &       BTICL(2),NSINC(2),BTICS(2),TICANG(2),ITYPTC(2),BTICLB(2),
     &       TICANL(2),ANGLAB(2),BSIZLB(2),ITYPLB(2),MAX_NLABCH,
     &       GRID(2),XDUM,YDUM,NTIC,MAXTIC(2))
        END IF
        MAX_NLABCHY = MAX(MAX_NLABCHY,MAX_NLABCH)
        DO J = 1, NTIC
          XTICARR(J,2,2) = XDUM(J)
          YTICARR(J,2,2) = YDUM(J)
        END DO
        NUMTIC(2) = NTIC
      END IF
C
C  Plot the top and the left sides of the BOX, after getting the
C  required parameters
C
C  NOTE:  See previous notes
C
      BOXXS0(1) = XLAXIS + (YUAXIS-YLAXIS) * COSY
      BOXYS0(1) = YLAXIS + (YUAXIS-YLAXIS) * SINY
      BOXTIC    = GETNAM('TOPTIC')
      BOXNUM    = GETNAM('TOPNUM')
      BTICL(1)  = TICL(1) * BOXTIC
      BTICS(1)  = TICS(1) * BOXTIC
      BTICLB(1) = TICLAB(1) * BOXNUM
      BSIZLB(1) = SIZLAB(1) * ABS(BOXNUM)
      BOXXS0(2) = XLAXIS
      BOXYS0(2) = YLAXIS
      BOXTIC    = GETNAM('LEFTIC')
      BOXNUM    = GETNAM('LEFNUM')
      BTICL(2)  = TICL(2) * BOXTIC
      BTICS(2)  = TICS(2) * BOXTIC
      BTICLB(2) = TICLAB(2) * BOXNUM
      BSIZLB(2) = SIZLAB(2) * ABS(BOXNUM)
C
C  Plot the top side of the BOX
C
      IF( CTRLC_FLAG )RETURN 1
      IF( (XS0(1) .NE. BOXXS0(1)) .OR. (YS0(1) .NE. BOXYS0(1)) .OR.
     &    (AXIS(1) .EQ. 0.) )THEN
        IF( ABS(LOG(1)) .LE. 1. )THEN
          CALL AXLINE(BOXXS0(1),BOXYS0(1),SLEN(1),THETAS(1),
     &       XMIN(1),XMAX(1),NLINC(1),NSINC(1),BTICL(1),BTICS(1),
     &       TICANG(1),ITYPTC(1),BTICLB(1),TICANL(1),ANGLAB(1),
     &       BSIZLB(1),NPOS(1),NDEC(1),IPOW(1),ITYPLB(1),MAX_NLABCH,
     &       AXMOD(1),XOFF(1),LEADZ(1),GRID(1),XDUM,YDUM,NTIC,MAXTIC(1),
     &       NXS,XVMIN(1),XVMAX(1),XLAXIS,XUAXIS,XS1,0.)
        ELSE
          CALL AXLOG(LOG(1),BOXXS0(1),BOXYS0(1),
     &       SLEN(1),THETAS(1),XMIN(1),XMAX(1),NLINC(1),
     &       BTICL(1),NSINC(1),BTICS(1),TICANG(1),ITYPTC(1),BTICLB(1),
     &       TICANL(1),ANGLAB(1),BSIZLB(1),ITYPLB(1),MAX_NLABCH,
     &       GRID(1),XDUM,YDUM,NTIC,MAXTIC(1))
        END IF
        MAX_NLABCHX = MAX(MAX_NLABCHX,MAX_NLABCH)
        DO J = 1, NTIC
          XTICARR(J,2,1) = XDUM(J)
          YTICARR(J,2,1) = YDUM(J)
        END DO
      END IF
C
C  Plot the left side of the BOX
C
      IF( CTRLC_FLAG )RETURN 1
      IF( (XS0(2) .NE. BOXXS0(2)) .OR. (YS0(2) .NE. BOXYS0(2)) .OR.
     &    (AXIS(2) .EQ. 0.) )THEN
        IF( ABS(LOG(2)) .LE. 1. )THEN
          CALL AXLINE(BOXXS0(2),BOXYS0(2),SLEN(2),THETAS(2),
     &       XMIN(2),XMAX(2),NLINC(2),NSINC(2),BTICL(2),BTICS(2),
     &       TICANG(2),ITYPTC(2),BTICLB(2),TICANL(2),ANGLAB(2),
     &       BSIZLB(2),NPOS(2),NDEC(2),IPOW(2),ITYPLB(2),MAX_NLABCH,
     &       AXMOD(2),XOFF(2),LEADZ(2),GRID(2),XDUM,YDUM,NTIC,MAXTIC(2),
     &       NYS,XVMIN(2),XVMAX(2),YLAXIS,YUAXIS,0.,YS1)
        ELSE
          CALL AXLOG(LOG(2),BOXXS0(2),BOXYS0(2),
     &       SLEN(2),THETAS(2),XMIN(2),XMAX(2),NLINC(2),
     &       BTICL(2),NSINC(2),BTICS(2),TICANG(2),ITYPTC(2),BTICLB(2),
     &       TICANL(2),ANGLAB(2),BSIZLB(2),ITYPLB(2),MAX_NLABCH,
     &       GRID(2),XDUM,YDUM,NTIC,MAXTIC(2))
        END IF
        MAX_NLABCHY = MAX(MAX_NLABCHY,MAX_NLABCH)
        DO J = 1, NTIC
          XTICARR(J,1,2) = XDUM(J)
          YTICARR(J,1,2) = YDUM(J)
        END DO
      END IF
C
C  Write out the GRID lines parallel to the X axis, if requested.
C  The grid lines are drawn on every NXGRIDth (sic) long tic mark
C  on the Y axis.
C
      DO I = 1, NUMTIC(2)
        IF( CTRLC_FLAG )RETURN 1
        CALL PLOT_R(XTICARR(I,1,2),YTICARR(I,1,2),3)
        CALL PLOT_R(XTICARR(I,2,2),YTICARR(I,2,2),2)
      END DO
C
C  Write out the GRID lines parallel to the Y axis, if requested.
C  The grid lines are drawn on every NYGRIDth (sic) long tic mark
C  on the X axis.
C
      DO I = 1, NUMTIC(1)
        IF( CTRLC_FLAG )RETURN 1
        CALL PLOT_R(XTICARR(I,1,1),YTICARR(I,1,1),3)
        CALL PLOT_R(XTICARR(I,2,1),YTICARR(I,2,1),2)
      END DO
C
C    Plot out the labels for the axes
C
C  Plot out the X axis label.  It is positioned:
C  	1) centered on the X axis if the X axis is at
C  	   the bottom of the Y axis;
C  	2) centered on the part of the X axis that is
C  	   larger after being cut by the Y axis.
C
C  NOTE:  1) The label is composed of two parts:
C  		 a) The string found in the label 'XAXIS'
C  		 b) The factor by which all the numbers
C  		    labelling the axis should be multiplied to
C                   get the users units.
C
600   YLWIND = GETNAM('YLWIND')
      YUWIND = GETNAM('YUWIND')
      XLWIND = GETNAM('XLWIND')
      XUWIND = GETNAM('XUWIND')
      IF( CTRLC_FLAG )RETURN 1
      IF( (AXIS(1) .NE. 0.) .OR. (BOX .NE. 0.) )THEN
        XLABEL    = GETLAB('XLABEL')
        LSIZLB(1) = GETNAM('XLABSZ')
        LENGTH    = LENSIG(XLABEL)
        YHITE     = LSIZLB(1)
      ELSE
        LENGTH = 0
      END IF
C
C    If XPAUTO = 2. then calculate the power but don't plot it
C
      IF((ABS(LOG(1)).LE.1.).AND.(IPOW(1).NE.0).AND.(XPAUTO.NE.2.))THEN
        XLABEL = XLABEL(:LENGTH)//'  (x10'//CHAR(3)
CC        XLABEL = XLABEL(:LENGTH)//CTRL(1:1)//'H'//'         '//CTRL(2:2)
CC     &         //'  (x10'//CHAR(3)
CC        WRITE(XLABEL(LENGTH+3:LENGTH+11),610)SIZLAB(1)
CC610     FORMAT(F9.2)
        CPOW=CHAR(  MIN(57,IFIX(LOG10(FLOAT(ABS(IPOW(1)))))+2+48) )
        VARFMT='(SP,I'//CPOW//')'
        WRITE(XLABEL(LENGTH+8:),VARFMT)IPOW(1)
CC        WRITE(XLABEL(LENGTH+20:),VARFMT)IPOW(1)
CC620     FORMAT(SP,I<IFIX(LOG10(FLOAT(ABS(IPOW(1)))))+2>)
        LENGTH = LENSIG(XLABEL)
        XLABEL = XLABEL(1:LENGTH)//CHAR(2)//')'
        LENGTH = LENGTH + 2
        YHITE  = MAX( LSIZLB(1), 1.1*SIZLAB(1) )
      END IF
      IF( CTRLC_FLAG )RETURN 1
      IF( (LENGTH .GT. 0) .AND. 
     &    ((AXIS(1) .NE. 0.) .OR. (BOX .NE. 0.)) )THEN
        X = MIN( XMAXHP, MAX( XMINHP,XLAXIS+0.5*(XUAXIS-XLAXIS)*COSX ) )
CC        X = MIN( XUWIND, MAX( XLWIND,XLAXIS+0.5*(XUAXIS-XLAXIS)*COSX ) )
        Y = MIN( YMAXHP, MAX( YMINHP,
     & YLAXIS+0.5*(XUAXIS-XLAXIS)*SINX-(TICLAB(1)+SIZLAB(1)+1.5*YHITE)))
CC        Y = MIN( YUWIND, MAX( YLWIND,
CC     & YLAXIS+0.5*(XUAXIS-XLAXIS)*SINX-(TICLAB(1)+SIZLAB(1)+1.5*YHITE)))
        CALL GPLOT_LINE(X,Y,LSIZLB(1),THETAS(1),XLABEL,LENGTH,2
     &                 ,FONT,LENFONT,*700)
        IF( FONT(1:LENFONT) .NE. FONTS(1:LENFONTS) )
     &    CALL PFONT(FONTS(1:LENFONTS),0)
        FONT = FONTS
        LENFONT = LENFONTS
        BOLD = BOLDS
        NHATCH(1) = NHATCHS(1)
        NHATCH(2) = NHATCHS(2)
        ITALICS = ITALICSS
        IF( ANGL .NE. ANGLS )CALL PSALPH('ANGL',ANGLS,IDUM)
        CTRL = CTRLS
        ASIS = ASISS
        IF( ((ICOLR1S.NE.ICOLR1) .OR. (ICOLR2S.NE.ICOLR2)) .AND.
     &    (ILEV.NE.1) )CALL PLOT_COLOR(ICOLR1S,ICOLR2S)
      END IF
C
C    Plot out the Y axis label.  It is positioned:
C  	1) centered on the Y axis if the Y axis is at the bottom of
C          the X axis;
C  	2) centered on the part of the Y axis that is larger after
C          being cut by the X axis.
C
C  NOTE:  See notes above
C
650   IF( (AXIS(2) .NE. 0.) .OR. (BOX .NE. 0.) )THEN 
        YLABEL    = GETLAB('YLABEL')
        LSIZLB(2) = GETNAM('YLABSZ')
        LENGTH    = LENSIG(YLABEL)
      ELSE
        LENGTH = 0
      END IF
C
C    If YPAUTO = 2. then calculate the power but don't plot it
C
      IF( CTRLC_FLAG )RETURN 1
      IF((ABS(LOG(2)).LE.1.).AND.(IPOW(2).NE.0).AND.(YPAUTO.NE.2.))THEN
        YLABEL = YLABEL(:LENGTH)//'  (x10'//CHAR(3)
CC        YLABEL = YLABEL(:LENGTH)//CTRL(1:1)//'H'//'         '//CTRL(2:2)
CC     &         //'  (x10'//CHAR(3)
CC        WRITE(YLABEL(LENGTH+3:LENGTH+11),610)SIZLAB(2)
        CPOW=CHAR(  MIN(57,IFIX(LOG10(FLOAT(ABS(IPOW(2)))))+2+48) )
        VARFMT='(SP,I'//CPOW//')'
        WRITE(YLABEL(LENGTH+8:),VARFMT)IPOW(2)
CC        WRITE(YLABEL(LENGTH+20:),VARFMT)IPOW(2)
CC630     FORMAT(SP,I<IFIX(LOG10(FLOAT(ABS(IPOW(2)))))+2>)
        LENGTH = LENSIG(YLABEL)
        YLABEL = YLABEL(1:LENGTH)//CHAR(2)//')'
        LENGTH = LENGTH + 2
      END IF
      IF( CTRLC_FLAG )RETURN 1
      IF( (LENGTH .GT. 0) .AND. 
     &    ((AXIS(2) .NE. 0.) .OR. (BOX .NE. 0.)) )THEN
        YDIG = FLOAT(MAX_NLABCHY)
        X    = MIN( XMAXHP, MAX( XMINHP+LSIZLB(2),
     &           XLAXIS + 0.5*(YUAXIS-YLAXIS)*COSY -
     &          (TICLAB(2)+YDIG*SIZLAB(2)+0.50*LSIZLB(2)) ) )
CC        X    = MIN( XUWIND, MAX( XLWIND+LSIZLB(2),
CC     &           XLAXIS + 0.5*(YUAXIS-YLAXIS)*COSY -
CC     &          (TICLAB(2)+YDIG*SIZLAB(2)+0.50*LSIZLB(2)) ) )
        Y    = MIN( YMAXHP, MAX( YMINHP,
     &          YLAXIS + 0.5*(YUAXIS-YLAXIS)*SINY ) )
CC        Y    = MIN( YUWIND, MAX( YLWIND,
CC     &          YLAXIS + 0.5*(YUAXIS-YLAXIS)*SINY ) )
        CALL GPLOT_LINE(X,Y,LSIZLB(2),THETAS(2),YLABEL,LENGTH,2
     &                 ,FONT,LENFONT,*700)
        IF( FONT(1:LENFONT) .NE. FONTS(1:LENFONTS) )
     &    CALL PFONT(FONTS(1:LENFONTS),0)
        BOLD = BOLDS
        NHATCH(1) = NHATCHS(1)
        NHATCH(2) = NHATCHS(2)
        ITALICS = ITALICSS
        IF( ANGL .NE. ANGLS )CALL PSALPH('ANGL',ANGLS,IDUM)
        CTRL = CTRLS
        ASIS = ASISS
        IF( ((ICOLR1S.NE.ICOLR1) .OR. (ICOLR2S.NE.ICOLR2)) .AND.
     &    (ILEV.NE.1) )CALL PLOT_COLOR(ICOLR1S,ICOLR2S)
      END IF
      RETURN
700   RETURN 1
      END
