      SUBROUTINE SETLAB( LABNAM, LABINP, * )
C
      CHARACTER*(*) LABNAM, LABINP

      CHARACTER*255 LABELS(20) 
      COMMON /GLABEL/ NLABS, LABELS

      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

      REAL*4 TABLE(103)
      COMMON /GPLOTC/ TABLE

      CHARACTER*255 T1, T2
      INTEGER*4     I, L1, L2
CCC
      IF( TABLE(1) .EQ. 0.0 )CALL GPLOTI
      L1 = LENSIG(LABNAM)
      L2 = MAX(1,LENSIG(LABINP))
      CALL UPRCASE( LABNAM, T1(1:L1) )
      DO I = 1, NLABS, 2
        IF( T1(1:L1) .EQ. LABELS(I) )THEN
          LABELS(I+1) = ' '
          LABELS(I+1) = LABINP
          IF( I .EQ. 7 )THEN                         ! text
            CALL GPLOT_TXT(.FALSE.,LABINP(1:L2))
          ELSE IF( I .EQ. 9 )THEN                    ! hex
            CALL GPLOT_TXT(.TRUE.,LABINP(1:L2))
          ELSE IF( I .EQ. 11 )THEN                   ! hexchr
            CALL HEX_TO_ASCII(LABINP(1:L2),T2,*199)
            LABELS(6) = ' '
            LABELS(6) = T2
          ELSE IF( I .EQ. 5 )THEN                    ! char
            WRITE(LABELS(12)(1:2),9)ICHAR(LABELS(6)(1:1))
            WRITE(LABELS(12)(3:4),9)ICHAR(LABELS(6)(2:2))
9           FORMAT(Z2)
          ELSE IF( I .EQ. 13 )THEN                   ! font
            CALL UPRCASE( LABINP(1:L2), T2(1:L2) )
            CALL PFONT(T2(1:L2),0,*99)
          END IF
          RETURN
        END IF
      END DO
      WRITE(IOUTS,*)
     & ' *** SETLAB ERROR: '//T1(1:L1)//' is not a GPLOT name'
      RETURN 1
99    WRITE(IOUTS,*)
     & ' *** SETLAB ERROR: '//T2(1:L2)//' is not a FONT name'
      RETURN 1
199   T2 = LABINP
      WRITE(IOUTS,*)' *** SETLAB ERROR: bad hexcodes: '//T2(1:L2)
      RETURN 1
      END
