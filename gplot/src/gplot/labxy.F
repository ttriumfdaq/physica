C                                                                      *
      SUBROUTINE LABXY(XTICL,YTICL,ANGLAB,COSLAB,SINLAB,
     *                 SIZLAB,FLENG,YLIM,XLAB,YLAB)
C================================================================
C================================================================
C==                                                            ==
C==   LABXY: Given the label location (XTICL,YTICL) relative to==
C==          a point on an axis and the label angle "ANGLAB"   ==
C==          relative to the axis direction, and the label     ==
C==          size and length "SIZLAB" and "FLENG", "LABXY"     ==
C==          determines the lower left hand corner (XLAB,YLAB),==
C==          at which the label is to be plotted, relative to  ==
C==          the same point on the axis.                       ==
C==                                                            ==
C==   Diagram: showing where (XTICL,YTICL) will be centered on ==
C==            the label perimeter as a function of the label  ==
C==            angle, shown in degrees. The values outside the ==
C==            box represent the label angles when the label   ==
C==            is below the axis (YTICL <= 0.), while the      ==
C==            values inside the box represent the label angles==
C==            when the label is above the axis (YTICL > 0.).  ==
C==            The label angle is linear over each of the four ==
C==            regions of the label perimeter.                 ==
C==                                                            ==
C==                     <------region #1------>                ==
C==                   315          0           45              ==
C==             ^      +-----------+-----------+     ^         ==
C==             |      | 135      180      225 |     |         ==
C==             |      |          #3           |     |         ==
C==     region #4  270 + 90 #2   LABEL  #4 270 + 90  region #2 ==
C==             |      |          #1           |     |         ==
C==             |      | 45        0       315 |     |         ==
C==             |      +-----------+-----------+     |         ==
C==                   225         180         135              ==
C==                     <------region #3------>                ==
C==                                                            ==
C==                                                            ==
C==   Written by Arthur Haynes, TRIUMF U.B.C., April 22, 1981. ==
C==                                                            ==
C==   Input  Parameters: XTICL,YTICL,ANGLAB,COSLAB,SINLAB,     ==
C==                      SIZLAB,FLENG,YLIM (R*4).              ==
C==                                                            ==
C==   Output Parameters: XLAB,YLAB (R*4).                      ==
C==                                                            ==
C==   Parameter Definitions:                                   ==
C==   --------- -----------                                    ==
C==                                                            ==
C==   XTICL : x-coordinate (parallel to the axis), relative to ==
C==           an arbitrary point on the axis, at which the     ==
C==           label perimeter will be centered.                ==
C==                                                            ==
C==   YTICL : y-coordinate (perpendicular to the axis), relative=
C==           to the axis, at which the label perimeter will be==
C==           centered. "YTICL" is positive if the label is to ==
C==           be plotted above the axis and "YTICL" is negative==
C==           or zero if the label is to be plotted below the  ==
C==           axis.                                            ==
C==                                                            ==
C==   ANGLAB: angle in degrees at which the label is to be     ==
C==           plotted relative to the axis direction.          ==
C==                                                            ==
C==   COSLAB: cosine of "ANGLAB".                              ==
C==                                                            ==
C==   SINLAB: sine   of "ANGLAB".                              ==
C==                                                            ==
C==   SIZLAB: height of the label in the same units as "XTICL" ==
C==           and "YTICL".                                     ==
C==                                                            ==
C==   FLENG : length of the label.                             ==
C==                                                            ==
C==   YLIM  : The label when plotted is not allowed to cross   ==
C==           the line "Y = YLIM". If, after determining the   ==
C==           position of (XTICL,YTICL) on the label perimeter ==
C==           using the scheme given in the above diagram, the ==
C==           label crosses the line "Y = YLIM" then the label ==
C==           is shifted away from the axis so that it just    ==
C==           touches the line "Y = YLIM" and so that the point==
C==           (XTICL,YTICL) is still on the label perimeter.   ==
C==                                                            ==
C==   XLAB  : x-coordinate of the lower left hand corner at    ==
C==           which the label is to be plotted, calculated in  ==
C==           the same coordinate system as (XTICL,YTICL).     ==
C==                                                            ==
C==   YLAB  : y-coordinate of the lower left hand corner at    ==
C==           which the label is to be plotted, calculated in  ==
C==           the same coordinate system as (XTICL,YTICL).     ==
C==                                                            ==
C================================================================
C================================================================
C==   If YLIM > YTICL > 0 or YLIM < YTICL <= 0 then set        ==
C==   YLIM2 = YTICL.                                           ==
C================================================================
      YLIM2=YLIM
      IF(YTICL.GT.0..AND.YLIM.GT.YTICL)YLIM2=YTICL
      IF(YTICL.LE.0..AND.YLIM.LT.YTICL)YLIM2=YTICL
C================================================================
C==   DISTX = length of the line segment which intersects the  ==
C==           2 lines: "Y = YTICL" and "Y = YLIM2" at an angle ==
C==           of "ANGLAB" degrees.                             ==
C==   DISTY = length of the line segment which intersects the  ==
C==           2 lines: "Y = YTICL" and "Y = YLIM2" at an angle ==
C==           of "ANGLAB + 90" degrees.                        ==
C================================================================
      DISTX=-1.
      DISTY=-1.
      IF(ABS(SINLAB).GT.1.E-3)DISTX=ABS((YLIM2-YTICL)/SINLAB)
      IF(ABS(COSLAB).GT.1.E-3)DISTY=ABS((YLIM2-YTICL)/COSLAB)
C================================================================
C==   Make sure that -45 <= ANGLE < 315  degrees.              ==
C==   Then calculate the region number: "IREG" of the "ANGLE". ==
C==   See the diagram above.                                   ==
C================================================================
      ANGLE=AMOD(ANGLAB+45.,360.)-45.
      IF(ANGLE.LT.-45.)ANGLE=ANGLE+360.
      IREG=(ANGLE+45.)/90.+1.
      IREG=MIN0(MAX0(IREG,1),4)
C================================================================
C==   Go to the appropriate section depending on the region    ==
C==   number: IREG.                                            ==
C================================================================
      GO TO (100,200,300,400),IREG
C================================================================
C==   Region number 1                                          ==
C================================================================
100   IF(YTICL.GT.0.)GO TO 150
      X=(ANGLE+45.)/90.*FLENG
      Y=SIZLAB
      IF(DISTX.LT.0.)GO TO 500
      IF(SINLAB.GT.0..AND.FLENG-X.GT.DISTX)X=FLENG-DISTX
      IF(SINLAB.LT.0..AND.X.GT.DISTX)X=DISTX
      GO TO 500
150   X=(45.-ANGLE)/90.*FLENG
      Y=0.
      IF(DISTX.LT.0.)GO TO 500
      IF(SINLAB.GT.0..AND.X.GT.DISTX)X=DISTX
      IF(SINLAB.LT.0..AND.FLENG-X.GT.DISTX)X=FLENG-DISTX
      GO TO 500
C================================================================
C==   Region number 2                                          ==
C================================================================
200   IF(YTICL.GT.0.)GO TO 250
      X=FLENG
      Y=(135.-ANGLE)/90.*SIZLAB
      IF(DISTY.LT.0.)GO TO 500
      IF(COSLAB.GT.0..AND.SIZLAB-Y.GT.DISTY)Y=SIZLAB-DISTY
      IF(COSLAB.LT.0..AND.Y.GT.DISTY)Y=DISTY
      GO TO 500
250   X=0.
      Y=(ANGLE-45.)/90.*SIZLAB
      IF(DISTY.LT.0.)GO TO 500
      IF(COSLAB.GT.0..AND.Y.GT.DISTY)Y=DISTY
      IF(COSLAB.LT.0..AND.SIZLAB-Y.GT.DISTY)Y=SIZLAB-DISTY
      GO TO 500
C================================================================
C==   Region number 3                                          ==
C================================================================
300   IF(YTICL.GT.0.)GO TO 350
      X=(225.-ANGLE)/90.*FLENG
      Y=0.
      IF(DISTX.LT.0.)GO TO 500
      IF(SINLAB.GT.0..AND.FLENG-X.GT.DISTX)X=FLENG-DISTX
      IF(SINLAB.LT.0..AND.X.GT.DISTX)X=DISTX
      GO TO 500
350   X=(ANGLE-135.)/90.*FLENG
      Y=SIZLAB
      IF(DISTX.LT.0.)GO TO 500
      IF(SINLAB.GT.0..AND.X.GT.DISTX)X=DISTX
      IF(SINLAB.LT.0..AND.FLENG-X.GT.DISTX)X=FLENG-DISTX
      GO TO 500
C================================================================
C==   Region number 4                                          ==
C================================================================
400   IF(YTICL.GT.0.)GO TO 450
      X=0.
      Y=(ANGLE-225.)/90.*SIZLAB
      IF(DISTY.LT.0.)GO TO 500
      IF(COSLAB.GT.0..AND.SIZLAB-Y.GT.DISTY)Y=SIZLAB-DISTY
      IF(COSLAB.LT.0..AND.Y.GT.DISTY)Y=DISTY
      GO TO 500
450   X=FLENG
      Y=(315.-ANGLE)/90.*SIZLAB
      IF(DISTY.LT.0.)GO TO 500
      IF(COSLAB.GT.0..AND.Y.GT.DISTY)Y=DISTY
      IF(COSLAB.LT.0..AND.SIZLAB-Y.GT.DISTY)Y=SIZLAB-DISTY
C================================================================
C==   Calculate the location of the lower left hand corner of  ==
C==   the label: (XLAB,YLAB).                                  ==
C================================================================
500   XLAB=XTICL-X*COSLAB+Y*SINLAB
      YLAB=YTICL-X*SINLAB-Y*COSLAB
      RETURN
      END

