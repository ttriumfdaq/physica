      SUBROUTINE GAUTO( X, Y, NPT )
C
C  Determine automatic scaling for the NPT (x,y) points 
C
C  Originally written by ARTHUR HAYNES, TRIUMF U.B.C., March 5, 1979
C  Major modifications by JOSEPH CHUMA
C
C  Input  parameters: X, Y     (R*4) arrays
C                     NPT      (I*4)
C
C  X       : array of x coordinates to be used in scaling
C  Y       : array of y coordinates to be used in scaling
C  NPT     : number of points in the x,y arrays
C
      REAL*4    X(1), Y(1)
      INTEGER*4 NPT

      REAL*4 TABLE(103)
      COMMON /GPLOTC/ TABLE
CCC
      IF( TABLE(1) .EQ. 0.0 )CALL GPLOTI

      NLXINC = ABS(GETNAM('NLXINC'))   ! # of major x increments
      XAUTO  = GETNAM('XAUTO')         ! XAUTO = 0. --> don't auto scale x
      XMIN   = GETNAM('XMIN')          ! minimum x on graph
      XMAX   = GETNAM('XMAX')          ! maximum x on graph
      XZERO  = GETNAM('XZERO')
      XLOG   = GETNAM('XLOG')
      IF( (XAUTO.EQ.2.0) .OR. (XAUTO.EQ.3.0) )NLXINC = 0
      IF( (XMIN.EQ.XMAX) .AND. (XAUTO.EQ.0.0) )XAUTO = 1.0

      NLYINC = ABS(GETNAM('NLYINC'))   ! # of major y increments
      YAUTO  = GETNAM('YAUTO')         ! YAUTO = 0. --> don't auto scale y
      YMIN   = GETNAM('YMIN')          ! minimum y on graph
      YMAX   = GETNAM('YMAX')          ! maximum y on graph
      YZERO  = GETNAM('YZERO')
      YLOG   = GETNAM('YLOG')
      IF( (YAUTO.EQ.2.0) .OR. (YAUTO.EQ.3.0) )NLYINC = 0
      IF( (YMIN.EQ.YMAX) .AND. (YAUTO.EQ.0.0) )YAUTO = 1.0

      IF( XAUTO .EQ. 0.0 )THEN           ! x-axis scale is fixed
        IF( INT(ABS(XLOG)) .LE. 1 )THEN  ! x-axis is linear
          IF( XZERO .NE. 0.0 )THEN       ! force zero to be on x-axis
            IF( XMIN .GT. 0.0 )THEN
              XMIN = 0.0
              CALL SETNAM('XMIN',0.0)
            END IF
            IF( XMAX .LT. 0.0 )THEN
              XMAX = 0.0
              CALL SETNAM('XMAX',0.0)
            END IF
            CALL SETNAM('XZERO',0.0)
          END IF
          IF( NLXINC .EQ. 0 )THEN
            CALL SCALE1(XMIN,XMAX,5,XMINS,XMAXS,XINC)
            NLXINC = INT( (XMAXS-XMINS)/XINC + 0.5 )
            CALL SETNAM('NLXINC',FLOAT(NLXINC))
            CALL SETNAM('XVMIN',XMINS)
            CALL SETNAM('XVMAX',XMAXS)
          END IF
        END IF
      ELSE IF( XAUTO .NE. 0.0 )THEN    ! x-axis automatic scaling
        IF( YAUTO .EQ. 0.0 )THEN              ! y-axis scale is fixed
          IF( INT(ABS(YLOG)) .GT. 1 )THEN     ! y-axis is logarithmic
            YMIN = EXP(YMIN*LOG(ABS(YLOG)))
            YMAX = EXP(YMAX*LOG(ABS(YLOG)))
          END IF
          DO I = 1, NPT
            IF( (Y(I)-YMIN)*(YMAX-Y(I)) .GE. 0.0 )GO TO 10
          END DO
          CALL SETNAM('NLXINC',1.0)
          CALL SETNAM('XMIN',0.0)
          CALL SETNAM('XMAX',1.0)
          RETURN        ! no data values within the y range
        END IF
   10   XMIN = +1.E35
        XMAX = -XMIN
        IF( YAUTO .EQ. 0.0 )THEN              ! y-axis scale is fixed
C                        only consider points within the y-axis range
          DO I = 1, NPT
            IF( (Y(I)-YMIN)*(YMAX-Y(I)) .GE. 0.0 )THEN
              IF( X(I) .LT. XMIN )XMIN = X(I)
              IF( X(I) .GT. XMAX )XMAX = X(I)
            END IF
          END DO
        ELSE
          DO I = 1, NPT
            IF( X(I) .LT. XMIN )XMIN = X(I)
            IF( X(I) .GT. XMAX )XMAX = X(I)
          END DO
        END IF
        IF( XZERO .NE. 0.0 )THEN
          IF( XMIN .GT. 0.0 )XMIN = 0.0
          IF( XMAX .LT. 0.0 )XMAX = 0.0
          CALL SETNAM('XZERO',0.0)
        END IF
        IF( INT(ABS(XLOG)) .LE. 1 )THEN      ! x-axis is linear
          IF( NLXINC .EQ. 0 )THEN
            CALL SCALE1(XMIN,XMAX,5,XMINS,XMAXS,XINC)
            NLXINC = INT( (XMAXS-XMINS)/XINC + 0.5 )
            CALL SETNAM('NLXINC',FLOAT(NLXINC))
          ELSE
            CALL SCALE2(XMIN,XMAX,MAX(NLXINC,2),XMINS,XMAXS,XINC,SVAL,0)
          END IF
        ELSE                                 ! x-axis is logarithmic
          IF( XMIN .LE. 0.0 )XMIN = 1.E-36 
          XMINS = ALOG(XMIN)/ALOG(ABS(XLOG))
          IF( XMINS .LT. INT(XMINS) )XMINS = XMINS-1.0
          XMINS = INT(XMINS) 
          IF( XMAX .LE. 0.0 )XMAX = 1.E-36 
          XMAXS = ALOG(XMAX)/ALOG(ABS(XLOG))
          IF( XMAXS .GT. INT(XMAXS) )XMAXS = XMAXS+1.0
          XMAXS = INT(XMAXS) 
          IF( NLXINC .EQ. 0 )GO TO 20
          IF(INT(XMAXS-XMINS)/NLXINC*NLXINC.EQ.INT(XMAXS-XMINS))
     &         GO TO 30
   20     NLXINC = MAX(5,NLXINC)
          CALL SCALE1(XMINS,XMAXS,NLXINC,XMINP,XMAXP,XINC)
          NLXINC = INT( (XMAXP-XMINP)/XINC + 0.5 )
          CALL SETNAM('NLXINC',FLOAT(NLXINC))
          XMINS = INT(XMINP)
          XMAXS = INT(XMAXP)
        END IF
   30   IF( (XAUTO.EQ.1.0) .OR. (XAUTO.EQ.2.0) )THEN
          CALL SETNAM('XMIN',XMINS)
          CALL SETNAM('XMAX',XMAXS)
        ELSE IF( XAUTO .EQ. 3.0 )THEN
          CALL SETNAM('XMIN',XMIN)
          CALL SETNAM('XMAX',XMAX)
          CALL SETNAM('XVMIN',XMINS)
          CALL SETNAM('XVMAX',XMAXS)
        END IF
        CALL SETNAM('XAUTO',0.0)
        IF( INT(ABS(XLOG)) .GT. 1 )THEN
          IF( XLOG .GT. 0.0 )THEN
            NXDIG1P = 1
            IF( ABS(XMINS) .GT. 0.0 )NXDIG1P = INT(LOG10(ABS(XMINS)))+1
            IF( XMINS .LT. 0.0 )NXDIG1P = NXDIG1P+1
            NXDIG1B = INT(LOG10(ABS(XLOG)))+1
            NXDIG1  = NXDIG1P+NXDIG1B
            NXDIG2P = 1
            IF( ABS(XMAXS) .GT. 0.0 )NXDIG2P = INT(LOG10(ABS(XMAXS)))+1
            IF( XMAXS .LT. 0.0 )NXDIG2P = NXDIG2P+1
            NXDIG2  = NXDIG2P+NXDIG1B
            NXDIG = MAX(NXDIG1,NXDIG2)
            CALL SETNAM('NXDIG',FLOAT(NXDIG))
          END IF
        END IF
      END IF
      IF( YAUTO .EQ. 0.0 )THEN
        IF( INT(ABS(YLOG)) .LE. 1 )THEN
          IF( YZERO .NE. 0.0 )THEN
            IF( YMIN .GE. 0.0 )THEN
              YMIN = 0.0
              CALL SETNAM('YMIN',YMIN)
            END IF
            IF( YMAX .LE. 0.0 )THEN
              YMAX = 0.0
              CALL SETNAM('YMAX',YMAX)
            END IF
            CALL SETNAM('YZERO',0.0)
          END IF
          IF( NLYINC .EQ. 0 )THEN
            CALL SCALE1(YMIN,YMAX,5,YMINS,YMAXS,YINC)
            NLYINC = INT( (YMAXS-YMINS)/YINC + 0.5 )
            CALL SETNAM('NLYINC',FLOAT(NLYINC))
            CALL SETNAM('YVMIN',YMINS)
            CALL SETNAM('YVMAX',YMAXS)
          END IF
        END IF
      ELSE IF( YAUTO .NE. 0.0 )THEN  ! y-axis automatic scaling
        IF( XAUTO .EQ. 0.0 )THEN
          IF( INT(ABS(XLOG)) .GT. 1 )THEN
            XMIN = EXP(XMIN*LOG(ABS(XLOG)))
            XMAX = EXP(XMAX*LOG(ABS(XLOG)))
          END IF
          DO I = 1, NPT
            IF( (X(I)-XMIN)*(XMAX-X(I)) .GE. 0.0 )GO TO 40
          END DO
          CALL SETNAM('NLYINC',1.0)
          CALL SETNAM('YMIN',0.0)
          CALL SETNAM('YMAX',1.0)
          RETURN        ! no data values within the x range
        END IF
   40   YMIN = +1.E35
        YMAX = -YMIN
        IF( XAUTO .EQ. 0.0 )THEN
C                        only consider points within the x-axis range
          DO I = 1, NPT
            IF( (X(I)-XMIN)*(XMAX-X(I)) .GE. 0.0 )THEN
              IF( Y(I) .LT. YMIN )YMIN = Y(I)
              IF( Y(I) .GT. YMAX )YMAX = Y(I)
            END IF
          END DO
        ELSE
          DO I = 1, NPT
            IF( Y(I) .LT. YMIN )YMIN = Y(I)
            IF( Y(I) .GT. YMAX )YMAX = Y(I)
          END DO
        END IF
        IF( YZERO .NE. 0.0 )THEN
          IF( YMIN .GE. 0.0 )YMIN = 0.0
          IF( YMAX .LE. 0.0 )YMAX = 0.0
          CALL SETNAM('YZERO',0.0)
        END IF
        IF( INT(ABS(YLOG)) .LE. 1 )THEN
          IF( NLYINC .EQ. 0 )THEN
            CALL SCALE1(YMIN,YMAX,5,YMINS,YMAXS,YINC)
            NLYINC = INT( (YMAXS-YMINS)/YINC + 0.5 )
            CALL SETNAM('NLYINC',FLOAT(NLYINC))
          ELSE
            CALL SCALE2(YMIN,YMAX,MAX(NLYINC,2),YMINS,YMAXS,YINC,SVAL,0)
          END IF
        ELSE
          IF( YMIN .LE. 0.0 )YMIN = 1.E-36 
          YMINS = ALOG(YMIN)/ALOG(ABS(YLOG))
          IF( YMINS .LT. INT(YMINS) )YMINS = YMINS-1.0
          YMINS = INT(YMINS)
          IF( YMAX .LE. 0.0 )YMAX = 1.E-36 
          YMAXS = ALOG(YMAX)/ALOG(ABS(YLOG))
          IF( YMAXS .GT. INT(YMAXS) )YMAXS = YMAXS+1.0
          YMAXS = INT(YMAXS) 
          IF( NLYINC .EQ. 0 )GO TO 50
          IF(INT(YMAXS-YMINS)/NLYINC*NLYINC.EQ.INT(YMAXS-YMINS))
     &         GO TO 60
   50     NLYINC = MAX(5,NLYINC)
          CALL SCALE1(YMINS,YMAXS,NLYINC,YMINP,YMAXP,YINC)
          NLYINC = INT( (YMAXP-YMINP)/YINC + 0.5 )
          CALL SETNAM('NLYINC',FLOAT(NLYINC))
          YMINS = INT(YMINP)
          YMAXS = INT(YMAXP)
        END IF
   60   IF( (YAUTO.EQ.1.0) .OR. (YAUTO.EQ.2.0) )THEN
          CALL SETNAM('YMIN',YMINS)
          CALL SETNAM('YMAX',YMAXS)
        ELSE IF( YAUTO .EQ. 3.0 )THEN
          CALL SETNAM('YMIN',YMIN)
          CALL SETNAM('YMAX',YMAX)
          CALL SETNAM('YVMIN',YMINS)
          CALL SETNAM('YVMAX',YMAXS)
        END IF
        CALL SETNAM('YAUTO',0.0)
        IF( INT(ABS(YLOG)) .GT. 1 )THEN
          IF( YLOG .GT. 0. )THEN
            NYDIG1P = 1
            IF( ABS(YMINS) .GT. 0.0 )NYDIG1P = INT(LOG10(ABS(YMINS)))+1
            IF( YMINS .LT. 0.0 )NYDIG1P = NYDIG1P+1
            NYDIG1B = INT(LOG10(ABS(YLOG)))+1
            NYDIG1  = NYDIG1P+NYDIG1B
            NYDIG2P = 1
            IF( ABS(YMAXS) .GT. 0.0 )NYDIG2P = INT(LOG10(ABS(YMAXS)))+1
            IF( YMAXS .LT. 0.0 )NYDIG2P = NYDIG2P+1
            NYDIG2  = NYDIG2P+NYDIG1B
            NYDIG = MAX(NYDIG1,NYDIG2)
            CALL SETNAM('NYDIG',FLOAT(NYDIG))
          END IF
        END IF
      END IF
      RETURN
      END
