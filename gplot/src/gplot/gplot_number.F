      SUBROUTINE GPLOT_NUMBER(XI,YI,SYMS,XVAL,THETA,N)

      DATA IBASE /48/
      BYTE MZERO(2)
C
      LOGICAL FIRST
      COMMON /GPLOT_NUMBER_FIRST/ FIRST
      INTEGER*4 IOUTS
      COMMON /PLOT_OUTPUT_UNIT/ IOUTS

C  Modified by J.L.Chuma, 08-Apr-1997 to elimate SIND, COSD for g77
      REAL*4 DTOR /0.017453292519943/
C
      XPL(XII,YII) = XLAXIS + COSX*(AX*XII + BX) + COSY*(AY*YII + BY)
      YPL(XII,YII) = YLAXIS + SINX*(AX*XII + BX) + SINY*(AY*YII + BY)
CCC
      MZERO(1) = ICHAR('0')  ! modified by J.Chuma 21Mar97 for g77
      MZERO(2) = ICHAR('.')
      IF( FIRST )THEN
        FIRST  = .FALSE.
        YUWIND = GETNAM('YUWIND')
        YLWIND = GETNAM('YLWIND')
        XAXISA = GETNAM('XAXISA')
        YAXISA = GETNAM('YAXISA')
        XLAXIS = GETNAM('XLAXIS')
        YLAXIS = GETNAM('YLAXIS')
        XUAXIS = GETNAM('XUAXIS')
        YUAXIS = GETNAM('YUAXIS')
        XMIN   = GETNAM('XMIN')
        XMAX   = GETNAM('XMAX')
        YMIN   = GETNAM('YMIN')
        YMAX   = GETNAM('YMAX')
        XLOG   = ABS(GETNAM('XLOG'))
        YLOG   = ABS(GETNAM('YLOG'))
        IF( XLOG .GT. 1. )ALOGX = ALOG(XLOG)
        IF( YLOG .GT. 1. )ALOGY = ALOG(YLOG)
        COSX = COS(XAXISA*DTOR)
        SINX = SIN(XAXISA*DTOR)
        COSY = COS(YAXISA*DTOR)
        SINY = SIN(YAXISA*DTOR)
        IF( ABS(COSX) .LE. 1.E-7 )COSX = 0.0
        IF( ABS(SINX) .LE. 1.E-7 )SINX = 0.0
        IF( ABS(COSY) .LE. 1.E-7 )COSY = 0.0
        IF( ABS(SINY) .LE. 1.E-7 )SINY = 0.0
        AX = (XUAXIS - XLAXIS) / (XMAX - XMIN)
        BX = -AX * XMIN
        AY = (YUAXIS - YLAXIS) / (YMAX - YMIN)
        BY = -AY * YMIN
      END IF
      X = XI
      Y = YI
      IF( XLOG .GT. 1. )THEN 
        IF( X .LE. 0. )X = 1.E-36
        X = ALOG(X) / ALOGX
      END IF
      IF( YLOG .GT. 1. )THEN 
        IF( Y .LE. 0. )Y = 1.E-36
        Y = ALOG(Y) / ALOGY
      END IF
      IF( ((X-XMIN)*(XMAX-X) .LT. 0.) .OR. ((Y-YMIN)*(YMAX-Y) .LT. 0.) )
     & GO TO 100
      S   = SYMS * (YUWIND - YLWIND) / 100.
      ANG = THETA*0.0174533
      DY  = S*0.857143
      DX  = DY*COS(ANG)
      DY  = DY*SIN(ANG)
      X0  = XPL(X,Y)
      Y0  = YPL(X,Y)-0.5*S

C  CHECK FOR ZERO MAGNITUDE (XVAL)

      ABXVAL = ABS(XVAL)
      IF( ABXVAL )2, 2, 1

C  NON ZERO MAGNITUDE (XVAL)

    1 R = -N
      IF( N )3, 3, 5

C  ROUND TO INTEGRAL VALUE

    3 ABXVAL = ABXVAL + 0.5
      GO TO 4

C  ROUND TO N DECIMAL PLACES

    5 ABXVAL = ABXVAL + 0.5*10.0**R

C  CHECK FOR NEGATIVE VALUE

    4 IF( XVAL )6, 7, 7

C  WRITE MINUS SIGN

#ifdef g77
    6 CALL PSYM(X0,Y0,S,'-',THETA,1,*999)
#else
    6 CALL PSYM(X0,Y0,S,%REF('-'),THETA,1,*999)
#endif
      X0 = X0 + DX
      Y0 = Y0 + DY

C  DETERMINE NUMBER OF INTEGRAL PLACES (XVAL)

    7 J   = 0
      AM  = 0.1
      ANG = ABXVAL
   16 IF( ANG-1.0 )23, 24, 24
   24 ANG = ANG/10.0
      J   = J+1
      AM  = AM*10.0
      GO TO 16
   23 IF( J )15, 15, 9

C  WRITE DIGIT ZERO

#ifdef g77
   15 CALL PSYM(X0,Y0,S,'0',THETA,1,*999)
#else
   15 CALL PSYM(X0,Y0,S,%REF('0'),THETA,1,*999)
#endif
      X0 = X0 + DX
      Y0 = Y0 + DY
      GO TO 8

C  WRITE INTEGRAL DIGITS

    9 DO M = 1, J
        K = ABXVAL/AM

C  CONVERT TO CODED CHARACTER

#ifdef g77
        CALL PSYM(X0,Y0,S,CHAR(K+IBASE),THETA,1,*999)
#else
        CALL PSYM(X0,Y0,S,%REF(CHAR(K+IBASE)),THETA,1,*999)
#endif
        ABXVAL = ABXVAL-FLOAT(K)*AM
        AM    = AM/10.0
        X0    = X0 + DX
        Y0    = Y0 + DY
      END DO

C  CHECK FOR INTEGRAL VALUE ONLY

    8 IF( N )100, 11, 11

C  WRITE DECIMAL POINT

#ifdef g77
   11 CALL PSYM(X0,Y0,S,'.',THETA,1,*999)
#else
   11 CALL PSYM(X0,Y0,S,%REF('.'),THETA,1,*999)
#endif
      X0 = X0 + DX
      Y0 = Y0 + DY

C  WRITE DECIMAL PLACES

      DO M = 1, N
        J = ABXVAL*10.0
#ifdef g77
        CALL PSYM(X0,Y0,S,CHAR(J+IBASE),THETA,1,*999)
#else
        CALL PSYM(X0,Y0,S,%REF(CHAR(J+IBASE)),THETA,1,*999)
#endif
        ABXVAL = ABXVAL*10.0-FLOAT(J)
        X0     = X0 + DX
        Y0     = Y0 + DY
      END DO
      GO TO 100

C  WRITE ZERO MAGNITUDE

    2 J = 1
      IF( N )13, 13, 14
   14 J = 2
   13 CALL PSYM(X0,Y0,S,MZERO,THETA,J,*999)
  100 RETURN
  999 CALL TRANSPARENT_MODE(0)
      CALL TRANSPARENT_MODE2(0)
      WRITE(IOUTS,*)'*** Error return from PSYM in GPLOT_NUMBER'
      RETURN
      END
