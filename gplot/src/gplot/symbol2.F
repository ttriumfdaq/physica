      SUBROUTINE SYMBOL2(X,Y,SIZE,IBCD,THETA)
C
C     LIBRARY-ROUTINE
C
C                                                29/JULY/1980
C                                                C.J. KOST SIN
C  
C     reqd. KOSTL: routines - CILS2,PLOTR
C
C===================================================================
C
C      BASIC FORTRAN
C     THE ROUTINE DRAWS 1 CHARACTER STORED IN "ICBD" WITH THE BOTTOM
C     LEFT OF CHARACTER POSITIONED AT "X,Y" AND WITH HEIGHT "SIZE"
C     AT AN ANGLE "THETA" TO THE X-AXIS (HORIZONTAL).
C
C     THE UNITS OF "X,Y,SIZE" ARE IN "FACR" SCREEN CELLS UNITS.
C     (SEE SUBROUTINE FACTOR WHICH SETS "FACR" ).
C     THE UNITS OF THETA ARE IN DEGREES.
C
      BYTE IBCD      ! integer*2 in original symbol
      DIMENSION IAL(71)
      COMMON/CIL/IPENX,IPENY,IPS,IPC,IPCN,FACR,XM,SIZES,SIZEN,
     1SIZEL,TICK,STEP,XSPAC,IPNAB,ITAPE,IB,IBC,IBYTE,IBASE,IMMET
C      PDP 11 CHARACTERS CODES IN ASCENDING TABULAR ORDER(1-64)
C       !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ
C      [\]^_
      DATA IAL(1),IAL(2),IAL(3),IAL(4),IAL(5),IAL(6),IAL(7),IAL(8),
     1IAL(9),IAL(10),IAL(11),IAL(12),IAL(13),IAL(14),IAL(15),IAL(16),
     2IAL(17),IAL(18),IAL(19),IAL(20),IAL(21),IAL(22),IAL(23)/
     A0,0,15405,0,4847,0,0,15398,13348,13092,5867,5861,13606,5858,
     B13669,12898,42,803,1128,1646,2790,3177,3756/
      DATA
     3IAL(24),IAL(25),IAL(26),IAL(27),IAL(28),IAL(29),IAL(30),
     4IAL(31),IAL(32),IAL(33),IAL(34),IAL(35),IAL(36),IAL(37)/
     B4517,1650,5037,
     C13673,13610,14499,14244,14691,16236,0,6568,7148,8040,7079/
      DATA IAL(38),IAL(39),IAL(40),IAL(41),IAL(42),IAL(43),IAL(44),
     3 IAL(45),IAL(46),IAL(47),IAL(48),IAL(49),IAL(50),IAL(51)/
     C8551,
     D8614,7914,8998,802,9381,9766,7203,10149,10468,105,10727,107,10729/
      DATA
     7IAL(52),IAL(53),IAL(54),IAL(55),IAL(56),IAL(57),IAL(58),
     8IAL(59),IAL(60),IAL(61),IAL(62),IAL(63),IAL(64),IAL(65),IAL(66),
     9IAL(67),IAL(68),IAL(69),IAL(70),IAL(71)/
     E5036,11300,9382,11555,11749,12071,12517,12836,15140,10530,
     F14884,805,0,16997,17253,17001,17574,17893,17578,17259/
CCC
C        UNPACK IAL ENTRY TO GIVE IPAIR ADDRESS AND # OF POINTS

      NP  = IAL(IBCD-31)
      IPA = NP/32
      NP  = NP-32*IPA

      IF( NP .LE. 0 )RETURN
      IF( SIZE .LE. 0.0 )RETURN

      XA = (SIZE/7.0) * COS( THETA*.017453293 )
      YA = (SIZE/7.0) * SIN( THETA*.017453293 )

      IC = 3         ! PEN UP
      DO I = 1, NP   ! UNPACK OFF-SET COORDINATES
        CALL CILS2( IPA, XP, YP )
        X1 = X + XP*XA - YP*YA
        Y1 = Y + XP*YA + YP*XA
        CALL PLOT_R( X1, Y1, IC )
        IC = 2
        IPA = IPA+2
      END DO
      RETURN
      END
