      INTEGER*4 FUNCTION GET_ITEM( NAME )

      CHARACTER*(*) NAME

      CHARACTER*8 NAMES(103)
      COMMON /GNAMES/ NNAMES, NAMES

      REAL*4 TABLE(103)
      COMMON /GPLOTC/ TABLE

      CHARACTER*8 TEMP
      INTEGER*4   LNAME, I, ITM
CCC
      IF( TABLE(1) .EQ. 0.0 )CALL GPLOTI
      CALL UPRCASE(NAME,TEMP)
      LNAME = LENSIG(TEMP)
      IF( TEMP(1:1) .EQ. '%' )THEN
        I = 1
      ELSE
        I = 2
      END IF
      DO ITM = 1, NNAMES
        IF( TEMP(1:LNAME) .EQ. NAMES(ITM)(I:) )THEN
          GET_ITEM = ITM
          RETURN
        END IF
      END DO
      GET_ITEM = 0
      RETURN
      END
