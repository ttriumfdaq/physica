      SUBROUTINE AXLOG( BASE, XS0, YS0, SLEN, THETAS, POWMIN, POWMAX
     #             ,NLINC, TICL, NSINCIN, TICS, TICANG, ITYPTC
     #             ,TICLAB, TICANL, ANGLAB, SIZLAB, ITYPLB
     #             ,MAX_NLABCH, GRID, XTICARR, YTICARR, NUMTIC, MAXTIC )
C
C   AXLOG plots a general logarithmic axis with base "BASE" and
C         minimum power "POWMIN" and maximum power "POWMAX"
C
C  BASE  : the logarithmic base used in plotting the axis
C
C  XS0   : screen x-coordinate of the start of the axis
C
C  YS0   : screen y-coordinate of the start of the axis
C
C  SLEN  : length of axis in screen units
C
C  THETAS: angle of the axis in degrees relative to the horizontal
C
C  POWMIN: minimum power of the base "BASE" of the axis
C
C  POWMAX: maximum power of the base "BASE" of the axis
C
C  NLINC : number of large increments into which the axis is to be
C          subdivided.  The number of large tic marks plotted on the
C          axis which delineate the large increments will be NLINC+1
C
C  TICL  : length of the large tic marks which are used to indicate
C          the large increments
C
C  TICS  : length of the small tic marks which are used to indicate
C          the small increments
C
C  TICANG: angle of the tic marks w.r.t. the axis direction
C
C  ITYPTC: tells "AXLOG" what type of tic mark to plot
C          If ITYPTC = 1 then the tic marks are plotted only on one side
C          of the axis, i.e. that side given by "TICANG"
C          If ITYPTC = 2 then the tic marks are plotted symmetrically
C          on both sides of the axis as line segments crossing the
C          axis at an angle of "TICANG" degrees
C
C  TICLAB: length of the virtual pointer (tic mark), which points to
C          the location where the axis label will be centered on its
C          perimeter at the large tic mark locations
C
C  TICANL: angle of the virtual pointer (tic mark), which points to the
C          location where the axis label will be centered on its
C          perimeter at the large tic mark locations. This angle is in
C          degrees relative to the axis direction
C
C          Note: The virtual pointers are vectors whose starting points
C                are on the axis at the large tic mark locations and
C                end points determine the positions of the axis labels
C
C  ANGLAB: angle of the axis labels in degrees, w.r.t. the "horizontal"
C          screen direction if |ITYPLB| = 1, or w.r.t. the axis
C          direction if |ITYPLB| = 2
C
C  SIZLAB: height of the axis label values. If SIZLAB <= 0, then no
C          axis labels will be plotted
C
C  ITYPLB: Denotes the type of label. See ANGLAB
C
C  Modified by J.L.Chuma, 08-Apr-1997 to elimate SIND, COSD for g77
C
      REAL*4    BASE, POWMIN, POWMAX
      INTEGER*4 ITICS(2:9)
      LOGICAL   DECIMAL
      DATA      EXP1 / 2.718281828 /
C
      REAL*4    XTICARR(1), YTICARR(1), GRID
      INTEGER*4 NUMTIC
C
      CHARACTER STRING*25
CVAX  LOGICAL*1 LABEL(25), UP /Z09/
      BYTE LABEL(25), UP /9/, ZERO /'0'/ ! modified JLC 20Mar97 for g77
      EQUIVALENCE (LABEL(1),STRING(1:1))
C
      INTEGER*4 NHATCH(2)
      COMMON /PSYM_HATCHING/ NHATCH
C
      INTEGER*4 NUMBOLD
      COMMON /BOLD_AXIS_NUMBERS/ NUMBOLD
      REAL*4 DTOR /0.017453292519943/
C
      NHATCH(1) = NUMBOLD
      NHATCH(2) = 0
C
      MAX_NLABCH = 0
C
      IGRID  = IFIX(GRID)
      NUMTIC = 0
      NSINC  = NSINCIN
C
C  If DECIMAL = .TRUE. then write the axis labels as decimal numbers
C  instead of as powers, e.g., 0.1  1  10   100   1000  instead of 
C
C                                -1    0    2    3
C                              10    10   10   10
C
      IF( BASE .LT. 0. )THEN
        DECIMAL = .TRUE.
        BASE    = ABS(BASE)
        IF( BASE .EQ. EXP1 )DECIMAL = .FALSE.
      ELSE
        DECIMAL = .FALSE.
      END IF
C
C   Convert the base "BASE" into a logical array "LABEL" with
C   "NSIG1" characters.  Then put the "UP" character (hexadecimal "09")
C   at the end of the string. This signifies to PSYM that an exponent
C   is to follow and therefore to be shifted up as well as reduced in
C   size.
C
      IBASE = IFIX(BASE)
      IF( BASE .EQ. EXP1 )THEN   ! Base e
        STRING(1:1) = 'e'      ! modified J.Chuma 20Mar97 for g77
        NSIG1    = 1
      ELSE
        CALL BTD(IBASE,LABEL(1),0,NSIG1,ZERO)
      END IF
      CALL MOVEC(1,UP,LABEL(NSIG1+1))
C
C   Initialize constants needed throughout the subroutine.
C
      COSTH  = COS(THETAS*DTOR)
      SINTH  = SIN(THETAS*DTOR)
      COSTIC = COS(TICANG*DTOR)
      SINTIC = SIN(TICANG*DTOR)
      ANGLB  = ANGLAB
      IF( IABS(ITYPLB) .NE. 2 )ANGLB = ANGLAB-THETAS
      COSLAB = COS(ANGLB*DTOR)
      SINLAB = SIN(ANGLB*DTOR)
C
C   (XTICL,YTICL) are the coordinates of the end of the virtual pointer
C   (tic mark) which points to the location where the axis label will
C   be centered on its perimeter.
C
      XTICL = COS(TICANL*DTOR)*TICLAB
      YTICL = SIN(TICANL*DTOR)*TICLAB
C
C   Set "YLIM" for the call to "LABXY" which determines the label
C   position relative to the axis at each long tic mark. The label is
C   not allowed to cross the line "Y = YLIM", where "YLIM" is set to be
C   the maximum perpendicular distance of the long and short tic marks
C   from the axis on the labelled side of the axis.
C
      IF( ITYPTC .EQ. 2 )THEN
C
C   Tic marks are plotted on both sides of the axis.
C
        YLIM = SIGN(1.,YTICL)*MAX(ABS(SINTIC*TICL),ABS(SINTIC*TICS))
        IF( YTICL .EQ. 0. )YLIM = 0.
      ELSE
C
C   Tic marks are plotted on one side of the axis.
C
        IF( YTICL .LE. 0. )YLIM = MIN(SINTIC*TICL,SINTIC*TICS,0.)
        IF( YTICL .GT. 0. )YLIM = MAX(SINTIC*TICL,SINTIC*TICS,0.)
      END IF
C
C   N1 = minimum power displayed on the axis
C
      IF( POWMIN .GE. 0.0 )THEN
        N1 = INT(POWMIN+0.99999)
      ELSE
        N1 = INT(POWMIN)
      END IF
C
C   N2 = maximum power displayed on the axis
C
      IF( POWMAX .GE. 0.0 )THEN
        N2 = INT(POWMAX)
      ELSE
        N2 = INT(POWMAX-0.99999)
      END IF
C
      IF( NLINC .EQ. 0 )THEN
cc        NLINC2 = ABS(N2-N1+1)
        NLINC2 = ABS(N2-N1)
      ELSE
cc        NLINC2 = MIN( ABS(NLINC)+1, ABS(N2-N1+1) )
        NLINC2 = MIN( ABS(NLINC), ABS(N2-N1) )
      END IF
cc      NLINC2 = MAX( NLINC2, 2 )              ! number of large tic marks
      NLINC2 = MAX( NLINC2, 1 )              ! number of large tic marks
C
cc      INCR = MAX( ABS(N2-N1+1)/(NLINC2), 1 ) ! large tic mark increment
      INCR = MAX( ABS(N2-N1)/(NLINC2), 1 ) ! large tic mark increment

cc      write(*,*)'n1= ',n1,', n2= ',n2,', nlinc= ',nlinc,
cc     & ', nlinc2= ',nlinc2,', incr= ',incr

C
C  Do not allow specifying short tic marks by digits of NSINC
C  when BASE > 10
C
      IF( (NSINC .LT. 0) .AND. (IBASE .GT. 10) )NSINC = ABS(NSINC)
C
      DO I = 2, 9
        ITICS(I) = 0
      END DO
      IF( NSINC .LT. 0 )THEN   ! the short tic mark locations are
C                                specified by the digits of NSINC
        NSABS = ABS(NSINC)
        DO WHILE (NSABS .NE. 0)
          INS = NSABS/10
          IDX = NSABS - INS*10
          IF( IDX .NE. 0 )ITICS(IDX) = 1
          NSABS = INS
        END DO
        INCRS = 1              ! increment between short tic marks
C                                using ITICS look at every short tic
      ELSE                     ! NSINC = number of short tic marks 
        NSINC = MIN( IBASE-1, NSINC )
CC        IF( INCR .NE. 1 )NSINC = -1  ! did not allow short tics
        IF( IBASE .EQ. 10 )THEN      ! special case log base 10
          IF( NSINC .EQ. 1 )THEN
            ITICS(5) = 1
          ELSE IF( NSINC .EQ. 2 )THEN
            ITICS(2) = 1
            ITICS(5) = 1
          ELSE IF( NSINC .EQ. 3 )THEN
            ITICS(2) = 1
            ITICS(4) = 1
            ITICS(7) = 1
          ELSE IF( NSINC .EQ. 4 )THEN
            ITICS(2) = 1
            ITICS(4) = 1
            ITICS(6) = 1
            ITICS(8) = 1
          ELSE IF( NSINC .GE. 5 )THEN
            DO I = 2, 9
              ITICS(I) = 1
            END DO
          END IF
          NSINC = -1
          INCRS = 1               ! increment between short tic marks
        ELSE
          IF( NSINC .GT. 0 )THEN
            INCRS = (IBASE-1)/NSINC
          ELSE                    ! NSINC must be zero
            INCRS = IBASE
          END IF
        END IF
      END IF
C
      BLOG10 = LOG10(BASE)
      BPOW   = BASE**(POWMIN-N1+1)  ! actual value at lower end of axis
      M      = 2
      DO WHILE ( M .LE. IBASE )
        IF( FLOAT(M) .GE. BPOW )GO TO 30
        M = M + INCRS
      END DO
C
C   put the pen up at the origin of the axis
C
30    CALL PLOT_R(XS0,YS0,3)
      PFACT = SLEN/(POWMAX-POWMIN)
      IF( NSINC .LT. 0 )THEN
        DO I = M, IBASE-1
          IF( ITICS(I) .EQ. 1 )THEN
C                                                  N1-1
C   plot the axis line and the small tics at I*BASE
C
            STIC = (N1-1+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
            XS = XS0+COSTH*STIC
            YS = YS0+SINTH*STIC
            CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
            IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
              XTICARR(NUMTIC+1) = XS
              YTICARR(NUMTIC+1) = YS
              NUMTIC = NUMTIC + 1
            END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
            XT = COSTIC*TICS
            YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
            XTS = XS+COSTH*XT-SINTH*YT
            YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
            CALL PLOT_R(XTS,YTS,2)
            IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
              XTSN = XS-COSTH*XT+SINTH*YT
              YTSN = YS-SINTH*XT-COSTH*YT
              CALL PLOT_R(XTSN,YTSN,2)
            END IF
            CALL PLOT_R(XS,YS,3)
          END IF
        END DO
      ELSE
        DO I = M, IBASE-1, INCRS
C                                                  N1-1
C   plot the axis line and the small tics at I*BASE
C
          STIC = (N1-1+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
          XS = XS0+COSTH*STIC
          YS = YS0+SINTH*STIC
          CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
          IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
            XTICARR(NUMTIC+1) = XS
            YTICARR(NUMTIC+1) = YS
            NUMTIC = NUMTIC + 1
          END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
          XT = COSTIC*TICS
          YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
          XTS = XS+COSTH*XT-SINTH*YT
          YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
          CALL PLOT_R(XTS,YTS,2)
          IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
            XTSN = XS-COSTH*XT+SINTH*YT
            YTSN = YS-SINTH*XT-COSTH*YT
            CALL PLOT_R(XTSN,YTSN,2)
          END IF
          CALL PLOT_R(XS,YS,3)
        END DO
      END IF
      IPOW = N1 - 1
cc      J = 0
      J = -1
      DO K = N1, N2
        J = J+1
        IPOW = IPOW+1
cc        IF( (K .EQ. N1) .OR. (K .EQ. N2) .OR. (J .EQ. INCR) )J = 0
        IF( (K.EQ.N1) .OR. (K.EQ.N2) .OR. (J/INCR*INCR.EQ.J) )J = 0

cc        write(*,*)'k= ',k,', j= ',j,', incr= ',incr
C
C                           IPOW
C   plot a large tic at BASE
C
        STIC = (IPOW - POWMIN)*PFACT
        XS = XS0+COSTH*STIC
        YS = YS0+SINTH*STIC
        CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
        IF( (J.EQ.0) .AND. (IGRID.NE.0) .AND. (NUMTIC.LT.MAXTIC) )THEN
          IF( (IGRID .LT. 0) .OR. 
     #        ((K-N1+2)/IGRID*IGRID-(K-N1+2) .EQ. 0) )THEN
            XTICARR(NUMTIC+1) = XS
            YTICARR(NUMTIC+1) = YS
            NUMTIC = NUMTIC + 1
          END IF
        END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
        XT = COSTIC*TICL
        YT = SINTIC*TICL
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
        XTS = XS+COSTH*XT-SINTH*YT
        YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
        CALL PLOT_R(XTS,YTS,2)
        IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
          XTSN = XS-COSTH*XT+SINTH*YT
          YTSN = YS-SINTH*XT-COSTH*YT
          CALL PLOT_R(XTSN,YTSN,2)
        END IF
        IF( J .NE. 0 )GO TO 34
C
C   A label is to be plotted.  IPOW is the power of the label being
C   plotted.  Append the characters of the power to the base + UP (09)
C   character already stored in "LABEL".
C
        IF( .NOT.DECIMAL )THEN
32        CALL BTD(IPOW,LABEL(NSIG1+2),0,NSIG2,ZERO)
          IF( IPOW .EQ. 0 )THEN
            NSIG2 = 1
            CALL MOVEC(1,ZERO,LABEL(NSIG1+2))
          END IF
C
C   NLABCH is the number of characters in "LABEL".
C   FLENG = length in screen units of the label, as plotted by "PSYM"
C
          NLABCH = NSIG1+1+NSIG2
          IDC1 = 1
        ELSE
          XOUT = BASE**IPOW
          IF( (XOUT .GT. 9999999.0) .OR. (XOUT .LT. 0.0000001) )THEN
            CALL BTD(IBASE,LABEL(1),0,NSIG1,ZERO)
            CALL MOVEC(1,UP,LABEL(NSIG1+1))
            GO TO 32
          END IF
          STRING = ' '
          WRITE(STRING,321)XOUT
321       FORMAT(F25.7)
          IDC1 = 0
322       IDC1 = IDC1 + 1
          IF( STRING(IDC1:IDC1) .NE. ' ' )GO TO 323
          GO TO 322
323       IDC2 = 26
324       IDC2 = IDC2 - 1
          IF( (STRING(IDC2:IDC2) .NE. '0') .AND. 
     #        (STRING(IDC2:IDC2) .NE. ' ') )GO TO 325
          GO TO 324
325       IF( STRING(IDC1:IDC1) .EQ. '.' )THEN
            IDC1 = IDC1 - 1
            STRING(IDC1:IDC1) = '0'
          END IF
          IF( STRING(IDC2:IDC2) .EQ. '.' )IDC2 = IDC2 - 1
          NLABCH = IDC2 - IDC1 + 1
        END IF
        MAX_NLABCH = MAX(MAX_NLABCH,NLABCH)
        FLENG = PSMLEN(LABEL(IDC1),NLABCH,SIZLAB)
C
C   Determine the lower left hand corner: (XLAB,YLAB) at which the
C   label is to be plotted, relative to the axis direction, and the
C   location "STIC" of the large tic mark on the axis.
C
        CALL LABXY(XTICL,YTICL,ANGLB,COSLAB,SINLAB,SIZLAB,FLENG
     #            ,YLIM,XLAB,YLAB)
C
C   (XLS,YLS) are the rotated screen coordinates at which the label is
C   to be plotted by "PSYM".
C
        XLS = XS+COSTH*XLAB-SINTH*YLAB
        YLS = YS+SINTH*XLAB+COSTH*YLAB
        IF( XLS .EQ. -0. )XLS = 0.
        IF( YLS .EQ. -0. )YLS = 0.
C
C   Plot the label at an angle relative to the horizontal of "ANGLE"
C   degrees.
C
        ANGLE = ANGLB+THETAS
        CALL PSYMBOLD(XLS,YLS,SIZLAB,LABEL(IDC1),ANGLE,NLABCH)
C
C   Move with the pen up to the beginning of the tic mark.
C
34      CALL PLOT_R(XS,YS,3)
        IF( IPOW .NE. N2 )THEN
          IF( NSINC .LT. 0 )THEN
            DO I = 2, 9
              IF( ITICS(I) .EQ. 1 )THEN
C                                                  IPOW
C   plot the axis line and the small tics at I*BASE
C
                STIC = (IPOW+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
                XS = XS0+COSTH*STIC
                YS = YS0+SINTH*STIC
                CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
                IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
                  XTICARR(NUMTIC+1) = XS
                  YTICARR(NUMTIC+1) = YS
                  NUMTIC = NUMTIC + 1
                END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
                XT = COSTIC*TICS
                YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
                XTS = XS+COSTH*XT-SINTH*YT
                YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
                CALL PLOT_R(XTS,YTS,2)
                IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
                  XTSN = XS-COSTH*XT+SINTH*YT
                  YTSN = YS-SINTH*XT-COSTH*YT
                  CALL PLOT_R(XTSN,YTSN,2)
                END IF
                CALL PLOT_R(XS,YS,3)
              END IF
            END DO
          ELSE
            DO I = 2, IBASE-1, INCRS
C                                                  IPOW
C   plot the axis line and the small tics at I*BASE
C
              STIC = (IPOW+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
              XS = XS0+COSTH*STIC
              YS = YS0+SINTH*STIC
              CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
              IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
                XTICARR(NUMTIC+1) = XS
                YTICARR(NUMTIC+1) = YS
                NUMTIC = NUMTIC + 1
              END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
              XT = COSTIC*TICS
              YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
              XTS = XS+COSTH*XT-SINTH*YT
              YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
              CALL PLOT_R(XTS,YTS,2)
              IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
                XTSN = XS-COSTH*XT+SINTH*YT
                YTSN = YS-SINTH*XT-COSTH*YT
                CALL PLOT_R(XTSN,YTSN,2)
              END IF
              CALL PLOT_R(XS,YS,3)
            END DO
          END IF
        END IF
      END DO
C
C   Find the excess small tics past the last large tic 
C
      M = 2
      DO I = 2, IBASE-1
        IF( BASE**(POWMAX-N2) .LE. FLOAT(M) )GO TO 40
        M = M + 1
      END DO
40    M = M - 1
      IF( NSINC .LT. 0 )THEN
        DO I = 2, M
C                                                  N2
C   plot the axis line and the small tics at I*BASE
C
          IF( ITICS(I) .EQ. 1 )THEN
            STIC = (N2+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
            XS = XS0+COSTH*STIC
            YS = YS0+SINTH*STIC
            CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
            IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
              XTICARR(NUMTIC+1) = XS
              YTICARR(NUMTIC+1) = YS
              NUMTIC = NUMTIC + 1
            END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
            XT = COSTIC*TICS
            YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
            XTS = XS+COSTH*XT-SINTH*YT
            YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
            CALL PLOT_R(XTS,YTS,2)
            IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
              XTSN = XS-COSTH*XT+SINTH*YT
              YTSN = YS-SINTH*XT-COSTH*YT
              CALL PLOT_R(XTSN,YTSN,2)
            END IF
            CALL PLOT_R(XS,YS,3)
          END IF
        END DO
      ELSE
        DO I = 2, M, INCRS
C                                                  N2
C   plot the axis line and the small tics at I*BASE
C
          STIC = (N2+LOG10(FLOAT(I))/BLOG10 - POWMIN)*PFACT
          XS = XS0+COSTH*STIC
          YS = YS0+SINTH*STIC
          CALL PLOT_R(XS,YS,2)
C
C   Store the tic mark locations for the grid
C
          IF( (IGRID .LT. 0) .AND. (NUMTIC .LT. MAXTIC) )THEN
            XTICARR(NUMTIC+1) = XS
            YTICARR(NUMTIC+1) = YS
            NUMTIC = NUMTIC + 1
          END IF
C
C   (XT,YT) are the unrotated coordinates of the end of the tic mark
C   relative to the location "STIC".
C
          XT = COSTIC*TICS
          YT = SINTIC*TICS
C
C   (XTS,YTS) are the rotated screen coordinates of the end of the tic
C   mark.
C
          XTS = XS+COSTH*XT-SINTH*YT
          YTS = YS+SINTH*XT+COSTH*YT
C
C   Plot the tic mark by moving the pen from the tic mark location
C   (XS,YS) to the end of the tic mark (XTS,YTS) with the pen down.
C
          CALL PLOT_R(XTS,YTS,2)
          IF( ITYPTC .EQ. 2 )THEN
C
C   ITYPTC = 2 : Plot the tic mark symmetrically on both sides of the
C                axis as a straight line segment crossing the axis at
C                an angle of "TICANG" degrees.
C
            XTSN = XS-COSTH*XT+SINTH*YT
            YTSN = YS-SINTH*XT-COSTH*YT
            CALL PLOT_R(XTSN,YTSN,2)
          END IF
          CALL PLOT_R(XS,YS,3)
        END DO
      END IF
C                                POWMAX
C   Plot the axis line up to BASE
C
      XS = XS0+COSTH*SLEN
      YS = YS0+SINTH*SLEN
      CALL PLOT_R(XS,YS,2)
      NHATCH(1) = 0
      NHATCH(2) = 0
      RETURN
      END
