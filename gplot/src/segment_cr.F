      LOGICAL FUNCTION SEGMENT_CROSS(X1,Y1,X2,Y2,X1P,Y1P,X2P,Y2P,
     *                               XC,YC,SC)
C
C  reqd. routines - NONE 
C
C=================================================================
C=================================================================
C==                                                             ==
C==   SEGMENT_CROSS: returns the function value .TRUE./(.FALSE.)==
C==       if the line segment (X1,Y1)-->(X2,Y2) crosses/(does   ==
C==       not cross) the line segment (X1P,Y1P)-->(X2P,Y2P). If ==
C==       the line segments cross then it also returns the point==
C==       (XC,YC) at which they cross, as well as the relative  ==
C==       distance "SC" along the segment (X1,Y1)-->(X2,Y2) at  ==
C==       which they cross, i.e.                                ==
C==       (XC,YC) = (X1,Y1) + SC * (X2-X1,Y2-Y1).               ==
C==                                                             ==
C==   Written by Arthur Haynes, TRIUMF U.B.C., July 6, 1981.    ==
C==                                                             ==
C==   Input  Parameters: X1,Y1,X2,Y2,X1P,Y1P,X2P,Y2P (R*4).     == 
C==                                                             ==
C==   Output Parameters: XC,YC,SC (R*4).                        ==
C==                                                             ==
C=================================================================
C=================================================================
      DX1=X1-X1P
      DY1=Y1-Y1P
      A11=X2P-X1P
      A12=X1-X2
      A21=Y2P-Y1P
      A22=Y1-Y2
      DETA=A11*A22-A12*A21
      SEGMENT_CROSS=.FALSE.
      IF(ABS(DETA).LE.1.E-30)RETURN   ! modified by JLC on June29/92
C                                       was 1.E-10
      T=(A22*DX1-A12*DY1)/DETA
      S=(A11*DY1-A21*DX1)/DETA
      IF(S.LT.0. .OR. S.GT.1.)RETURN
      IF(T.LT.0. .OR. T.GT.1.)RETURN
      SEGMENT_CROSS=.TRUE.
      SC=S
      XC=X1+S*(X2-X1)
      YC=Y1+S*(Y2-Y1)
      RETURN
      END
