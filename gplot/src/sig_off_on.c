 /* This routine was necessary for Digital Unix 4.0 (and others, like SGI)         */
 /* since a program would stall doing graphics if the alarm was set to 1 second    */
 /* The alarm is set to 100000 seconds in xvst_flush_lines_ and xvst_flush_points_ */
 /* routines.  It should be set back to 1 second in the calling program when the   */
 /* graphics is finished, by calling sig_on_() (in FORTRAN use CALL SIG_ON).       */
    
#include <signal.h>

#ifdef VMS
#define sig_off_ sig_off
#define sig_on_ sig_on
#endif

void sig_off_()
{
#ifdef VMS
#else
#ifndef sgi
#ifndef g77
#ifndef __SVR4
#ifndef _AIX
#ifndef __hpux
   alarm(100000);
#endif
#endif
#endif
#endif
#endif
#endif
}

void sig_on_()
{
#ifdef VMS
#else
#ifndef sgi
#ifndef g77
#ifndef __SVR4
#ifndef _AIX
#ifndef __hpux
   alarm(1);
#endif
#endif
#endif
#endif
#endif
#endif
}
 /* end of file */
