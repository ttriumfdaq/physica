      SUBROUTINE CHREAL(CH,NCH,REAL,NREAL,*)
C
C  reqd. routines - NONE
C 
C THIS ROUTINE MUST BE IN PRV2:[KOST]LIB2.OLB AND IN LIB.OLB
C
C===================================================================
C== This version of CHREAL uses the CHREAL_SCAN lexical scanner to==
C== perform the I/O conversion of an ASCII numeric string to a    ==
C== (single or double precision) real number. Due to the efficieny==
C== of operations used, this routine is approximately 30% faster  ==
C== than the CHREAL routine written by A. Haynes.                 ==
C== REAL*4 returned in REAL if NREAL=1                            ==
C== REAL*8 returned in REAL if NREAL=2                            ==
C===================================================================
      IMPLICIT REAL*8 (A-H,O-Z)
      LOGICAL*1 CH(1)
      LOGICAL INIT/.TRUE./
      REAL REAL4(2),REAL(2)
      REAL*8 REAL8
      EQUIVALENCE (REAL4,REAL8)
C===================================================================
C== Lexical scanner definitions (and actions) follow:             ==
C==                                                               ==
C== The following OUTPUT states are recognized:(CHREAL_CHIP array)==
C==   -2: Successful read, ILAST left alone                       ==
C==   -1: Successful read, ILAST= ILAST-1                         ==
C==    0: Conversion error on read                                ==
C==                                                               ==
C== The following ACTION states are recognized:(CHREAL_ACTION array)
C==    0(all even numbers): No action taken                       ==
C==    1: Accumulate integer portion of number                    ==
C==    3: Set sign                                                ==
C==    5: Accumulate decimal portion of number                    ==
C==    7: Accumulate integer exponent                             ==
C==    9: Set exponent sign                                       ==
C===================================================================
      INTEGER*2 CHREAL_CHIP(8,8)
     & /-1,  0, -1,  0,  0, -1, -1,  0,   !End of line (EOL)
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Invalid characters
     &   1,  0, -1,  0,  0, -1, -1,  0,   !Blank
     &   0,  0,  5,  0,  0,  5,  0,  0,   !d,e,D,E
     &   3,  3,  3,  6,  7,  6,  7,  7,   !0-9
     &   4,  4,  6,  0,  0,  0,  0,  0,   !. (decimal point)
     &   2,  0,  0,  0,  8,  0,  0,  0,   !+-
     &  -2,  0, -2,  0,  0, -2, -2,  0/   !,TAB (non-blank deli.)
      INTEGER*2 CHREAL_ACTION(8,8)
     & / 0,  0,  0,  0,  0,  0,  0,  0,   !End of line (EOL)
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Invalid characters
     &   0,  0,  0,  0,  0,  0,  0,  0,   !Blank
     &   0,  0,  0,  0,  0,  0,  0,  0,   !d,e,D,E
     &   1,  1,  1,  5,  7,  5,  7,  7,   !0-9
     &   0,  0,  0,  0,  0,  0,  0,  0,   !. (decimal point)
     &   3,  0,  0,  0,  9,  0,  0,  0,   !+-
     &   0,  0,  0,  0,  0,  0,  0,  0/   !,TAB (non-blank deli.)
C       
C        S   S   I   D   S   R   R   E
C        t   i   n   e   t   e   e   x
C        a   g   t   c   a   a   a   p
C        r   n   e   i   r   l   l   o
C        t       g   m   t       +   n
C                e   a       n   e   e
C                r   l   e   u   x   n
C                        x   m   p   t
C                    p   p   b   o
C                    o   o   e   n   s
C                    i   n   r   e   i
C                    n   e       n   g
C                    t   n       t   n
C                        t
C===================================================================
      INTEGER*2 CTABLE(128)
      IF(INIT) CALL CHREAL_TABLE(CTABLE)
      INIT= .FALSE.
      CALL CHREAL_SCAN(CH,NCH,1,LEN,ISTATE,CTABLE,
     & CHREAL_CHIP,CHREAL_ACTION,8,REAL8,*90)
#ifdef VMS
      REAL(1)= REAL4(1)
      IF(NREAL.GE.2) REAL(2)= REAL4(2)
#endif
#ifdef unix
      REAL(1)= REAL8
      IF(NREAL.GE.2) THEN
        REAL(1)= REAL4(1)
        REAL(2)= REAL4(2)
      ENDIF
#endif
      RETURN
90    RETURN 1
      END
      SUBROUTINE CHREAL_SCAN(INPUT,NINPUT,IFIRST,ILAST,ISTATE,
     &                  CTABLE,STABLE,ACTION,NSTATE,RESULT,*)
C================================================================
C================================================================
C==                                                            ==
C==   CHREAL_SCAN: PERFORMS A LEXICAL SCAN OF THE "INPUT" ARRAY,=
C==         STARTING AT THE "IFIRST" LOCATION IN "INPUT", USING==
C==         THE TRANSITION TABLE "STABLE(NSTATE,NCLASS)".      ==
C==         THE TOKEN WHICH SCAN RETURNS LIES WITHIN THE       ==
C==         LOCATIONS "IFIRST" AND "ILAST" OF "INPUT".         ==
C==                                                            ==
C==   WRITTEN BY ARTHUR HAYNES, TRIUMF U.B.C., JANUARY 3, 1980.==
C==   Modified by Phil Bennett and Joe Chuma, August 7, 1984,  ==
C==   to efficiently handle real number I/O conversion.        ==
C==                                                            ==
C==   INPUT  PARAMETERS: INPUT(NINPUT) (L*1,I*2,I*4,R*4,OR R*8);=
C==                      NINPUT,IFIRST,NSTATE (I*4);           ==
C==                      CTABLE (USUALLY A 128 I*2 ARRAY);     ==
C==                      STABLE(NSTATE,NCLASS) (I*2);          ==
C==                      ACTION(NSTATE,NCLASS) (I*2).          ==
C==                                                            ==
C==   OUTPUT PARAMETERS: ILAST,ISTATE (I*4);                   ==
C==                      RESULT (the REAL*8 converted number). ==
C==                                                            ==
C==   PARAMETER DEFINITIONS:                                   ==
C==   --------- -----------                                    ==
C==                                                            ==
C==   INPUT : A L*1,I*2,I*4,R*4, OR R*8 ARRAY OF ELEMENTS TO BE==
C==           SCANNED FOR TOKENS (OR TERMS).                   ==
C==                                                            ==
C==   NINPUT: NUMBER OF ELEMENTS TO BE SCANNED IN "INPUT".     ==
C==                                                            ==
C==   IFIRST: FIRST LOCATION IN "INPUT" AT WHICH THE SCAN IS TO==
C==           START.                                           ==
C==                                                            ==
C==   ILAST : LAST LOCATION IN "INPUT" AT WHICH THE SCAN IS    ==
C==           TERMINATED. THE TOKEN RETURNED BY SCAN LIES      ==
C==           WITHIN THE LOCATIONS "IFIRST" AND "ILAST" OF     ==
C==           "INPUT".                                         ==
C==                                                            ==
C==   ISTATE: OUTPUT STATE RETURNED BY SCAN WHICH CORRESPONDS  ==
C==           TO THE TYPE OF TOKEN RETURNED. SEE "STABLE".     ==
C==                                                            ==
C==                                                            ==
C==   CTABLE: IS USUALLY AN INTEGER*2 CLASS TABLE "CTABLE(128)".=
C==           IF "CHAR" IS A CHARACTER (1 BYTE) THEN IT HAS A  ==
C==           NUMERIC VALUE "ICHAR" IN THE RANGE               ==
C==           0 <= "ICHAR" <= 255 AND ITS CLASS NUMBER IS      ==
C==           GIVEN BY "CTABLE(ICHAR+1)".                      ==
C==                                                            ==
C==   STABLE: LEXICAL SCANNER TRANSITION TABLE DIMENSIONED AS: ==
C==           STABLE(NSTATE,NCLASS) WHERE "NSTATE" IS THE NUMBER=
C==           OF TRANSITION STATES AND "NCLASS" IS THE NUMBER  ==
C==           OF "INPUT" ELEMENT CLASSES.                      ==
C==           IF 1 <= "STABLE(ISTATE,ICLASS)" <= NSTATE THEN IT==
C==           IS A TRANSITION STATE.                           ==
C==           IF "STABLE(ISTATE,ICLASS)" < 1 THEN IT IS AN     ==
C==           OUTPUT STATE AND THE SCAN POINTER "ILAST" IS     ==
C==           SHIFTED BY "CLASS" BACK BY 1.                    ==
C==           IF "STABLE(ISTATE,ICLASS)" > NSTATE THEN IT IS AN==
C==           OUTPUT STATE AND THE SCAN POINTER IS NOT CHANGED.==
C==           THE VALUE OF THE OUTPUT STATE IS RETURNED BY     ==
C==           "SCAN" IN "ISTATE".                              ==
C==                                                            ==
C==   NSTATE: NUMBER OF TRANSITION STATES.                     ==
C==                                                            ==
C==   ACTION: INTEGER*2 array of actions corresponding to the  ==
C==           transitions in the table STABLE. If no action is ==
C==           desired upon a particular transition (the usual  ==
C==           situation) then a zero value should appear in the==
C==           corresponding position in the ACTION table.      ==
C==           Actions to be taken should be indexed by positive==
C==           ODD numbers in the ACTION table (i.e. 1,3,5,7...).=
C==                                                            ==
C==   RESULT: REAL*8 number returned as the converted value of ==
C==           the input numeric string INPUT.                  ==
C==                                                            ==
C==   ALGORITHM USED BY "SCAN":                                ==
C==   --------- ---- -- ------                                 ==
C==                                                            ==
C==   1) ILAST  <-- IFIRST - 1                                 ==
C==                                                            ==
C==   2) ISTATE <-- 1                                          ==
C==                                                            ==
C==   3) IF "ISTATE" IS AN OUTPUT STATE THEN RETURN.           ==
C==      IF "ISTATE" IS AN ACTION STATE THEN PERFORM THE ACTION.=
C==                                                            ==
C==   4) ILAST  <-- ILAST + 1                                  ==
C==                                                            ==
C==   5) ICLASS <-- CLASS("INPUT(ILAST)")                      ==
C==                                                            ==
C==   6) IF ACTION(ISTATE,ICLASS)  --> INDEX  --> do action    ==
C==                                                            ==
C==   7) ISTATE <-- STABLE(ISTATE,ICLASS)                      ==
C==                                                            ==
C==   8) GO TO 3).                                             ==
C==                                                            ==
C================================================================
C================================================================
      IMPLICIT REAL*8 (A-H,O-Z)
      REAL*8 RESULT,DIV
      INTEGER*2 STABLE(NSTATE,1),ACTION(NSTATE,1),CTABLE(128)
      INTEGER ICHAR/0/,EXP,SIGN,EXPSGN,IDIG
      LOGICAL EXPRD
      LOGICAL*1 LCHAR(4),INPUT(1)
      EQUIVALENCE (ICHAR,LCHAR(1))
C================================================================
C== Initialize the various accumulators needed for I/O conversion
C================================================================
      RESULT= 0.0D0
      REAL= 0.0D0
      DIV= 1.0D0
      EXP= 0
      SIGN= 1
      EXPSGN= 1
      EXPRD= .FALSE.
C================================================================
C== Initialize end of field pointer ILAST and state flag ISTATE==
C================================================================
      ILAST=IFIRST-1
      ISTATE=1
C================================================================
C== Main loop: check the current state and if this is a        ==
C== transition state (i.e. the current field is not yet finished)
C== then proceed with the next input character; else end the   ==
C== field and return.                                          ==
C== If this is an output state, then the conversion of the     ==
C== number is complete and so the final evaluation is done.    ==
C================================================================
   10 IF(ISTATE.LT.0) THEN    !Output state: conversion completed 
          IF(ISTATE .EQ. -1) ILAST= ILAST-1
          IF(EXPRD) THEN
              RESULT= FLOAT(SIGN)*REAL*10.D0**(EXPSGN*EXP)
          ELSE
              RESULT= FLOAT(SIGN)*REAL
          ENDIF
          RETURN
      ELSE IF(ISTATE.EQ.0) THEN     !Error in converting number
          RETURN 1
      ENDIF
C================================================================
C== If 1 <= ISTATE < NSTATE, then ISTATE is a transition state,==
C== and so the scan pointer ILAST is incremented by 1.         ==
C================================================================
      ILAST= ILAST+1
C================================================================
C== If we've exceeded the length of the input line , i.e.      ==
C== ILAST > NINPUT, then ICLASS=1 (EOL), otherwise deduce the  ==
C== class of the current input character from the CTABLE array.==
C================================================================
      IF(ILAST.GT.NINPUT) THEN
          ICLASS=1
      ELSE
#ifdef BEND
          LCHAR(4)= INPUT(ILAST)
#else
          LCHAR(1)= INPUT(ILAST)
#endif
          ICLASS= CTABLE(ICHAR+1)
      ENDIF
C================================================================
C== Use the action table to flag necessary arithmetic actions  ==
C== during the real number conversion.                         ==
C== Note that the even integer values of ACTION (in particular ==
C== the value 0) have a FALSE logical value, while the odds are==
C== TRUE. Therefore, actions corresponding to certain table    ==
C== entries should be entered as 1,3,5... in the ACTION array, ==
C== while a 0 should be entered if no action is desired.       ==
C================================================================
CC      IF(.NOT.ACTION(ISTATE,ICLASS)) GOTO 50
      IF(ACTION(ISTATE,ICLASS)/2*2 .EQ. ACTION(ISTATE,ICLASS)) GOTO 50
      INDEX= (ACTION(ISTATE,ICLASS) + 1)/2
      IDIG= ICHAR - 48
      GOTO (100,200,300,400,500),INDEX
C================================================================
C== The appropriate actions necessary for the I/O conversion of==
C== the real number follow.                                    ==
C================================================================
  100 REAL= REAL*10.0D0 + FLOAT(IDIG)
      GOTO 50
  200 IF(ICHAR.EQ.45) SIGN=-1    !CHANGE SIGN IF "-"(ASCII 45)FOUND
      GOTO 50
  300 DIV= DIV*0.1D0
      REAL= REAL + FLOAT(IDIG)*DIV
      GOTO 50
  400 EXP= EXP*10 + IDIG
      EXPRD= .TRUE.
      GOTO 50
  500 IF(ICHAR.EQ.45) EXPSGN=-1    !CHANGE SIGN IF "-"
      EXPRD= .TRUE.
      GOTO 50
C================================================================
C== Use the transition table STABLE to evaluate the new state  ==
C== given the present state ISTATE and class ICLASS.           ==
C================================================================
   50 ISTATE=STABLE(ISTATE,ICLASS)
      GO TO 10
      END
      SUBROUTINE CHREAL_TABLE(CTABLE)
C================================================================
C================================================================
C==                                                            ==
C== CHREAL_CTABLE: sets up the INTEGER*2 class table CTABLE(128)=
C==    for use with the CHREAL routine to convert numeric      ==
C==    to REAL*8 (or REAL*4) values. This is performed using   ==
C==    the lexical scanner CHREAL_CHIP with appropriate actions==
C==    performed during the scan to achieve the actual         ==
C==    conversion.                                             ==
C==                                                            ==
C== INPUT PARAMETERS: NONE                                     ==
C==                                                            ==
C== OUTPUT PARAMETERS: CTABLE(128) (I*2)                       ==
C==                                                            ==
C== CTABLE IS RETURNED WITH THE FOLLOWING CLASS VALUES:        ==
C==                                                            ==
C==       CLASS       CHARACTERS                               ==
C==                                                            ==
C==          1      END OF LINE (off end of input record)      ==
C==          2      invalid characters                         ==
C==          3      BLANK                                      ==
C==          4      D,E,d,e                                    ==
C==          5      0-9                                        ==
C==          6      .                                          ==
C==          7      +-                                         ==
C==          8      , TAB (non-blank delimiters)               ==
C==                                                            ==
C================================================================
C================================================================
      INTEGER*2 CTABLE(128)
      CHARACTER*10 CHARR
C================================================================
C== Initialize the CTABLE array to CLASS 2 which corresponds to==
c== any arbitrary character not included in the other classes. ==
C================================================================
      DO 20 I=1,128
      CTABLE(I)=2
   20 CONTINUE
C================================================================
C== CLASS 3: BLANK                                             ==
C================================================================
      CHARR=' '
      CTABLE(ICHAR(CHARR(1:1))+1)=3
C================================================================
C== CLASS 4: D,E,d,e                                           ==
C================================================================
      CHARR='DEde'
      DO 40 I=1,4
      CTABLE(ICHAR(CHARR(I:I))+1)=4
   40 CONTINUE
C================================================================
C== CLASS 5: 0-9                                               ==
C================================================================
      CHARR='0123456789'
      DO 50 I=1,10
      CTABLE(ICHAR(CHARR(I:I))+1)=5
   50 CONTINUE
C================================================================
C== CLASS 6: .                                                 ==
C================================================================
      CHARR='.'
      CTABLE(ICHAR(CHARR(1:1))+1)=6
C================================================================
C== CLASS 7: +-                                                ==
C================================================================
      CHARR='+-'
      DO 70 I=1,2
      CTABLE(ICHAR(CHARR(I:I))+1)=7
   70 CONTINUE
C================================================================
C== CLASS 8: , TAB (non-blank delimiters)                      ==
C================================================================
      CTABLE(9+1)= 8             !TAB is X'09'
      CHARR= ','
      CTABLE(ICHAR(CHARR(1:1))+1)= 8
      RETURN
      END
