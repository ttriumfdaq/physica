      FUNCTION LENSIG(STRING)
C
C  Returns the significant length of a string.
C  Every character is significant with the 
C  exception of:
C            blank     (32)
C            tab       (9)  <----- No since MAR/28/85 !!!!!
C            null      (0)
C  
C  reqd. routines - NONE
C  modified by J.Chuma Feb 13,1990 to test for LENSIG = 0
C
      CHARACTER*(*) STRING
      LENSIG = LEN(STRING)
      I = ICHAR(STRING(LENSIG:LENSIG))
      DO WHILE (I.EQ.32 .OR. I.EQ.0)
         LENSIG = LENSIG - 1
         IF( LENSIG .EQ. 0 )RETURN
         I = ICHAR(STRING(LENSIG:LENSIG))
      END DO
      RETURN
      END      
