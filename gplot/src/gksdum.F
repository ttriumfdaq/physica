      SUBROUTINE GKSDUM
C======================================================================C
C  GKS dummy entry points.                                             C
C  This module should be removed from the library at sites where       C
C  the GKS Metafile is wanted as an output device.  Once this module   C
C  is removed, applications can be linked with a local GKS library     C
C  and GKS Metafiles can be produced.                                  C
C======================================================================C

      ENTRY GOPKS
      WRITE(*,*)'GKS library is not installed'
      RETURN

      ENTRY GOPWK
      ENTRY GACWK
      ENTRY GDAWK
      ENTRY GCLWK
      ENTRY GUWK
      ENTRY GCLRWK
      ENTRY GSWN
      ENTRY GSELNT
      ENTRY GCLKS
      ENTRY GPL
      ENTRY GPM
      ENTRY GSPLCI
      ENTRY GSPMCI
      ENTRY GSMK

      RETURN
      END      
