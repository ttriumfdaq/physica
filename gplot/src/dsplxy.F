      SUBROUTINE DSPLXY(S,X,Y,N,XX,YY,M,W1,W2,W3,W4,*)
C
C   This routine interpolates a curve through an ordered set of points.
C   It first parameterizes the points in terms of normalized arclength.
C   Normalized length in x is real length divided by the range of x, ie.
C   the maximum x value of input points minus the minimum.  Normalized
C   length in y is real length divided by the range of y, ie, the maximum
C   y value of input points minus the minimum.  The arclength at an input
C   point is approximated by the sum of the lengths of straight lines
C   connecting all points up to that input point.  A spline with tension
C   curve is computed for x versus arclength and y versus arclength.  For
C   equally spaced arclength, x and y values are interpolated separatly and
C   then combined to form output points.
C
C  input
C     S: is the normalized tension factor. It corresponds to the curviness
C        desired.  It is nonzero and its sign is ignored.  If S is close to
C        zero (eg. 0.001), then each interpolated function - x verses
C        arclength and y versus arclength - are almost cubic splines and the
C        resulting plot is quite curvy.  If sigma is large, eg. 100, then the
C        interpolation between points is almost linear.  A standard value for
C        sigma is 1.0.
C     X: array of the x values of the ordered input points (length N)
C     Y: array of the y values of the ordered input points (length N)
C     N: is the dimension of X and Y
C     M: is the number of output points
C        (approximately spaced by equal arc-length)
C    W1: work array (length N)
C    W2: work array (length M)
C    W3: work array (length N)
C    W4: work array (length N)
C
C  output
C    XX: array of the x values of the interpolated points (length M)
C    YY: array of the y values of the interpolated points (length M)
C
C  Originally written by CJ Kost, July 29, 1980 (@SIN)
C  Extensively modified by JL Chuma, March 29, 1994
C
      IMPLICIT NONE

      INTEGER*4 N, M
      REAL*8    S, X(1), Y(1), XX(1), YY(1), W1(1), W2(1), W3(1), W4(1)

C  local variables

      REAL*8    XMIN, XMAX, YMIN, YMAX
      INTEGER*4 I, J
CCC
      XMIN = X(1)   !  Calculate length normalizing factors
      XMAX = X(1)
      YMIN = Y(1)
      YMAX = Y(1)
      DO J = 2, N
        IF( X(J) .LT. XMIN )XMIN = X(J)
        IF( X(J) .GT. XMAX )XMAX = X(J)
        IF( Y(J) .LT. YMIN )YMIN = Y(J)
        IF( Y(J) .GT. YMAX )YMAX = Y(J)
      END DO
      W1(1) = 0.0D0  ! Calculate arclength parameterization for input points
      DO I = 2, N
        W1(I) = W1(I-1) + SQRT(
     &    ((X(I)-X(I-1))/(XMAX-XMIN))**2
     &   +((Y(I)-Y(I-1))/(YMAX-YMIN))**2 )
      END DO
      DO I = 1, M    ! Calculate arclengths of output points
        W2(I) = FLOAT(I-1)*W1(N)/FLOAT(M-1)
      END DO

C  perform spline with tension interpolation for
C  x vs arclength and y vs arclength

      CALL DSPL11(S,W1,X,N,W2,XX,M,W3,W4,*999,*999,*999)
      CALL DSPL11(S,W1,Y,N,W2,YY,M,W3,W4,*999,*999,*999)
      RETURN
  999 RETURN 1
      END
