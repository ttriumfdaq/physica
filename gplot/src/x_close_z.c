/* xvst_close_zoom:  UNMAPS THE ZOOM WINDOW           FWJ 23-Sept-91 */

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <X11/Xlib.h>

#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define xvst_close_zoom_ xvst_close_zoom
#endif

/* GLOBAL VARIABLES */

Display *dpy;
Screen *screen;
Window z_window, g_window;
GC fgc;
Bool zoom, zoom_box;
Bool enable_action;
int corner1_x, corner1_y, corner2_x, corner2_y;

/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;

void xvst_close_zoom_()
{
   enable_action = False;
   XFlush(dpy);

   XUnmapWindow(dpy,z_window);

/* Erase old zoom box */
   if (zoom_box) xvst_draw_zb_(dpy,screen,g_window,fgc,corner1_x,corner1_y,
                               corner2_x, corner2_y);
   XFlush(dpy);

   zoom = False;
   zoom_box = False;
   xvst_zoom_window_.zopen = False;
   xvst_zoom_window_.zact = False;

   enable_action = True;
}
