 /* bring in the gd library functions */
#include "gd.h"
 
 /* bring in the standard I/O so we can output the png to a file */
#include <stdio.h>
 
 /* GLOBAL VARIABLES */
 
 /* declare the image */
 gdImagePtr gd_image;
 
 /* declare output file */
 FILE *gd_pngout;
 
 /* declare colour indices */
 int gd_black;
 int gd_white;
 int gd_red;
 int gd_blue;
 int gd_green;
 int gd_cyan;
 int gd_magenta;
 int gd_yellow;
 int gd_orange;
 int gd_orangered;
 int gd_greencyan;
 int gd_bluecyan;
 
 void gd_setup_( float *fx, float *fy )
  {
    int x = (int)(*fx);
    int y = (int)(*fy);
    int white;
    
    gd_image = gdImageCreateTrueColor( x, y );
    white = gdTrueColor( 255, 255, 255 );
    gdImageFilledRectangle( gd_image, 0, 0, x, y, white );
    /*
       allocate the colours
       the first colour in a new image, will be the background colour
    */
    gd_black = gdImageColorAllocate( gd_image, 0, 0, 0 );
    gd_white = gdImageColorAllocate( gd_image, 255, 255, 255 );
    gd_red = gdImageColorAllocate( gd_image, 255, 0, 0 );
    gd_blue = gdImageColorAllocate( gd_image, 0, 0, 255 );
    gd_green = gdImageColorAllocate( gd_image, 0, 255, 0 );
    gd_cyan = gdImageColorAllocate( gd_image, 102, 255, 255 );
    gd_magenta = gdImageColorAllocate( gd_image, 238, 130, 238 );
    gd_yellow = gdImageColorAllocate( gd_image, 255, 255, 0 );
    gd_orange = gdImageColorAllocate( gd_image, 255, 165, 0 );
    gd_orangered = gdImageColorAllocate( gd_image, 255, 51, 0 );
    gd_greencyan = gdImageColorAllocate( gd_image, 102, 200, 153 );
    gd_bluecyan = gdImageColorAllocate( gd_image, 0, 165, 255 );
  }

 void gd_getdimensions_( char *file, int *x, int *y )
  {
    gdImagePtr gdip;
    FILE *in;
    /*printf( "file: %s\n", file );*/
    in = fopen( file, "rb" );
    if( !in )
    {
      fprintf( stderr, "unable to open png file %s\n", file );
      *x = 0;
      *y = 0;
      return;
    }
    gdip = gdImageCreateFromPng( in );
    fclose( in );
    *x = gdImageSX( gdip );
    *y = gdImageSY( gdip );

    /*printf( "x = %d, y = %d\n", *x, *y );*/
    /*printf( "colorsTotal = %d\n", gdImageColorsTotal(gdip) );*/
    
    gdImageDestroy( gdip );
  }

 void gd_copyfile_( char *file, int *x, int *y )
  {
    gdImagePtr gdip;
    int red, blue, green, dum;
    FILE *in;
    int ix, iy;
    
    in = fopen( file, "rb" );
    gdip = gdImageCreateFromPng( in );
    fclose( in );
    if( gd_image != 0 )gdImageDestroy( gd_image );
    
    ix = *x;
    iy = *y;
    
    gd_image = gdImageCreateTrueColor( ix, iy );
    
    gdImageCopy( gd_image, gdip, 0,0, 0,0, *x, *y );
    gdImageDestroy( gdip );
    
    gd_black = gdImageColorResolve( gd_image, 0,0,0 );
    gd_white = gdImageColorResolve( gd_image, 255,255,255 );
    gd_red = gdImageColorResolve( gd_image, 255,0,0 );
    gd_blue = gdImageColorResolve( gd_image, 0,0,255 );
    gd_green = gdImageColorResolve( gd_image, 0,255,0 );
    gd_cyan = gdImageColorResolve( gd_image, 102,255,255 );
    gd_magenta = gdImageColorResolve( gd_image, 238,130,238 );
    gd_yellow = gdImageColorResolve( gd_image, 255,255,0 );
    gd_orange = gdImageColorResolve( gd_image, 255,165,0 );
    gd_orangered = gdImageColorResolve( gd_image, 255,51,0 );
    gd_greencyan = gdImageColorResolve( gd_image, 102,200,153 );
    gd_bluecyan = gdImageColorResolve( gd_image, 0,165,255 );
  }
 
 void gd_openfile_( char *file )
  {
    gd_pngout = fopen( file, "wb" );
  }

 void gd_writefile_()
  {
    gdImagePng( gd_image, gd_pngout );
    fclose( gd_pngout );
  }
 
 void gd_free_()
  {
    if( gd_image != 0 )gdImageDestroy( gd_image );
  }

 void gd_draw_line_( int *ix1, int *iy1, int *ix2, int *iy2, int *icolour )
  {
    int x1, y1, x2, y2, colour;
    x1 = *ix1;
    x2 = *ix2;
    y1 = *iy1;
    y2 = *iy2;
    colour = *icolour;
    /*printf( "color = %d\n", colour );*/
    switch( colour )
    {
     case 0:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_white );
       break;
     case 1:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_black );
       break;
     case 2:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_red );
       break;
     case 4:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_blue );
       break;
     case 7:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_magenta );
       break;
     case 3:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_green );
       break;
     case 5:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_yellow );
       break;
     case 6:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_cyan );
       break;
     case 8:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_orange );
       break;
     case 9:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_orangered );
       break;
     case 10:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_greencyan );
       break;
     case 11:
       gdImageLine( gd_image, x1, y1, x2, y2, gd_bluecyan );
       break;
    }
  }
 
 /* end of file */
