      SUBROUTINE END_PLOT
C======================================================================C
C  This routine is OBSOLETE.  Replace with FLUSH_PLOT and CLEAR_PLOT.  C
C==                                                                    C
C==   END_PLOT: ends the plot by flushing out the remainder of         C
C==             the plot buffer. The next call to PLOT_R will          C
C==             then clear the screen and initialize the plot          C
C==             buffer, before plotting.                               C
C==                                                                    C
C==   Written by Arthur Haynes, TRIUMF U.B.C., July 6, 1981.           C
C==                                                                    C
C==   Modified by Alan Carruthers, March 15, 1983.                     C
C==     -- CLEAR1, CLEAR2 and CLEARH flags introduced. They are        C
C==        passed in a common block to CLEAR_PLOT.                     C
C==        END_PLOT sets these flags to .FALSE. to indicate that       C
C==        monitors 1 and 2 and hardcopy bitmap have not been          C
C==        cleared since plot terminated.                              C
C     Modified by F.Jones 02-MAY-91: VS11 removed                      C
C                                                                      C
C======================================================================C
      COMMON /PLOTMONITOR/ IMONITOR,IOUTM
      COMMON /PLOTMONITOR2/ IMONITOR2,IOUTM2
      COMMON /PLOT_CLEAR/ CLEAR
      LOGICAL CLEAR
      COMMON /PLOT_CLEAR_A/CLEAR1, CLEAR2, CLEARH, CLEARUNC
      LOGICAL CLEAR1, CLEAR2, CLEARH, CLEARUNC

      IF(CLEAR)CALL CLEAR_PLOT
      CLEAR=.TRUE.
      CLEAR1 = .FALSE.
      CLEAR2 = .FALSE.
      CLEARH = .FALSE.
      IF(IMONITOR.EQ.0)GO TO 100
      IF(IMONITOR.EQ.3)RETURN
      IF(IMONITOR.EQ.4)GO TO 100
C=================================================================
C==   IMONITOR =1,2 or 6 Monitor is a VT640 or Tektronix 4010/12,=
C==             or CIT-467                                       =
C=================================================================
      CALL PACK_BUFFER(31)  !US: Alpha mode control code.
      CALL PRINT_BUFFER

100   IF(IMONITOR2.EQ.0)RETURN
      IF(IMONITOR2.EQ.3)RETURN
      IF(IMONITOR2.EQ.4)RETURN

C=================================================================
C==   IMONITOR2=1,2 or 6 Monitor is a VT640 or Tektronix 4010/12,=
C==             or CIT-467                                       =
C=================================================================
      CALL PACK_BUFFER2(31)  !US: Alpha mode control code.
      CALL PRINT_BUFFER2
      RETURN
      END
