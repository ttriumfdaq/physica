      SUBROUTINE PACK_BUFFER(ICHAR)
C======================================================================C
C                                                                      C
C     PACK_BUFFER: puts the character represented by the integer       C
C                  (decimal) value "ICHAR" into the plot buffer.       C
C                                                                      C
C     Written by Arthur Haynes, TRIUMF U.B.C., July 6, 1981.           C
C                                                                      C
C     Input  Parameters: ICHAR (I*4).                                  C
C                                                                      C
C======================================================================C
      BYTE BUFFER(78)  ! Modified by J.Chuma, 19Mar97 for g77
C                         was LOGICAL*1
      COMMON /PLOT_BUFFER/ INDEX, IMODE, BUFFER
C
      INDEX = INDEX+1
      IF( INDEX .LE. 78 )BUFFER(INDEX) = ICHAR
      RETURN
      END
