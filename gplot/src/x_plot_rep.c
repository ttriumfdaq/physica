/* Modified July 1991 by F.Jones: zoom window code revised    */
/* and expanded.  Support added for graphics input, odometer, */
/* long crosshairs, and control by applications.              */

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

#include <stdio.h>
#include <X11/Xlib.h>

/*      GLOBAL  VARIABLES      */

Display *dpy;
Window g_window, z_window;
GC ggc, zgc;
XPoint glines[500], gpoints[500];
XPoint zlines[500], zpoints[500];
unsigned long gcfore, gcback;
unsigned long icmap[12];
int gheight, gwidth;
int nlines, npoints;
Bool replay_zoom,zoom;
int offset_x, offset_y;
float ratio;
float scalx,scaly;

#define Max(a,b)  ((a) > (b) ? (a) : (b))
#define Min(a,b)  ((a) < (b) ? (a) : (b))

/*******************************************************************
*  xvst_plot_clear_replay
*
*   REPLAYS THE GRAPHICS FROM DYNAMIC MEMORY BUT IF THE ALLOC
*   MEMORY CALLED FAILED, THE PREVIOUS BLOCK OF VECTORS MAY
*   HAVE BEEN LOST.  NOTE THAT ALL THESE ROUTINES BELOW ARE 
*   IDENTICAL TO THE ROUTINES IN XVST_PLOT EXCEPT THAT THEY 
*   REPLAY THE VECTORS INSTEAD OF STORING THE VECTORS FROM 
*   DYNAMIC MEMORY.
*
********************************************************************/

void xvst_plot_clear_replay_()
{
   if ( dpy != NULL ) {
      if (replay_zoom)
         XClearWindow(dpy, z_window);
      else {
         XClearWindow(dpy, g_window);
         if (zoom) XClearWindow(dpy, z_window);
      }
      XFlush( dpy );
   }
   else
      xvst_setup_(0);

   nlines = 0;
   npoints = 0;
}


/***********************************************************************
*  xvst_plot_replay_
*  (IPEN = 3 EXECUTES MOVE) (IPEN = 2 DRAW) (IPEN = 20 PLOT POINT)
************************************************************************/
void xvst_plot_replay_( x, y, ipen )
   int *ipen;
   float *x, *y;
{
   int k;

   if ( nlines >= 299 ) xvst_flush_lines_();
   if ( npoints >= 299 ) xvst_flush_points_();

   if ( *ipen == 2 ) {
      if (npoints >= 1) xvst_flush_points_();
      nlines = nlines + 1;
      glines[nlines-1].x = (*x)*gwidth + 0.5;
      glines[nlines-1].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zlines[nlines-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zlines[nlines-1].y = ((1.-(*y)/0.75)*gheight - offset_y)/scaly + 0.5;
      }
   }
   else if ( *ipen == 3 ) {
      if ( npoints >= 1 ) xvst_flush_points_();
      if ( nlines >= 2 ) xvst_flush_lines_();
      nlines = 1;
      glines[ nlines -1 ].x = (*x)*gwidth + 0.5;
      glines[ nlines -1 ].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zlines[nlines-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zlines[nlines-1].y = ((1.-(*y)/0.75)*gheight - offset_y)/scaly +0.5;
      }
   }
   else if ( *ipen == 20 ) {
      if ( nlines >= 2 ) xvst_flush_lines_();
      npoints = npoints + 1;
      gpoints[ npoints -1 ].x = (*x)*gwidth + 0.5;
      gpoints[ npoints -1 ].y = (1-(*y)/0.75)*gheight + 0.5;
      if (zoom) {
         zpoints[npoints-1].x = ((*x)*gwidth - offset_x)/scalx + 0.5;
         zpoints[npoints-1].y = ((1-(*y)/0.75)*gheight - offset_y)/scaly + 0.5;
      }
   }
}


void xvst_plot_mode_replay_( imode )
   int imode;
{
   if ( imode == 0 ) {
      XSetForeground( dpy, ggc, gcfore );
      XSetForeground( dpy, zgc, gcfore );
/*    XSetForeground( dpy, pgc, gcfore ); */
   }
   else if ( imode == 1 ) {
      XSetForeground( dpy, ggc, gcback );
      XSetForeground( dpy, zgc, gcback );
/*    XSetForeground( dpy, pgc, gcback ); */
   }
}


xvst_plot_colour_replay_(ic)
   int *ic;
{
   int ic1;

   ic1 = Min( Max( 1, *ic ), 11 );

   XSetForeground( dpy, ggc, icmap[ic1] );
   XSetForeground( dpy, zgc, icmap[ic1] );
/* XSetForeground( dpy, pgc, icmap[ic1] );  */
   gcfore = icmap[ic1];
}
