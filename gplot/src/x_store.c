/***********************************************************************
*  xvst_store_                                     F.W. Jones, TRIUMF  *
*                                                                      *
*  Allocates replay memory as needed and if icode=1 stores the values  *
*  icode,a,b,c (a,b,c are x,y,ipen).  If icode>1, stores the values    *
*  icode and a only.                                                   *
*                                                                      *
*  Memory is allocated in banks NWORDS long.  Pointers to the banks    *
*  are stored in the array "bank", the maximum number of               *
*  banks being set by parameter NBMAX.                                 *
*                                                                      *
*  Xvst_store_reset *must* be called prior to using this routine.      *
*  It deallocates any existing replay memory and initializes for the   *
*  next plot to be stored.                                             *
*                                                                      *
***********************************************************************/

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

/* Modified 27-SEP-94 by FWJ: increased number of bank pointers */
/* from 150 to 1000 */

#include <stdio.h>
#include <X11/Xlib.h>

#ifdef VMS
#define xvst_rep_ xvst_rep
#endif

#define NBMAX 1000
#define NWORDS 16000

/* GLOBAL VARIABLES */

int *bank[NBMAX+1];
int nstore, nbank, ilocal;

void xvst_allocate();

struct { int istore; } xvst_rep_;


void xvst_store_(icode,a,b,c)
   int *icode,*a,*b,*c;
{
   Bool istat;

   ilocal++;
   if (ilocal == NWORDS) {
      xvst_allocate(&istat);
      if (!istat) return;
   }
   *(bank[nbank]+ilocal) = *icode;

   ilocal++;
   if (ilocal == NWORDS) {
      xvst_allocate(&istat);
      if (!istat) return;
   }
   *(bank[nbank]+ilocal) = *a;

   if (*icode > 1) {
      nstore = nstore+2;
      return;
   }

   ilocal++;
   if (ilocal == NWORDS) {
      xvst_allocate(&istat);
      if (!istat) return;
   }
   *(bank[nbank]+ilocal) = *b;

   ilocal++;
   if (ilocal == NWORDS) {
      xvst_allocate(&istat);
      if (!istat) return;
   }
   *(bank[nbank]+ilocal) = *c;

   nstore = nstore+4;
   return;
}


void xvst_store_reset_()
{
   int i;

   for (i=1; i<=nbank; i++) free(bank[i]);

   nbank = 0;
   nstore = 0;
   xvst_rep_.istore = 1;
   ilocal = NWORDS-1;

   return;
}


void xvst_allocate(istat)
   Bool *istat;
{
   nbank++;
   if (nbank > NBMAX) {
      printf("Unable to allocate further replay memory\n");
      printf("Exceeded maximum number of banks: %d\n",NBMAX);
      nbank--;
      xvst_rep_.istore = 0;
      *istat = False;
      return;
   }

   bank[nbank] = (int *)malloc(NWORDS*4);

   if (bank[nbank]==0) {
      printf("Unable to allocate replay memory bank %d\n",nbank);
      xvst_rep_.istore = 0;
      *istat = False;
      return;
   }

   ilocal = 0;
   *istat = True;
   return;
}
