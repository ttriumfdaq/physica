      SUBROUTINE PIXEL_EXTENT(XMIN,XMAX,YMIN,YMAX
     &                       ,XINC, YINC, NXMIN, NXMAX, NYMIN, NYMAX
     &                       ,XINCB,YINCB,NXMINB,NXMAXB,NYMINB,NYMAXB)
C
C   Input:  XMIN, XMAX, YMIN, YMAX          the boundaries of the
C                                           rectangle in world coord's
C   Output: XINC, YINC                      the distance between pixels
C                                           in terminal units
C           NXMIN, NXMAX, NYMIN, NYMAX      the terminal pixel numbers
C                                           for XMIN, XMAX, YMIN, YMAX
C           XINCB, YINCB                    the distance between pixels
C                                           in bitmap units
C           NXMINB, NXMAXB, NYMINB, NYMAXB  the bitmap pixel numbers for
C                                           XMIN, XMAX, YMIN and YMAX
C  Modified 19-AUG-93 by FWJ: UIS support removed

      COMMON /PLOTMONITOR/     IMONITOR,  IOUTM
      COMMON /PLOTMONITOR2/    IMONITOR2,  IOUTM2
      COMMON /MONITORRANGE/    XMINMP, XMAXMP, YMINMP, YMAXMP
     &                         ,XMINM,  XMAXM,  YMINM,  YMAXM
      COMMON /HARDCOPYRANGE/   XMINHP, XMAXHP, YMINHP, YMAXHP, IORIENTH
      COMMON /HARDCOPYRANGE2/  XMINH2, XMAXH2, YMINH2, YMAXH2
C
C  height and width in pixels of X Window display
C
      INTEGER XVST_HEIGHT, XVST_WIDTH
      COMMON /XVST_VAR/ XVST_HEIGHT, XVST_WIDTH
C
C   Determine total width & height in bitmap dots
C
      IF( IORIENTH .EQ. +1 )THEN
        DXB = ABS(XMAXH2 - XMINH2)
        DYB = ABS(YMAXH2 - YMINH2)
      ELSE
        DYB = ABS(XMAXH2 - XMINH2)
        DXB = ABS(YMAXH2 - YMINH2)
      END IF
      DXB = MAX(1.,DXB)
      DYB = MAX(1.,DYB)
C
C  Distance between bitmap dots in world coordinate units
C
      XINCB = (XMAXHP - XMINHP)/DXB  ! horizontal distance
      YINCB = (YMAXHP - YMINHP)/DYB  ! vertical distance
C
      NXMINB = (XMIN-XMINHP)/XINCB+1 ! bitmap dot number for XMIN
      NYMINB = (YMIN-YMINHP)/YINCB+1 ! bitmap dot number for YMIN
      NXMAXB = (XMAX-XMINHP)/XINCB+1 ! bitmap dot number for XMAX
      NYMAXB = (YMAX-YMINHP)/YINCB+1 ! bitmap dot number for YMAX
C
C   Determine the width, height of the terminal monitor in pixels
C
      IF( IMONITOR .EQ. 18 )THEN          ! X Window display
C        DX = XVST_WIDTH * ABS(XMAXM-XMINM)/1.0 + 0.9999
C        DY = XVST_HEIGHT * ABS(YMAXM-YMINM)/0.75 + 0.9999
        DX = XVST_WIDTH * ABS(XMAXM-XMINM)/1.0 
        DY = XVST_HEIGHT * ABS(YMAXM-YMINM)/0.75 
      ELSE                                ! all other terminal monitors
        DX = ABS(XMAXM - XMINM)
        DY = ABS(YMAXM - YMINM)
      END IF
      DX = MAX(1.,DX)
      DY = MAX(1.,DY)
C
C  Distance between pixels in world coordinate units
C
      XINC = (XMAXHP - XMINHP)/DX   ! horizontal distance
      YINC = (YMAXHP - YMINHP)/DY   ! vertical distance
C
      NXMIN = (XMIN-XMINHP)/XINC+1  ! pixel number for XMIN
      NYMIN = (YMIN-YMINHP)/YINC+1  ! pixel number for YMIN
      NXMAX = (XMAX-XMINHP)/XINC+1  ! pixel number for XMAX
      NYMAX = (YMAX-YMINHP)/YINC+1  ! pixel number for YMAX
      RETURN
      END
