      SUBROUTINE SCALE1(XMIN,XMAX,N,XMINP,XMAXP,XINC)
C  
C     reqd. routines - NONE
C======================================================================
C==                                                                  ==
C==   SCALE1:  Axis scaling routine.  Given XMIN, XMAX, and N        ==
C==            SCALE1 finds a new range XMINP to XMAXP divisible     ==
C==            into approximately N linear intervals of size         ==
C==            XINC.  XMINP and XMAXP are integer multiples of       ==
C==            XINC.                                                 ==
C==                                                                  ==
C==   Entered by Alan Carruthers, TRIUMF U.B.C, June 18, 1982        ==
C==                                                                  ==
C==   Source:  Algorithm 463                                         ==
C==            Lewart, C. R.  1973.  Algorithms SCALE1, SCALE2,      ==
C==              SCALE3 for determination of scales on computer      ==
C==              generated plots.  Comm. ACM 16: 639-640             ==
C==                                                                  ==
C==                                                                  ==
C==   Altered by Joe Chuma, February 13, 1984 to allow scaling       ==
C==        when XMIN = XMAX, i.e. if XMIN = 0  then scale from -1    ==
C==        to +1, if not = 0 then scale from 0 to twice value        ==
C==                                                                  ==
C==   Input  Parameters:  XMIN, XMAX,   (REAL*4)                     ==
C==                       N             (INTEGER*4)                  ==
C==                                                                  ==
C==   Output Parameters:  XMINP, XMAXP, XINC   (REAL*4)              ==
C==                                                                  ==
C==   Parameter Definitions:                                         ==
C==   ---------------------                                          ==
C==                                                                  ==
C==   XMIN   : Minimum of values that axis must encompass.           ==
C==                                                                  ==
C==   XMAX   : Maximum of values that axis must encompass.           ==
C==                                                                  ==
C==   N      : Requested number of axis intervals.                   ==
C==                                                                  ==
C==   XMINP  : Returned axis minimum.                                ==
C==                                                                  ==
C==   XMAXP  : Returned axis maximum.                                ==
C==                                                                  ==
C==   XINC   : Returned length of axis intervals.                    ==
C==                                                                  ==
C======================================================================
C==                                                                  ==
C======================================================================
C==   VINT is an array of acceptable values for XINC (times an       ==
C==   integer power of 10).                                          ==
C==   SOR is an array of geometric means of adjacent values of       ==
C==   VINT; SOR is used as break points to determine which value     ==
C==   of VINT to assign to XINC.                                     ==
C======================================================================
      DIMENSION VINT(4), SOR(3)
      DATA VINT /1.,2.,5.,10./
      DATA SOR /1.414214,3.162278,7.071068/
C======================================================================
C==   Check whether proper input supplied to SCALE1                  ==
C======================================================================
      IF(XMIN .LT. XMAX .AND. N .GT. 0)GOTO 10
      IF(N .LE. 0.)THEN
        WRITE(6,99999)
99999   FORMAT(' *** SCALE1 *** Requested number of intervals <= 0.')
        RETURN
      END IF
      IF(XMIN .GT. XMAX)THEN
        WRITE(6,999)
999     FORMAT(' *** SCALE1 *** Minimum > maximum')
        RETURN
      END IF
      IF(XMIN .EQ. XMAX)THEN
        IF(XMIN .EQ. 0.)THEN
          XMIN = -1.
          XMAX = +1.
        ELSE IF(XMIN .LT. 0.)THEN
          XMIN = 2.*XMIN
          XMAX = 0.
        ELSE
          XMIN = 0.
          XMAX = 2.*XMAX
        END IF
      END IF
C======================================================================
C==   DEL accounts for computer roundoff.  DEL should be greater     ==
C==   than the roundoff expected from a division and float           ==
C==   operation, it should be less than the minimum increment of     ==
C==   the plotting device used by the main program divided by        ==
C==   the plot size times the number of intervals N.                 ==
C======================================================================
 10   DEL = .00002
      FN = N
C======================================================================
C==   Find approximate interval size A                               ==
C======================================================================
      A = (XMAX - XMIN) / FN
      AL = ALOG10(A)
      NAL = AL
      IF(A .LT. 1.) NAL = NAL - 1
C======================================================================
C==   A is scaled into variable B between 1 and 10                   ==
C======================================================================
      B = A / (10.**NAL)
C======================================================================
C==   The closest permissible value for B is found                   ==
C======================================================================
      DO 20 I = 1,3
         IF(B .LT. SOR(I)) GOTO 30
 20   CONTINUE
      I = 4
C======================================================================
C==   The interval size is computed                                  ==
C======================================================================
 30   XINC = VINT(I) * 10. ** NAL
      FM1 = XMIN / XINC
      M1 = FM1
      IF(FM1 .LT. 0.) M1 = M1 - 1
      IF(ABS(FLOAT(M1)+1.-FM1) .LT. DEL) M1 = M1 + 1
C======================================================================
C==   The new minimum and maximum limits are found                   ==
C======================================================================
      XMINP = XINC * FLOAT(M1)
      FM2 = XMAX / XINC
      M2 = FM2 + 1.
      IF(FM2 .LT. (-1.)) M2 = M2 - 1
      IF(ABS(FM2+1.-FLOAT(M2)) .LT. DEL) M2 = M2 - 1
      XMAXP = XINC * FLOAT(M2)
C======================================================================
C==   Adjust limits to account for roundoff if necessary             ==
C======================================================================
      IF(XMINP .GT. XMIN) XMINP = XMIN
      IF(XMAXP .LT. XMAX) XMAXP = XMAX
      RETURN
      END
