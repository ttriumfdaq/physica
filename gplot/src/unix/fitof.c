/* Fortran interface to itof() (Ultrix only)      FWJ */

/* Note: itof does not appear to return zero on success as documented */
/* in the man pages, so status code istat cannot currently be used to */
/* diagnose an error. */

void fitof_(x, istat)
   float *x;
   int *istat;
{
#ifdef sgi
   printf("FITOF: itof() not available\n");
   return;
#else
#ifdef __osf__
   printf("FITOF: itof() not available\n");
   return;
#else
   *istat = itof(x);
#endif
#endif
}
