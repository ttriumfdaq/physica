/********************************************************************/
/*  read_key_trap                                              FWJ  */
/*  Establishes a signal handler for the CONTINUE signal.           */
/*  If the process is stopped with CTRL-Z and later resumed,        */
/*  the handler will reset the terminal i/o characteristics         */
/*  (altered by READ_KEY) to their normal values.                   */
/********************************************************************/

#include<signal.h>

void read_key_trap_()
{
  extern void read_key_reset_();

  (void) signal(SIGCONT,read_key_reset_);
}
