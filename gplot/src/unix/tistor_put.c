/* Output routine for atari displays on Ultrix.                 FWJ */
/* Replaces QIO output used on VMS.                                 */
/* Note: the fflush call is required to actually get the characters */
/*       written out, otherwise they are retained in the output     */
/*       buffer until the calling program exits.                    */

#include <stdio.h>

void
tistor_put_(bbuff,ipoint)
   char bbuff[72];
   int *ipoint;
{
   int i;
   for (i = 0; i < *ipoint; i++)
      putchar(bbuff[i]);
   fflush(stdout);
}
