      SUBROUTINE PASCEBC(STRIN,STROUT,LEN,*)
C
C     reqd. routines - NONE
C================================================================
C==  PASCEBC: TRANSLATES INPUT ASCII STRING INTO EBCDIC EQUIVALENT==
C             JUST FOR USE BY PSYM SO VT100 KEYBOARD CHARACTERS
C             COME OUT CORRECTLY IN TRIUMF.2 AND TSAN FONTS
C                          J. Chuma     July 19, 1985       
C==                                                            ==
C==  INPUT PARAMETERS: STRIN, LEN                              ==
C==    STRIN: INPUT ASCII STRING                               ==
C==    LEN  : LENGTH OF INPUT STRING (BYTES)                   ==
C==                                                            ==
C==  OUTPUT PARAMETERS: STROUT				       ==
C==    STROUT: EBCDIC EQUIVALENT OF ASCII INPUT STRING         ==
C==                                                            ==
C==  RETURN CODES:                                             ==
C==   RETURN 0: NORMAL RETURN, SUCCESSFUL TRANSLATION          ==
C==   RETURN 1: INVALID ASCII CHAR. (HEX > 127) IN "STRIN"     ==
C================================================================
      BYTE STRIN(1),STROUT(1),LCH(4),TAB(128)
      INTEGER NCH,LEN
      EQUIVALENCE (NCH,LCH)
C  Modified by J.Chuma, 23Apr97 for Absoft f77  changed X' to Z'
      DATA TAB/
     & Z'00',Z'01',Z'02',Z'03',Z'37',Z'2D',Z'2E',Z'2F',Z'16',Z'05',
     & Z'15',Z'0B',Z'0C',Z'0D',Z'0E',Z'0F',Z'10',Z'11',Z'12',Z'13',
     & Z'3C',Z'3D',Z'32',Z'26',Z'18',Z'19',Z'3F',Z'27',Z'1C',Z'1D',
     & Z'1E',Z'1F',Z'40',Z'5A',Z'7F',Z'7B',Z'5B',Z'6C',Z'50',Z'7D',
     & Z'4D',Z'5D',Z'5C',Z'4E',Z'6B',Z'60',Z'4B',Z'61',Z'F0',Z'F1',
     & Z'F2',Z'F3',Z'F4',Z'F5',Z'F6',Z'F7',Z'F8',Z'F9',Z'7A',Z'5E',
     & Z'4C',Z'7E',Z'6E',Z'6F',Z'7C',Z'C1',Z'C2',Z'C3',Z'C4',Z'C5',
     & Z'C6',Z'C7',Z'C8',Z'C9',Z'D1',Z'D2',Z'D3',Z'D4',Z'D5',Z'D6',
     & Z'D7',Z'D8',Z'D9',Z'E2',Z'E3',Z'E4',Z'E5',Z'E6',Z'E7',Z'E8',
     & Z'E9',Z'AD',Z'AC',Z'BD',Z'B1',Z'6D',Z'B3',Z'81',Z'82',Z'83',
     & Z'84',Z'85',Z'86',Z'87',Z'88',Z'89',Z'91',Z'92',Z'93',Z'94',
     & Z'95',Z'96',Z'97',Z'98',Z'99',Z'A2',Z'A3',Z'A4',Z'A5',Z'A6',
     & Z'A7',Z'A8',Z'A9',Z'8B',Z'4F',Z'9B',Z'67',Z'07'/
C
C    CHANGED Z5F --> Z67   ~
C            Z79 --> ZB3   `
C            ZAA --> ZB1   ^
C            ZBA --> ZAC   \           July 19, 1985  Chuma
C================================================================
C==  NOW TRANSLATE DATA FROM ASCII TO EBCDIC                   ==
C================================================================
      IF(LEN.LE.0) RETURN
      DO 10 I=1,LEN
      NCH=0
#ifdef BEND
      LCH(4)=STRIN(I)
#else
      LCH(1)=STRIN(I)
#endif
      IF(NCH.GT.127) RETURN 1
      STROUT(I)=TAB(NCH+1)
   10 CONTINUE
      RETURN
      END
