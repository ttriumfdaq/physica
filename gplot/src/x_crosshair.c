/* Modified July 1991 by F.Jones: zoom window code revised    */
/* and expanded.  Support added for graphics input, odometer, */
/* long crosshairs, and control by applications.              */

/* Modified 5-Dec-91 by FWJ: global event_mask now contains   */
/* the current event mask at all times.  Cursor creation      */
/* removed to xvst_setup.                                     */

/* Modified 27-Jan-92 by FWJ: this routine no longer changes  */
/* the keyboard input focus unless enabled by the Xdefaults   */
/* parameter TRIUMF.appSetFocus.                              */

/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

/* Modified 12-JAN-95 by FWJ: XSetInputFocus is only done if  */
/* input window is viewable.  (Avoids BadMatch error when     */
/* crosshair_r is called immediately after clear_plot).       */

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define cursor_preset_ cursor_preset
#define xvst_focus_ xvst_focus
#define xvst_crosshair_ xvst_crosshair
#endif

/*    GLOBAL VARIABLES   */

Display *dpy;
Screen *screen;
Window g_window, t_window, z_window;
GC fgc; 
Bool creadout,fsc,enable_action,button1,zoom,replay_zoom,zoom_box;
Bool fsc_exists;
int cx,cy,zcx,zcy;
int event_mask;
int gwidth,gheight,offset_x,offset_y;
int corner1_x,corner1_y,corner2_x,corner2_y;
int zwidth, zheight;
float scalx,scaly;
Cursor xhaircursor, busycursor;

/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;

struct { Bool cset; } cursor_preset_;  /*  COMMON BLOCK  */

/* COMMON BLOCK - AUTOFOCUS SWITCH */
struct { Bool appsetfocus; } xvst_focus_;

/**********************************************************************
*
*    THIS IS THE MAIN EVENT HANDLING LOOP WHEN THE PROGRAM IS IN
*    TEXT MODE.  THE PROGRAM WILL REMAIN IN AN INFINITE LOOP
*    UNTIL A RECOGNIZED EVENT TO END THE LOOP (ie. key or button press)
*    IS RECEIVED.  NOTE THAT THIS LOOP HAS A SEPERATE MECHANISM FOR
*    HANDLING RESIZE EVENTS THAN THE TIMEOUT ROUTINE.  THIS ROUTINE
*    WILL RETURN THE KEY CODE AND THE LOCATION OF THE POINTER IN THE
*    WINDOW.
*
***********************************************************************/

void xvst_crosshair_(x,y,code,xl,yl)
   float *x,*y,*xl,*yl;
   int *code;
{
   static Bool ffirst = True;
   int icode;
   Bool done = False;
   float xx, yy;
   int ixl, iyl;
   XEvent ahead;
   XWindowAttributes window_attributes;
   XEvent event;

/*******************************************
*     UNARM THE EXPOSE ACTION ROUTINE  
********************************************/

   enable_action = False;

/* INITITIALIZATION */
   if (ffirst) {
      ffirst = False;
      cx = gwidth/2;
      cy = gheight/2;
/**********************************************************************
*  DRAW THE FIRST CROSSHAIR SINCE THE COMPLEMENT OF THE COMPLEMENT IS
*  NOT ITSELF.  IT IS THE HIGHLIGHT COLOR OF THE WINDOW!!! (SYSTEM BUG)
***********************************************************************/
/*      xvst_draw_fsc_(dpy,screen,g_window,0,0,fgc,0,0,0);
*/
   }

/* ENABLE SMALL CROSSHAIR CURSOR */
   XDefineCursor(dpy,g_window,xhaircursor);
   XDefineCursor(dpy,z_window,xhaircursor);

/*************************************************************************
*  FLUSH ALL THE PREVIOUS EVENTS RECEIVED AND WAIT FOR ALL EVENTS TO BE
*  PROCESSED BY THE SERVER.  IF THE SECOND ARGUMENT OF XSYNC IS TRUE,
*  ALL THE EVENTS IN THE INPUT QUEUE IS DISCARDED.
**************************************************************************/

   XSync(dpy,True);

/*********************************************************************
*   SET THE INPUT FOCUS (KEYBOARD FOCUS) TO THE appropriate WINDOW
**********************************************************************/
   if (xvst_focus_.appsetfocus) {
      if (xvst_zoom_window_.zact) {
         XGetWindowAttributes(dpy, z_window, &window_attributes);
         if (window_attributes.map_state == IsViewable) 
           XSetInputFocus(dpy, z_window, RevertToPointerRoot, CurrentTime);
      }
      else {
         XGetWindowAttributes(dpy, g_window, &window_attributes);
         if (window_attributes.map_state == IsViewable) 
           XSetInputFocus(dpy, g_window, RevertToPointerRoot, CurrentTime);
      }
   }

/* Pre-set the pointer if required */
    if (cursor_preset_.cset) {
       ixl = cx = (*xl) * gwidth + 0.5;
       iyl = cy = (1 - (*yl)/0.75) * gheight + 0.5;
       XWarpPointer(dpy,None,g_window,0,0,0,0,ixl,iyl);
    }

/**********************************************************************
*    WARP THE POINTER TO THE PREVIOUS POSITION, WHEN THE CROSSHAIR
*    MODE WAS LAST UP.
***********************************************************************/
/*    XWarpPointer(dpy,None,g_window,0,0,0,0,cx,cy); */

/**********************************************************************
*  SET UP EVENT MASK
***********************************************************************/

    event_mask = ButtonPressMask |ButtonReleaseMask |
                 KeyPressMask | StructureNotifyMask;
    if (fsc || creadout) event_mask = event_mask | PointerMotionMask;

    XSelectInput(dpy,g_window,event_mask);
    XSelectInput(dpy,z_window,event_mask);
/* SELECT FOR KEYPRESS EVENTS IN THE TERMINAL WINDOW */
    XSelectInput(dpy,t_window,KeyPressMask);

/* DRAW FSC */
    if (fsc) {
       xvst_draw_fsc();
       fsc_exists=True;
    }

/*****************************
*       MAIN EVENT LOOP      *
******************************/

   while (!done)
   {
      XNextEvent(dpy,&event);

      switch(event.type)
      {
      case ButtonPress:
         do_ButtonPress(&event,&icode,&done,&xx,&yy);
         break;
      case ButtonRelease:
         do_ButtonRelease(&event,&icode,&done,&xx,&yy);
         break;
      case KeyPress:
/*       printf("keypress event\n"); */
         do_KeyPress(&event,&icode,&done,&xx,&yy);
         break;
      case MotionNotify:
/**************************************************************************
*   COMPRESS THE MOTION NOTIFY EVENTS.  IF THERE IS A LONG STRING OF
*   MOTION NOTIFY EVENTS IN A ROW, USE ONLY THE LAST ONE.
*   THE XEVENTSQUEUED ENSURES THAT WE WON'T CHECK THE NEXT EVENT IF THERE IS
*   NO MORE EVENTS IN THE QUEUE.
***************************************************************************/
         while (XEventsQueued(dpy,QueuedAfterReading) > 0)
         {
/********************************************************************
*   CHECK WHAT THE NEXT EVENT IN THE QUEUE IS WITHOUT REMOVING IT
*********************************************************************/
            XPeekEvent(dpy,&ahead);
            if (ahead.type != MotionNotify) break;
            XNextEvent(dpy,&event);
         }
         if (button1)
	    do_zoom_MotionNotify(&event);
         else
	    do_MotionNotify(&event);
         break;

      case ConfigureNotify:
        if (event.xconfigure.window == g_window)
           do_ConfigureNotify(&event);
        else if (event.xconfigure.window == z_window)
           do_zoom_ConfigureNotify(&event);
        break;

      } /* END SWITTCH */
   } /* END WHILE (MAIN LOOP) */

   *code = icode;  /*  RETURN CODE OF KEYPRESS AND  */
   *x = xx;        /*  COORDINATES OF THE CURSOR    */
   *y = yy;

/*********************************************************************
*  REARM THE EXPOSE_ACTION ROUTINE TO CHECK FOR RESIZE 
**********************************************************************/

   enable_action = True;

/**********************************************************************
*   RESET THE GRAPHICS WIND0W TO RECEIVE ONLY RESIZE EVENTS
*   AND THE APPLICATION WINDOW TO RECEIVE NO EVENTS.
***********************************************************************/
   event_mask = StructureNotifyMask;
   XSelectInput(dpy,g_window,event_mask);
   XSelectInput(dpy,z_window,event_mask);
   XSelectInput(dpy,t_window,0);

/**********************************************************************
*   GET THE COORDINATES OF THE ODOMETER WINDOW BEFORE UNMAPPING.
***********************************************************************/
/* ERASE THE FSC */
    if (fsc_exists) {
       xvst_draw_fsc();
       fsc_exists=False;
    }

/* Reset cursors to normal */
   XUndefineCursor(dpy,g_window);
   XUndefineCursor(dpy,z_window);

   XFlush(dpy);
}
