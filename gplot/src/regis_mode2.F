      SUBROUTINE REGIS_MODE2
C
C  Put the terminal, MONITOR 2, into REGIS mode
C
      CHARACTER*1 ESC

      COMMON /PLOTMONITOR2/ IMONITOR2, IOUTM2

      ESC = CHAR(27)  ! modified by J.Chuma, 19Mar97 for g77
      WRITE(IOUTM2,1000)ESC,'Pp'
1000  FORMAT(' ',A1,A)
      RETURN
      END

