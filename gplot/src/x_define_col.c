/*
 * Modified 15-APR-93 by FWJ: general review of all routines
 * to rationalize the naming and scoping of variables and
 * eliminate redundant code.
 *
 * Modified 20-DEC-93 by FWJ: re-wrote colour assignment loops and 
 * added fall-back colour assignments in case colour allocation fails.
 *
 * Modified 28-MAR-95 by FWJ: routine xvst_set_color added
 * (see below).
 */

#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xresource.h>

#ifdef VMS
#include <descrip.h>
#define xvst_xcolour_ xvst_xcolour
#endif

/* GLOBAL VARIABLES */

Display *dpy;
Screen *screen;
Visual *visual;
unsigned long icmap[12];

#define NCNAME 12
#define LCNAME 41
char color_name[NCNAME][LCNAME] = {
   "BLACK",
   "RED",
   "BLUE",
   "MAGENTA",
   "GREEN",
   "YELLOW",
   "CYAN",
   "WHITE",
   "CORAL",
   "ORANGERED",
   "SPRING GREEN",
   "SLATE BLUE"
};

struct {Bool xcolour;} xvst_xcolour_;

/**********************************************************************
*   THIS ROUTINE SETS UP THE STANDARD COLORMAP TO BE USED
***********************************************************************/

void xvst_define_color_(dpy, screen, visual)
   Display *dpy;
   Screen *screen;
   Visual *visual;
{
   XColor screen_color, exact_color;
   Colormap colormap;
   int n, status;
   char *background;
   unsigned long black = XBlackPixelOfScreen(screen);
   unsigned long white = XWhitePixelOfScreen(screen);
   Bool whitebg = False;

   background = XGetDefault(dpy, "TRIUMF", "background");

/* COLOUR DISPLAY... */

   if (visual->class == PseudoColor || visual->class == StaticColor ||
       visual->class == DirectColor || visual->class == TrueColor ) { 
/* SET COMMON BLOCK VARIABLE */
      xvst_xcolour_.xcolour = True;
/* GET DEFAULT COLOURMAP ID */
      colormap = XDefaultColormapOfScreen(screen);
/* FWJ 09-DEC-91: impose white background if specified by user */
      if (background != NULL && strcmp(background, "white") == 0) {
         whitebg = True;
         strcpy(color_name[0], "WHITE");
         strcpy(color_name[7], "BLACK");
         strcpy(color_name[5], "GOLDENROD");
      }
/* ALLOCATE COLOURS */
      for (n = 0; n < 12; n++) {
         status = XAllocNamedColor(dpy, colormap, color_name[n],
                                   &screen_color, &exact_color);
         if (status != 0)
            icmap[n] = screen_color.pixel;
         else {
            fprintf(stderr, "Cannot allocate color %d %s, using default\n",
                    n, color_name[n]);
            if (whitebg) {
               icmap[n] = black;
               if (n == 0) icmap[n] = white;
            }
            else {
               icmap[n] = white;
               if (n == 0) icmap[n] = black;
            }
         }
      }
   }

/* MONOCHROME DISPLAY... */

   else {
/* SET COMMON BLOCK VARIABLE */
      xvst_xcolour_.xcolour = False;
      icmap[0] = white;
      for (n = 1; n < 12; n++)
         icmap[n] = black;
/* FWJ 09-DEC-91: impose black background if specified by user */
      if (background != NULL && strcmp(background, "black") == 0) {
         icmap[0] = black;
         for (n = 1; n < 12; n++)
            icmap[n] = white;
      }
   }
}


/*
 * xvst_set_color : override default colors
 * Morgan Burke (TRIUMF) 1995-Mar-20
 *
 * Modified 28-MAR-95 by FWJ: VMS support added and other
 * changes for inclusion in GPLOT library.
 */

/*
 * xvst_set_color_ - Fortran callable
 * icolor: GPLOT color number (0 - 11)
 * color: color name
 * lcolor: length of color name (passed automatically by Fortran)
 */

#ifdef VMS
void xvst_set_color(icolor, color)
   int *icolor;
   struct dsc$descriptor_s *color;
#else
void xvst_set_color_(icolor, color, lcolor)
   int *icolor;
   char *color;
   int lcolor;
#endif
{
   XColor screen_color, exact_color;
   Colormap colormap;
   int status;
   char name[LCNAME];

#ifdef VMS
   int lcolor = color->dsc$w_length;
#endif

   if (*icolor > 11 || *icolor < 0) {
      fprintf(stderr, "XVST_SET_COLOR: index %d out of range [0,11]\n",
              *icolor);
      return;
   }

   if (lcolor > LCNAME - 1) {
      fprintf(stderr, "XVST_SET_COLOR: colour name too long\n");
      return;
   }

#ifdef VMS
   strncpy(name, color->dsc$a_pointer, lcolor);
#else
   strncpy(name, color, lcolor);
#endif
   name[lcolor] = '\0';

   if (xvst_xcolour_.xcolour) {
      colormap = XDefaultColormapOfScreen(screen);
      XFreeColors(dpy, colormap, &icmap[*icolor], 1, 0);
      status = XAllocNamedColor(dpy, colormap, name,
                                &screen_color, &exact_color);
      if (status != 0)
         icmap[*icolor] = screen_color.pixel;
      else {
         fprintf(stderr, "Cannot allocate color %d %s\n",
                 *icolor, color);
      }
   }
   else {
    /*
     * xcolour not set - assume that xvst_setup has not been called
     * yet, and modify the default color list (in global color_name).
     * On mono displays this assumption is false, but this call should
     * have no effect.
     */
      strncpy(color_name[*icolor], name, lcolor+1);
   }
}
