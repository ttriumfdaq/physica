      SUBROUTINE WRITE_HP300(IOUT,LASER,*)
C======================================================================C
C                                                                      C
C  WRITE_HP300                                     F.W. Jones, TRIUMF  C
C                                                                      C
C  Writes the bitmap stored in the dynamic array HPPAINT on unit       C
C  IOUT in HP Laserjet format for 300 dpi density.                     C
C                                                                      C
C  If LASER=1 non-TeX inclusion                                        C
C  If LASER=2 the plot file will be made for TeX inclusion.            C
C  If LASER=3 the plot file will be for TeX inclusion and all white    C
C  space will be trimmed from the top and left side of the image       C
C  so that the significant portion of the image will start at the      C
C  current writing position on the TeX page.                           C
C  If LASER=4 non-TeX inclusion appended                               C
C                                                                      C
C  Modified 08-MAR-94 by FWJ: removed VMS IOFAST dependency, since     C
C  IOFAST is not available on ALPHA VMS.                               C
C                                                                      C
C  Modified 1-Feb-95 by J.Chuma:  allow appended plot files            C
C                                                                      C
C======================================================================C

      BYTE HPPAINT(4)    ! dynamic array dimensioned NXDIM,NYDIM
      COMMON/GRAPHICS_BITMAP/HPPAINT,NBYTES_HP,IOFFSET,NXDIM,NYDIM,MAXY

      COMMON /HARDCOPYRANGE2/ XMINH2,XMAXH2,YMINH2,YMAXH2

      CHARACTER*1 ESC,FF

C   Output buffer

      BYTE LINE(282)
      CHARACTER*282 CLINE
      EQUIVALENCE(LINE,CLINE)

      CHARACTER*3 CNPUT
CCC
      ESC=CHAR(27)
      FF=CHAR(12)
      IBITS=INT(YMAXH2)+1    ! Find extent of bitmap data
      JBITS=INT(XMAXH2)+1
      IEND_MAX=IBITS/8
      IF( MOD(IBITS,8).NE.0 )IEND_MAX=IEND_MAX+1
      IEND_MAX=MIN(IEND_MAX,282)      !To avoid buffer overflow
      JEND_MAX=MIN(JBITS,NYDIM)
      DO JEND=JEND_MAX,1,-1
        DO I=1,IEND_MAX
          IF( HPPAINT(I+(JEND-1)*NXDIM+IOFFSET).NE.0 )GO TO 50
        END DO
      END DO
      JEND=1
   50 IBEG=1   ! Normal start of bitmap data
      JBEG=1

C Reset, set density, set left margin, start raster graphics:

      IF( LASER.EQ.1 )THEN       ! non-TeX output
        WRITE(IOUT,75,ERR=999)
     &    ESC//'E'//ESC//'*t300R'//ESC//'&a4C'//ESC//'*r1A'
   75   FORMAT(A)
      ELSE IF( LASER.EQ.2 )THEN  ! TeX output
        WRITE(IOUT,75,ERR=999)ESC//'*t300R'//ESC//'*r1A'
      ELSE IF( LASER.EQ.3 )THEN  ! justified TeX output
        IBEG=IEND_MAX            ! find additional cropping limits
        JBEG=0
        DO 60 J=1,JEND
          DO I=1,IEND_MAX
            IF( HPPAINT(I+(J-1)*NXDIM+IOFFSET).NE.0 )THEN
              IF( JBEG.EQ.0 )JBEG=J
              IBEG=MIN(IBEG,I)
              GO TO 60
            END IF
          END DO
   60   CONTINUE
        IF( JBEG.EQ.0 )JBEG=1
        WRITE(IOUT,75,ERR=999)ESC//'*t300R'//ESC//'*r1A'
      ELSE IF( LASER.EQ.4 )THEN  ! non-TeX appended output
        WRITE(IOUT,75,ERR=999)
     &    FF//ESC//'*t300R'//ESC//'&a4C'//ESC//'*r1A'
      END IF
      DO 100 J=JBEG,JEND
        DO IEND=IEND_MAX,IBEG,-1 ! Find no. bytes from current line
          IF( HPPAINT(IEND+(J-1)*NXDIM+IOFFSET).NE.0 )GO TO 80
        END DO
        WRITE(IOUT,75,ERR=999)ESC//'*b000W'   ! Empty line
        GO TO 100
   80   NPUT=IEND-IBEG+1
        WRITE(CNPUT,85)NPUT      !For raster data header
   85   FORMAT(I3.3)
        IF( LASER.NE.3 )THEN
          DO I=1,NPUT
            LINE(I)=HPPAINT(I+(J-1)*NXDIM+IOFFSET)
          END DO
        ELSE
          DO I=1,NPUT
            LINE(I)=HPPAINT(IBEG+I-1+(J-1)*NXDIM+IOFFSET)
          END DO
        END IF
        WRITE(IOUT,75,ERR=999)ESC//'*b'//CNPUT//'W'//CLINE(1:NPUT)
  100 CONTINUE

C   End raster graphics

      WRITE(IOUT,75,ERR=999)ESC//'*rB'
      IF( (LASER.EQ.2) .OR. (LASER.EQ.3) )
     & WRITE(IOUT,75,ERR=999)ESC//'*rB' ! fix line-eating bug in DVIHP
      RETURN
  999 WRITE(*,*)'Error writing bitmap file on unit',IOUT
      CALL FORMSG
      RETURN 1
      END
