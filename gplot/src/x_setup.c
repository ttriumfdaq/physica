/* Modified July 1991 by F.Jones: zoom window code revised      */
/* and expanded.  Support added for graphics input, odometer,   */
/* long crosshairs, and control by applications.                */
/* Modified 5-Dec-91 by FWJ: this routine now creates the       */
/* cursors used by xvst_crosshair and xvst_replay.              */
/* Modified 27-Jan-92 by FWJ: reads the Xdefaults parameter     */
/* TRIUMF.appSetFocus.                                          */
/* Modified 06-Apr-92 by FWJ: X error handler added.            */
/* Modified 25-Nov-92 by FWJ: parameter mxapp added, to allow   */
/* this routine to be called by applications that have made     */
/* their own display connection (e.g. Motif/Xt applications).   */
/* The display pointer is passed through global variable dpy.   */
/* Modified 09-MAR-93 by FWJ: added signal handling for OSF/1.  */
/* To allow i/o operations to survive the alarm signal, used    */
/* routine sigaction() instead of signal().                     */
/* Modified 15-APR-93 by FWJ: general review of all routines    */
/* to rationalize the naming and scoping of variables and       */
/* eliminate redundant code.                                    */

/* Modified 24-MAR-95 by FWJ: disabled alarm signal for HP-UX.  */

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
 
 /* #ifdef __osf__           */
 /* #include <sys/utsname.h> */
 /* #endif                   */
 
#ifdef VMS
#define xvst_zoom_window_ xvst_zoom_window
#define xvst_var_ xvst_var
#define xvst_focus_ xvst_focus
#endif

/*        GLOBAL  VARIABLES        */

Display *dpy;
Screen *screen;
Window g_window, c_window, t_window, z_window;
GC ggc, cgc, fgc, zgc;
Bool enable_action, creadout, fsc;
Bool resize_m, resize_z;
unsigned long gcfore, gcback;
unsigned long icmap[12];
int gwidth, gheight;
int zwidth, zheight;
int cx, cy;
int event_mask;
int offset_x, offset_y;
float scalx,scaly;
Cursor xhaircursor, busycursor;
Bool ismxapp;
int xvst_error();
void resize_action();

Window g_window_save, png_window;
int gwidth_save, gheight_save, png_width, png_height;
 
/* COMMON BLOCK FOR ZOOM WINDOW */
struct {Bool zopen,zact; float zw,zh,zx1,zx2,zy1,zy2;} xvst_zoom_window_;
/* COMMON BLOCK - VARIOUS */
struct {int curheight; int curwidth;}xvst_var_;
/* COMMON BLOCK - AUTOFOCUS SWITCH */
struct {Bool appsetfocus;} xvst_focus_;

/***********************************************************************
*
*    THIS ROUTINE DOES A GLOBAL INITIALIZATION.  IT DOES ALL THE
*    NECESSARY STEPS IN ORDER TO HAVE A GRAPHICS WINDOW READY TO
*    HAVE VECTORS DRAWN INTO IT.  THE MAJOR STEPS ARE:
*
*
*  1)  CONNECT THE CLIENT TO THE X SERVER WITH XOPENDISPLAY
*      AND EXIT IF THE CONNECTION FAILED.
*  2)  GET INFORMATION ABOUT THE PHYSICAL SCREEN.  GET THE SCREEN ID,
*      WIDTH, HEIGHT, DEPTH (NO. OF BITS PER PIXEL TO REPRESENT COLOR
*      OR GRAY SCALES), AND THE VISUAL STRUCTURE OF THE SCREEN.
*  3)  SET UP THE WINDOW ATTRIBUTES.  SELECT THE TYPES OF EVENTS THE
*      WINDOW IS TO RECEIVE, AND WHETHER TO HAVE BACKING STORE.
*  4)  CREATE THE WINDOW WITH XCREATEWINDOW.  NOTE THAT CREATING THE
*      WINDOW DOES NOT MAKE IT VISIBLE.
*  5)  SET UP THE STANDARD PROPERTIES FOR THE WINDOW MANAGER WITH
*      THE XSIZEHINTS STRUCTURE AND THE XSETNORMALHINTS CALL.
*  6)  CREATE AND SET UP THE GRAPHICS CONTEXT FOR THE WINDOW WITH
*      THE XGCVALUES STRUCTURE AND THE XCREATEGC CALL.
*  7)  STORE THE NAMES OF THE WINDOWS WITH XSTORENAME.
*  8)  DISPLAY THE WINDOW WITH XMAPWINDOW.
*  9)  WAIT FOR THE FIRST EXPOSE EVENT TO ARRIVE.  NO GRAPHICS CAN 
*      BE DISPLAYED BEFORE THE FIRST EXPOSE EVENT ARRIVES SINCE IT
*      IS NOT CONSIDERED TO BE VISIBLE YET.
*
*
*   NOTE THAT NO PIXMAPS ARE USED.  SINCE THE BACKING STORE ATTRIBUTE
*   IS SET TO "ALWAYS" THE SERVER ALWAYS MAINTAINS THE CONTENTS OF
*   THE WINDOW WHEN IT IS OBSCURED OR UMMAPPED IN A PIXMAP ALREADY.
*   THE ONLY THING THAT HAS TO BE LOOKED AFTER IS A RESIZE.  THUS THE
*   GRAPHICS IS STORED IN DYNAMIC MEMORY AS IT IS DRAWN AND WHEN NEEDED,
*   IS SCALED AND REPLAYED.
*
*
*       DESCRIPTION OF GLOBAL VARIABLES AND COMMON BLOCKS
*-------------------------------------------------------------------------
*    NAME        TYPE          DESCRIPTION
*-------------------------------------------------------------------------
*    *dpy        Display       display ID
*    *screen     Screen        screen's ID
*    window      Window        graphics window's ID
*    win2        Window        odometer window's ID
*    win3        Window        application (dialog) window's ID
*    z_window    Window        zoom window's ID
*    gc          GC            dialog window's graphics context
*    zgc         GC            zoom window'x graphics context
*    gc2         GC            odometer window's graphics context
*    fgc         GC            full screen cursor's graphics context
*    glines[]    XPoint        coordinates of the ends of the lines
*    gpoints[]   XPoint        coordinates of the points
*
*  STRUCTURES
*  ----------
*    *visual     Visual        visual structure of screen
*    gcvl        XGCValues     the graphics context value structure
*    hints       XSizeHints    the window manager's size hints structure
*    event       XEvent        the event structure
*    wa          XSetWindowAttributes
*                              the set window attributes structure
*
*  LOGICAL    
*  -------
*    act         Bool          toggle odometer
*    fsc         Bool          toggle full screen crosshair
*    mono        Bool          monochrome monitor?
*    istore      Bool          storing in dynamic memory?
*    enable_action
*                Bool          timeout to check for resize events
*
*  CURSOR & DIMENSION
*  ------------------
*    cx          int           current x coordinate
*    cy          int           current y coordinate
*    px          int           previous x coordinate
*    py          int           previous y coordinate
*    width       int           current width of the graphics window
*    height      int           current height of the graphics window
*
*  MISC
*  ----
*    gcfore      int           current GC's foreground color
*    gcback      int           current GC's background color
*    icmap[]   unsigned long   array of color indices
*    *mem_replay float         pointer to dynamic memory block
*    *banner[]   char          scaled cursor location in odometer
*
*  COMMON BLOCKS      -fortran common blocks are accessible in C through
*  -------------       the use of structures.
*
* struct S { float xcroff; float xcrscale; float ycroff; float ycrscale;
*            float ycof; float xcof; }cursor_readout_;
*
*                -COMMON BLOCK CURSOR_READOUT IN FORTRAN CODE;
*                -scales to be used to calculate the banner in 
*                 the odometer.
*
* struct S { Bool cset }cursor_preset_; 
*
*                -COMMON BLOCK CURSOR_PRESET_ IN FORTRAN CODE;
*                -logical to signal if the cursor is preset.
*
***************************************************************************/

void xvst_setup_(mxapp)
   int mxapp;
{
   Visual *visual;
   XSetWindowAttributes wa;
   XEvent TheEvent;
   int depth;
   int wa_mask;
   unsigned long fgcmask;
   int g_window_x = 50;
   int g_window_y = 330;
   unsigned int g_window_w = 640;
   unsigned int g_window_h = 480;
   int z_window_x = 330;
   int z_window_y = 160;
   int c_window_x = 10;
   int c_window_y = 770;
   unsigned int c_window_w = 250;
   unsigned int c_window_h = 50;

   int revert_to;
   char *xgd;

#ifdef __osf__
   struct sigaction action;
   /* struct utsname name;     */
   /* int iuname;              */
#endif 
  
/* Set global variable for external application type */
   ismxapp = (mxapp == 1);

/*************************************************
*   TURN OFF ODOMETER AND FULL SCREEN CROSSHAIR
**************************************************/
   creadout = False;
   fsc = False;

/*****************************************
*  OPEN DISPLAY AND CHECK IF SUCCESSFUL
******************************************/

   if (mxapp == 0) {
      dpy = XOpenDisplay(NULL);
      if (dpy == NULL) {
         printf("Display could not be opened!!!");
         exit(-1);
      }
   }

   XSetErrorHandler(xvst_error);

   screen = XDefaultScreenOfDisplay(dpy);
   depth = XDefaultDepthOfScreen(screen);
   visual = XDefaultVisualOfScreen(screen);

/************************************************
*  GET THE WINDOW ID OF THE TERMINAL WINDOW 
*************************************************/
   XGetInputFocus(dpy, &t_window, &revert_to);

   xvst_define_color_(dpy, screen, visual);

/* FWJ 09-DEC-91: IMPOSE USER-DEFINED WINDOW GEOMETRY IF ANY */
   XParseGeometry(XGetDefault(dpy, "TRIUMF", "geometry"), 
                  &g_window_x, &g_window_y, &g_window_w, &g_window_h);

/* SET AUTOFOCUS SWITCH */
   xgd = XGetDefault(dpy, "TRIUMF", "appSetFocus");
   if (xgd != NULL && strcmp(xgd, "False") == 0)
      xvst_focus_.appsetfocus = False;
   else
      xvst_focus_.appsetfocus = True;

/*********************************************************
*    SET UP THE DEFAULT FOREGROUND AND BACKGROUND COLOR  
**********************************************************/
   gcfore = icmap[7];
   gcback = icmap[0];

/* Find out if the server does backing store: example only, not in use */
/* int backingstore;
 * Bool pixmap;
 * backingstore = XDoesBackingStore(screen);
 * pixmap = (backingstore == NotUseful);
 * if (!pixmap) printf("X server does backing store\n");
 */

/***********************************
*     SET UP WINDOW ATTRIBUTES  
************************************/

   wa_mask = CWBackPixel | CWEventMask | CWBackingStore;    
   wa.background_pixel = gcback;
   wa.backing_store = Always;

   event_mask = ExposureMask|StructureNotifyMask|VisibilityChangeMask;
   wa.event_mask = event_mask;

/***************************
*     CREATE WINDOW  1  
****************************/

   g_window = XCreateWindow(dpy, XRootWindowOfScreen(screen),
      g_window_x, g_window_y, g_window_w, g_window_h, 0, depth, 
      InputOutput, visual, wa_mask, &wa);

/*******************************************
*      CREATE THE ZOOM WINDOW WITH THE SAME
*      CHARACTERISTICS AS WINDOW 1
********************************************/

   z_window = XCreateWindow(dpy, XRootWindowOfScreen(screen),
      z_window_x, z_window_y, g_window_w, g_window_h, 0, depth,
      InputOutput, visual, wa_mask, &wa);

/* Initialize global variables */
   zwidth = g_window_w;
   zheight = g_window_h;
   xvst_zoom_window_.zw=zwidth;
   xvst_zoom_window_.zh=zheight;

/* if (pixmap) {
 *    pixw = XWidthOfScreen(screen);
 *    pixh = XHeightOfScreen(screen);
 *    pixmap = XCreatePixmap(dpy, g_window, pixw, pixh, depth);
 * }
 */

/********************************
*  CREATE CURSOR READOUT WINDOW
********************************/

   wa_mask = CWBackPixel | CWBackingStore;
   wa.backing_store = Always;
   wa.background_pixel = XWhitePixelOfScreen(screen);

   c_window = XCreateWindow(dpy, XRootWindowOfScreen(screen),
      c_window_x, c_window_y, c_window_w, c_window_h, 4, depth,
      InputOutput, visual, wa_mask, &wa);

/*****************************
*     SET GRAPHICS CONTEXT  
******************************/

   ggc = XCreateGC(dpy, g_window, 0, 0);
   zgc = XCreateGC(dpy, z_window, 0, 0);
   cgc = XCreateGC(dpy, c_window, 0, 0);

/* Set foreground and background colours */
   XSetForeground(dpy, ggc, gcfore);
   XSetBackground(dpy, ggc, gcback);
   XSetForeground(dpy, zgc, gcfore);
   XSetBackground(dpy, zgc, gcback);
/* Odometer readout in reverse video */
   XSetForeground(dpy, cgc, gcback);
   XSetBackground(dpy, cgc, gcfore);

/* if (pixmap) {
 *    pgc = XCreateGC(dpy, pixmap, 0, 0);
 *    XSetBackground(dpy, pgc, gcback);
 *    XSetForeground(dpy, pgc, gcfore);
 * }
 */

/* SET UP GC FOR LONG CROSSHAIRS */
/* Colour section modified 29NOV90 by F. Jones: XOR with white
 * brush did not work with  VS3200 server.  Now XOR is with a brush
 * with all 1's set, and it is performed only on the bitplanes that
 * differ between black and white.  Ref: Oliver Jones, "Introduction
 * to the X Window System", Prentice Hall 1989, p. 159. */

   fgc = XCreateGC(dpy, g_window, 0, 0);
   if (visual->class == GrayScale || visual->class == StaticGray)
      XSetFunction(dpy, fgc, GXinvert);
   else {
      fgcmask = XBlackPixelOfScreen(screen)^XWhitePixelOfScreen(screen);
      XSetForeground(dpy, fgc, 0xffffffff);
      XSetBackground(dpy, fgc, 0);
      XSetFunction(dpy, fgc, GXxor);
      XSetPlaneMask(dpy, fgc, fgcmask);
   }

/*****************************************
*     STORE THE NAME OF THE WINDOWS  
******************************************/

   XStoreName(dpy, g_window, "Graphics Window");
   XStoreName(dpy, z_window, "Zoom Window");
   XStoreName(dpy, c_window, "Cursor Location");

/*********************************************
*      CLEAR PIXMAP BEFORE MAPPING WINDOW
*      DRAW THE BACKGROUND TO THE FOREGROUND
**********************************************/
/*  if (pixmap) {
 *    XSetForeground(dpy, pgc, gcback);
 *    XFillRectangle(dpy, pixmap, pgc, 0, 0, pixw, pixh);
 *    XSetForeground(dpy, pgc, gcfore);
 * }
 */

   XMapWindow(dpy, g_window);
   XFlush(dpy);

/********************************************************************
*     WAIT FOR THE FIRST EXPOSE EVENT TO ARRIVE BEFORE GRAPHING  
*     Wait till all expose events are in.   FWJ 16-AUG-91
*********************************************************************/

   while (1) {
      XNextEvent(dpy, &TheEvent);
      //printf("event type %d\n", TheEvent.type);
      if (TheEvent.type==VisibilityNotify) break;
      if (TheEvent.type==Expose && TheEvent.xexpose.count==0) break;
   }
   XClearWindow(dpy, g_window);

/********************************************************************
*     INITIALIZE WIDTH AND HEIGHT OF WINDOW AND CURSOR POSITION  
*********************************************************************/

   gwidth = g_window_w;
   gheight = g_window_h;
   xvst_var_.curwidth = gwidth;
   xvst_var_.curheight = gheight;
   cx = 0;
   cy = 0;

/*****************************************************************
*   INITIALIZE BOOLEANS TO CHECK RESIZES IN BOTH THE MAIN 
*   GRAPHICS WINDOW AND THE ZOOM WINDOW.
******************************************************************/
   resize_m = False;
   resize_z = False;

/*****************************************************************
*  INITIALIZE THE ASPECT RATIO TO BE ONE, AND THE OFFSETS TO BE 0
******************************************************************/
   scalx = 1.;
   scaly = 1.;
   offset_x = 0;
   offset_y = 0;

/* CREATE CURSORS FOR USE LATER */
   xhaircursor = XCreateFontCursor(dpy, XC_tcross);
   busycursor = XCreateFontCursor(dpy, XC_watch);

/* FOR MOTIF APPLICATIONS OR OTHERS WITH AN EVENT LOOP,    */
/* ENABLE EVENT REPORTING IN THE GRAPHICS AND ZOOM WINDOWS */
   if (mxapp == 1) {
      cx = gwidth/2;
      cy = gheight/2;
      XDefineCursor(dpy, g_window, xhaircursor);
      XDefineCursor(dpy, z_window, xhaircursor);
      event_mask = ButtonPressMask |ButtonReleaseMask | StructureNotifyMask;
      event_mask = event_mask | KeyPressMask;
      XSelectInput(dpy, g_window, event_mask);
      XSelectInput(dpy, z_window, event_mask);
   }

/* ARM ALARM FOR EXPOSE EVENTS  (all except SGI) */
   enable_action = True;
   if (mxapp == 0) {
#ifdef VMS
      XSelectAsyncEvent(dpy,g_window,ConfigureNotify,resize_action,NULL);
      XSelectAsyncEvent(dpy,z_window,ConfigureNotify,resize_action,NULL);
#else
#ifdef __osf__
      /* For Digital Unix v4.0, a program would hang while doing graphics if the    */
      /* alarm was set. Now, the alarm is set to 100000 seconds in xvst_flush_lines */
      /* and xvst_flush_points and the user must reset the alarm to 1 second in the */
      /* calling program (e.g., physica calls sig_on in GET_CMND.FP).               */
      /* iuname = uname( &name ); */
      /* if( name.release[1] != '4' ) */
      {
        action.sa_handler = resize_action;
        action.sa_mask = NULL;
        action.sa_flags = SA_RESTART;
        sigaction(SIGALRM, &action, NULL);
        alarm(1);
      }
#else
#ifndef sgi
#ifndef g77
#ifndef __hpux
#ifndef __SVR4
#ifndef _AIX
      signal(SIGALRM, resize_action);
      alarm(1);
#endif
#endif
#endif
#endif
#endif
#endif
#endif
   }

   XFlush(dpy);
}

 void change_window_to_png_()
  {
    g_window_save = g_window;
    g_window = png_window;
    gwidth_save = gwidth;
    gheight_save = gheight;
    gwidth = png_width;
    gheight = png_height;
  }
 
 void change_window_to_original_()
  {
    g_window = g_window_save;
    gwidth = gwidth_save;
    gheight = gheight_save;
  }
 
