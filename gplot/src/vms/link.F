	SUBROUTINE L$INK(FILNAM,CODE,*)
C======================================================================C
C
C.		Subroutine to link object file(s) into dynamically
C.	loadable images.  The object files are created by the standard
C.	System Compilers such as FORTRAN and MACRO, and it is assumed
C.	that the code will be loaded into holding array "CODE".  The
C.	address of "CODE" is calculated, and passed as a parameter
C.	to the Linker, so the object code does not have to be position
C.	independent.  The address of "CODE" is rounded up to the
C.	next page boundary.
C.
C.		If the LINK results in a WARNING, this procedure will
C.	complain on SYS$OUTPUT, but will take the Success exit.  Only
C.	a SEVERE condition will cause this procedure to take the
C.	error return...
C.
C.	Modifications:
C.
C.	   Who		  When		What
C.	   ---		  ----		----
C.	T. Miles	16-Mar-1982	Original
C.
C.	T. Miles	30-Sep-1982	Included /NOSHSSYR for VMS 3.0
C.
C.	H. Baragar	10-Dec-1982	changed EXECUTE_CLI to LIB$SPAWN
C.					& added COLLECT comments (above)
C======================================================================C

C--->	Specification Statements
	IMPLICIT  INTEGER*4   (A-Z)
	CHARACTER FILNAM*(*), LINE*1024
	BYTE                  CODE(*)
	PARAMETER FACNAM = 'L$INK'

C--->	Procedure begins...

C--	Set up LINK command
	NEXT_PAGE = %LOC(CODE) + '000001FF'X .AND. 'FFFFFE00'X
	WRITE (LINE,100) NEXT_PAGE, FILNAM
100	FORMAT('Link/Nomap/Nosysshr/System=%X',Z8.8,' ',A,'*')
	SIZE=INDEX(LINE,'*')-1

C--	Pass LINK command to DCL for execution...
        STATUS=LIB$SPAWN(LINE(1:SIZE))

C--	Examine return status
	NO_ERROR     =  STATUS   .AND. '0001'X
	SEVERE_ERROR =  STATUS   .AND. '0004'X
	IF (NO_ERROR  .EQ. 0) THEN
		CALL SYS$GETMSG(%VAL(STATUS),SIZE,LINE,%VAL(-1),)
		TYPE *,'%'//FACNAM//LINE(INDEX(LINE,'-'):SIZE)
		IF (SEVERE_ERROR .NE. 0)  GOTO 999
	END IF

C--	Link was successful
	RETURN

C--	Link failed...
999	RETURN 1
	END
