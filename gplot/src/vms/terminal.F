      SUBROUTINE TERMINAL(TERM)
C
C=====================================================================
C    Returns the terminal name string (i.e. the device name) padded   
C    with trailing blanks to 7 characters in length.
C    Refer to P. BENNETT for any problems. NOV 9 1983.
C=====================================================================
C
C    REFERENCE: PAGE 6-10 OF VAX-11 FORTRAN USER'S GUIDE
C    & PAGE 132 OF VAX SYSTEM SERVICES REFERENCE MANUAL.
C  
C    reqd. routines - NONE
C
C    
C   set up implicit data types so that the data types indicate sizes
C
      IMPLICIT INTEGER*2 (W), INTEGER*4 (L)
C
C   Define the symbolic value of the EXTERNAL identifiers
C
      EXTERNAL JPI$_TERMINAL
      CHARACTER*1 NULL/0/
      CHARACTER*7 TERM
C
C   Initialize all static values in the item list
C
      DATA W_LEN1,W_LEN2/7,2/
      DATA W_CODE2 /0/
      DATA L_LENADDR1/0/
C
C   Place the SYS$GETJPI item list in a COMMON block so as to be
C   contiguous.
C
      COMMON/ITEMS02/W_LEN1,W_CODE1,L_ADDR1,L_LENADDR1,
     #                W_LEN2,W_CODE2
      W_CODE1= %LOC(JPI$_TERMINAL)
C
C   Return the address of the descriptor of the CHARACTER variable
C   USERNAME.
C
      L_ADDR1= %LOC(TERM)
      TERM=' '
C
C   Perform the system service call
C
      CALL SYS$GETJPI(,,,W_LEN1,,,)
C
C   Find the (non-blank) length of the TERMINAL name.
C
      LEN1= INDEX(TERM,' ') - 1
      LEN2= INDEX(TERM,NULL) - 1
      IF(LEN1.LT.0) LEN1=7
      IF(LEN2.LT.0) LEN2=7
      LEN= MIN0(LEN1,LEN2)
      TERM= TERM(1:LEN)
      RETURN
      END
